<?php
/**
 * 都道府県一覧（コード：JIS X 0401準拠）
 * @var [type]
 * zip_head: 郵便番号上２桁の都道府県判別用のコード
 */

$regions = [
    // 北海道
    'hokkaido' => 		['value' => 1, 'ja' => '北海道', 'en' => 'Hokkaido', 'pref' => [
        'hokkaido' =>   ['value' => 1, 	'ja' => '北海道',  	'en' => 'Hokkaido', 'zip_head' => ['00', '04', '05', '06', '07', '08', '09'] ],
    ]],
    // 東北
    'touhoku' => 		['value' => 2, 'ja' => '東北', 	'en' => 'Touhoku', 'pref' => [
        'aomori' =>     ['value' => 2, 	'ja' => '青森県',  	'en' => 'Aomori' , 'zip_head' => ['3']],
        'iwate' =>      ['value' => 3, 	'ja' => '岩手県',  	'en' => 'Iwate', 'zip_head' => ['2']],
        'miyagi' =>     ['value' => 4, 	'ja' => '宮城県',  	'en' => 'Miyagi', 'zip_head' => ['98']],
        'akita' =>      ['value' => 5, 	'ja' => '秋田県',  	'en' => 'Akita', 'zip_head' => ['1']],
        'yamagata' =>   ['value' => 6, 	'ja' => '山形県',  	'en' => 'Yamagata', 'zip_head' => ['99']],
        'fukushima' =>  ['value' => 7, 	'ja' => '福島県',  	'en' => 'Fukushima', 'zip_head' => ['96', '97']],
    ]],
    // 関東
    'kanto' => 			['value' => 3, 'ja' => '関東', 	'en' => 'Kanto', 'pref' => [
        'ibaraki' =>    ['value' => 8, 	'ja' => '茨城県',  	'en' => 'Ibaraki', 'zip_head' => ['30', '31']],
        'tochigi' =>    ['value' => 9, 	'ja' => '栃木県',  	'en' => 'Tochigi', 'zip_head' => ['32']],
        'gunma' =>      ['value' => 10, 'ja' => '群馬県',  	'en' => 'Gunma', 'zip_head' => ['37']],
        'saitama' =>    ['value' => 11, 'ja' => '埼玉県',  	'en' => 'Saitama', 'zip_head' => ['33', '34', '35', '36']],
        'chiba' =>      ['value' => 12, 'ja' => '千葉県',  	'en' => 'Chiba', 'zip_head' => ['26', '27', '28', '29']],
        'tokyo' =>      ['value' => 13, 'ja' => '東京都',  	'en' => 'Tokyo', 'zip_head' => ['10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']],
        'kanagawa' =>   ['value' => 14, 'ja' => '神奈川県', 	'en' => 'Kanagawa', 'zip_head' => ['21', '22', '23', '24', '25']],
    ]],
    // 甲信越
    'koushinetsu' => 	['value' => 4, 'ja' => '甲信越', 'en' => 'Koushinetsu', 'pref' => [
        'niigata' =>    ['value' => 15, 'ja' => '新潟県', 	'en' => 'Niigata', 'zip_head' => ['94', '95']],
        'yamanashi' =>  ['value' => 19, 'ja' => '山梨県', 	'en' => 'Yamanashi', 'zip_head' => ['40']],
        'nagano' =>     ['value' => 20, 'ja' => '長野県', 	'en' => 'Nagano', 'zip_head' => ['38', '39']],
    ]],
    // 北陸
    'hokuriku' => 		['value' => 5, 'ja' => '北陸', 	'en' => 'Hokuriku', 'pref' => [
        'toyama' =>     ['value' => 16, 'ja' => '富山県', 	'en' => 'Toyama', 'zip_head' => ['93']],
        'ishikawa' =>   ['value' => 17, 'ja' => '石川県', 	'en' => 'Ishikawa', 'zip_head' => ['92']],
        'fukui' =>      ['value' => 18, 'ja' => '福井県', 	'en' => 'Fukui', 'zip_head' => ['91']],
    ]],
    // 東海
    'toukai' => 		['value' => 6, 'ja' => '東海', 	'en' => 'Toukai', 'pref' => [
        'gifu' => 		['value' => 21, 'ja' => '岐阜県', 	'en' => 'Gifu', 'zip_head' => ['50']],
        'shizuoka' => 	['value' => 22, 'ja' => '静岡県', 	'en' => 'Shizuoka', 'zip_head' => ['41', '42', '43']],
        'aichi' => 		['value' => 23, 'ja' => '愛知県', 	'en' => 'Aichi', 'zip_head' => ['44', '45', '46', '47', '48', '49']],
        'mie' => 		['value' => 24, 'ja' => '三重県', 	'en' => 'Mie', 'zip_head' => ['51']],
    ]],
    // 近畿
    'kinki' => 			['value' => 7, 'ja' => '近畿', 	'en' => 'Kinki', 'pref' => [
        'shiga' => 		['value' => 25, 'ja' => '滋賀県', 	'en' => 'Shiga', 'zip_head' => ['52']],
        'kyoto' => 		['value' => 26, 'ja' => '京都府', 	'en' => 'Kyoto', 'zip_head' => ['60', '61', '62']],
        'osaka' => 		['value' => 27, 'ja' => '大阪府', 	'en' => 'Osaka', 'zip_head' => ['53', '54', '55', '56', '57', '58', '59']],
        'hyogo' => 		['value' => 28, 'ja' => '兵庫県', 	'en' => 'Hyogo', 'zip_head' => ['65', '66', '67']],
        'nara' => 		['value' => 29, 'ja' => '奈良県', 	'en' => 'Nara', 'zip_head' => ['63']],
        'wakayama' => 	['value' => 30, 'ja' => '和歌山県', 	'en' => 'Wakayama', 'zip_head' => ['64']],
    ]],
    // 中国
    'chugoku' => 		['value' => 8, 'ja' => '中国', 	'en' => 'Chugoku', 'pref' => [
        'tottori' => 	['value' => 31, 'ja' => '鳥取県', 	'en' => 'Tottori', 'zip_head' => ['68']],
        'shimane' => 	['value' => 32, 'ja' => '島根県', 	'en' => 'Shimane', 'zip_head' => ['69']],
        'okayama' => 	['value' => 33, 'ja' => '岡山県', 	'en' => 'Okayama', 'zip_head' => ['70', '71']],
        'hiroshima' => 	['value' => 34, 'ja' => '広島県', 	'en' => 'Hiroshima', 'zip_head' => ['72', '73']],
        'yamaguchi' => 	['value' => 35, 'ja' => '山口県', 	'en' => 'Yamaguchi', 'zip_head' => ['74', '75']],
    ]],
    // 四国
    'shikoku' => 		['value' => 9, 'ja' => '四国', 	'en' => 'Shikoku', 'pref' => [
        'tokushima' => 	['value' => 36, 'ja' => '徳島県', 	'en' => 'Tokushima', 'zip_head' => ['77']],
        'kagawa' => 	['value' => 37, 'ja' => '香川県', 	'en' => 'Kagawa', 'zip_head' => ['76']],
        'ehime' => 		['value' => 38, 'ja' => '愛媛県', 	'en' => 'Ehime', 'zip_head' => ['79']],
        'kochi' => 		['value' => 39, 'ja' => '高知県', 	'en' => 'Kochi', 'zip_head' => ['78']],
    ]],
    // 九州
    'kyushu' => 		['value' => 10,  'ja' => '九州', 	'en' => 'Kyushu', 'pref' => [
        'fukuoka' => 	['value' => 40, 'ja' => '福岡県', 	'en' => 'Fukuoka', 'zip_head' => ['80', '81', '82', '83']],
        'saga' => 		['value' => 41, 'ja' => '佐賀県', 	'en' => 'Saga', 'zip_head' => ['84']],
        'nagasaki' => 	['value' => 42, 'ja' => '長崎県', 	'en' => 'Nagasaki', 'zip_head' => ['85']],
        'kumamoto' => 	['value' => 43, 'ja' => '熊本県', 	'en' => 'Kumamoto', 'zip_head' => ['86']],
        'oita' => 		['value' => 44, 'ja' => '大分県', 	'en' => 'Oita', 'zip_head' => ['87']],
        'miyazaki' => 	['value' => 45, 'ja' => '宮崎県', 	'en' => 'Miyazaki', 'zip_head' => ['88']],
        'kagoshima' => 	['value' => 46, 'ja' => '鹿児島県', 	'en' => 'Kagoshima', 'zip_head' => ['89']],
    ]],
    // 沖縄
    'okinawa' => 		['value' => 11, 'ja' => '沖縄', 	'en' => 'Okinawa', 'pref' => [
        'okinawa' => 	['value' => 47, 'ja' => '沖縄県', 	'en' => 'Okinawa', 'zip_head' => ['90']],
    ]],
];

// 都道府県コードのみのリスト作成
$prefectures = [];
foreach($regions as $region) {
    $prefectures = array_merge($prefectures, $region['pref']);
}


return [
    'prefecture' => [
        // 都道府県コード
        'codes' => $prefectures,
        // 日本地域区分
        'regions' => $regions,
    ],

    'system' => [
        'flag' => [
            'yes' => ['value' => 1, 'ja' => 'あり', 'en' => 'yes'],
            'no' => ['value' => 0, 'ja' => 'なし', 'en' => 'no'],
        ]
    ]
];
