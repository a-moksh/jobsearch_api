<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRequiresEmailAndLogin()
    {
        $this->json('POST', 'api/v1/user/login')
            ->assertStatus(422)
            ->assertJson([
                'message' => "The given data was invalid."
            ]);
    }

    public function testValidUserLogin(){

        $user = factory(\App\Models\User::class)->create([
            'email' => 'testinguser@gmail.com',
            'password' => bcrypt('testinguser')
        ]);

        $this->json('POST','api/v1/user/login',['email'=>'testinguser@gmail.com','password'=>'testinguser'])
            ->assertStatus(200);
    }
}
