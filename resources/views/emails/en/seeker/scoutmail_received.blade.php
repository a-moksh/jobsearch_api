@extends('emails.en.seeker.layouts.main')
@section('content')
Thank you very much for Using 「jobsearch」.<br><br>
You have received a Scout mail from the company.<br><br>

You can check the received information in the 「scout」 section<br><br>
after login to jobsearch from the following link.<br><br>

<a href="{{ config('app.seeker_url').'/user/scout' }}">{{ config('app.seeker_url').'/user/scout' }}</a><br><br>

You can reply to Company from 「Message」<br><br>

Thank you for your continued support for jobsearch.
<br><br>
@endsection
