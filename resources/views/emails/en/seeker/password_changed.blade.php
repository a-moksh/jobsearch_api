@extends('emails.en.seeker.layouts.main')
@section('content')
Password has be changed<br><br>

※Login ID and password are required to login.<br><br>
Please keep it in a safe.<br><br>

※  If you are not aware of this email, we are thankful <br><br>
if you could contact us by following contact us form<br><br>
<a href="{{ config('app.seeker_url').'/contact' }}">{{ config('app.seeker_url').'/contact' }}</a> <br><br>
Thank you for your continued support for jobsearch.
<br><br>

@endsection
