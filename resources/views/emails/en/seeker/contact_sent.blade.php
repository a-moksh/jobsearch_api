_____________________________________________________________ <br><br>

This email is sent when your sent information arrives at the server<br><br>
It is sent automatically from the system.<br><br>
_____________________________________________________________<br><br>



Thank you for contacting {{ env('APP_NAME') }}.<br><br>
Please confirm if the following content is correct.<br><br>

_____________________________________________________________<br><br>

Name<br><br>
{{ $contact->full_name }}<br><br>

Mail address<br><br>
{{ $contact->email }}<br><br>

Inquiry<br><br>
{{ $contact->message }}<br><br>

_____________________________________________________________<br><br>

In addition, I will reply from the person in charge sequentially<br><br>
When you are away, it may take some time to reply.<br><br>
Please note that we will reply within 2 business days.<br><br>

@include('emails.en.seeker.layouts.footer')