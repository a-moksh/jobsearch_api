@extends('emails.en.seeker.layouts.main')
@section('content')
Thank you for registering with jobsearch.<br><br>

The registered「Login ID ＝Mail Address」 is as follows.<br><br>
Please check if it is correct.<br><br>
_____________________________________________________________
<br><br>
Login ID: {{ $user->email }}
<br><br>
_____________________________________________________________
<br><br>
[jobsearch] is scheduled to open in June.<br><br>
We will notify you when we open it, so please stay tuned.<br><br>

※ Login ID and password are required to login.<br><br>
Please keep it  safe.<br><br>
We handle personal information in accordance with the privacy policy statement.<br><br>
※ Please check the following URL for details.<br><br>
<a href="{{ config('app.seeker_url').'/privacy-policy' }}">{{ config('app.seeker_url').'/privacy-policy' }}</a> <br><br>
※  If you are not aware of this email, we are thankful <br><br>
if you could contact us by following contact us form<br><br>
<a href="{{ config('app.seeker_url').'/contact' }}">{{ config('app.seeker_url').'/contact' }}</a> <br><br>

Thank you for your continuous support for jobsearch.<br>
@endsection
