@extends('emails.en.seeker.layouts.main')
@section('content')
Thank you for always Using [jobsearch].
<br><br>

About the applied job information
<br><br>
You received a message from the Company
<br><br>


After logging in to jobsearch from the URL below,
<br><br>
Please confirm the contents from "Message".
<br><br>



<a href="{{ config('app.seeker_url').'/user/messages' }}">{{ config('app.seeker_url').'/user/messages' }}</a> <br><br>

You can reply to message from「Message」.
<br><br>
_____________________________________________________________
<br><br>
[Applied Job information]
<br><br>

&nbsp;Job Id
<br><br>
&nbsp;{{ $job->id }}<br><br>

&nbsp;Company Name<br><br>
&nbsp;{{ $provider->company_name }}<br><br>

&nbsp;Title<br><br>
&nbsp;{{ $job->title }}<br><br>
_____________________________________________________________
<br><br>
Thank you for your continuous support for jobsearch.
<br><br>
@endsection
