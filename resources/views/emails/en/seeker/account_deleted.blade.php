@extends('emails.en.seeker.layouts.main')
@section('content')
<br>
Thank you for your contact,<br><br>
Account delete Process has been completed.<br><br>
Thank you very much for using [jobsearch] until now.<br><br>
※※  If you are not aware of this email, we are thankful if you could contact us by following contact us form.<br><br><br><br>

<a href="{{ env('APP_SEEKER_URL').'/'.$language }}/contact">
{{ env('APP_SEEKER_URL').'/'.$language }}/contact
</a>

<br><br>
@endsection
