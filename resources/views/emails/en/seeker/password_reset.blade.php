@extends('emails.en.seeker.layouts.main')
@section('content')
Thank you very much for Using 「jobsearch」.<br><br>

We will guide you through the password reset procedure.<br><br>

If you access [with in 4 hrs] to the following URL.<br><br>
Your Current Password will changed.<br><br><br><br>

<a href="{{ $link }}">{{ $link }}</a>

<br><br>
<br><br>
In displaying page<br><br>
Please enter log in email and new password.<br><br>

※ If it exceeds 4 hrs time from our mail sent,<br><br>
The above URL will expire for security reasons.<br><br>
In that case, please repeat the procedure.<br><br>

※ If you issued a URL for a password reset by mistake,<br><br>
Please discard the mail as it is.<br><br>
You can use the current password as it is.<br><br>
※ If you issue a URL for password reset multiple times, <br><br>
the old one becomes invalid and only the latest one becomes valid.<br><br>
※ If you are not aware of this email we are really sorry. You can discard this email.<br><br>

Thank you for your Continuous support for jobsearch.<br><br>

@endsection
