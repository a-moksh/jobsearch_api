@extends('emails.en.seeker.layouts.main')
@section('content')
Thank you very much for Using 「jobsearch」. <br><br>

[Temporary registration] has been set for identification.<br><br>

To complete the Registration process you need to access<br><br>
the following Link 「With in 4 hrs」 <br><br>

<br>
@if($user->user_type == 'seeker')
    <a href="{{config('app.seeker_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}">{{config('app.seeker_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}</a>
@endif
@if ($user->user_type == 'provider')
    <a href="{{config('app.provider_url').'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}">{{config('app.provider_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}</a>
@endif
@if ($user->user_type == 'admin')
    <a href="{{config('app.admin_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}">{{config('app.admin_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}</a>
@endif
<br>

<br><br>
※ If it exceeds [4 hrs] time from our email sending time,<br><br>
The above URL will expire for security reasons.<br><br>
In that case, please process from the beginning again.<br><br>
※ If you are not aware of this email we are really sorry.<br><br>
You can discard this email.<br><br>
_____________________________________________________________
<br><br>
[Registered information]<br><br>
&nbsp;Full Name<br>
&nbsp;{{ $user->full_name }}<br><br>
&nbsp;Email address<br>
&nbsp;{{ $user->email }}<br>
@endsection
