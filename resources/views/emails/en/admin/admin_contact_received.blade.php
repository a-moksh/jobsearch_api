{{ env('APP_NAME') }}に、お問い合わせがありました。 <br><br>
内容を確認し、返信してください。<br><br>

_____________________________________________________________<br><br>

@if($contact->company_name)
    会社名<br><br>
    {{ $contact->company_name }}<br><br>
@endif

ご担当者名<br><br>
{{ $contact->full_name }}<br><br>

メールアドレス<br><br>
{{ $contact->email }}<br><br>

お問い合わせ内容<br><br>
{{ $contact->message }}<br><br>
_____________________________________________________________<br><br>
