The new company has registered in jobsearch.<br><br>
After logging in to the management screen, please check the contents.<br><br>
_____________________________________________________________<br><br>

Customer Id<br><br>
{{ $user->provider->provider_identifier_id }}<br><br>

Company Name<br><br>
{{ $user->provider->company_name }}<br><br>

Company management URL (login required)<br><br>
{{ $user->provider->url }}<br><br>
_____________________________________________________________<br><br>
