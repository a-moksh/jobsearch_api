New job information has been registered in jobsearch.<br><br>
After logging in to the management screen, please check the contents and review.<br><br>
_____________________________________________________________<br><br>

Job Id<br><br>
{{ $job->job_identifier_id }}<br><br>

Company Name<br><br>
{{ $job->provider->company_name }}<br><br>

Title<br><br>
{{ $job->title }}<br><br>

Job information management URL (login required)<br><br>
<a href="{{ env('APP_ADMIN_URL').'/'.$language  }}/jobs">
    {{ env('APP_ADMIN_URL').'/'.$language  }}/jobs
</a><br><br>
_____________________________________________________________<br><br>
