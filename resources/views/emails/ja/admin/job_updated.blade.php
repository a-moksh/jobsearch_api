
jobsearchに、xxx日間、審査されていない求人情報があります。<br><br>
管理画面にログイン後、内容を確認し、審査してください。<br><br>

_____________________________________________________________<br><br>

求人ID<br><br>
{{ $job->job_identifier_id }}<br><br>

会社名<br><br>
{{ $job->provider->company_name }}<br><br>

タイトル<br><br>
{{ $job->title }}<br><br>

求人情報管理URL（要ログイン）<br><br>
<a href="{{ env('APP_ADMIN_URL').'/'.$language  }}/jobs">
    {{ env('APP_ADMIN_URL').'/'.$language  }}/jobs
</a><br><br>
_____________________________________________________________<br><br>
