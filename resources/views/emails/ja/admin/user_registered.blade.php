
jobsearchに、新規企業の登録がありました。<br><br>
管理画面にログイン後、内容を確認してください。<br><br>

_____________________________________________________________<br><br>

顧客ID<br><br>
{{ $user->provider->provider_identifier_id }}<br><br>

会社名<br><br>
{{ $user->provider->company_name }}<br><br>

企業管理URL（要ログイン）<br><br>
<a href="{{ env('APP_ADMIN_URL').'/'.$language  }}/companies">
    {{ env('APP_ADMIN_URL').'/'.$language  }}/companies
</a>
<br><br>
_____________________________________________________________<br><br>
