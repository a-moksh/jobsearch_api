@extends('emails.ja.seeker.layouts.main')
@section('content')
いつも「jobsearch」をご愛顧いただき、ありがとうございます。<br><br>

パスワードが変更されました。<br><br>

※ログインIDとパスワードは、ログインする際に必要となります。<br><br>
　大切に保管願います。<br><br>
※当メールにお心当たりの無い場合は、誠に恐れ入りますが<br><br>
　お問い合わせフォームからご連絡いただければ幸いです。<br><br>
　
<a href="{{ config('app.seeker_url').'/'.$language.'/contact' }}">{{ config('app.seeker_url').'/'.$language.'/contact' }}</a>
<br><br>

今後とも jobsearch をどうぞ宜しくお願いいたします。<br><br>
@endsection
