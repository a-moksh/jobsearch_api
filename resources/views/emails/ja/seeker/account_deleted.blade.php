@extends('emails.ja.seeker.layouts.main')
@section('content')
<br>
ご連絡、ありがとうございます。<br><br>
退会手続きが完了いたしました。<br><br>
今まで「jobsearch」をご愛顧いただき、ありがとうございました。<br><br>
※当メールにお心当たりの無い場合は、誠に恐れ入りますがお問い合わせフォームからご連絡いただければ幸いです。
<br><br><br><br>

<a href="{{ env('APP_SEEKER_URL').'/'.$language }}/contact">
{{ env('APP_SEEKER_URL').'/'.$language }}/contact
</a>

<br><br>
@endsection
