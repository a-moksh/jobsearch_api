@extends('emails.ja.seeker.layouts.main')
@section('content')
この度は「jobsearch」にご登録いただき、ありがとうございます。
<br><br>

ご登録の「ログインID = メールアドレス」は、下記の通りです。<br><br>
お間違いないかご確認ください。
<br><br>
_____________________________________________________________
<br><br>
ログイン ID: {{ $user->email }}
<br><br>
_____________________________________________________________
<br><br>
「jobsearch」は6月にオープン予定です。<br><br>
オープン時に登録メールへお知らせしますので楽しみにお待ちください。
<br><br>

※ ログインIDとパスワードは、ログインする際に必要となります。<br><br>
　大切に保管願います。
<br><br>
※ 個人情報は個人情報保護方針記載の要領に沿って取り扱います。<br><br>
　詳しくは次のURLをご確認ください。
<br><br>
<a href="https://jobsearch.jp/privacy-policy">https://jobsearch.jp/privacy-policy</a> <br><br>
※ 当メールにお心当たりの無い場合は、誠に恐れ入りますが<br><br>
　お問い合わせフォームからご連絡いただければ幸いです。
<br><br>
<a href="https://jobsearch.jp/contact">https://jobsearch.jp/contact</a> <br><br>

今後とも jobsearch をどうぞ宜しくお願いいたします。
<br>
@endsection
