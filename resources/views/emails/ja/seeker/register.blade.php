@extends('emails.ja.seeker.layouts.main')
@section('content')
この度は「jobsearch」のご利用、ありがとうございます。<br><br>
ご本人様確認のため、現在は「仮登録」となっております。<br><br>

本登録を完了していただくためには、「 4 時間以内」<br><br>
に 下記のURLにアクセスしていただく必要があります。
<br><br>

<br>
@if($user->user_type == 'seeker')
    <a href="{{config('app.seeker_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}">{{config('app.seeker_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}</a>
@endif
@if ($user->user_type == 'provider')
    <a href="{{config('app.provider_url').'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}">{{config('app.provider_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}</a>
@endif
@if ($user->user_type == 'admin')
    <a href="{{config('app.admin_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}">{{config('app.admin_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}</a>
@endif
<br>

<br><br>
※ 当メール送信時刻より [4時間]を超過しますと、<br><br>
セキュリティ保持のため上記URLは有効期限切れとなります。<br><br>
その場合は再度、最初からお手続きをお願いします。<br><br>
※ メールにお心当たりの無い場合は、誠に恐れ入りますが<br><br>
破棄していただけますよう、よろしくお願いいたします。<br><br>
_____________________________________________________________
<br><br>
[ご登録内容]<br><br>
&nbsp;名前<br>
&nbsp;{{ $user->full_name }}<br><br>
&nbsp;メールアドレス<br>
&nbsp;{{ $user->email }}<br>
@endsection
