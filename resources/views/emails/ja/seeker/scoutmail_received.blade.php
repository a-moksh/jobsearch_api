@extends('emails.ja.seeker.layouts.main')
@section('content')
いつも「jobsearch」をご愛顧いただき、ありがとうございます。<br><br>

企業からスカウトされました。<br><br>

下記のURLより jobsearch へログイン後、<br><br>
「スカウト」より、内容をご確認ください。<br><br>

<a href="{{ config('app.seeker_url').'/user/scout' }}">{{ config('app.seeker_url').'/user/scout' }}</a>
<br><br>

「メッセージ」から企業に返信することもできます。<br><br>

今後とも jobsearch をどうぞ宜しくお願いいたします。<br><br>

@endsection
