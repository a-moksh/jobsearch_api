@extends('emails.ja.seeker.layouts.main')
@section('content')
いつも「jobsearch」をご愛顧いただき、ありがとうございます。<br><br>

ご応募された求人情報について<br><br>
企業からメッセージが届いています。<br><br>

下記のURLより jobsearch へログイン後、<br><br>
「メッセージ」より、内容をご確認ください。<br><br>

<a href="{{ config('app.seeker_url').'/user/messages' }}">{{ config('app.seeker_url').'/user/messages' }}</a>

「メッセージ」から企業に返信することもできます。<br><br>
_____________________________________________________________
<br><br>
[応募している求人情報]<br><br>

&nbsp;求人ID<br><br>
&nbsp;{{ $job->id }}<br><br>

&nbsp;会社名<br><br>
&nbsp;{{ $provider->company_name }}<br><br>

&nbsp;タイトル<br><br>
&nbsp;{{ $job->title }}<br><br>
_____________________________________________________________
<br><br>
今後とも jobsearch をどうぞ宜しくお願いいたします。<br><br>

<br><br>
@endsection
