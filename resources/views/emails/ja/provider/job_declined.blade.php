@extends('emails.ja.provider.layouts.main')
@section('content')
    いつも「{{ env('APP_NAME') }}」をご愛顧いただき、ありがとうございます。<br><br>

    掲載を希望されている求人情報の正当性について<br><br>
    {{ env('APP_NAME') }}のガイドラインに則って審査を行いましたが<br><br>
    承認されませんでした。

    下記のURLより {{ env('APP_NAME') }} へログイン後、<br><br>
    「求人情報管理」より、状況をご確認ください。<br><br>

    <a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/job/{{ $job->id }}">
        {{ env('APP_PROVIDER_URL').'/'.$language  }}/job/{{ $job->id }}
    </a>
    <br><br>

    _____________________________________________________________<br><br>

    【対象となる求人情報】<br><br>

    求人ID<br><br>
    {{ $job->job_identifier_id }}<br><br>

    タイトル<br><br>
    {{ $job->title }}<br><br>
    _____________________________________________________________<br><br>

    今後とも {{ env('APP_NAME') }} をどうぞ宜しくお願いいたします。<br><br>

@endsection('content')