_____________________________________________________________<br><br>
【{{ env('APP_NAME') }} お問い合わせ】<br>
&nbsp;&nbsp;&nbsp;フリーダイヤル {{ env('FREE_DIAL_NO')  }}<br>
&nbsp;&nbsp;&nbsp;月〜金 10:00〜12:00 13:00〜17:00<br>
&nbsp;&nbsp;&nbsp;{{ env('CONTACT_MAIL') }}<br>
&nbsp;&nbsp;<a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/contact">
    {{ env('APP_PROVIDER_URL').'/'.$language  }}/contact
</a>
    <br><br>
【運営元】<br>
&nbsp;&nbsp;ウェブモ株式会社<br>
&nbsp;&nbsp;{{ env('COMPANY_ADDRESS') }}<br><br>
&nbsp;&nbsp;神奈川県川崎市宮前区宮前平1-10-3-504<br><br>
_____________________________________________________________<br>
