@extends('emails.ja.provider.layouts.main')
@section('content')
    いつも「{{ env('APP_NAME') }}」をご愛顧いただき、ありがとうございます。 <br><br>

    パスワードが変更されました。<br><br>

    ※ログインIDとパスワードは、ログインする際に必要となります。<br><br>
    　大切に保管願います。<br><br>
    ※当メールにお心当たりの無い場合は、誠に恐れ入りますが<br><br>
    　お問い合わせフォームからご連絡いただければ幸いです。<br><br>
    　<a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/contact">
        {{ env('APP_PROVIDER_URL').'/'.$language  }}/contact
    </a>
    <br><br>
    今後とも {{ env('APP_ENV') }} をどうぞ宜しくお願いいたします。<br><br>

@endsection
