@extends('emails.ja.provider.layouts.main')
@section('content')
    ご連絡、ありがとうございます。<br><br>

    退会手続きが完了いたしました。<br><br>

    今まで「jobsearch」をご愛顧いただき、ありがとうございました。<br><br>

    ※保有されていたポイントは無効とさせていただきました。<br><br>
    ※当メールにお心当たりの無い場合は、誠に恐れ入りますが<br><br>
    　お問い合わせフォームからご連絡いただければ幸いです。<br><br>
    　<a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/contact">
        {{ env('APP_PROVIDER_URL').'/'.$language  }}/contact
    </a>

@endsection('content')