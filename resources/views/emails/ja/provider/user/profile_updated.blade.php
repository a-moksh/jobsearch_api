@extends('emails.ja.provider.layouts.main')
@section('content')
    いつも「{{ env('APP_NAME') }}」をご愛顧いただき、ありがとうございます。 <br><br>

    ご登録の会員情報が変更されました。<br><br>

    ご変更いただいた内容は、下記のURLより jobsearch へログイン後、<br><br>
    「設定 ＞ 会社情報」よりご確認いただけます。<br><br>

    <a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/profile">
        {{ env('APP_PROVIDER_URL').'/'.$language  }}/profile
    </a>
    <br><br>

    ※当メールにお心当たりの無い場合は、誠に恐れ入りますが<br><br>
    　お問い合わせフォームからご連絡いただければ幸いです。<br><br>
    　<a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/contact">
        {{ env('APP_PROVIDER_URL').'/'.$language  }}/contact
    </a>
    <br><br>

    今後とも {{ env('APP_NAME') }} をどうぞ宜しくお願いいたします。<br><br>

@endsection
