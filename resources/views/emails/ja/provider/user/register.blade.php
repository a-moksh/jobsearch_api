<br>
{{ $user->provider->company_name }} 様<br><br>

この度は「{{ env('APP_NAME') }}」のご利用、ありがとうございます。<br>

ご本人様確認のため、現在は「仮登録」となっております。<br><br>

本登録を完了していただくためには、「4時間以内」に<br><br>
下記のURLにアクセスしていただく必要があります。<br><br>


<a href="{{config('app.provider_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token)}}">
    {{ config('app.provider_url').'/'.$language.'/user/verify/email/?token='.urlencode($user->verifyUser->token) }}
</a>
<br><br>

※当メール送信時刻より4時間を超過しますと、<br><br>
　セキュリティ保持のため上記URLは有効期限切れとなります。<br><br>
　その場合は再度、最初からお手続きをお願いします。<br><br>
※当メールにお心当たりの無い場合は、誠に恐れ入りますが <br><br>
　破棄していただけますよう、よろしくお願いいたします。<br><br>


_____________________________________________________________<br><br>

【ご登録内容】<br><br>

    会社名<br><br>
    {{ $user->provider->company_name }}<br><br>

    メールアドレス<br><br>
    {{ $user->email }}<br><br>
@include('emails.ja.provider.layouts.footer')
