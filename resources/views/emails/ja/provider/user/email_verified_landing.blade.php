@extends('emails.ja.provider.layouts.main')
@section('content')
    この度は「{{ env('APP_NAME') }}」にご登録いただき、ありがとうございます。<br><br>

    ご登録の「ログインID（＝メールアドレス）」は、下記の通りです。<br><br>
    お間違いないかご確認ください。<br><br>
    _____________________________________________________________<br><br>

    ログインID: {{ $user->email }}<br><br>
    _____________________________________________________________<br><br>



    「{{ env('APP_NAME') }}」は6月にオープン予定です。<br><br>

    オープン時に登録メールへお知らせしますので楽しみにお待ちください。<br><br>



    ※ログインIDとパスワードは、ログインする際に必要となります。<br><br>
    　大切に保管願います。<br><br>
    ※個人情報は個人情報保護方針記載の要領に沿って取り扱います。<br><br>
    　詳しくは次のURLをご確認ください。<br><br>
    　<a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/privacy">
        &nbsp;&nbsp;{{ env('APP_PROVIDER_URL').'/'.$language  }}/privacy
    </a><br><br>
    ※当メールにお心当たりの無い場合は、誠に恐れ入りますが<br><br>
    　お問い合わせフォームからご連絡いただければ幸いです。<br><br>
    　<a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/contact">
        &nbsp;&nbsp;{{ env('APP_PROVIDER_URL').'/'.$language  }}/contact
    </a><br><br>

    今後とも {{ env('APP_NAME') }} をどうぞ宜しくお願いいたします。<br><br>

@endsection
