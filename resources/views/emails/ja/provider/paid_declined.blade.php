@extends('emails.ja.provider.layouts.main')
@section('content')
    この度は「{{ env('APP_NAME') }}」をご愛顧いただき、ありがとうございます。<br><br>

    締め支払い決済「Paid（ペイド）」の審査が完了し<br><br>
    残念ながら、お申込のご意向に沿えない結果となりました。<br><br>

    審査内容につきましては、弊社ではお応えできませんので、<br><br>
    ご了承くださいますようお願い申しあげます。<br><br>

    ご登録いただいた{{ env('APP_NAME') }}のアカウントにつきましては<br><br>
    引き続きご利用いただけますが、<br><br>
    求人広告の出稿やスカウト、プロによる翻訳オプションなど<br><br>
    有料コンテンツがご利用いただけません。<br><br>

    退会をご希望の場合は、下記のURLより jobsearch へログイン後、<br><br>
    「設定 ＞ 会社情報」よりアカウントを削除してください。<br><br>

    <a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/setting/{{ $user->provider->id }}">
        {{ env('APP_PROVIDER_URL').'/'.$language  }}/setting/{{ $user->provider->id }}
    </a><br><br>


    今後とも {{ env('APP_NAME') }} をどうぞ宜しくお願いいたします。<br><br>

@endsection('content')