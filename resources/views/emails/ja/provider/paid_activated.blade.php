@extends('emails.ja.provider.layouts.main')
@section('content')
   いつも「{{ env('APP_NAME') }}」をご愛顧いただき、ありがとうございます。<br><br>

   締め支払い決済「Paid（ペイド）」への登録・審査が完了し、<br><br>
   ポイントをご購入いただけるようになりました。<br><br>

   求人広告の出稿やスカウト、プロによる翻訳オプションなど<br><br>
   有料コンテンツをご利用の場合は<br><br>
   下記のURLより {{ env('APP_NAME') }} へログイン後、<br><br>
   「支払管理」より、事前にポイントを購入してご利用ください。<br><br>

   <a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/payment">
      {{ env('APP_PROVIDER_URL').'/'.$language  }}/payment
   </a><br><br>

   ポイント残高、利用履歴などもこちらでご確認いただけます。<br><br>

   なお、Paidのアカウント情報や利用限度額などにつきましては、<br><br>
   Paidより直接メールが届きますので、そちらをご確認ください。<br><br>

   今後とも {{ env('APP_NAME') }} をどうぞ宜しくお願いいたします。<br><br>


@endsection('content')