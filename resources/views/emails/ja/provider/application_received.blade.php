@extends('emails.ja.provider.layouts.main')
@section('content')
    いつも「{{ env('APP_NAME') }}」をご愛顧いただき、ありがとうございます。<br><br>

    掲載されている求人情報に応募がありました。<br><br>

    下記のURLより {{ env('APP_NAME') }} へログイン後、<br><br>
    「応募者」より、プロフィールをご確認ください。<br><br>

    <a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/applications/{{ $seeker_job->id }}">
        {{ env('APP_PROVIDER_URL').'/'.$language  }}/applications/{{ $seeker_job->id }}
    </a>
    <br><br>

    管理画面上から応募者にメッセージを送ることもできます。<br><br>
    _____________________________________________________________<br><br>

    【応募のあった求人情報】<br><br>

    求人ID<br><br>
    {{ $job->job_identifier_id }}<br><br>

    タイトル<br><br>
    {{ $job->title }}<br><br>
    _____________________________________________________________<br><br>

    今後とも {{ env('APP_NAME') }} をどうぞ宜しくお願いいたします。<br><br>

@endsection('content')