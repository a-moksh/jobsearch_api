@extends('emails.ja.provider.layouts.main')
@section('content')

    いつも「{{ env('APP_NAME') }}」をご愛顧いただき、ありがとうございます。 <br><br>

    掲載を希望されている求人情報の正当性について<br><br>
    {{ env('APP_NAME') }}のガイドラインに則って審査・承認されましたので、<br><br>
    公開させていただきました。<br><br>

    下記のURLより {{ env('APP_NAME') }} へログイン後、<br><br>
    「求人情報管理」より、状況をご確認ください。<br><br>

    <a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/job/{{ $job->id }}">
        {{ env('APP_PROVIDER_URL').'/'.$language  }}/job/{{ $job->id }}
    </a>
    <br><br>

    ※掲載の一時停止・削除をご希望のかたは<br><br>
    　求人情報のステータスを「非公開」にご変更ください。<br><br>
    ※求人情報の正当性を保つため、掲載内容は変更できません。<br><br>
    　古い情報を非公開とし、新たな情報を作成し直してください。<br><br>

    _____________________________________________________________<br><br>

    【公開された求人情報】<br><br>

    求人ID<br><br>
    {{ $job->job_identifier_id }}<br><br>

    タイトル<br><br>
    {{ $job->title }}<br><br>
    _____________________________________________________________<br><br>

    今後とも {{ env('APP_NAME') }} をどうぞ宜しくお願いいたします。<br><br>

@endsection('content')