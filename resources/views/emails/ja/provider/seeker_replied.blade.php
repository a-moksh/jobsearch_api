@extends('emails.ja.provider.layouts.main')
@section('content')

    いつも「{{ env('APP_NAME') }}」をご愛顧いただき、ありがとうございます。<br><br>

    メッセージを送った応募者から返信が届いています。<br><br>

    下記のURLより {{ env('APP_NAME') }} へログイン後、<br><br>
    「メッセージ」より、内容をご確認ください。<br><br>

    <a href="{{ env('APP_PROVIDER_URL').'/'.$language  }}/message">
        {{ env('APP_PROVIDER_URL').'/'.$language  }}/message
    </a>
    <br><br>

    _____________________________________________________________<br><br>

    【メッセージを送った応募者】<br><br>

    ユーザーID<br><br>
    {{ $seeker_user->seeker->seeker_identifier_id }}<br><br>

    名前<br><br>
    {{ $seeker_user->full_name }}<br><br>
    _____________________________________________________________<br><br>

    今後とも {{ env('APP_NAME') }} をどうぞ宜しくお願いいたします。<br><br>

@endsection('content')