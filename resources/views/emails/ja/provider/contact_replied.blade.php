{{ $data['company_name'] }}　採用ご担当者様<br><br>
この度は jobsearchへのお問い合わせ、ありがとうございます。<br><br>
{{ $data['admin_reply'] }}

<br><br><br><br>
_____________________________________________________________
<br><br>
[jobsearch お問い合わせ]<br><br>
&nbsp;フリーダイヤル 0120-xxx<br><br>
&nbsp;月〜金 10:00〜12:00 13:00〜17:00<br><br>
&nbsp;{{ env('CONTACT_MAIL') }}<br><br>
&nbsp;<a href="{{ env('APP_SEEKER_URL').'/'.$data['language']  }}/contact">
{{ env('APP_SEEKER_URL').'/'.$data['language']  }}/contact
</a><br><br>
[運営元]<br><br>
&nbsp;ウェブモ株式会社<br><br>
&nbsp;神奈川県川崎市宮前区宮前平1-10-3-504<br><br>
_____________________________________________________________