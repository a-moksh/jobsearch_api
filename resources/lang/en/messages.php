<?php
/**
 * Created by PhpStorm.
 * User: krishna
 * Date: 11/19/2018
 * Time: 11:29 AM
 */

return [
    'created'=> 'Your data has been successfully created.',
    'deleted'=> 'Your data has been successfully deleted.',
    'updated'=> 'Your data has been successfully updated.',


    'loggedout'=> 'Successfully logged out.',

    'job_applied'=> 'Your application has been submitted to the provider.',
    'job_wishlisted'=> 'Selected job is now on you wishlist.',

    'imagelimit'=> 'you can upload only 5 image per job.',
    'duplicateimage'=> 'You cannot upload duplicate image.',
    'already_exist_email'  => 'This address is already registered',

    // custom messages for some validations
    'terms'=> 'You must accept terms and conditions to continue.',
    'required_unless_japan' => 'The :attribute field is required unless country is Japan.',
    'required_if_japan' => 'The :attribute field is required if country is Japan.',
    'required_after_30days' => 'The :attribute must be after 30 days.',

    'min' => 'The :attribute must be in correct format.',
    'duplicate_entry'=> 'The selected Id is already exist',
    'duplicate_scout_message'=>'You have already scouted given seeker for this job.',
    'scout_application_message_sent'=>'Great ! Your reply was sent.',
    'required_unless_graduation'=>'The :attribute field is required unless graduation status is studying.',
    'required_if_not_working'=>'The :attribute field is required if currently not working in this company.',
    'required_if_experienced'=>'The :attribute field is required if experienced',
    'password_regex'=>'Invalid ! Please input at least one alphabet and one number between 8-15 length.',
    'after'=>'selected date must be in future date',
    'greater_than_field'=>'The max salary must be greater than min salary',
    'not_eligible'=>'you cant buy point now, your account is under verification',
    'success_sync'=> 'Synchronization successful',
    'point_purchase_success'=>'point brought successfully',
    'not_enough_balance'=>'Your balance is not enough.',
    'wrong_email'=>'This email is either not registered or not verified',


    //@rabi add in japanese

    'failed'=>'Request failed. try again later.',
    'points_purchase_error'=>'Point cannot be purchased at the moment.',
    'points_not_enough'=>'You do not have enough points.',
    'application_already_opened'=>'Application already opened.',
    'seeker_already_opened'=>'Seeker already opened',
    'seeker_opened'=>'Seeker Opened Successfully',
    'already_synced'=>'Account already synced.',
    'sync_success'=>'Account sync success.',
    'please_open_first'=>'Open first to reply the application.',
    'company_skype_id_required_if'=>'If the interview method is Skype then Skype Id is required',
    'job_location_id_required_if'=>'If interview is in working place work location is required',
    'interview_place_required_if'=>'if interview method is company visit then interview address is required',
    'receive_mail_language'=>'Language is required if receive mail was selected',
    'receive_mail'=>'Receive Mail is required field',
    'image_limit' => 'Atleast 1 or Maximum 5 image is required',
    'job_renewed' => 'Your job has been renewed. It will be active for 90 more days.',
    'account_already_active' => 'Your paid account is already active.',
    'paid_active' => 'Your paid account is verified.',
    'error' => 'sorry !! some thin went wrong.',
    'certificate_year'=>'Certificate year must be a date before today',
    'contact'=>[
        'success'=>'Thank you for your message, we will get back to you as soon as possible'
    ],
    'seeker_account_deleted'=>'Your account has been successfully deleted.',
    'mail'=>[
        'register_subject'=>'[jobsearch] Request to complete registration',
        'seeker_email_verified' => '[jobsearch] Registration Completed',
        'password_reset' => '[jobsearch] Password Reset',
        'password_changed' => '[jobsearch] Password Reset complete',
        'scout_received'=> '[jobsearch] Received a scout',
        'scout_replied'=>'[jobsearch] Received a message from applied company',
        'provider_email_verified' => '[jobsearch] 会員登録が完了しました',
        'provider_profile_updated' => '[jobsearch] 会員情報が変更されました',
        'provider_account_deleted' => '[jobsearch] 退会しました',
        'job_application_received' => '[jobsearch] 求人に応募がありました',
        'job_application_reply_received' => '[jobsearch] メッセージを送った応募者から返信が届きました',
        'job_scout_reply_received' => '[jobsearch] スカウトした人材からメッセージが届きました',
        'paid_verification_approved' => '[jobsearch] お支払方法で「Paid」が利用可能になりました',
        'paid_verification_declined' => '[jobsearch] お支払方法が承認されませんでした',
        'job_approved' => '[jobsearch] 審査中の求人情報が公開されました',
        'job_declined' => '[jobsearch] 審査中の求人情報が承認されませんでした',
        'company_registered'=>'Company Registered',
        'job_created'=>'New Job Created',
        'job_updated'=>'[jobsearch] 審査していない求人があります',
        'seeker_contact'=>'[jobsearch] お問い合わせがありました',
        'provider_contact'=>'[jobsearch] お問い合わせがありました',
        'contact_replied'=>'[jobsearch] Thank you for contacting',
        'seeker_account_deleted'=>'[jobsearch] Account Deleted'
    ]
];
