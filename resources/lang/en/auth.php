<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Email address or password is incorrect.',
    'tokenFailed' => 'The submitted information is invalid.',
    'success' => 'Login Successful.',
    'not_registered'=>'This e-mail address has not been registered',
    'throttle' => 'Too many login attempts. Please try again in :minute minutes.',
    'email' => [
        'resendSuccess'=>'Email verification link has been sent to your email.',
        'notVerified'=>'You have not verified email.',
        'valid' => 'Provided email is not a valid format.',
        'invalid' => 'Provided email is not valid.',
        'unique' => 'Email address is already used.',
        'verified' => 'Your email has been verified.',
        'alreadyVerified' => 'Your email has already been verified.',
        'notFound' => 'The submitted email is not valid.',
        'required' => 'The email field is required.',
        'token_expired'=>'Your Email Verification Token time is expire please register again  to get new verification token'
    ],
    'password' => [
        'required' => 'The password field is required.',
    ],
    'string' => 'Provided data must be a string type.',
    'token_creation_failed' => 'Token cannot be created.',
    'confirmed' => 'Password confirmation is required.',

    'reset_sent' => 'Password reset link sent to the email.',
    'reset_failed' => 'Reset link cannot be sent.',
    'reset_successful' => 'Password Reset Successful.',
    'reset_fail_message' => 'Password reset request failed.',
    'user_not_deletable'=>'User Cant be deleted',
    'register'=> [
        'success' => 'Congratulation ! registration successful, please check your mail for account verification.'
    ]
];
