<?php
/**
 * Created by PhpStorm.
 * User: krishna
 * Date: 11/19/2018
 * Time: 11:29 AM
 */

return [
    '400'=> 'error 400',
    '403'=> 'You are not allowed to perform this action',
    '404'=> 'Resource you are looking was not found',
];