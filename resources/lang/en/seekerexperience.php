<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Store SeekerExperience Language translations
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during seeker experience creation process
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'company_name' => 'The Company Name field is required.',
    'job_title' => 'The Company Name field is required.',
    'job_from' => 'The Company Name field is required.',
    'job_to' => 'The Company Name field is required.',
    'currently_working_here' => 'The Company Name field is required.',
    'annual_salary' => 'The Company Name field is required.',
    'job_description' => 'The Company Name field is required.',
];