<?php

return [
    'password_link' => 'Password Reset Link',
    'reset_password' => 'Reset Password',
    'link_is_here' => 'Your password reset link is here.',
    'thankyou' => 'Thank You for Using '.config('app.name').'.',
    'hello' => 'Hello, ',
    'regards' => 'Regards, ',
];
