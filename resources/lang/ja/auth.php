<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'メールアドレスまたはパスワードが違います。',
    'tokenFailed' => '入力された情報は無効です.',
    'success' => 'ログイン成功.',
    'not_registered'=>'このメールアドレスは登録されていません',
    'throttle' => 'ログイン試行回数が多すぎます。 :minute分後にもう一度試してください',
    'email' => [
        'valid' => '提供されたメールアドレスが無効です.',
        'notVerified'=>'You have not verified email.',
        'invalid' => '提供されたメールアドレスが無効です.',
        'unique' => '入力されたメールアドレスは既に使用されています.',
        'verified' => 'アカウントは確認済みです.',
        'alreadyVerified' => 'メールアドレスはすでに確認されています.',
        'notFound' => '提供されたメールアドレスが無効です.',
        'required' => 'メールアドレスは必要な項目です.',
        'token_expired'=>'あなたのEメール検証トークンは期限が切れました検証トークンを得るためにもう一度登録してください'

    ],
    'password' => [
        'required' => 'パスワードは必要な項目です.',
    ],
    'string' => '文字列で入力してください.',
    'token_creation_failed' => 'トークンを作成できません.',
    'confirmed' => 'パスワードの確認が必要です.',

    'reset_sent' => 'パスワードリセットリンクはメールに送信しました.',
    'reset_failed' => 'リセットリンク送付することができません.',
    'reset_successful' => 'パスワードの変更は寛郎しました.',
    'reset_fail_message' => 'パスワードリセットできません.',
    'user_not_deletable'=>'削除できません',
    'register'=> [
        'success' => 'ご登録、ありがとうございます。仮登録が完了しました。'
    ]
];
