<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Store Seeker Language translations
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during seeker creation process
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'visa_status' => 'ビーザステタスは必須です.',

];
