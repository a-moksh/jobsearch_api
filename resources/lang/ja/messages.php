<?php
/**
 * Created by PhpStorm.
 * User: krishna
 * Date: 11/19/2018
 * Time: 11:29 AM
 */

return [
    'created'=> '作成しました.',
    'deleted'=> '削除しました.',
    'updated'=> 'アップデートされました',


    'loggedout'=> 'ログアウトされました.',

    'job_applied'=> '応募を企業に送付されました.',
    'job_wishlisted'=> 'お気入りに追加しました.',

    'imagelimit'=> '画像は５枚までアップロードできます.',
    'duplicateimage'=> '重複した画像アップロードできません.',
    // custom messages for some validations
    'terms'=> '続行するには利用規約に同意する必要があります.',
    'required_unless_japan' => '日本のばいいは:attributeは必須です.',
    'required_if_japan' => '日本の場合は:attributeは必須です',
    'required_after_30days' => ':attributeは30日後にする必要があります',
    'already_exist_email'  => 'このアドレスは既に登録されています。',
    'min' => ':attributeは正しくありません.',
    'regex' => ':attribute must be in correct format.',
    'duplicate_entry'=> '選択したIDはすでに登録してあります',
    'duplicate_scout_message'=>'すでにこの仕事のために与えられたユーザーを探し出しています.',
    'scout_application_message_sent'=>'返信が送信されました。',
    'required_unless_graduation'=>'卒業ステータスが勉強中でない限り、:attribute フィールドは必須です',
    'required_if_not_working'=>'現在この会社で働いていない場合、:attribute フィールドは必須です',
    'required_if_experienced'=>'経験ありの場合は:attributeは必須です',
    'password_regex'=>'無効です。長さ8〜15の範囲で、少なくとも1つの文字と1つの数字を入力してください。',
    'after'=>'選択された日付は今日以降の日付にする必要があります',
    'greater_than_field'=>'最高給与は最低給料より大きいする必要があります',
    'not_eligible'=>'アカウント検証中ですからポイント購入できません',
    'success_sync'=> 'ペイドに接続完了しました、検証終わりましたらポイント購入できます',
    'point_purchase_success'=>'ポイント購入完了しました',
    'not_enough_balance'=>'バランスリミットオーバーしてあります',
    'points_purchase_error'=>'購入することができません.',
    'points_not_enough'=>'十分なポイントがありません.',
    'application_already_opened'=>'すでに開かれています.',
    'seeker_already_opened'=>'人材すでに開かれています',
    'seeker_opened'=>'人材正常に開きました',
    'already_synced'=>'アカウントすでに同期してあります.',
    'sync_success'=>'アカウント同期完了',
    'please_open_first'=>'最初に開いてから返信する.',
    'failed'=>'失敗しました。 一度試してみてください.',
    'company_skype_id_required_if'=>'面接方法スカイプを選択した時スカイプIDは必須です',
    'job_location_id_required_if'=>'面接場所を働く場所の場合は働く場所は必須です',
    'interview_place_required_if'=>'面接方法を会社訪問の場合は面接場所は必須です',
    'receive_mail_language'=>'メール配信するを選択した場合は言語が必要です',
    'receive_mail'=>'メール受信必須フィールド',
    'image_limit' => 'アップロードできる画像は最低1枚と最大5枚です',
    'job_renewed' => '日付を更新しました.三ヶ月追加しました',
    'account_already_active' => 'アカウントすでに活動なってあります',
    'paid_active' => 'アカウント承認済みです',
    'error' => '申し訳ありません!!何問題発生した理由で要求を完了できません',
    'certificate_year'=>'資格年度は本日より前の日付にする必要があります',
    'wrong_email'=>'このメールアドレスは登録されていません',
    'seeker_account_deleted'=>'退会が完了しました。',
    'mail'=>[
        'register_subject'=>'[jobsearch] 仮登録完了と本登録のお願い',
        'seeker_email_verified' => '[jobsearch] 会員登録が完了しました',
        'provider_email_verified' => '[jobsearch] 会員登録が完了しました',
        'provider_profile_updated' => '[jobsearch] 会員情報が変更されました',
        'password_reset' => '[jobsearch] パスワードがリセットされました',
        'password_changed' => '[jobsearch] パスワードが変更されました',
        'provider_account_deleted' => '[jobsearch] 退会しました',
        'job_application_received' => '[jobsearch] 求人に応募がありました',
        'job_application_reply_received' => '[jobsearch] メッセージを送った応募者から返信が届きました',
        'job_scout_reply_received' => '[jobsearch] スカウトした人材からメッセージが届きました',
        'paid_verification_approved' => '[jobsearch] お支払方法で「Paid」が利用可能になりました',
        'paid_verification_declined' => '[jobsearch] お支払方法が承認されませんでした',
        'job_approved' => '[jobsearch] 審査中の求人情報が公開されました',
        'job_declined' => '[jobsearch] 審査中の求人情報が承認されませんでした',
        'scout_received'=> '[jobsearch] 企業からスカウトされました',
        'scout_replied'=>'[jobsearch] 応募した企業からメッセージが届きました',
        'company_registered'=>'[jobsearch] 新規登録された企業を確認してください',
        'job_created'=>'[jobsearch] 新規登録された求人を審査してください',
        'job_updated'=>'[jobsearch] 審査していない求人があります',
        'seeker_contact'=>'[jobsearch] お問い合わせがありました',
        'provider_contact'=>'[jobsearch] お問い合わせがありました',
        'seeker_account_deleted'=>'[jobsearch] 退会しました'
    ],
    'contact'=>[
        'success'=>'この度は jobsearchへのお問い合わせ、ありがとうございます。2営業日以内にお返事いたしますので、ご了承ください',
        'contact_sent_subject'=>'contact sent',
        'contact_received'=>'contact received',
        'contact_replied'=>'[jobsearch] 問い合わせありがとうございます'
    ]
];
