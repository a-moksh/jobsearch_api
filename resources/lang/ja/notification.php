<?php
/**
 * Created by PhpStorm.
 * User: krishna
 * Date: 12/02/2018
 * Time: 11:29 AM
 */

return [
    'common'=>[
        'password_changed_en'=>'Your Password is changed Successfully',
        'password_changed_ja'=>'パスワード変更を完了しました',
        'user_created_en'=>'Welcome to the jobsearch',
        'user_created_ja'=>'ジョブバンクへようこそう',
        'profile_update_en'=>'Your profile information has been updated',
        'profile_update_ja'=>'Your profile information has been updated',
    ],

    'seeker'=>[
        //remainigng rabi
        'provider_replied_application_en'=>'you received an reply for your job application.',
        'provider_replied_application_ja'=>'返信をもらいました',

        'provider_sent_scout_en'=>'You received an Scout Message',
        'provider_sent_scout_ja'=>'メッセージを受け取りました',
        'seeker_replied'=>':user_email さんから 返信があります'

    ],

    'provider'=>[
        'job_created_en'=> 'New job :job_title created',
        'job_created_ja'=> ' :job_title 作成しました',
        'application_received_en'=>'User :user_email applied on job :job_title',
        'application_received_ja'=>':user_email さんから :job_title へ応募あります',
        'job_approved_en'=>':job_title has been approved.',
        'job_approved_ja'=>':job_title は承認されました',
        'job_declined_ja'=>':job_title は修正必要になりました。',
        'job_declined_en'=>':job_title has been declined.',
        'paid_account_active_en'=>'Your paid account is now active.',
        'paid_account_active_ja'=>'あなたの有料アカウントは現在有効です',
    ],

    'admin'=>[
        'user_created_admin_en'=>'New user :user_email (:user_type) registered to the system',
        'user_created_admin_ja'=>'新しい :user_email (:user_type) jobsearchに登録されました',
        'job_created_admin_en'=>'New job :job_title created on the system',
        'job_created_admin_ja'=>'新しい求人情報 :job_title 作成しました',
        'job_updated_admin_en'=>'Job :job_title updated on the system',
        'job_updated_admin_ja'=>'求人情報 :job_title アップデートされました',
        'job_deleted_admin_en'=>'Job :job_title deleted on the system',
        'job_deleted_admin_ja'=>'求人情報 :job_title を削除されました',

        //remainign
        'provider_deleted_en'=>'Company :company_name deleted from the system',
        'provider_deleted_ja'=>':company_name　様 はアカウント削除しました',

    ]
];
