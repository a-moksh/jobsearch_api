<?php

return [
    'password_link' => 'パスワードリセットリンク',
    'reset_password' => 'パスワードリセット',
    'link_is_here' => 'パスワードリセットリンクはこちらにあります.',
    'thankyou' => ''.env('APP_NAME').'使ってありがとうございます',
    'hello' => 'こんにちは, ',
    'regards' => '以上ですよろしくお願いいたします, ',
];
