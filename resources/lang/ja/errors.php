<?php
/**
 * Created by PhpStorm.
 * User: krishna
 * Date: 11/19/2018
 * Time: 11:29 AM
 */

return [
    '400'=> 'error 400',
    '403'=> 'error 403',
    '404'=> 'error 404 not found',
];