<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Store SeekerExperience Language translations
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during seeker experience creation process
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'company_name' => '会社名は必須です.',
    'job_title' => '職名は必須です.',
    'job_from' => '仕事始めた日付は必須です',
    'job_to' => '仕事完了日付は必須です.',
    'currently_working_here' => '現在こちらに働いているは必須です.',
    'annual_salary' => '年給料は必須です.',
    'job_description' => '仕事の説明は必須です.',
];
