<?php
if (!function_exists('array_normalize_recursive')) {
    function array_normalize_recursive($array)
    {
        array_walk_recursive($array, function (&$value) {
            if (is_string($value)) {
                $value = normalizer_normalize($value, \Normalizer::FORM_KC);
                $value = trim(preg_replace('/[ ]+/', ' ', $value));
            }
        });
        return $array;
    }
}

