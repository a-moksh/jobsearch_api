<?php
if (!function_exists('array_digit_to_integer_recursive')) {
    function array_digit_to_integer_recursive(array $array)
    {
        array_walk_recursive($items, function (&$value) {
            if ($value !== null) {
                if (is_object($value)) {
                    array_digit_to_integer_recursive($value);
                } else {
                    if (ctype_digit((string)$value)) {
                        $value = (int)$value;
                    }
                }
            }
        });
        return $items;
    }
}
