<?php

namespace App\Jobs;

use App\Models\Job;
use App\Models\User;
use App\Models\SeekerMailSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Notifications\ExceptionCaught;
use Illuminate\Support\Facades\DB;

class FindJobMatchingUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $job_item;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Job $job_item)
    {
        $this->job_item = $job_item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $direct_match_column = [
                'visa_support',
                'social_insurance',
                'housing_allowance',
                'hairstyle_freedom',
                'clothing_freedom'
            ];
            $job = Job::where('id', $this->job_item->id)->with([
                'locations',
                'languages',
                'provider'
            ])->first()->toArray();
            //print_r($job);
            SeekerMailSetting::where('receive_mail', 1)
            ->chunk(\Config::get('app.mail_setting_chunk_limit'), function($seeker_setting) use ($job, $direct_match_column) {
                
                $matched_seekers = array_filter($seeker_setting->toArray(), function ($seeker) use ($job, $direct_match_column) {
                    $match = false;
                    //CONTINUE IF $match IS TRUE FROM THIS POINT FORWARD ESLE RETURN FALSE:
                    //DIRECT MATCHING
                    foreach ($direct_match_column as $key => $val) {
                        //if($seeker[$val] != $job[$val] && $seeker[$val] != NULL){
                        if ($seeker[$val] === null || $seeker[$val] == 0) {
                            $match = true;
                        } else {
                            if ($seeker[$val] == 1) {
                                if ($seeker[$val] != $job[$val]) {
                                    $match = false;
                                    break;
                                } else {
                                    $match = true;
                                }
                            }
                        }
                    };
                    //}
                    if ($match == true && ($seeker['salary_type'] !== null && $seeker['salary_type'] != '')) {
                        if ($seeker['salary_type'] == $job['salary_type']) {
                            $match = true;
                        } else {
                            $match = false;
                        }
                    }
                    //SALARY MATCHING
                    if (($seeker['min_salary'] !== null || $seeker['max_salary'] !== null) && $match == true) {
                        if ($seeker['min_salary'] !== null && $job['min_salary'] >= $seeker['min_salary']) {
                            $match = true;
                        } else {
                            $match = false;
                        }
                        //if($seeker['max_salary'] !== NULL && $job['max_salary'] <= $seeker['min_salary']) $match = true;
                    }
    
                    //EMPLOYMENT TYPE MATCHING
                    if ($match == true && ($seeker['employment_type'] !== null && $seeker['employment_type'] != '')) {
                        $seek_emp_type = explode(',', $seeker['employment_type']);
                        $match = in_array($job['employment_type'], $seek_emp_type);
                    }
                    //CATEGORY MATCHING
                    if ($match == true && ($seeker['job_category'] !== null && $seeker['job_category'] != '')) {
                        $seek_cat = explode(',', $seeker['job_category']);
                        $match = in_array($job['category_id'], $seek_cat);
                    }
                    //JOB LOCATION
                    if ($seeker['all_prefecture'] === 0 || $seeker['all_prefecture'] === null) {
                        if ($match == true && ($seeker['job_location'] !== null && $seeker['job_location'] != '')) {
                            $seek_job_location = explode(',', $seeker['job_location']);
                            foreach ($job['locations'] as $j_location) {
                                if (in_array($j_location['prefecture_id'], $seek_job_location)) {
                                    $match = true;
                                    break;
                                } else {
                                    $match = false;
                                    continue;
                                }
                            }
                        }
                    }
    
                    //JOB LANGUAGES
                    if ($match == true) {
                        $japanese_match = false;
                        $english_match = false;
                        //english -> 0 , japanese -> 1
                        foreach ($job['languages'] as $j_lang) {
                            //FOR JAPANESE AND ENGLISH ONLY
                            if ($j_lang['language'] == 1) {
                                if ($seeker['japanese_level'] === null) {
                                    $japanese_match = true;
                                    continue;
                                } else {
                                    if ($j_lang['level'] == 100) {
                                        $japanese_match = true;
                                        continue;
                                    } else {
                                        if ($seeker['japanese_level'] >= $j_lang['level']) {
                                            $japanese_match = true;
                                            continue;
                                        } else {
                                            $japanese_match = false;
                                            continue;
                                        }
                                    }
                                }
                            }
                            if ($j_lang['language'] == 0) {
                                if ($seeker['english_level'] === null) {
                                    $english_match = true;
                                    continue;
                                } else {
                                    if ($j_lang['level'] == 100) {
                                        $english_match = true;
                                        continue;
                                    } else {
                                        if ($seeker['english_level'] >= $j_lang['level']) {
                                            $english_match = true;
                                            continue;
                                        } else {
                                            $english_match = false;
                                            continue;
                                        }
                                    }
                                }
                            }
                            $match = ($english_match == true && $japanese_match == true);
                        }
                    }
    
                    return $match;
    
                });
                //INSERT TO TABLE FOR DELIVERYING MAIL
                if (count($matched_seekers) > 0) {
                    $seeker_ids = [];
                    foreach ($matched_seekers as $match) {
                        $seeker_ids[] = $match['seeker_id'];
                    }
                    $insert_seeker_ids = implode(',', $seeker_ids);
                    $insert = DB::table('job_notify_users')->insert([
                        'job_id' => $job['id'],
                        'seeker_ids' => $insert_seeker_ids
                    ]);
                }
            });
        } catch (Exception $e) {
            $err_message = $e->getMessage();
            $admins = User::whereIn('email',\Config::get('app.admin_exception_mail_list'))->get();
            \Notification::send($admins, new ExceptionCaught($err_message, '/commands/matchedmailseekertoqueue'));
            report($e);
            return false;
        }
    }

    /**
     * The job failed to process.
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $err_message = $exception->getMessage();
        $admins = User::whereIn('email',\Config::get('app.admin_exception_mail_list'))->get();
        \Notification::send($admins, new ExceptionCaught($err_message, '/commands/matchedmailseekertoqueue'));
    }
}
