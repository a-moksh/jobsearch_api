<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class SeekerEducation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seeker_id',
        'university_institute',
        'degree',
        'faculty',
        'graduation_country',
        'graduation_status',
        'graduation_year'
    ];

    protected $searchable = [
        'graduation_status'
    ];

    public function seeker()
    {
        return $this->belongsTo(Seeker::class);
    }

    public function setGraduationYearAttribute($value)
    {
        if ($value) {
            $date = new \DateTime($value);
            $this->attributes['graduation_year'] = $date->format('Y-m-d');
        } else {
            $this->attributes['graduation_year'] = null;
        }
    }

    /**
     * @return array
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

}
