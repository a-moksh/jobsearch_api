<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class ProviderTransaction extends Model
{
    protected $table = 'provider_transactions';
    protected $fillable = [
        'provider_id',
        'points',
        'transaction_type',
        'charge_type',
        'foreign_table',
        'foreign_id',
        'paid_status',
        'trans_identifier_id',
        'amount'
    ];
}
