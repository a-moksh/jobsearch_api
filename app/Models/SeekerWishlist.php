<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class SeekerWishlist extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seeker_id',
        'job_id',
    ];

    public function seeker()
    {
        return $this->belongsTo(Seeker::class);
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    /*
    * @return isWishlisted
    */
    function isApplied($job_id)
    {
        if (auth()->user() && auth()->user()->user_type == 'seeker') {
            return (auth()->user()->seeker->hasApplied($job_id, auth()->user()->seeker->id));
        } else {
            return false;
        }

    }

}
