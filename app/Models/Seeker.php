<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class Seeker extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'seeker_identifier_id',
        'visa_status',
        'visa_issue_date',
        'visa_expiry_date',
        'japanese_language_level',
        'english_language_level',
        'japanese_writing_level',
        'english_writing_level',
        'desired_employment_type',
        'nearest_station',
        'willing_to_relocate',
        'is_experienced',
        'experience',
        'portfolio_url',
        'github_url',
        'qiita_id',
        'country',
        'zip_code',
        'prefecture_id',
        'address',
        'street_address',
        'self_introduction',
        'living_country',
        'state',
        'desired_job_category',
        'desired_annual_min_salary_range',
        'experience_year'
    ];
    /**
     *
     */
    protected $searchable = [
        'visa_issue_date',
        'visa_status',
        'country',
        'living_country',
        'seeker_identifier_id',
        'japanese_language_level',
        'japanese_writing_level',
        'english_language_level',
        'is_experienced',
        'experience',
        'desired_job_category',
//        'desired_annual_min_salary_range',
//        'experience_year'
    ];

    /**
     *
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     *
     */
    public function experiences()
    {
        return $this->hasMany(SeekerExperience::class);
    }

    /**
     *
     */
    public function certificates()
    {
        return $this->hasMany(SeekerCertificate::class);
    }

    /**
     *
     */
    public function educations()
    {
        return $this->hasMany(SeekerEducation::class);
    }

    /**
     *
     */
    public function skills()
    {
        return $this->hasMany(SeekerSkill::class);
    }

    /**
     *
     */
    public function mailSetting()
    {
        return $this->hasOne(SeekerMailSetting::class);
    }

    /**
     *
     */
    public function wishlists()
    {
        return $this->hasMany(SeekerWishlist::class);
    }

    /**
     *
     */
    public function applications()
    {
        return $this->hasMany(SeekerJob::class);
    }

    /**
     *
     */
    public function medias()
    {
        return $this->hasMany(Media::class);
    }

    /**
     *
     */
    public function annualIncome($seeker_id)
    {
        $annual_salary = SeekerExperience::where('seeker_id', $seeker_id)
            ->where('currently_working_here', 1)
            ->orderBy('currently_working_here', 'DESC')
            ->orderBy('job_to', 'DESC')
            ->first();
        if ($annual_salary) {
            return $annual_salary->annual_salary;
        } else {
            return '';
        }

    }

    /**
     *
     */
    public function hasWishlist($jobId, $seekerId)
    {
        return !is_null(
            SeekerWishlist::where('job_id', $jobId)->where('seeker_id', $seekerId)->first()
        );
    }

    /**
     *
     */
    public function setVisaIssueDateAttribute($value)
    {
        if ($value) {
            $date = new \DateTime($value);
            $this->attributes['visa_issue_date'] = $date->format('Y-m-d');
        } else {
            $this->attributes['visa_issue_date'] = null;
        }
    }

    /**
     *
     */
    public function setVisaExpiryDateAttribute($value)
    {
        if ($value) {
            $date = new \DateTime($value);
            $this->attributes['visa_expiry_date'] = $date->format('Y-m-d');
        } else {
            $this->attributes['visa_expiry_date'] = null;
        }
    }

    /**
     * @return array
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

    /**
     *
     */
    public function getProfilePercentage()
    {
        //65 will be minimum while verifying the email address 
        $percentage = 65;
        if ($this->experiences()->exists()) {
            $percentage += 20;
        }
        if ($this->educations()->exists()) {
            $percentage += 15;
        }
        return $percentage;
    }

    /**
     *
     */
    public function hasApplied($jobId, $seekerId)
    {
        return !is_null(
            SeekerJob::where('job_id', $jobId)->where('seeker_id', $seekerId)->first()
        );
    }
}
