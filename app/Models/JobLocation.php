<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class JobLocation extends Model
{
    protected $table = 'job_locations';

    protected $fillable = [
        'job_id',
        'zip_code',
        'address',
        'street_address',
        'prefecture_id',
    ];

    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
