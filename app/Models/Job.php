<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Str;

class Job extends Model
{

    protected $table = 'jobs';
    protected $fillableTranslation = [
        'description',
        'qualifications',
        'welfare',
        'title',
    ];
    protected $fillable = [
        'provider_id',
        'category_id',
        'title',
        'max_salary',
        'min_salary',
        'description',
        'qualifications',
        'welfare',
        'language_translation',
        'working_hour_from',
        'working_hour_to',
        'morning_shift',
        'night_shift',
        'visa_support',
        'social_insurance',
        'bonus',
        'clothing_freedom',
        'housing_allowance',
        'other_details',
        'remote',
        'company_service',
        'practical_experience',
        'total_annual_leave',
        'posting_start_date',
        'slug',
        'salary_type',
        'holiday',
        'holiday_days',
        'long_term',
        'short_term',
        'status',
        'admin_status',
        'employment_type',
        'views',
        'sub_category_id',
        'time_period',
        'job_identifier_id',
        'published_at'
    ];

    protected $searchable = [
        'max_salary',
        'min_salary',
        'job_identifier_id',
        'category_id',
        'employment_type',
        'visa_support',
        'remote',
        'pratical_experience',
        'salary_type'
    ];

    protected $dates = ['deleted_at', 'published_at'];

    /**
     * @return array
     */
    public function getFillableTranslations()
    {
        return $this->fillableTranslation;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locations()
    {
        return $this->hasMany(JobLocation::class);
    }

    public function languages()
    {
        return $this->hasMany(JobLanguage::class);
    }

    public function medias()
    {
        return $this->hasMany(Media::class, 'foreign_id');
    }

    public function applications()
    {
        return $this->hasMany(SeekerJob::class);
    }

    public function jobLogs()
    {
        return $this->hasMany(JobLogs::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(Media::class, 'foreign_id');
    }

    /**
     * @return jobimages
     */
    public function jobImages()
    {
        return $this->images()->where('foreign_table', 'jobs');
    }

    /**
     * Create a job title slug.
     *
     * @param  string $title
     * @return string
     */
    public function makeSlugFromTitle($job_title)
    {
        $job_title = auth()->user()->provider->company_name . ' ' . $job_title;

        $slug = Str::slug($job_title);

        $count = Job::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

        return $count ? "{$slug}-{$count}" : $slug;
    }

    /*
    * @return isWishlisted
    */
    function isWishListed($job_id)
    {
        if (auth()->user() && auth()->user()->user_type == 'seeker') {
            return (auth()->user()->seeker->hasWishlist($job_id, auth()->user()->seeker->id));
        } else {
            return false;
        }

    }
    /*
     * working time manage
     */
//    public function getWorkingHourFromAttribute($value)
//    {
//
//        if($value){
//            $time_from = (explode(':', $value));
//            return $time_from[0]. ':'. $time_from[1];
//        }
//
//    }

    /*
    * working time manage
    */
//    public function getWorkingHourToAttribute($value)
//    {
//        if($value){
//            $time_to = (explode(':', $value));
//            return $time_to[0]. ':'. $time_to[1];
//        }
//    }

    /**
     * @return array
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

    public function canViewDetail($published_at, $status)
    {
        return ($published_at > Carbon::now()->subDays(90)) && ($status == 1);
    }

}
