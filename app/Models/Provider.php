<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillableTranslation = [
        'company_name',
        'ceo_name',
        'contact_person',
    ];
    protected $table = 'providers';
    protected $fillable = [
        'user_id',
        'email',
        'company_name',
        'ceo_name',
        'contact_person',
        'established_year',
        'telephone',
        'fax',
        'capital',
        'no_of_employee',
        'business_content',
        'sub_business_content',
        'zip_code',
        'prefecture_id',
        'address',
        'street_address',
        'related_url',
        'provider_identifier_id',
        'paidId',
        'mansion_name',
        'paid_status',
        'purchased_on'
    ];

    protected $searchable = [
        'telephone',
        'email',
        'provider_identifier_id',
        'prefecture_id',
        'business_content'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    public function providerPaidjp()
    {
        return $this->hasOne(ProviderPaidjp::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function favourites()
    {
        return $this->hasMany(Favourite::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function scoutmails()
    {
        return $this->hasMany(ScoutMail::class);
    }

    public function applications()
    {
        return $this->hasManyThrough('App\Models\SeekerJob', 'App\Models\Job');
    }

    public function setEstablishedYearAttribute($value)
    {
        if ($value) {
            $date = new \DateTime($value);
            $this->attributes['established_year'] = $date->format('Y-m-d');
        } else {
            $this->attributes['established_year'] = null;
        }
    }

    /**
     * @return array
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

}
