<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class AttributeGroup extends Model
{
    //
    protected $fillable = [
        'id',
        'name',
        'description',
        'updated_at',
        'created_at'

    ];
    protected $table = "attribute_groups";

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attribute()
    {
        return $this->hasMany(Attribute::class);
    }
}
