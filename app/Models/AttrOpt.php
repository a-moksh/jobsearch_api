<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class AttrOpt extends Model
{
    protected $fillable = [
        'attr_id',
        'data_type',
        'label',
        'value',
        'child_id',
        'description',
        'display_order',
    ];

    protected $fillableTranslation = [
        'label',
    ];

    public function child()
    {
        return $this->belongsTo('App\Models\Attr', 'child_id', 'id');
    }


    public function getValueAttribute($value)
    {
        $dataType = $this->data_type;

        if ($dataType === 'string') {
            $value = strval($value);
        } else {
            if ($dataType === 'integer') {
                $value = intval($value);
            } else {
                if ($dataType === 'boolean') {
                    $value = boolValue($value);
                }
            }
        }

        return $value;
    }
}
