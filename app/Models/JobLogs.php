<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class JobLogs extends Model
{
    protected $fillable = [
        'comment',
        'job_detail',
        'job_id',
    ];
}
