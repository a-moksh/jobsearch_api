<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class ZipCode extends Model
{
    protected $guarded = ['id'];
}
