<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class ScoutMail extends Model
{
    protected $table = 'scout_mails';
    protected $fillable = [
        'provider_id',
        'seeker_id',
        'job_id',
        'message',
        'interview_method',
        'interview_place',
        'job_location_id',
        'resume',
        'cv',
        'writing_util',
        'other',
        'status',
        'date',
        'time',
        'type',
        'seeker_status',
        'provider_status',
        'provider_skype_id',
        'seeker_skype_id',
        'provider_read',
        'seeker_read',
        're_count',
        'seeker_reply_count',
        'provider_reply_count',
        'seeker_replied_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scoutDetails()
    {
        return $this->hasMany(ScoutDetail::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seeker()
    {
        return $this->belongsTo(Seeker::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function job()
    {
        return $this->belongsTo(Job::class);
    }

}
