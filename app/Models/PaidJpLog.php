<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class PaidJpLog extends Model
{
    protected $table = 'paid_jp_logs';

    protected $fillable = [
        'request',
        'response',
        'foreign_table',
        'foreign_id',
    ];
}
