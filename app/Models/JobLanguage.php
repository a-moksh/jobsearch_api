<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class JobLanguage extends Model
{
    protected $table = 'job_languages';

    protected $fillable = [
        'job_id',
        'language',
        'level'
    ];

    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
