<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class ConvertedMedias extends Model
{
    protected $table = 'converted_medias';
    protected $fillable = [
        'media_id',
        'media_type',
        'media_value',
        'media_dimension',
    ];


}
