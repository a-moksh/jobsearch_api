<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class SeekerExperience extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seeker_id',
        'company_name',
        'job_title',
        'job_from',
        'job_to',
        'currently_working_here',
        'annual_salary',
        'job_description'
    ];

    protected $searchable = [
        'annual_salary'
    ];

    public function seeker()
    {
        return $this->belongsTo(Seeker::class);
    }

    public function seekerexperience()
    {
        return $this->belongsTo(SeekerExperience::class);
    }

    public function setJobToAttribute($value)
    {
        if (!$value) {
            $this->attributes['job_to'] = null;
        } else {
            $date = new \DateTime($value);
            $this->attributes['job_to'] = $date->format('Y-m-d');
        }
    }

    public function setJobFromAttribute($value)
    {
        if ($value) {
            $date = new \DateTime($value);
            $this->attributes['job_from'] = $date->format('Y-m-d');
        } else {
            $this->attributes['job_from'] = null;
        }
    }

    /**
     * @return array
     */
    public function getSearchable()
    {
        return $this->searchable;
    }
}
