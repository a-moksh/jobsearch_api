<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Sequence extends Model
{
    protected $dates = [];

    protected $fillable = [
        'table_name',
        'last_number',
        'group_value',
        'group_name'
    ];

    /**
     * Generate and return a new service number for provider
     *
     * @return string
     */
    public static function getProviderIdentifier()
    {
        $table = 'providers';
        $name = 'provider_identifier_id';
        $value = 'TESTJBP-' . Carbon::now()->format('dym');
        $number = self::generate($table, $name, null, $value);
        $number = str_pad($number, 4, 0, STR_PAD_LEFT);
        return "{$value}{$number}";
    }

    /**
     * Generate and return a new service number for seeker
     *
     * @return string
     */
    public static function getSeekerIdentifier()
    {
        $table = 'seekers';
        $name = 'seeker_identifier_id';
        $value = 'JBS-' . Carbon::now()->format('dym');
        $number = self::generate($table, $name, null, $value);
        $number = str_pad($number, 4, 0, STR_PAD_LEFT);
        return "{$value}{$number}";
    }

    /**
     * Generate and return a new service number for seeker
     *
     * @return string
     */
    public static function getUserIdentifier()
    {
        $table = 'users';
        $name = 'user_identifier_id';
        $value = 'JBU-' . Carbon::now()->format('dym');
        $number = self::generate($table, $name, null, $value);
        $number = str_pad($number, 4, 0, STR_PAD_LEFT);
        return "{$value}{$number}";
    }

    /**
     * Generate and return a new service number for seeker
     *
     * @return string
     */
    public static function getJobIdentifier()
    {
        $table = 'jobs';
        $name = 'job_identifier_id';
        $value = 'JBJ-' . Carbon::now()->format('dym');
        $number = self::generate($table, $name, null, $value);
        $number = str_pad($number, 4, 0, STR_PAD_LEFT);
        return "{$value}{$number}";
    }

    /**
     * Generate and return a new service number for transaction
     *
     * @return string
     */
    public static function getTransactionIdentifier()
    {
        $table = 'provider_transactions';
        $name = 'trans_identifier_id';
        $value = 'TESTJBT-' . Carbon::now()->format('dym');
        $number = self::generate($table, $name, null, $value);
        $number = str_pad($number, 4, 0, STR_PAD_LEFT);
        return "{$value}{$number}";
    }

    /**
     * @param $table
     * @param null $column
     * @param null $groupColumn
     * @param null $groupValue
     * @param int $addNumber
     * @return mixed
     */
    public static function generate($table, $column = null, $groupColumn = null, $groupValue = null, $addNumber = 1)
    {
        $column = is_null($column) ? '' : $column;
        $groupColumn = is_null($groupColumn) ? '' : $groupColumn;
        $groupValue = is_null($groupValue) ? '' : $groupValue;
        $model = ucfirst(camel_case(str_singular($table)));
        $model = '\App\Models\\' . $model;

        if (!class_exists($model)) {
            abort(500, "Model '{$model}' not found");
        }

        $model = (new $model);

        if (!empty($column)) {
            $columns = $model->getColumns();
            if (!in_array($column, $columns)) {
                abort(500, sprintf(
                    "The specified column does not exist. [%s].[%s]", $model, $column
                ));
            }
        }
        if (!empty($groupColumn)) {
            $columns = $model->getColumns();
            if (!in_array($groupColumn, $columns)) {
                abort(500, sprintf(
                    "The specified column does not exist. [%s].[%s]", $model, $groupColumn
                ));
            }
        }

        $sql = "INSERT INTO sequences (table_name, column_name, group_column_name, group_value, last_number)
                VALUES(:table_name, :column_name, :group_column_name, :group_value, LAST_INSERT_ID({$addNumber}))
                ON DUPLICATE KEY UPDATE last_number = LAST_INSERT_ID(last_number + {$addNumber})";
        DB::insert($sql, [
            'table_name' => $table,
            'column_name' => $column,
            'group_column_name' => $groupColumn,
            'group_value' => $groupValue
        ]);
        return DB::getPdo()->lastInsertId();
    }

    public static function getLastNumber($table, $column = null, $groupColumn = null, $groupValue = null)
    {
        $column = is_null($column) ? '' : $column;
        $groupColumn = is_null($groupColumn) ? '' : $groupColumn;
        $groupValue = is_null($groupValue) ? '' : $groupValue;
        $number = 0;
        $sequence = static::withTrashed()->where('table_name', $table)->where('column_name',
            $column)->where('group_column_name', $groupColumn)->where('group_value', $groupValue)->first();
        if ($sequence) {
            $number = $sequence->last_number;
        }
        return $number;
    }
}
