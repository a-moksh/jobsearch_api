<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class ProviderPaidjp extends Model
{
    protected $table = 'provider_paidjp';

    protected $fillable = [
        'provider_id',
        'provider_info'
    ];
}
