<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'medias';
    protected $fillable = [
        'foreign_table',
        'foreign_id',
        'media_type',
        'media_value',
    ];

    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    public function seeker()
    {
        return $this->belongsTo(Seeker::class);
    }

    public function converted()
    {
        return $this->hasMany(ConvertedMedias::class);
    }

    public function withConverted($foreign_id, $foreign_table)
    {
        $images = '';
        $s3_driver = ($foreign_table == 'users') ? 's3_private' : 's3_public';
        $raw_media = $this->where('foreign_id', $foreign_id)->where('foreign_table', $foreign_table)->get([
            'id',
            'media_value'
        ]);

        if (count($raw_media) > 0) {
            foreach ($raw_media as $raw) {
                $converted = $raw->converted()->get(['media_value', 'media_dimension']);
                foreach ($converted as $convert) {
                    if ($s3_driver == 's3_private') {
                        $convert['url'] = \Storage::disk($s3_driver)->temporaryUrl(
                            $convert->media_value, now()->addMinutes(10)
                        );
                    }
                    if ($s3_driver == 's3_public') {
                        $disk = \Storage::disk($s3_driver);
                        $adapter = $disk->getAdapter();
                        $client = $adapter->getClient();
                        $convert['url'] = $client->getObjectUrl($adapter->getBucket(), $convert->media_value);
                    }
                }
                $raw['converted'] = $converted;
                if ($s3_driver == 's3_private') {
                    $raw['url'] = \Storage::disk($s3_driver)->temporaryUrl(
                        $raw->media_value, now()->addMinutes(10)
                    );
                }
                if ($s3_driver == 's3_public') {
                    $disk = \Storage::disk($s3_driver);
                    $adapter = $disk->getAdapter();
                    $client = $adapter->getClient();
                    $raw['url'] = $client->getObjectUrl($adapter->getBucket(), $raw->media_value);
                }
            }
            $images = $raw_media;
            return $images;
        }
        return $images;
    }

}
