<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;
use Illuminate\Support\Str;

class VerifyUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'token',
    ];

    protected $guarded = [];

    /**
     *
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($verify_user) {
            $verify_user->token = (string)bcrypt(Str::uuid());
        });
    }

    /**
     *
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
