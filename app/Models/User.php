<?php

namespace App\Models;

use App\Models\Traits\CommonTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, CommonTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_identifier_id',
        'first_name',
        'last_name',
        'full_name',
        'katakana_first_name',
        'katakana_last_name',
        'katakana_full_name',
        'dob',
        'gender',
        'email',
        'telephone',
        'mobile',
        'user_type',
        'password',
        'deletable',
        'last_login',
        'receive_mail_language'
    ];

    protected $searchable = [
        'email',
        'telephone',
        'mobile',
        'dob',
        'full_name',
        'katakana_full_name',
        'gender',
        'verified',
        'user_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function verifyUser()
    {
        return $this->hasOne('App\Models\VerifyUser');
    }

    public function provider()
    {
        return $this->hasOne('App\Models\Provider');
    }

    public function seeker()
    {
        return $this->hasOne('App\Models\Seeker');
    }

    public function experiences()
    {
        return $this->hasManyThrough(
            'App\Models\SeekerExperience',
            'App\Models\Seeker',
            'user_id',
            'seeker_id',
            'id',
            'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasOne(Media::class, 'foreign_id');
    }

    /**
     * @return userimages
     */
    public function userImage()
    {
        return $this->images()->where('foreign_table', 'users');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     *
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\ResetPassword($token, app()->getLocale()));
    }

    /**
     *
     */
    public function setDobAttribute($value)
    {
        $date = new \DateTime($value);
        $this->attributes['dob'] = $date->format('Y-m-d');
    }

    /**
     * @return array
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

}
