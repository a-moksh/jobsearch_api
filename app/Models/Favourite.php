<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class Favourite extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_id',
        'seeker_id',
    ];


    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function seeker()
    {
        return $this->belongsTo(Seeker::class);
    }
}
