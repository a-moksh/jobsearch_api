<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class SeekerSkill extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'skill_id',
        'level',
        'seeker_id',
        'skill_type'
    ];

    public function seeker()
    {
        return $this->belongsTo(Seeker::class);
    }
}
