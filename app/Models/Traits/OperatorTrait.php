<?php

namespace App\Models\Traits;

use App\Models\User;

trait OperatorTrait
{
    protected static $createdByColumn = 'created_by';
    protected static $updatedByColumn = 'updated_by';
    protected static $deletedByColumn = 'deleted_by';
    protected static $uniqueInColumn = 'unique_in';

    /**
     * The "booting" method of the model.
     *
     * @see \Illuminate\Database\Eloquent\Model::boot()
     * @return void
     */
    protected static function bootOperatorTrait()
    {
        static::creating(function ($model) {
            if ($model->usesTimestamps()) {
                if (in_array($model->getCreatedByColumn(), $model->getColumns())) {
                    if (!$model->exists && !is_null($model->getCreatedByColumn()) && !$model->isDirty($model->getCreatedByColumn())) {
                        $model->{$model->getCreatedByColumn()} = static::getUserId();
                    }
                }

                if (in_array($model->getUpdatedByColumn(), $model->getColumns())) {
                    if (!is_null($model->getUpdatedByColumn()) && !$model->isDirty($model->getUpdatedByColumn())) {
                        $model->{$model->getUpdatedByColumn()} = static::getUserId();
                    }
                }
            }
        });

        static::updating(function ($model) {
            if ($model->usesTimestamps()) {
                if (in_array($model->getUpdatedByColumn(), $model->getColumns())) {
                    if (!is_null($model->getUpdatedByColumn()) && !$model->isDirty($model->getUpdatedByColumn())) {
                        $model->{$model->getUpdatedByColumn()} = static::getUserId();
                    }
                }
            }
        });

        static::deleting(function ($model) {
            if ($model->usingSoftDeletes()) {
                if ($model->usesTimestamps()) {
                    if (in_array($model->getDeletedByColumn(), $model->getColumns())) {
                        if (!is_null($model->getDeletedByColumn()) && !$model->isDirty($model->getDeletedByColumn())) {
                            $model->{$model->getDeletedByColumn()} = static::getUserId();
                        }
                    }
                    if (in_array($model->getDeletedAtColumn(), $model->getColumns())) {
                        if (!is_null($model->getDeletedAtColumn()) && !$model->isDirty($model->getDeletedAtColumn())) {
                            $model->{$model->getDeletedAtColumn()} = $model->freshTimestamp();
                        }
                    }
                }

                if (in_array($model->getUniqueInColumn(), $model->getColumns())) {
                    $model->{$model->getUniqueInColumn()} = null;
                }
                $model->save();
                return false;
            }
        });

        static::restoring(function ($model) {
            if (!$model->usesTimestamps()) {
                return false;
            }

            if ($model->usingSoftDeletes()) {
                if (in_array($model->getDeletedByColumn(), $model->getColumns())) {
                    $model->{$model->getDeletedByColumn()} = null;
                }
                if (in_array($model->getUniqueInColumn(), $model->getColumns())) {
                    $model->{$model->getUniqueInColumn()} = 1;
                }
            }
        });
    }

    /**
     * Get the name of the "logical unique" column.
     *
     * @return string
     */
    public function getUniqueInColumn()
    {
        return defined('static::UNIQUE_IN') ? static::UNIQUE_IN : static::$uniqueInColumn;
    }

    /**
     * Get the name of the "deleted by" column.
     *
     * @return string
     */
    public function getCreatedByColumn()
    {
        return defined('static::CREATED_BY') ? static::CREATED_BY : static::$createdByColumn;
    }

    /**
     * Get the name of the "deleted by" column.
     *
     * @return string
     */
    public function getUpdatedByColumn()
    {
        return defined('static::UPDATED_BY') ? static::UPDATED_BY : static::$updatedByColumn;
    }

    /**
     * Get the name of the "deleted by" column.
     *
     * @return string
     */
    public function getDeletedByColumn()
    {
        return defined('static::DELETED_BY') ? static::DELETED_BY : static::$deletedByColumn;
    }

    /**
     * Has the model loaded the SoftDeletes trait.
     *
     * @return bool
     */
    protected function usingSoftDeletes()
    {
        static $usingSoftDeletes;
        if (!isset($usingSoftDeletes)) {
            $usingSoftDeletes = in_array('Illuminate\Database\Eloquent\SoftDeletes',
                class_uses_recursive(get_called_class()));
        }
        return $usingSoftDeletes;
    }

    /**
     * Returns user id under login
     *
     * @return int|null
     */
    protected static function getUserId()
    {
        return auth()->check() ? auth()->user()->id : null;
    }

    /**
     * Created user relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, $this->getCreatedByColumn());
    }

    /**
     * Updated user relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, $this->getUpdatedByColumn())->withDefault();
    }

    /**
     * Deleted user relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deletedBy()
    {
        return $this->belongsTo(User::class, $this->getDeletedByColumn());
    }
}
