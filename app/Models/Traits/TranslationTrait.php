<?php

namespace App\Models\Traits;

use App\Services\TranslationService;

trait TranslationTrait
{
    /**
     * The "booting" method of the model.
     *
     * @see \Illuminate\Database\Eloquent\Model::boot()
     * @return void
     */
    protected static function bootTranslationTrait()
    {
        static::retrieved(function ($model) {
            TranslationService::addRetrieved($model);
        });

        static::deleting(function ($model) {
            TranslationService::delete($model);
        });
    }

    /**
     * Get the fillableTranslation attributes for the model.
     *
     * @return array
     */
    public function getFillableTranslation()
    {
        return $this->fillableTranslation;
    }

    /**
     * Translate to all the defined languages​ by defined_locales.
     *
     * @return mixed
     */
    public function translateLocales()
    {
        return TranslationService::translate($this, config('app.defined_locales'));
    }

    /**
     * Create a model and register a translation.
     *
     * @param string|array $locales
     * @return \Illuminate\Database\Eloquent\Model
     */
    public static function createTranslation($values, $locales = null)
    {
        return TranslationService::create(get_called_class(), $values, $locales);
    }

    /**
     * Create a model and register all language translations defined in defined_locales.
     *
     * @param array $values
     * @return \Illuminate\Database\Eloquent\Model
     */
    public static function createTranslationLocales($values)
    {
        return static::createTranslation($values, config('app.defined_locales'));
    }

    /**
     * Save a model and register a translation.
     *
     * @param string|array $locales
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function saveTranslation($locales = null)
    {
        return TranslationService::save($this, $locales);
    }

    /**
     * Save translate to all the defined languages​ by defined_locales.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function saveTranslationLocales()
    {
        return static::saveTranslation(config('app.defined_locales'));
    }
}
