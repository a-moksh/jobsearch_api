<?php

namespace App\Models\Traits;

use App\Models\Sequence;
use Illuminate\Support\Facades\DB;

trait DisplayOrderTrait
{
    /**
     * Generate and return the display order
     *
     * @param string $groupName
     * @param int|string $groupValue
     * @param int $addNumber
     * @return int
     */
    public static function generateDisplayOrder($groupName = null, $groupValue = null, $addNumber = 1)
    {
        $class = get_called_class();
        $model = (new $class);
        $table = $model->getTable();
        $name = $model->getDisplayOrderColumn();
        return Sequence::generate($table, $name, $groupName, $groupValue, $addNumber);
    }

    /**
     * Get the last display order number.
     *
     * @param string $groupName
     * @return int
     */
    public function getLastDisplayOrder($groupName = null, $groupValue = null)
    {
        $displayOrderColumn = $this->getDisplayOrderColumn();
        $groupValue = null;
        if ($groupName !== null) {
            $groupValue = $this->{$groupName};
        }
        return Sequence::getLastNumber($this->getTable(), $displayOrderColumn, $groupName, $groupValue);
    }

    /**
     * Get the name of the "display order" column.
     *
     * @return string
     */
    public function getDisplayOrderColumn()
    {
        return defined('static::DISPLAY_ORDER') ? static::DISPLAY_ORDER : 'display_order';
    }

    /**
     * Update display order for Builder
     *
     * @param string $groupName
     * @param int|string $groupValue
     * @return $this
     */
    public function updateDisplayOrder($groupName = null)
    {
        static::registerModelEvent('updating', function ($model) use ($groupName) {
            $dispatcher = $model->getEventDispatcher();
            $dispatcher->forget("eloquent.updating: " . static::class);

            $displayOrderColumn = $this->getDisplayOrderColumn();
            $columns = $model->getFillable();

            if (in_array($displayOrderColumn, $columns)) {
                if ($groupName !== null && !in_array($groupName, $columns)) {
                    abort(500, 'Invalid group name');
                }

                $groupValue = $model->{$groupName};
                $last = Sequence::getLastNumber($model->getTable(), $displayOrderColumn, $groupName, $groupValue);
                $old = $model->getOriginal($displayOrderColumn);
                $new = $model->{$displayOrderColumn};

                if (!ctype_digit((string)$old)) {
                    abort(500, 'Invalid old number');
                }
                if (!ctype_digit((string)$new)) {
                    abort(500, 'Invalid new number');
                }
                if (!ctype_digit((string)$last)) {
                    abort(500, 'Invalid last number');
                }

                $old = (int)$old;
                $new = (int)$new;
                $last = (int)$last;

                if ($old < 1 || $last < $old) {
                    abort(500, 'Invalid old number');
                }
                if ($new < 1 || $last < $new) {
                    abort(500, 'Invalid new number');
                }

                if ($new !== $old) {
                    if ($new > $old) {
                        $query = DB::table($model->getTable());
                        if ($groupName !== null && $groupValue !== null) {
                            $query->where($groupName, $groupValue);
                        }
                        $query->where($displayOrderColumn, '>=', $old)->where($displayOrderColumn, '<=',
                            $new)->lockForUpdate()->get();
                        $query->decrement($displayOrderColumn, 1);
                    } else {
                        $query = DB::table($model->getTable());
                        if ($groupName !== null && $groupValue !== null) {
                            $query->where($groupName, $groupValue);
                        }
                        $query->where($displayOrderColumn, '<=', $old)->where($displayOrderColumn, '>=',
                            $new)->lockForUpdate()->get();
                        $query->increment($displayOrderColumn, 1);
                    }

                    $query = DB::table($model->getTable());
                    $query->where($model->getKeyName(), $model->getKey())->update([$displayOrderColumn => $new]);

                    $model->syncOriginalAttribute($displayOrderColumn);

                    if (empty($model->getDirty())) {
                        return false;
                    }
                }
            }
        });

        return $this;
    }
}
