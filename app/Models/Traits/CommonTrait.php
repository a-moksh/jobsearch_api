<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\SoftDeletes;

trait CommonTrait
{
    use SoftDeletes, TranslationTrait, OperatorTrait, DisplayOrderTrait;

    /**
     * Get columns
     *
     * @return array
     */
    public function getColumns()
    {
        static $columns;
        if (!isset($columns)) {
            $columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
        }
        return $columns;
    }
}
