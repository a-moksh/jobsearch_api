<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class Language extends Model
{
    protected $fillable = [
        'table_name',
        'foreign_id',
        'column_name',
        'locale_code',
        'locale_text',
    ];
}
