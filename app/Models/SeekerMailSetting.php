<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class SeekerMailSetting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seeker_id',
        'employment_status',
        'employment_type',
        'industry',
        'japanese_level',
        'english_level',
        'min_salary',
        'max_salary',
        'job_location',
        'visa_support',
        'social_insurance',
        'housing_allowance',
        'receive_mail',
        'hairstyle_freedom',
        'clothing_freedom',
        'job_category',
        'receive_mail_language',
        'salary_type',
        'all_prefecture'
    ];

    protected $searchable = [
        'japanese_level',
        'english_level',
    ];

    /**
     * @return array
     */
    public function getSearchable()
    {
        return $this->searchable;
    }

    public function seeker()
    {
        return $this->belongsTo(Seeker::class);
    }
}
