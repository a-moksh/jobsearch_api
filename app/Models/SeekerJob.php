<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class SeekerJob extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seeker_id',
        'job_id',
        'user_data',
        'status',
        'video_converted_details'
    ];

    public function seeker()
    {
        return $this->belongsTo(Seeker::class);
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function video()
    {
        return $this->hasOne(Media::class, 'foreign_id');
    }

    /**
     *
     */
    public function streamVideoUrl($user_data)
    {
        $decode = json_decode($user_data);
        //send media id and return video after validating if the user has rights
        $url = '';
        if (explode('.', $decode->video)[1] == 'm3u8') {
            $url = Storage::disk('s3_private')->temporaryUrl(
                $decode->video, now()->addMinutes(10)
            );
        }
        return $url;
    }

    public function videoThumbmail($user_data, $seeker_id)
    {
        $decode = json_decode($user_data);
        //send media id and return video after validating if the user has rights
        $url = '';
        $file_name_extension = explode('.', $decode->video);
        //print_r($file_name_extension);die;
        $file_name_last = explode('/', $file_name_extension[0]);
        $file_name = $file_name_last[count($file_name_last) - 1] . '_hls.0000000.jpg';

        $url = 'video/job/application/' . $seeker_id . '/' . $file_name;

        if (Storage::disk('s3_private')->exists($url)) {
            $url = Storage::disk('s3_private')->temporaryUrl(
                $url, now()->addMinutes(10)
            );
        } else {
            $url = '';
        }
        //echo $full_path;die;
        return $url;
    }

    /**
     *
     */
    public function isOpened($seeker_job_id)
    {
        $isOpen = ProviderTransaction::where('foreign_id', $seeker_job_id)->where('foreign_table',
            'seeker_jobs')->first();
        return ($isOpen) ? true : false;
    }
}
