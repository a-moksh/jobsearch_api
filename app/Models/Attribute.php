<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $fillable = [
        'id',
        'group_id',
        'value',
        'name',
        'parent_group_id',
        'parent_value',
        'display_order',
        'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('App\Models\AttributeGroup');
    }

    /**
     * @param $value
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getTranslatedNameAttribute($value)
    {
        $value = $this->name;
//        var_dump($this->group->name);exit;
        $rootKey = "attribute.{$this->group->name}";
        return trans("{$rootKey}.{$value}");
    }
}
