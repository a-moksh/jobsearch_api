<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;


class Attr extends Model
{
    protected $fillable = [
        'name',
        'label',
        'data_type',
        'default_value',
        'description',
        'display_order',
        'is_private',
    ];

    protected $fillableTranslation = [
        'label',
    ];

    public function opts()
    {
        return $this->hasMany('App\Models\AttrOpt', 'attr_id', 'id');
    }
}
