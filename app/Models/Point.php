<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class Point extends Model
{
    protected $fillable = [
        'status',
        'point',
        'amount',
        'description'
    ];
}
