<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class SeekerCertificate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seeker_id',
        'certificate_name',
        'year'
    ];

    public function seeker()
    {
        return $this->belongsTo(Seeker::class);
    }

    public function setYearAttribute($value)
    {
        $date = new \DateTime($value);
        $this->attributes['year'] = $date->format('Y-m-d');
    }
}
