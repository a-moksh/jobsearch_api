<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ContactUs extends Model
{
    use Notifiable;
    protected $table = 'contact_us';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'email',
        'mobile',
        'receive_mail_language',
        'message',
        'terms',
        'admin_reply',
        'company_name',
        'type',
        'admin_replied'
    ];
}
