<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class ScoutDetail extends Model
{
    protected $table = 'scout_details';
    protected $fillable = [
        'time',
        'date',
        'scout_mail_id'
    ];

    public function scoutMessage()
    {
        return $this->belongsTo(ScoutMail::class);
    }

    public function setDateAttribute($value)
    {
        if ($value) {
            $date = new \DateTime($value);
            $this->attributes['date'] = $date->format('Y-m-d');
        } else {
            $this->attributes['date'] = null;
        }
    }
}
