<?php

namespace App\Models;

use App\Extensions\Database\Eloquent\Model;

class JobEmploymentType extends Model
{
    protected $table = 'job_employment_types';

    protected $fillable = [
        'job_id',
        'employment_type'
    ];

    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
