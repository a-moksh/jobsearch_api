<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;


class Attribute extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'attr';
    }
}

