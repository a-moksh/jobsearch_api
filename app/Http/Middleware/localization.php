<?php

namespace App\Http\Middleware;

use Closure;

class localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*$locale = locale_accept_from_http(request()->server('HTTP_ACCEPT_LANGUAGE'));
        if (!empty($locale)) {
            $locale = preg_replace('/_.*$/', '', $locale);
            app()->setLocale($locale);
        }*/


        // Check header request and determine localizaton
        $local = ($request->hasHeader('Accept-Language')) ? $request->header('Accept-Language') : 'en';
        // set laravel localization
        app()->setLocale($local);

        // continue request
        return $next($request);
    }
}
