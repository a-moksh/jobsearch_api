<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginWithToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($request->header()['authorization'])) {
            try {
                JWTAuth::parseToken()->authenticate();
            } catch (\Exception $e) {
            }
        }
        return $next($request);
    }
}
