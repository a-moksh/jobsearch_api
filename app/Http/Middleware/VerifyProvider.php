<?php

namespace App\Http\Middleware;

use Closure;

class VerifyProvider
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user() && auth()->user()->user_type == 'provider') {
            return $next($request);
        }
        abort(403, trans('errors.403'));
    }
}
