<?php

namespace App\Http\Middleware;

use Closure;

class Request
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $request->headers->set('Accept', 'application/json');
////        $request->headers->set('X-Requested-With', 'XMLHttpRequest');
//
//        // Add query string to Paginator
//        Paginator::currentPathResolver(function () use($request) {
//            $url = $request->url();
//            $params = array_filter($request->query(), 'strlen');
//            unset($params['page']);
//            $url .= (!empty($params)) ? '?' . http_build_query($params) : '';
//            return $url;
//        });
        return $next($request);
    }
}
