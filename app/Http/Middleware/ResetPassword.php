<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ResetPassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->filled('token')) {
            $token = Hash::make($request->input('token'));
            $email_db = DB::table('password_resets')->where('token', $token)->first();
            return response(['message' => $token], 402);
            print_r($email_db);
            die;
        } else {
            abort(401, trans('auth.reset_fail_message'));
        }
        return $next($request);

    }
}
