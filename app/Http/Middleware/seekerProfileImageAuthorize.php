<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class seekerProfileImageAuthorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = $request->route()->parameters()['key'];
        $image = explode('/', $key);
        if ($image[0] == 'jobs') {
            return $next($request);
        }
        if (auth()->user() && auth()->user()->user_type == 'seeker') {
            //check if key provided is of that user
            $media_obj = DB::table('medias')->where('foreign_table', 'users')->where('foreign_id',
                auth()->user()->id)->where('media_value', $key)->first();
            if ($media_obj && $media_obj->media_value == $key) {
                return $next($request);
            }
            abort(403, trans('errors.403'));
        }
        abort(403, trans('errors.403'));
    }
}
