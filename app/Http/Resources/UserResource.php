<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;
use App\Models\Media;

/**
 * Class UserResource
 *
 * @OA\Schema(
 *    schema="UserResource",
 *    @OA\Property(
 *        property="id",
 *        description="id of the user",
 *        type="integer"
 *    ),@OA\Property(
 *        property="verified",
 *        description="user verified or not",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="full name",
 *        description="full name of the user",
 *        type="string"
 *    ),@OA\Property(
 *        property="katakana full name",
 *        description="katakana full name of the user",
 *        type="string"
 *    ),@OA\Property(
 *        property="telephone",
 *        description="telephone of the user",
 *        type="integer"
 *    ),@OA\Property(
 *        property="mobile",
 *        description="mobile of the user",
 *        type="integer"
 *    ),@OA\Property(
 *        property="dob",
 *        description="dob of the user",
 *        type="string",
 *        format="date"
 *    ),@OA\Property(
 *        property="gender",
 *        description="gender of the user",
 *        type="integer"
 *    ),@OA\Property(
 *        property="email",
 *        description="email of the user",
 *        type="string"
 *    ),@OA\Property(
 *        property="user_type",
 *        description="user type of the user",
 *        type="string"
 *    ),@OA\Property(
 *        property="user_type_id ",
 *        description="user type id  of the user",
 *        type="integer"
 *    ),@OA\Property(
 *        property="company_name",
 *        description="company name of the user if user type is provider",
 *        type="string"
 *    ),@OA\Property(
 *        property="image",
 *        description="image of the user",
 *        type="string"
 *    ),@OA\Property(
 *        property="deletable",
 *        description="deletable of the user",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="receive_mail_language",
 *        description="receive_mail_language of the user",
 *        type="string"
 *    )
 * )
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'verified' => $this->verified,
            'full_name' => $this->full_name,
            'katakana_full_name' => $this->katakana_full_name,
            'telephone' => $this->telephone,
            'mobile' => $this->mobile,
            'dob' => $this->dob,
            'gender' => $this->gender,
            'email' => $this->email,
            'user_type' => $this->user_type,
            'user_type_id' => ($this->user_type != 'admin') ? (($this->user_type == 'provider') ? $this->provider->id : $this->seeker->id) : '001',
            'company_name' => isset($this->provider->id) ? $this->provider->company_name : '',
            'image' => (new Media())->withConverted($this->id, 'users'),
            'deletable' => $this->deletable,
            'receive_mail_language' => $this->receive_mail_language
        ];
    }
}
