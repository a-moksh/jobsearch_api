<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class SeekerExperienceResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="SeekerExperienceResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="exp id of database",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="seeker_id",
 *        description="seeker id",
 *        type="integer",
 *        example=2
 *      ),@OA\Property(
 *        property="company_name",
 *        description="company name",
 *        type="string"
 *      ),@OA\Property(
 *        property="job_title",
 *        description="title of the job",
 *        type="string"
 *      ),@OA\Property(
 *        property="job_from",
 *        description="job started date",
 *        type="string"
 *      ),@OA\Property(
 *        property="job_to",
 *        description="end date of job",
 *        type="string"
 *      ),@OA\Property(
 *        property="currently_working_here",
 *        description="working or not",
 *        type="boolean"
 *      ),@OA\Property(
 *        property="annual_salary",
 *        description="annual salary of the seeker for this job",
 *        type="integer"
 *      ),@OA\Property(
 *        property="job_description",
 *        description="description of the job",
 *        type="text"
 *      )
 *   )
 * )
 */
class SeekerExperienceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
