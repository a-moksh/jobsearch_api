<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class SeekerSkillResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="SeekerSkillResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="skill id of database",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="skil_id",
 *        description="atribute skill id",
 *        type="integer",
 *        example=2
 *      ),@OA\Property(
 *        property="level",
 *        description="level attribte id",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="skil_type",
 *        description="skill type attribute value",
 *        type="string",
 *        example="skill.framework"
 *      )
 *   )
 * )
 */
class SeekerSkillResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
