<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;
use App\Models\Media;

/**
 * Class SeekerWishlistResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="SeekerWishlistResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="job id of database",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="category_id",
 *        description="category id of job",
 *        type="integer",
 *        example=2
 *      ),@OA\Property(
 *        property="sub_category_id",
 *        description="sub category id",
 *        type="string"
 *      ),@OA\Property(
 *        property="job_title",
 *        description="job title",
 *        type="string"
 *      ),@OA\Property(
 *        property="faculty",
 *        description="faculty of education",
 *        type="string"
 *      ),@OA\Property(
 *        property="description",
 *        description="desc of job",
 *        type="string"
 *      ),@OA\Property(
 *        property="characteristics",
 *        description="charactersticts of job",
 *        type="object",
 *          @OA\Property(
 *              property="visa_support",
 *              description="visa support of job",
 *              type="boolean",
 *          ),@OA\Property(
 *              property="over_time",
 *              description="over_time of job",
 *              type="boolean",
 *          ),@OA\Property(
 *              property="social_insurance",
 *              description="social_insurance of job",
 *              type="boolean",
 *          ),@OA\Property(
 *              property="bonus",
 *              description="bonus of job",
 *              type="boolean",
 *          ),@OA\Property(
 *              property="remote",
 *              description="remote of job",
 *              type="boolean",
 *          ),@OA\Property(
 *              property="practical_experience",
 *              description="practical experience of job",
 *              type="boolean",
 *          ),@OA\Property(
 *              property="company_service",
 *              description="company servide of job",
 *              type="boolean",
 *          ),@OA\Property(
 *              property="clothing_freedon",
 *              description="clothing freedom of job",
 *              type="boolean",
 *          ),@OA\Property(
 *              property="short_term",
 *              description="short term of job",
 *              type="boolean",
 *          ),@OA\Property(
 *              property="long_term",
 *              description="long term of job",
 *              type="boolean",
 *          ),@OA\Property(
 *              property="housing_allowance",
 *              description="housing allowance of job",
 *              type="boolean",
 *          )
 *      ),@OA\Property(
 *        property="salary_type",
 *        description="salary_type of job",
 *        type="integer",
 *     ),@OA\Property(
 *        property="min_salary",
 *        description="min_salary of job",
 *        type="integer",
 *     ),@OA\Property(
 *        property="max_salary",
 *        description="max_salary of job",
 *        type="integer",
 *     ),@OA\Property(
 *        property="is_wishlisted",
 *        description="wishlisted or not",
 *        type="boolean",
 *     ),@OA\Property(
 *        property="slug",
 *        description="slug of job",
 *        type="string",
 *     ),@OA\Property(
 *        property="langs",
 *        description="languages of job",
 *        type="array",
 *        @OA\Items(
 *          type="object",
 *          ref="#/components/schemas/JobLanguageResource"
 *        )
 *     ),@OA\Property(
 *        property="employment_type",
 *        description="emp type of job",
 *        type="integer",
 *     ),@OA\Property(
 *        property="images",
 *        description="languages of job",
 *        type="array",
 *        @OA\Items(
 *          type="object",
 *        )
 *     ),@OA\Property(
 *        property="provider_info",
 *        description="provider info of job",
 *        type="object",
 *        ref="#/components/schemas/ProviderResource"
 *     ),@OA\Property(
 *        property="created_at",
 *        description="created at of wishlist",
 *        type="string"
 *     ),@OA\Property(
 *        property="time_period",
 *        description="time period of job",
 *        type="integer",
 *     ),@OA\Property(
 *        property="is_applied",
 *        description="is job applied or not",
 *        type="boolean"
 *     )
 *   )
 * )
 */
class SeekerWishlistResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->job->id,
            'category_id' => $this->job->category_id,
            'sub_category_id' => $this->job->sub_category_id,
            'job_title' => $this->job->title,
            'description' => $this->job->description,
            'characteristics' => [
                'visa_support' => $this->job->visa_support,
                'over_time' => $this->job->overtime,
                'social_insurance' => $this->job->social_insurance,
                'bonus' => $this->job->bonus,
                'remote' => $this->job->remote,
                'practical_experience' => $this->job->practical_experience,
                'company_service' => $this->job->company_service,
                'clothing_freedom' => $this->job->clothing_freedom,
                'short_term' => $this->job->short_term,
                'long_term' => $this->job->long_term,
                'housing_allowance' => $this->job->housing_allowance,
            ],
            'salary_type' => $this->job->salary_type,
            'min_salary' => $this->job->min_salary,
            'max_salary' => $this->job->max_salary,
            'isWishlisted' => $this->job->isWishListed($this->job->id),
            'slug' => urlencode($this->job->slug),
            'langs' => JobLanguageResource::collection($this->job->languages),
            'employment_type' => $this->job->employment_type,
            'images' => (new Media())->withConverted($this->job->id, 'jobs'),
            'provider_info' => ProviderResource::make($this->job->provider),
            'created_at' => $this->created_at->diffForHumans(),
            'time_period' => $this->job->time_period,
            'addresses' => JobLocationResource::collection($this->job->locations),
            'isApplied' => $this->isApplied($this->job->id),
        ];
    }
}
