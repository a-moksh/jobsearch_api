<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class JobTitleResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="JobTitleResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="text",
 *        description="job title",
 *        type="string"
 *      ),@OA\Property(
 *        property="value",
 *        description="job id",
 *        type="integer"
 *      ),@OA\Property(
 *        property="slug",
 *        description="job slug",
 *        type="string"
 *      ),
 *   )
 * )
 */
class JobTitleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'text' => $this->title,
            'value' => $this->id,
            'slug' => urlencode($this->slug),
        ];
    }
}
