<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class NotificationResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="NotificationResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="id of transaction",
 *        type="integer"
 *      ),@OA\Property(
 *        property="type",
 *        description="id of transaction",
 *        type="integer"
 *      ),@OA\Property(
 *        property="data",
 *        description="noti data",
 *        type="string"
 *      ),@OA\Property(
 *        property="read_at",
 *        description="read time",
 *        type="string",
 *        format="date",
 *      ),@OA\Property(
 *        property="created_at",
 *        description="created time",
 *        type="string"
 *      ),@OA\Property(
 *        property="updated_at",
 *        description="updated_At",
 *        type="string"
 *      ),
 *   )
 * )
 */
class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => class_basename($this->type),
            'data' => $this->data,
            'read_at' => $this->read_at,
            'created_at' => $this->created_at->diffForHumans(),
            'updated_at' => $this->updated_at->diffForHumans(),
        ];
    }
}
