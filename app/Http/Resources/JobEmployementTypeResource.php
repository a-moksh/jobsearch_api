<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

class JobEmployementTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'employment_type' => $this->employment_type
        ];

    }

}
