<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;
use App\Models\ProviderTransaction;
use App\Models\ScoutMail;


/**
 * Class SeekerJobDetailResource
 *
 * @OA\Schema(
 *    schema="SeekerJobDetailResource",
 *    @OA\Property(
 *        property="id",
 *        description="id of the seekerjob",
 *        type="integer"
 *    )
 * )
 */
class SeekerJobDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        //resource used by provider
        $showinfo = ProviderTransaction::where('foreign_id', $this->id)->where('provider_id',
            auth()->user()->provider->id)->where('foreign_table', 'seeker_jobs')->first();
        $userdata = json_decode($this->user_data);
        if (!$showinfo) {
            $userdata->basicUpdate->full_name = '-----------------';
            $userdata->basicUpdate->katakana_full_name = '-----------------';
            $userdata->basicUpdate->address = '-----------------';
            $userdata->basicUpdate->email = '-----------------';
            $userdata->basicUpdate->mobile = '-----------------';
            $userdata->basicUpdate->street_address = '-----------------';
            $userdata->basicUpdate->zip_code = '-----------------';
            // $userdata->basicUpdate->prefecture_id = '-----------------';
            $userdata->resumeUpdate->portfolio_url = '-----------------';
            $userdata->resumeUpdate->qiita_id = '-----------------';
            $userdata->resumeUpdate->github_url = '-----------------';
            $userdata->video = null;
            $userdata->image = null;
            $userdata->email = '-----------------';
        } else {
            $userdata->image = \Storage::disk('s3_private')->temporaryUrl($userdata->image, now()->addMinutes(10));
            $userdata->video = $this->streamVideoUrl($this->user_data);
        }

        return [
//            'company_name' => $this->job->provider->company_name,
//            'min_salary' => $this->job->min_salary,
//            'max_salary' => $this->job->max_salary,
//            'employment_type' => $this->job->employment_type,
//            'category_id' => $this->job->category_id,
//            'sub_category_id' => $this->job->sub_category_id,
//            'slug' => $this->job->slug,
//            'locations' => JobLocationResource::collection($this->job->locations),
//            'morning_shift'=>$this->job->morning_shift,
//            'night_shift'=>$this->job->night_shift,
            'job_id' => $this->job_id,
            'seeker_id' => $this->seeker_id,
            'job_title' => $this->job->title,
            'job_identifier_id' => $this->job->job_identifier_id,
            'locations' => JobLocationResource::collection($this->job->locations),
            'id' => $this->id,
            'user_data' => $userdata,
            'application_status' => $this->status,
            'showinfo' => ($showinfo) ? true : false,
            'seeker_identifier_id' => $this->seeker->seeker_identifier_id,
            'created_at' => $this->created_at->format('Y/m/d'),
            'scouted' => !is_null(ScoutMail::where('job_id', $this->job_id)->where('seeker_id',
                $this->seeker_id)->where('type', '2')->first()),
            'user_settings' => SeekerMailSettingResource::make($this->seeker->mailSetting)
        ];
    }
}
