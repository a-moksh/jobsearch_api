<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class SeekerMailSettingResource
 *
 * @OA\Schema(
 *    schema="SeekerMailSettingResource",
 *    @OA\Property(
 *        property="id",
 *        description="id of the setting",
 *        type="integer"
 *    ),
 * )
 */
class SeekerMailSettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
