<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class ScoutMailResource
 *
 * @OA\Schema(
 *    schema="ScoutMailResource",
 *    @OA\Property(
 *        property="id",
 *        description="id ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="provider_id",
 *        description="provider_id",
 *        type="integer"
 *    ),@OA\Property(
 *        property="date",
 *        description="date",
 *        type="string"
 *    ),@OA\Property(
 *        property="time",
 *        description="time",
 *        type="integer"
 *    ),@OA\Property(
 *        property="interview_method",
 *        description="interview method",
 *        type="integer"
 *    ),@OA\Property(
 *        property="interview_place",
 *        description="interview place",
 *        type="integer"
 *    ),@OA\Property(
 *        property="resume",
 *        description="resume",
 *        type="string",
 *    ),@OA\Property(
 *        property="cv",
 *        description="cv",
 *        type="integer"
 *    ),@OA\Property(
 *        property="writing_util",
 *        description="writing_util",
 *        type="integer"
 *    ),@OA\Property(
 *        property="other",
 *        description="other",
 *        type="string"
 *    ),@OA\Property(
 *        property="type",
 *        description="scout or message",
 *        type="integer"
 *    ),@OA\Property(
 *        property="seeker_status",
 *        description="seeker status read replied attribute value",
 *        type="integer"
 *    ),@OA\Property(
 *        property="provider status",
 *        description="attr value of provider status ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="interview_timing",
 *        description="interview time set by provider",
 *        type="string"
 *    ),@OA\Property(
 *        property="job_title",
 *        description="title of job",
 *        type="string"
 *    ),@OA\Property(
 *        property="job_slug",
 *        description="slug for detail job",
 *        type="string"
 *    ),@OA\Property(
 *        property="company_name",
 *        description="name of company",
 *        type="string"
 *    ),@OA\Property(
 *        property="created_at",
 *        description="date value of created",
 *        type="string"
 *    ),@OA\Property(
 *        property="katakana_full_name",
 *        description="full name in katakana of seeker",
 *        type="string"
 *    ),@OA\Property(
 *        property="seeker_identifier_id",
 *        description="identifier id of seeker",
 *        type="integer"
 *    ),@OA\Property(
 *        property="job_location_id",
 *        description="id of job location for interview",
 *        type="integer"
 *    ),@OA\Property(
 *        property="seeker_read",
 *        description="read by seeker or not",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="re_count",
 *        description="reply count",
 *        type="integer"
 *    ),@OA\Property(
 *        property="seeker_reply_count",
 *        description="seeker reply count",
 *        type="integer"
 *    ),@OA\Property(
 *        property="provider_reply_count",
 *        description="reply count of provider",
 *        type="integer"
 *    )
 * )
 */
class ScoutMailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'date' => $this->date,
            'time' => $this->time,
            'interview_method' => $this->interview_method,
            'interview_place' => $this->interview_place,
            'resume' => $this->resume,
            'cv' => $this->cv,
            'writing_util' => $this->writing_util,
            'other' => $this->other,
            'type' => $this->type,
            'seeker_status' => $this->seeker_status,
            'provider_status' => $this->provider_status,
            'interview_timing' => $this->scoutDetails,
            'job_title' => $this->job->title,
            'job_slug' => urlencode($this->job->slug),
            'company_name' => $this->provider->company_name,
            'created_at' => $this->created_at->diffForHumans(),
            'katakana_full_name' => $this->katakana_full_name,
            'seeker_identifier_id' => $this->seeker_identifier_id,
            'job_location_id' => $this->job_location_id,
            'seeker_read' => $this->seeker_read,
            're_count' => $this->re_count,
            'seeker_reply_count' => $this->seeker_reply_count,
            'provider_reply_count' => $this->provider_reply_count
        ];
    }
}
