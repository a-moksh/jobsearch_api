<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class JobLanguageResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="JobLanguageResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="language id of database",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="language",
 *        description="language value id of attrubute",
 *        type="integer",
 *        example=2
 *      ),@OA\Property(
 *        property="level",
 *        description="level of language attr value",
 *        type="integer"
 *      )
 *   )
 * )
 */
class JobLanguageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'id' => $this->id,
            'language' => $this->language,
            'level' => $this->level,
        ];
    }
}
