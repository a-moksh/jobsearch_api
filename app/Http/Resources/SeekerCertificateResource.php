<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class SeekerCertificateResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="SeekerCertificateResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="certificate id of database",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="seeker_id",
 *        description="seeker id",
 *        type="integer",
 *        example=2
 *      ),@OA\Property(
 *        property="certificate name",
 *        description="certificate name",
 *        type="string"
 *      ),@OA\Property(
 *        property="year",
 *        description="year of certificate",
 *        type="string"
 *      )
 *   )
 * )
 */
class SeekerCertificateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'seeker_id' => $this->seeker_id,
            'certificate_name' => $this->certificate_name,
            'year' => $this->year,
        ];
    }
}
