<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;
use App\Models\Media;

/**
 * Class JobSeekerResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="JobSeekerResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="job id",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="created_at",
 *        description="created_At time",
 *        type="string",
 *      ),@OA\Property(
 *        property="job_identifier_id",
 *        description="job_identifier_id ",
 *        type="integer",
 *      ),@OA\Property(
 *        property="category_id",
 *        description="category_id time",
 *        type="integer",
 *      ),@OA\Property(
 *        property="sub_category_id",
 *        description="sub_category_id ",
 *        type="integer",
 *      )
 *   )
 * )
 */
class JobSeekerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'sub_category_id' => $this->sub_category_id,
            'job_title' => $this->title,
            'min_salary' => $this->min_salary,
            'max_salary' => $this->max_salary,
            'description' => $this->description,
            'qualifications' => $this->qualifications,
            'welfare' => $this->welfare,
            'language_translation' => $this->language_translation,
            'working_hour_from' => $this->working_hour_from,
            'working_hour_to' => $this->working_hour_to,
            'holiday_days' => $this->holiday_days,
            'holiday' => $this->holiday,
            'characteristics' => [
                'visa_support' => $this->visa_support,
                'over_time' => $this->overtime,
                'social_insurance' => $this->social_insurance,
                'bonus' => $this->bonus,
                'remote' => $this->remote,
                'practical_experience' => $this->practical_experience,
                'company_service' => $this->company_service,
                'clothing_freedom' => $this->clothing_freedom,
                'short_term' => $this->short_term,
                'long_term' => $this->long_term,
                'housing_allowance' => $this->housing_allowance,
            ],
            'other_details' => $this->other_details,
            'total_annual_leave' => $this->total_annual_leave,
            'salary_type' => $this->salary_type,
            'status' => $this->status,
            'admin_status' => $this->admin_status,
            'views' => $this->views,
            'isWishlisted' => $this->isWishListed($this->id),
            'slug' => urlencode($this->slug),
            'addresses' => JobLocationResource::collection($this->locations),
            'langs' => JobLanguageResource::collection($this->languages),
            'employment_type' => $this->employment_type,
            'images' => (new Media())->withConverted($this->id, 'jobs'),
            'provider_info' => ProviderSeekerResource::make($this->provider),
            'morning_shift' => $this->morning_shift,
            'night_shift' => $this->night_shift,
            'time_period' => $this->time_period,
        ];
    }
}
