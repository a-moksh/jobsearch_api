<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;
use App\Models\SeekerSkill;


/**
 * Class ScoutMailListResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="ScoutMailListResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="id of scoutmail",
 *        type="integer",
 *      ),@OA\Property(
 *        property="created_at",
 *        description="created time value",
 *        type="string",
 *      )
 *   )
 * )
 */
class ScoutMailListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at->format('Y/m/d'),
            'english_language_level' => $this->english_language_level,
            'japanese_language_level' => $this->japanese_language_level,
            'prefecture_id' => $this->prefecture_id,
            'visa_status' => $this->visa_status,
            'visa_expiry_date' => $this->visa_expiry_date,
            'katakana_full_name' => $this->katakana_full_name,
            'seeker_skills' => SeekerSkillResource::make(SeekerSkill::where('seeker_id', $this->seeker_id)->get()),
            'seeker_identifier_id' => $this->seeker_identifier_id,
            'employment_type' => explode(',', $this->employment_type),
            'annual_income' => $this->seeker->annualIncome($this->seeker_id)
        ];
    }
}
