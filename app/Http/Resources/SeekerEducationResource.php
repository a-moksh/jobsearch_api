<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class SeekerEducationResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="SeekerEducationResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="education id of database",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="seeker_id",
 *        description="seeker id",
 *        type="integer",
 *        example=2
 *      ),@OA\Property(
 *        property="university_institute",
 *        description="uni name",
 *        type="string"
 *      ),@OA\Property(
 *        property="degree",
 *        description="degree attribute id",
 *        type="integer"
 *      ),@OA\Property(
 *        property="faculty",
 *        description="faculty of education",
 *        type="string"
 *      ),@OA\Property(
 *        property="graduation_country",
 *        description="country of graduation",
 *        type="integer"
 *      ),@OA\Property(
 *        property="graduation_status",
 *        description="graduation status",
 *        type="integer"
 *      )
 *   )
 * )
 */
class SeekerEducationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'id' => $this->id,
            'seeker_id' => $this->seeker_id,
            'university_institute' => $this->university_institute,
            'degree' => $this->degree,
            'faculty' => $this->faculty,
            'graduation_country' => $this->graduation_country,
            'graduation_status' => $this->graduation_status,
            'graduation_year' => $this->graduation_year,
            'order' => $this->order,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => OperatorResource::make($this->createdBy),
            'updated_by' => OperatorResource::make($this->updatedBy),
        ];
    }
}
