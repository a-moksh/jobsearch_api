<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class SeekerJobResource
 *
 * @OA\Schema(
 *    schema="SeekerJobResource",
 *    @OA\Property(
 *        property="id",
 *        description="application id",
 *        type="integer"
 *    ),@OA\Property(
 *        property="job_title",
 *        description="job title",
 *        type="string"
 *    ),@OA\Property(
 *        property="company_name",
 *        description="company name",
 *        type="string"
 *    ),@OA\Property(
 *        property="min_salary",
 *        description="job min_salary",
 *        type="integer"
 *    ),@OA\Property(
 *        property="max_salary",
 *        description="job max_salary",
 *        type="integer"
 *    ),@OA\Property(
 *        property="employment_type",
 *        description="job emp type attr value",
 *        type="integer"
 *    ),@OA\Property(
 *        property="category_id",
 *        description="job catefory",
 *        type="integer"
 *    ),@OA\Property(
 *        property="sub_category_id",
 *        description="job sub_category_id",
 *        type="integer"
 *    ),@OA\Property(
 *        property="slug",
 *        description="job slug",
 *        type="string"
 *    ),@OA\Property(
 *        property="morning_shifg",
 *        description="job morning shift",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="night_shift",
 *        description="job night shift",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="created_at",
 *        description="job apply date",
 *        type="string"
 *    ),@OA\Property(
 *        property="addresses",
 *        description="addresss of job",
 *        type="array",
 *        @OA\Items(
 *          type="object",
 *          ref="#/components/schemas/JobLocationResource"
 *        )
 *     ),@OA\Property(
 *        property="can_view_detail",
 *        description="can view detail or not",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="video",
 *        description="video url for streming",
 *        type="string"
 *    ),@OA\Property(
 *        property="thumbnail",
 *        description="thumbnail of video",
 *        type="string"
 *    ),@OA\Property(
 *        property="is_opened",
 *        description="opened or not",
 *        type="boolean"
 *    )
 * )
 */
class SeekerJobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'job_title' => $this->job->title,
            'company_name' => $this->job->provider->company_name,
            'min_salary' => $this->job->min_salary,
            'max_salary' => $this->job->max_salary,
            'salary_type' => $this->job->salary_type,
            'employment_type' => $this->job->employment_type,
            'category_id' => $this->job->category_id,
            'sub_category_id' => $this->job->sub_category_id,
            'slug' => urlencode($this->job->slug),
            'morning_shift' => $this->job->morning_shift,
            'night_shift' => $this->job->night_shift,
            'created_at' => $this->created_at->format('Y/m/d'),
            'addresses' => JobLocationResource::collection($this->job->locations),
            'isOpened' => $this->isOpened($this->id),
            'time_period' => $this->job->time_period,
            'video' => $this->streamVideoUrl($this->user_data),
            'thumbnail' => $this->videoThumbmail($this->user_data, $this->seeker_id),
            'video_converted_details' => $this->video_converted_details,
            'can_view_detail' => $this->job->canViewDetail($this->job->published_at, $this->job->status)
        ];
    }
}
