<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;
use App\Models\ProviderTransaction;
use App\Models\SeekerSkill;


/**
 * Class ProviderSeekerJobListResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="ProviderSeekerJobListResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="id of seekerjob",
 *        type="integer"
 *      )
 *   )
 * )
 */
class ProviderSeekerJobListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        $showinfo = ProviderTransaction::where('foreign_id', $this->seeker_job_id)->where('provider_id',
            auth()->user()->provider->id)->where('foreign_table', 'seeker_jobs')->first();

        $app_data = json_decode($this->user_data);

        $this->katakana_full_name = $app_data->basicUpdate->katakana_full_name;
        $this->japanese_language_level = $app_data->basicUpdate->japanese_language_level;
        $this->english_language_level = $app_data->basicUpdate->english_language_level;

        $this->prefecture_id = isset($app_data->basicUpdate->prefecture_id) ? $app_data->basicUpdate->prefecture_id : null;
        $this->state = isset($app_data->basicUpdate->state) ? $app_data->basicUpdate->state : null;
        $this->visa_status = isset($app_data->basicUpdate->visa_status) ? $app_data->basicUpdate->visa_status : null;
        $this->visa_expiry_date = isset($app_data->basicUpdate->visa_expiry_date) ? date('Y-m-d',strtotime($app_data->basicUpdate->visa_expiry_date)) : null;

        $annual_income = '';
        if((isset($app_data->experienceUpdate) && isset($app_data->experienceUpdate->experiences))){
            foreach ($app_data->experienceUpdate->experiences as $experience){
                if($experience->currently_working_here == 1)
                    $annual_income = $experience->annual_salary;
            }
            if(!$annual_income){
                $date = null;
                foreach ($app_data->experienceUpdate->experiences as $experience){

                    if($experience->job_to > $date){
                        $date = $experience->job_to;
                        $annual_income = $experience->annual_salary;
                    }
                }
            }
        }

        return [
            'id' => $this->seeker_job_id,
            'job_identifier_id' => $this->job->job_identifier_id,
            'job_id' => $this->job->id,
            'job_title' => $this->job->title,
            'employment_type' => $this->job->employment_type,
            'category_id' => $this->job->category_id,
            'sub_category_id' => $this->job->sub_category_id,
            'created_at' => $this->created_at->format('Y/m/d'),
            'seeker_identifier_id' => $this->seeker_identifier_id,
            'showinfo' => ($showinfo) ? true : false,
            'status' => $this->status,
            'last_login' => date('Y/m/d', strtotime($this->last_login)),

            'prefecture_id' => $this->prefecture_id,
            'state' => $this->state,
            'katakana_full_name' => ($showinfo) ? $this->katakana_full_name : '-----------------',
            'visa_status' => $this->visa_status,
            'visa_expiry_date' => $this->visa_expiry_date,
            'japanese_language_level' => $this->japanese_language_level,
            'english_language_level' => $this->english_language_level,
            'seeker_skills' => (isset($app_data->skillUpdate) && isset($app_data->skillUpdate->skills)) ? $app_data->skillUpdate->skills: [],
            'annual_income' => $annual_income

//            'annual_income' => $this->seeker->annualIncome($this->seeker_id),
//            'seeker_skills' => SeekerSkillResource::make(SeekerSkill::where('seeker_id', $this->seeker->id)->get()),
        ];
    }
}
