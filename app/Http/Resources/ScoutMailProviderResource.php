<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class ScoutMailProviderResource
 *
 * @OA\Schema(
 *    schema="ScoutMailProviderResource",
 *    @OA\Property(
 *        property="scout_details",
 *        description="scout details",
 *        type="object"
 *    ),@OA\Property(
 *        property="seeker_details",
 *        description="seeker details",
 *        type="object"
 *    )
 * )
 */
class ScoutMailProviderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'scout_details' => [
                'id' => $this->id,
                'job_identifier_id' => $this->job->job_identifier_id,
                'job_title' => $this->job->title,
                'provider_id' => $this->provider_id,
                'date' => $this->date,
                'time' => $this->time,
                'interview_method' => $this->interview_method,
                'interview_place' => $this->interview_place,
                'resume' => $this->resume,
                'cv' => $this->cv,
                'writing_util' => $this->writing_util,
                'other' => $this->other,
                'created_at' => $this->created_at->format('Y/m/d'),
                'interview_timing' => $this->scoutDetails,
                'provider_read' => $this->provider_read,
                're_count' => $this->re_count,
            ],
            'seeker_details' => SeekerResource::make($this->seeker)
        ];
    }
}
