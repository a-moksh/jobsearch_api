<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AttrOptResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="AttrOptResource",
 *    @OA\Property(
 *        property="id",
 *        description="汎用属性選択肢ID",
 *        type="integer",
 *        example=1
 *    ),
 *    @OA\Property(
 *        property="attr_id",
 *        description="汎用属性ID",
 *        type="integer",
 *        example=1
 *    ),
 *    @OA\Property(
 *        property="label",
 *        description="汎用属性選択肢ラベル",
 *        type="string",
 *        example=""
 *    ),
 *    @OA\Property(
 *        property="value",
 *        description="汎用属性選択肢値",
 *        type="string",
 *        example=""
 *    ),
 *    @OA\Property(
 *        property="child_id",
 *        description="子汎用属性ID",
 *        type="integer",
 *        example=1
 *    ),
 *    @OA\Property(
 *        property="description",
 *        description="説明",
 *        type="string",
 *        example=""
 *    ),
 *    @OA\Property(
 *        property="display_order",
 *        description="表示順",
 *        type="integer",
 *        example=1
 *    ),
 *    @OA\Property(
 *        property="created_at",
 *        ref="#/components/schemas/Timestamp"
 *    ),
 *    @OA\Property(
 *        property="updated_at",
 *        ref="#/components/schemas/Timestamp"
 *    )
 * )
 */
class AttrOptResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'attr_id' => $this->attr_id,
            'label' => $this->label,
            'value' => $this->value,
            'child' => AttrResource::make($this->child),
            'description' => $this->description,
            'display_order' => $this->display_order
        ];
    }
}
