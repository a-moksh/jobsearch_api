<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;
use App\Models\ProviderTransaction;
use App\Models\SeekerSkill;

/**
 * Class FavouriteListResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="FavouriteListResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="favourite id",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="created_at",
 *        description="created_At time",
 *        type="string",
 *        example=2
 *      ),@OA\Property(
 *        property="seeker_id",
 *        description="seeker id",
 *        type="integer"
 *      ),@OA\Property(
 *        property="english_language_level",
 *        description="english language level",
 *        type="integer"
 *    ),@OA\Property(
 *        property="japanese_language_level",
 *        description="japanese language level",
 *        type="integer"
 *    ),@OA\Property(
 *        property="prefecture_id",
 *        description="prefecture id",
 *        type="integer"
 *    ),@OA\Property(
 *        property="visa_status",
 *        description="visa status",
 *        type="integer"
 *    ),@OA\Property(
 *        property="visa_expiry_date",
 *        description="visa expiry date",
 *        type="integer"
 *    ),@OA\Property(
 *        property="katakana_full_name",
 *        description="katakana full name",
 *        type="integer"
 *    ),@OA\Property(
 *        property="showinfo",
 *        description="show info",
 *        type="boolean"
 *    ),
 *   )
 * )
 */
class FavouriteListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $showinfo = ProviderTransaction::where('foreign_id', $this->seeker_id)->where('provider_id',
            auth()->user()->provider->id)->where('foreign_table', 'seekers')->first();
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'seeker_id' => $this->seeker_id,
            'english_language_level' => $this->english_language_level,
            'japanese_language_level' => $this->japanese_language_level,
            'prefecture_id' => $this->prefecture_id,
            'visa_status' => $this->visa_status,
            'visa_expiry_date' => $this->visa_expiry_date,
            'showinfo' => ($showinfo) ? true : false,
            'katakana_full_name' => ($showinfo) ? $this->katakana_full_name : '-----------------',
            'seeker_skills' => SeekerSkillResource::make(SeekerSkill::where('seeker_id', $this->seeker_id)->get()),
            'employemnt_type' => null,
            'last_login' => date('Y/m/d', strtotime($this->last_login)),
            'seeker_identifier_id' => $this->seeker_identifier_id,
            'employment_type' => explode(',', $this->employment_type),
            'annual_income' => $this->seeker->annualIncome($this->seeker_id),
        ];
    }
}
