<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;
use App\Models\SeekerSkill;

/**
 * Class SeekerListAdminResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="SeekerListAdminResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="id of seeker",
 *        type="integer"
 *      )
 *   )
 * )
 */
class SeekerListAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'seeker_identifier_id' => $this->seeker_identifier_id,
            'katakana_full_name' => $this->katakana_full_name,
            'full_name' => $this->full_name,
            'email' => $this->email,
            'is_experienced' => $this->is_experienced,
            'experienced' => $this->experience,
            'country' => $this->country,
            'living_country' => $this->living_country,
            'gender' => $this->gender,
            'visa_status' => $this->visa_status,
            'visa_issue_date' => $this->visa_issue_date,
            'mobile' => $this->user->mobile,
            'date_of_birth' => $this->user->dob,
            'visa_expiry_date' => $this->visa_expiry_date,
            'japanese_language_level' => $this->japanese_language_level,
            'japanese_writing_level' => $this->japanese_writing_level,
            'prefecture_id' => $this->prefecture_id,
            'english_language_level' => $this->english_language_level,
            'english_writing_level' => $this->english_writing_level,
            'last_annual_salary' => $this->last_annual_salary,
            'employment_type' => explode(',', $this->employment_type),
            'last_login' => date('Y/m/d', strtotime($this->last_login)),
            'annual_income' => $this->annualIncome($this->id),
            'seeker_skills' => SeekerSkillResource::make(SeekerSkill::where('seeker_id', $this->id)->get()),
        ];
    }
}
