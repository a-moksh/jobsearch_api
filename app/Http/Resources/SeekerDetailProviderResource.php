<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;
use App\Models\Media;
use App\Models\ProviderTransaction;

/**
 * Class SeekerDetailProviderResource
 *
 * @OA\Schema(
 *    schema="SeekerDetailProviderResource",
 *    @OA\Property(
 *        property="seeker_detail",
 *        type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="data",
 *        description="data",
 *        type="string"
 *      )
 *   )
 *    ),@OA\Property(
 *        property="user_detail",
 *        description="user_detail",
 *        type="object"
 *    ),@OA\Property(
 *        property="seeker_experiences",
 *        description="seeker_experiences",
 *        type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="data",
 *        description="data",
 *        type="string"
 *      )
 *   )
 *    ),@OA\Property(
 *        property="seeker_educations",
 *        description="seeker_educations",
 *        type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="data",
 *        description="data",
 *        type="string"
 *      )
 *   )
 *    ),@OA\Property(
 *        property="seeker_certifications",
 *        description="seeker_certifications",
 *        type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="data",
 *        description="data",
 *        type="string"
 *      )
 *   )
 *    ),@OA\Property(
 *        property="seeker_settings",
 *        description="seeker_settings",
 *        type="object",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="data",
 *        description="data",
 *        type="string"
 *      )
 *   )
 *    ),@OA\Property(
 *        property="seeker_skills",
 *        description="seeker_skills",
 *        type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="data",
 *        description="data",
 *        type="string"
 *      )
 *   )
 *    ),@OA\Property(
 *        property="showinfo",
 *        description="showinfo",
 *        type="boolean"
 *    )
 * )
 */
class SeekerDetailProviderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $showinfo = ProviderTransaction::where('foreign_id', $this->id)->where('provider_id',
            auth()->user()->provider->id)->where('foreign_table', 'seekers')->first();
        return [
            'seeker_detail' => [
                'id' => $this->id,
                'seeker_identifier_no' => $this->seeker_identifier_id,
                'visa_status' => $this->visa_status,
                'visa_issue_date' => $this->visa_issue_date,
                'visa_expiry_date' => $this->visa_expiry_date,
                'japanese_language_level' => $this->japanese_language_level,
                'japanese_writing_level' => $this->japanese_writing_level,
                'country' => $this->country,
                'living_country' => $this->living_country,
                'state' => $this->state,
                'zip_code' => ($showinfo) ? $this->zip_code : '---------------',
                'prefecture_id' => $this->prefecture_id,
                'street_address' => ($showinfo) ? $this->street_address : '---------------',
                'english_language_level' => $this->english_language_level,
                'english_writing_level' => $this->english_writing_level,
                'other_language_level' => $this->other_language_level,
                'desired_employment_type' => $this->desired_employment_type,
                'desired_job_category' => $this->desired_job_category,
                'desired_annual_min_salary_range' => $this->desired_annual_min_salary_range,
                'experience_year' => $this->experience_year,
                'nearest_station' => $this->nearest_station,
                'willing_to_relocate' => $this->willing_to_relocate,
                'is_experienced' => $this->is_experienced,
                'experience' => (!$this->experience) ? null : $this->experience,
                'last_annual_salary' => $this->last_annual_salary,
                'last_work_region' => $this->last_work_region,
                'portfolio_url' => ($showinfo) ? $this->portfolio_url : '-----------------',
                'github_url' => ($showinfo) ? $this->github_url : '-----------------',
                'qiita_id' => ($showinfo) ? $this->qiita_id : '-----------------',
                'self_introduction' => $this->self_introduction,
                'address' => ($showinfo) ? $this->address : '---------------'
            ],
            'user_detail' => [
                'id' => $this->id,
                'full_name' => ($showinfo) ? $this->user->full_name : '-----------------',
                'katakana_full_name' => ($showinfo) ? $this->user->katakana_full_name : '-----------------',
                'dob' => $this->user->dob,
                'gender' => $this->user->gender,
                'email' => ($showinfo) ? $this->user->email : '-----------------',
                'mobile' => ($showinfo) ? $this->user->mobile : '-----------------',
                'user_type' => $this->user->user_type,
                'image' => ($showinfo) ? isset($this->user->userImage) ? (new Media())->withConverted($this->id,
                    'users') : '' : null,
            ],
            'showinfo' => ($showinfo) ? true : false,
            'seeker_experiences' => SeekerExperienceResource::collection($this->experiences),
            'seeker_educations' => SeekerEducationResource::collection($this->educations),
            'seeker_certifications' => SeekerCertificateResource::collection($this->certificates),
            'seeker_settings' => SeekerMailSettingResource::make($this->mailSetting),
            'seeker_skills' => SeekerSkillResource::make($this->skills)

        ];
    }
}
