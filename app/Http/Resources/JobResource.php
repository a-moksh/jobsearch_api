<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;
use App\Models\Media;
use App\Models\ProviderTransaction;

/**
 * Class JobResource
 *
 * @OA\Schema(
 *    schema="JobResource",
 *    @OA\Property(
 *        property="id",
 *        description="id of the job",
 *        type="integer"
 *    ),@OA\Property(
 *        property="created_at",
 *        description="created_At time",
 *        type="string"
 *    ),@OA\Property(
 *        property="job_identifier_id",
 *        description="job identifer id",
 *        type="string"
 *    ),@OA\Property(
 *        property="category_id",
 *        description="category id",
 *        type="integer"
 *    ),@OA\Property(
 *        property="sub_category_id",
 *        description="subcategory id",
 *        type="integer"
 *    ),@OA\Property(
 *        property="job_title",
 *        description="job_title id",
 *        type="string"
 *    ),@OA\Property(
 *        property="min_salary",
 *        description=" min_salary",
 *        type="integer"
 *    ),
 * )
 */
class JobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'created_at' => $this->created_at->format('Y/m/d'),
            'id' => $this->id,
            'job_identifier_id' => $this->job_identifier_id,
            'category_id' => $this->category_id,
            'sub_category_id' => $this->sub_category_id,
            'job_title' => $this->title,
            'min_salary' => $this->min_salary,
            'max_salary' => $this->max_salary,
            'description' => $this->description,
            'qualifications' => $this->qualifications,
            'welfare' => $this->welfare,
            'language_translation' => $this->language_translation,
            'working_hour_from' => $this->working_hour_from,
            'working_hour_to' => $this->working_hour_to,
            'visa_support' => $this->visa_support,
            'over_time' => $this->overtime,
            'holiday_days' => $this->holiday_days,
            'holiday' => $this->holiday,
            'trial_period' => $this->trial_period,
            'social_insurance' => $this->social_insurance,
            'bonus' => $this->bonus,
            'hairstyle_freedom' => $this->hairstyle_freedom,
            'clothing_freedom' => $this->clothing_freedom,
            'with_uniform' => $this->with_uniform,
            'housing_allowance' => $this->housing_allowance,
            'other_details' => $this->other_details,
            'total_annual_leave' => $this->total_annual_leave,
            'salary_type' => $this->salary_type,
            'long_term' => $this->long_term,
            'short_term' => $this->short_term,
            'remote' => $this->remote,
            'practical_experience' => $this->practical_experience,
            'company_service' => $this->company_service,
            'status' => $this->status,
            'admin_status' => $this->admin_status,
            'views' => $this->views,
            'isWishlisted' => $this->isWishListed($this->id),
            'slug' => urlencode($this->slug),
            'time_period' => $this->time_period,
            'addresses' => JobLocationResource::collection($this->locations),
            'langs' => JobLanguageResource::collection($this->languages),
            'employment_type' => $this->employment_type,
            'images' => (new Media())->withConverted($this->id, 'jobs'),
            'provider_info' => ProviderResource::make($this->provider),
            'morning_shift' => $this->morning_shift,
            'night_shift' => $this->night_shift,
            'application_count' => $this->applications()->count(),
            'show_renew' => ($this->published_at) ? (now() >= $this->published_at->addDays(76) && now() <= $this->published_at->addDays(90) && $this->admin_status == 0) : false,
            // 'show_renew'=>(now() >= $this->published_at->addDays(76) && now() <= $this->published_at->addDays(90) && $this->admin_status == 0),
            'provider_id' => $this->provider_id,
            'admin_comment' => $this->jobLogs()->latest()->get(),
            'translation_paid' => (ProviderTransaction::where('foreign_id', $this->id)->where('provider_id',
                $this->provider_id)->where('foreign_table', 'jobs')->first()) ? true : false
        ];
    }
}
