<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class GlobalResource
 *
 * @OA\Schema(
 *    schema="GlobalResource",
 *    @OA\Property(
 *        property="id",
 *        description="id of the",
 *        type="integer"
 *    )
 * )
 */
class GlobalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
