<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class JobLocationResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="JobLocationResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="id of database",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="zip_code",
 *        description="zip_code",
 *        type="integer",
 *        example=2
 *      ),@OA\Property(
 *        property="prefecture_id",
 *        description="pref_id",
 *        type="integer"
 *      ),@OA\Property(
 *        property="address",
 *        description="address",
 *        type="string"
 *      ),@OA\Property(
 *        property="street_address",
 *        description="street address",
 *        type="string"
 *      )
 *   )
 * )
 */
class JobLocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'zip_code' => $this->zip_code,
            'prefecture_id' => $this->prefecture_id,
            'address' => $this->address,
            'street_address' => $this->street_address,
        ];
    }
}
