<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class ScoutMailListProviderResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="ScoutMailListProviderResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="id of scoutmail",
 *        type="integer"
 *      )
 *   )
 * )
 */
class ScoutMailListProviderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'provider_id' => $this->provider_id,
            'date' => $this->date,
            'time' => $this->time,
            'interview_method' => $this->interview_method,
            'interview_place' => $this->interview_place,
            'resume' => $this->resume,
            'cv' => $this->cv,
            'writing_util' => $this->writing_util,
            'other' => $this->other,
            'type' => $this->type,
            'seeker_status' => $this->seeker_status,
            'provider_status' => $this->provider_status,
            'interview_timing' => $this->scoutDetails,
            'job_title' => $this->job->title,
            'job_identifier_id' => $this->job_identifier_id,
            'updated_at' => $this->updated_at->diffForHumans(),
            'updated_at_time' => $this->updated_at->format('Y-m-d H:i:s'),
            'created_at' => $this->created_at->diffForHumans(),
            'created_at_time' => $this->created_at->format('Y-m-d H:i:s'),
            'katakana_full_name' => $this->katakana_full_name,
            'seeker_identifier_id' => $this->seeker_identifier_id,
            'provider_read' => $this->provider_read,
            'provider_reply_count' => $this->provider_reply_count,
            'provider_skype_id' => $this->provider_skype_id,
            'seeker_skype_id' => $this->seeker_skype_id,
            'seeker_replied_at' => $this->seeker_replied_at,
            'seeker_reply_count' => $this->seeker_reply_count,
        ];
    }
}
