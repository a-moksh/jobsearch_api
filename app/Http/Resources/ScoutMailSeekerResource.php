<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;


/**
 * Class ScoutMailSeekerResource
 *
 * @OA\Schema(
 *    schema="ScoutMailSeekerResource",
 *    @OA\Property(
 *        property="scout_details",
 *        description="scout details",
 *        type="object"
 *    ),@OA\Property(
 *        property="job_details",
 *        description="job details",
 *        type="object"
 *    )
 * )
 */
class ScoutMailSeekerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'scout_details' => [
                'id' => $this->id,
                'provider_id' => $this->provider_id,
                'date' => $this->date,
                'time' => $this->time,
                'interview_method' => $this->interview_method,
                'interview_place' => $this->interview_place,
                'job_location_id' => $this->job_location_id,
                'resume' => $this->resume,
                'cv' => $this->cv,
                'writing_util' => $this->writing_util,
                'other' => $this->other,
                'type' => $this->type,
                'seeker_status' => $this->seeker_status,
                'provider_status' => $this->provider_status,
                'interview_timing' => $this->scoutDetails,
                'created_at' => $this->created_at->format('Y/m/d'),
                'updated_at' => ($this->updated_at != null) ? $this->updated_at->format('Y/m/d') : null,
                'seeker_read' => $this->seeker_read,
                're_count' => $this->re_count,
                'seeker_reply_count' => $this->seeker_reply_count,
                'provider_reply_count' => $this->provider_reply_count,
                'provider_skype_id' => $this->provider_skype_id,
                'seeker_skype_id' => $this->seeker_skype_id,
                'seeker_replied_at' => $this->seeker_replied_at
            ],
            'job_details' => JobSeekerResource::make($this->job)
        ];
    }
}
