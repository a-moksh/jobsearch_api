<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;


/**
 * Class JobListResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="JobListResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="job id",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="created_at",
 *        description="created_At time",
 *        type="string",
 *      ),@OA\Property(
 *        property="job_identifier_id",
 *        description="job_identifier_id ",
 *        type="integer",
 *      ),@OA\Property(
 *        property="category_id",
 *        description="category_id time",
 *        type="integer",
 *      ),@OA\Property(
 *        property="sub_category_id",
 *        description="sub_category_id ",
 *        type="integer",
 *      )
 *   )
 * )
 */
class JobListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'created_at' => $this->created_at->format('Y/m/d'),
            'id' => $this->id,
            'job_identifier_id' => $this->job_identifier_id,
            'category_id' => $this->category_id,
            'sub_category_id' => $this->sub_category_id,
            'job_title' => $this->title,
            'min_salary' => $this->min_salary,
            'max_salary' => $this->max_salary,
            'description' => $this->description,
            'qualifications' => $this->qualifications,
            'welfare' => $this->welfare,
            'language_translation' => $this->language_translation,
            'working_hour_from' => $this->working_hour_from,
            'working_hour_to' => $this->working_hour_to,
            'visa_support' => $this->visa_support,
            'over_time' => $this->overtime,
            'holiday_days' => $this->holiday_days,
            'holiday' => $this->holiday,
            'trial_period' => $this->trial_period,
            'social_insurance' => $this->social_insurance,
            'bonus' => $this->bonus,
            'hairstyle_freedom' => $this->hairstyle_freedom,
            'clothing_freedom' => $this->clothing_freedom,
            'with_uniform' => $this->with_uniform,
            'housing_allowance' => $this->housing_allowance,
            'other_details' => $this->other_details,
            'total_annual_leave' => $this->total_annual_leave,
            'salary_type' => $this->salary_type,
            'long_term' => $this->long_term,
            'short_term' => $this->short_term,
            'remote' => $this->remote,
            'practical_experience' => $this->practical_experience,
            'company_service' => $this->company_service,
            'status' => $this->status,
            'admin_status' => $this->admin_status,
            'views' => $this->views,
            'slug' => urlencode($this->slug),
            'time_period' => $this->time_period,
            'employment_type' => $this->employment_type,
            'provider_info' => ProviderResource::make($this->provider),
            'morning_shift' => $this->morning_shift,
            'night_shift' => $this->night_shift,
            'application_count' => $this->applications()->count(),
            'admin_comment' => $this->jobLogs()->count()
        ];
    }
}
