<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class AttrResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="AttrResource",
 *    @OA\Property(
 *        property="id",
 *        description="属性ID",
 *        type="integer",
 *        example=1
 *    ),
 *    @OA\Property(
 *        property="name",
 *        description="属性名",
 *        type="string",
 *        example=""
 *    ),
 *    @OA\Property(
 *        property="label",
 *        description="属性ラベル",
 *        type="string",
 *        example=""
 *    ),
 *    @OA\Property(
 *        property="data_type",
 *        description="	データタイプ",
 *        type="string",
 *        example=""
 *    ),
 *    @OA\Property(
 *        property="default_value",
 *        description="初期値",
 *        type="string",
 *        example=""
 *    ),
 *    @OA\Property(
 *        property="description",
 *        description="説明",
 *        type="string",
 *        example=""
 *    ),
 *    @OA\Property(
 *        property="display_order",
 *        description="表示順",
 *        type="integer",
 *        example=1
 *    ),
 *    @OA\Property(
 *        property="created_at",
 *        ref="#/components/schemas/Timestamp"
 *    ),
 *    @OA\Property(
 *        property="updated_at",
 *        ref="#/components/schemas/Timestamp"
 *    )
 * )
 */
class AttrResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'label' => $this->label,
            'data_type' => $this->data_type,
            'default_value' => $this->default_value,
            'description' => $this->description,
            'display_order' => $this->display_order,
            'opts' => AttrOptResource::collection($this->opts),
        ];
    }
}
