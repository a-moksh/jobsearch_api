<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

class MediaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'media_value' => $this->media_value,
            'media_type' => $this->media_type,
        ];
    }
}
