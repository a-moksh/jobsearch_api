<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;
use App\Models\SeekerSkill;

/**
 * Class FavouriteResource
 *
 * @OA\Schema(
 *    schema="FavouriteResource",
 *    @OA\Property(
 *        property="id",
 *        description="id of the user",
 *        type="integer"
 *    ),@OA\Property(
 *        property="created_at",
 *        description="created time",
 *        type="string"
 *    ),@OA\Property(
 *        property="seeker_id",
 *        description="id of the seeker",
 *        type="integer"
 *    ),@OA\Property(
 *        property="english_language_level",
 *        description="english language level",
 *        type="integer"
 *    ),@OA\Property(
 *        property="japanese_language_level",
 *        description="japanese language level",
 *        type="integer"
 *    ),@OA\Property(
 *        property="prefecture_id",
 *        description="prefecture id",
 *        type="integer"
 *    ),@OA\Property(
 *        property="visa_status",
 *        description="visa status",
 *        type="integer"
 *    ),@OA\Property(
 *        property="visa_expiry_date",
 *        description="visa expiry date",
 *        type="integer"
 *    ),@OA\Property(
 *        property="katakana_full_name",
 *        description="katakana full name",
 *        type="integer"
 *    ),
 * )
 */
class FavouriteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'seeker_id' => $this->seeker_id,
            'english_language_level' => $this->english_language_level,
            'japanese_language_level' => $this->japanese_language_level,
            'prefecture_id' => $this->prefecture_id,
            'visa_status' => $this->visa_status,
            'visa_expiry_date' => $this->visa_expiry_date,
            'katakana_full_name' => $this->katakana_full_name,
            'seeker_skills' => SeekerSkillResource::make(SeekerSkill::where('seeker_id', $this->seeker_id)->get()),
            'employemnt_type' => null,
            'last_login' => date('Y/m/d', strtotime($this->last_login)),
            'seeker_identifier_id' => $this->seeker_identifier_id,
            'employment_type' => explode(',', $this->employment_type),
            'annual_income' => $this->seeker->annualIncome($this->seeker_id),
        ];
    }
}
