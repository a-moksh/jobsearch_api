<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class ZipCodeResource
 *
 * @OA\Schema(
 *    schema="ZipCodeResource",
 *    @OA\Property(
 *        property="id",
 *        description="id of the zipcode",
 *        type="integer"
 *    ),@OA\Property(
 *        property="zip_code",
 *        description="zip_code value",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="prefecture_ja",
 *        description="prefecture ",
 *        type="string"
 *    ),@OA\Property(
 *        property="address_ja",
 *        description="address",
 *        type="string"
 *    ),@OA\Property(
 *        property="street_address_ja",
 *        description="street_address",
 *        type="string"
 *    ),@OA\Property(
 *        property="prefecture_en",
 *        description="prefecture_en",
 *        type="string"
 *    ),@OA\Property(
 *        property="address_en",
 *        description="address_en",
 *        type="string"
 *    ),@OA\Property(
 *        property="street_address_en",
 *        description="street_address_en",
 *        type="string"
 *    )
 * )
 */
class ZipCodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => 1,
            "zip_code" => $this->zip_code,
            "prefecture_ja" => $this->prefecture_ja,
            "address_ja" => $this->address_ja,
            "street_address_ja" => $this->street_address_ja,
            "prefecture_en" => $this->prefecture_en,
            "address_en" => $this->address_en,
            "street_address_en" => $this->street_address_en,
        ];
    }
}
