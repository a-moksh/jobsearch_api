<?php

namespace App\Http\Resources;

use App\Extensions\Http\Resources\Json\JsonResource;

/**
 * Class ProviderSeekerResource
 * @package App\Http\Resources
 */

/**
 * @OA\Schema(
 *    schema="ProviderSeekerResource",
 *    type="array",
 *    @OA\Items(
 *      type="object",
 *      @OA\Property(
 *        property="id",
 *        description="id of provider",
 *        type="integer",
 *        example=1
 *      ),@OA\Property(
 *        property="established_year",
 *        description="established_year value",
 *        type="string",
 *      ),@OA\Property(
 *        property="provider_identifier_id",
 *        description="provider_identifier_id",
 *        type="string"
 *      ),@OA\Property(
 *        property="company_name",
 *        description="company_name",
 *        type="string"
 *      ),@OA\Property(
 *        property="ceo_name",
 *        description="ceo_name",
 *        type="string"
 *      ),@OA\Property(
 *        property="point_balance",
 *        description="point_balance",
 *        type="integer"
 *      ),@OA\Property(
 *        property="telephone",
 *        description="telephone",
 *        type="integer"
 *      ),@OA\Property(
 *        property="capital",
 *        description="capital",
 *        type="integer"
 *      ),@OA\Property(
 *        property="fax",
 *        description="fax",
 *        type="string"
 *      ),@OA\Property(
 *        property="mansion_name",
 *        description="mansion_name",
 *        type="string"
 *      ),@OA\Property(
 *        property="no_of_employee",
 *        description="no_of_employee",
 *        type="integer"
 *      ),@OA\Property(
 *        property="no_of_branch",
 *        description="no_of_branch",
 *        type="integer"
 *      ),@OA\Property(
 *        property="business_content",
 *        description="business_content",
 *        type="integer"
 *      ),@OA\Property(
 *        property="sub_business_content",
 *        description="sub_business_content",
 *        type="integer"
 *      ),@OA\Property(
 *        property="zip_code",
 *        description="zip_code",
 *        type="integer"
 *      ),@OA\Property(
 *        property="prefecture_id",
 *        description="prefecture_id",
 *        type="integer"
 *      ),@OA\Property(
 *        property="address",
 *        description="address",
 *        type="string"
 *      ),@OA\Property(
 *        property="street_address",
 *        description="street_address",
 *        type="string"
 *      ),@OA\Property(
 *        property="related_url",
 *        description="company_name",
 *        type="string"
 *      ),@OA\Property(
 *        property="contact_person",
 *        description="contact_person",
 *        type="string"
 *      )
 *   )
 * )
 */
class ProviderSeekerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'established_year' => $this->established_year,
            'company_name' => $this->company_name,
            'ceo_name' => $this->ceo_name,
            'telephone' => $this->telephone,
            'capital' => $this->capital,
            'fax' => $this->fax,
            'mansion_name' => $this->mansion_name,
            'no_of_employee' => $this->no_of_employee,
            'no_of_branch' => $this->no_of_branch,
            'business_content' => $this->business_content,
            'sub_business_content' => $this->sub_business_content,
            'zip_code' => $this->zip_code,
            'prefecture_id' => $this->prefecture_id,
            'address' => $this->address,
            'street_address' => $this->street_address,
            'related_url' => $this->related_url,
            'contact_person' => $this->contact_person
        ];
    }
}
