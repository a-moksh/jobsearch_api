<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Http\Request;
class NotificationController extends Controller
{
    /**
     * @OA\Get(
     *     path="/user/notifications",
     *     description="get user notification",
     *     tags={"Common APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="success ",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/NotificationResource"
     *         )
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     */

    public function index(Request $request)
    {
        if (auth()->user()) {
            $notifications = auth()->user()->unreadNotifications;
            if($request->has('read')){
                $notifications = auth()->user()->notifications()->paginate();
            }
            return NotificationResource::collection($notifications)
                ->additional(['count' => auth()->user()->unreadNotifications()->count()]);
        } else {
            abort(403);
        }

    }

    /**
     * set status of mark read notification
     * @OA\Post(
     *     path="/user/notifications/{notification}",
     *     description="set status of mark read notification",
     *     tags={"Common APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="seeker notification id of database",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     */
    public function markRead(DatabaseNotification $notification)
    {
        if (!auth()->user()) {
            abort(403);
        }
        if (auth()->user()->id == $notification->notifiable_id) {
            $notification->markAsRead();
        }
    }
}
