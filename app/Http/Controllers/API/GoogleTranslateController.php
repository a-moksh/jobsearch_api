<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class GoogleTranslateController extends Controller
{

    /**
     * get translation
     * @param Request $request
     *
     * /**
     * @OA\Post(
     *     path="/provider/job/translations",
     *     description="job google translation",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="title_en",
     *                 description="title",
     *                 type="string",
     *             ),@OA\Property(
     *                 property="description_en",
     *                 description="description ",
     *                 type="string",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTranslation(Request $request)
    {

//        print_r($request->all());

        $translations['title_en'] = $request->get('title_ja');
        $translations['description_en'] = $request->get('description_ja');
        $translations['welfare_en'] = $request->get('welfare_ja');
        $translations['qualifications_en'] = $request->get('qualifications_ja');

        return response($translations, 200);
    }
}
