<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResendEmailVerificationRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Models\VerifyUser;
use App\Notifications\ResendEmailVerification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    use Notifiable;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        //
        $query = User::query();
        $query = $this->SearchOperation($query, $request);
        if ($request->filled('id')) {
            $query->whereIn('id', str_getcsv($request->input('id')));
        }

        return UserResource::collection($query->paginateByUrl());
    }

    /**
     * get user detail
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Get(
     *     path="/provider/user",
     *     description="Get Provider User",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="User Detail",
     *         @OA\JsonContent(ref="#/components/schemas/UserResource")
     *     ),@OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     */
    public function getProviderUser()
    {
        return new UserResource(auth()->user());
    }

    /**
     * get user detail
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Get(
     *     path="/seeker/user",
     *     description="verify uer email",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="User Detail",
     *         @OA\JsonContent(ref="#/components/schemas/UserResource")
     *     ),@OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     */
    public function getUser()
    {
        return new UserResource(auth()->user());
    }

    /**
     * get user detail from token
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/verify/email/pre",
     *     description="verify uer email pre",
     *     tags={"Common APIs"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *          @OA\Property(
     *                 property="token",
     *                 description="token from email",
     *                 type="string"
     *             )
     *         )
     *     ),@OA\Response(
     *         response=200,
     *         description="Email from token"
     *     ),@OA\Response(
     *         response=400,
     *         description="other conditional errors"
     *     ),@OA\Response(
     *         response=401,
     *         description="token expired"
     *     ),@OA\Response(
     *         response=404,
     *         description="not found"
     *     )
     * )
     */
    public function getUserFromEmailToken(Request $request)
    {
        //echo html_entity_decode($token);die;
        $verifyUser = VerifyUser::where('token', $request->input('token'))->first();
        if (!$verifyUser) {
            return response(null, 404);
        } else {
            $user = $verifyUser->user;
            //get time if valid or not diff 4hr
            if ($verifyUser->created_at->diffInHours(Carbon::now()) > 4) {
                VerifyUser::where('token', $request->input('token'))->delete();
                return response()->json(['message' => trans('auth.email.token_expired'), 'email' => $user->email], 401);
            } else {
                if ($user) {
                    if ($user->verified) {
                        return response([
                            'message' => trans('auth.email.alreadyVerified'),
                            'email' => $user->email,
                        ], 400);
                    } else {
                        if ($user->user_type == 'seeker' || $user->user_type == 'admin') {
                            return response([
                                'email' => $user->email,
                                'full_name' => $user->full_name,
                            ], 200);
                        } else {
                            return response([
                                'email' => $user->email,
                                'company_name' => $user->provider->company_name,
                            ], 200);
                        }
                    }
                } else {
                    return response(null, 404);
                }
            }
        }
    }

    /**
     *
     */
    public function ReSendVerificationEmail(ResendEmailVerificationRequest $request)
    {
        return DB::transaction(function () use ($request) {
            $email = $request->input('email');
            //find user first
            $user = User::where('email', $email)->first();
            if (!$user) {
                return response()->json(['message' => trans('auth.not_found')], 404);
            } else {
                if ($user->verified) {
                    return response()->json(['message' => trans('auth.email.alreadyVerified')], 400);
                } else {
                    //delete all previous token associated with that user
                    VerifyUser::where('user_id', $user->id)->delete();
                    $verifyUser = new VerifyUser(['user_id' => $user->id]);
                    $user->verifyUser()->save($verifyUser);

                    $user->notify(new ResendEmailVerification($user));
                    $response['message'] = trans('auth.email.resendSuccess');
                    $response['email'] = $user->email;
                    return response($response, 201);
                }
            }
        });
    }

    /**
     *
     */
    private function SearchOperation($query, $request)
    {
        foreach ((new User())->getSearchable() as $search_q) {
            if ($request->filled($search_q)) {
                $query->whereIn($search_q, str_getcsv($request->input($search_q)));
            }
        }
        return $query;
    }

}
