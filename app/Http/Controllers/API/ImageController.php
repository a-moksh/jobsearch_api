<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Image\ImageStoreRequest;
use App\Http\Requests\Image\JobApplyVideoRequest;
use App\Http\Requests\MediaPreSignedRequest;
use App\Models\Media;
use App\Models\ProviderTransaction;
use App\Models\Seeker;
use App\Models\SeekerJob;
use Aws\Sns\Message;
use Aws\Sns\MessageValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{

    public function testExists()
    {
        if (Storage::disk('s3_public')->exists('/jobs/35/155135186508rHk.jpeg')) {
            echo 'yes public';
        } else {
            echo 'no public';
        }
        echo "-----";
        if (Storage::disk('s3_private')->exists('/users/5/1550631177xxQcc.jpeg')) {
            echo 'yes private';
        } else {
            echo 'no private';
        }
    }

    /**
     * @param $key
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function index($key)
    {
        $image = explode('/', $key);
        if ($image[0] != 'jobs' && !auth()->user()) {
            abort(404, trans('errors.404'));
        }
        if ($image[0] != 'jobs') {
            /*$url = Storage::disk('s3_private')->temporaryUrl(
                $key, now()->addMinutes(2)
            );*/
            $content = Storage::disk('s3_private')->get($key);
        } else {
            /*$disk =  Storage::disk('s3_public');
            $adapter = $disk->getAdapter();
            $client = $adapter->getClient();
            $url = $client->getObjectUrl($adapter->getBucket(), $key);*/
            $content = Storage::disk('s3_public')->get($key);
        }
        //return $url;    
        return response($content)
            ->header('Content-Type', 'image/jpeg');
    }

    public function indexAdmin(Request $request, $key)
    {
        $content = Storage::disk('s3_private')->get($key);
        return response($content)
            ->header('Content-Type', 'image/jpeg');
    }

    public function indexProvider(Request $request, $key)
    {
        $image = explode('/', $key);
        $show = false;
        if ($image[0] == 'jobs') {
            $show = true;
        } else {
            if ($request->has('application_id')) {
                $application = SeekerJob::where('id', $request->get('application_id'))->first();
                if ($application && ProviderTransaction::where('foreign_id', $application->id)
                        ->where('provider_id', auth()->user()->provider->id)
                        ->where('foreign_table', 'seeker_jobs')->first()) {
                    $key = json_decode($application->user_data)->image;
                    $show = true;
                }
            }
            if ($request->has('seeker_id')) {
                if (ProviderTransaction::where('foreign_id', $request->get('seeker_id'))
                    ->where('provider_id', auth()->user()->provider->id)
                    ->where('foreign_table', 'seekers')->first()) {
                    $key = Seeker::find($request->get('seeker_id'))->user->userImage->media_value;
                    $show = true;
                }
            }
        }
        if ($show) {
            $content = Storage::disk('s3_public')->get($key);
            return response($content)
                ->header('Content-Type', 'image/jpeg');
        }
        abort(404);
    }

    /**
     * @param MediaPreSignedRequest $request
     * @return mixed
     */
    /**
    * @OA\Get(
    *     path="/media/pre-signed-url",
    *     description="get presigned url to upload media to aws",
    *     tags={"Common APIs"},
    *     security={{"JWT":{""}}},
    *     @OA\Parameter(
    *        name="extension",
    *        in="query",
    *        required=true,
    *        allowEmptyValue=false,
    *        description="extension of file only - png,jpg,jpeg || mp4,mov,MOV,MP4",
    *        @OA\Schema(
    *            type="string",
    *           )
    *     ),@OA\Parameter(
    *        name="type",
    *        in="query",
    *        required=true,
    *        allowEmptyValue=false,
    *        description="media type image or video",
    *        @OA\Schema(
    *            type="string",
    *           )
    *     ),@OA\Response(
    *         response=200,
    *         description="signed url",
    *     ),@OA\Response(
    *         response=403,
    *         description="Unauthorized",
    *         @OA\JsonContent(
    *             ref="#/components/schemas/Unauthenticated"
    *         )
    *     ),@OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
    * )
    *
    */
    public function getPreSignedUrlPrivate(MediaPreSignedRequest $request){

        $extension = ($request->input('extension') == 'jpeg') ? 'jpg' : strtolower($request->input('extension'));

        $s3 = Storage::disk('s3_private');
        $client = $s3->getDriver()->getAdapter()->getClient();
        $expiry = "+2 minutes";
        $key = "temp/" . auth()->user()->id . "/" . time() . str_random(5) . "." . $extension;
        $command = $client->getCommand('PutObject', [
            'Bucket' => config('app.s3_private_bucket'),
            'Key' => $key
        ]);

        $request = $client->createPresignedRequest($command, $expiry);
        //save to database before returning
        $response['signedUrl'] = (string)$request->getUri();
        $response['tempImage'] = $key;
        return $response;
    }

    /**
     * @param MediaPreSignedRequest $request
     * @return mixed
     */
    public function getPreSignedUrlPublic(MediaPreSignedRequest $request)
    {

        $extension = ($request->input('extension') == 'jpeg') ? 'jpg' : strtolower($request->input('extension'));
        $s3 = Storage::disk('s3_private');
        $client = $s3->getDriver()->getAdapter()->getClient();
        $expiry = "+2 minutes";
        $key = "temp/" . auth()->user()->id . "/" . time() . str_random(5) . "." . $extension;
        $command = $client->getCommand('PutObject', [
            'Bucket' => config('app.s3_private_bucket'),
            'Key' => $key
        ]);

        $request = $client->createPresignedRequest($command, $expiry);
        //save to database before returning
        $response['signedUrl'] = (string)$request->getUri();
        $response['tempImage'] = $key;
        return $response;
    }


    /**
     * @param MediaPreSignedRequest $request
     * @param Media $media
     * @return mixed
     */

    public function jobImage(MediaPreSignedRequest $request, Media $media)
    {

//        $file = $request->image;

        $s3 = Storage::disk('s3_public');
        $client = $s3->getDriver()->getAdapter()->getClient();
        $expiry = "+10 minutes";

//        $filename = md5_file($file).'.'.$file->getClientOriginalExtension();
        if ($request->has('job_id')) {
//            $path = "jobs/".$request->get('job_id').'/'.$filename;
//            $oldMedia = Media::where('foreign_id',$request->input('job_id'))
//                ->where('foreign_table','jobs')
//                ->get();
//            if($oldMedia->count() >= 5)
//                return response(trans('messages.imagelimit'),400);
//            $oldMedia = $oldMedia->filter(function ($value, $key)
//            use ($path) {
//                return $value->media_value == $path;
//            });
//            if($oldMedia->count() > 0)
//                return response(trans('messages.duplicateimage'),400);
//            $path = Storage::disk('s3_public')->putFileAs(
//                "jobs/".$request->get('job_id'), $file, $filename
//            );
//            $media = Media::create([
//                'foreign_id'=>$request->get('job_id'),
//                'foreign_table'=>'jobs',
//                'media_value'=>$path,
//                'media_type'=>'image',
//            ]);
//            return MediaResource::make($media);
        } else {
            $key = "temp/{$this->providerId}/" . time() . "." . $request->input('extension');
            $command = $client->getCommand('PutObject', [
                'Bucket' => env('AWS_BUCKET', 'static.stg.jobbank.jp'),
                'Key' => $key,
            ]);
//            $path = Storage::disk('s3_public')->putFileAs(
//                "temp/{$this->providerId}/".time(), $file, $filename
//            );
            $request = $client->createPresignedRequest($command, $expiry);
            //save to database before returning
            $response['signedUrl'] = (string)$request->getUri();
            $response['tempImage'] = $key;
            return $response;
        }
//        return response(['data'=>$media],201);
    }

    /**
     * @param ImageStoreRequest $request
     * @return mixed
     */
    public function image(ImageStoreRequest $request)
    {

        $file = $request->image;

        $filename = md5_file($file) . '.' . $file->getClientOriginalExtension();

        $oldImage = Media::where('foreign_id', auth()->user()->id)->where('foreign_table', 'users')->first();

        $path = Storage::disk('s3_public')->putFileAs(
            "users/" . auth()->user()->id, $file, $filename
        );
        return DB::transaction(function () use ($oldImage, $path) {
            if ($oldImage) {
                if (Storage::disk('s3_public')->exists($oldImage->media_value)) {
                    Storage::disk('s3_public')->delete($oldImage->media_value);
                }
                $oldImage->delete();
            }
            $mediaCreate = Media::create([
                'foreign_id' => auth()->user()->id,
                'foreign_table' => 'users',
                'media_value' => $path,
                'media_type' => 'image',
            ]);
            return response(['data' => $path, 'id' => $mediaCreate->id], 200);
        });
    }

    /**
     * @param Media $media
     * @return mixed
     */
    public function destroy(Media $media)
    {

        $data = DB::table($media->foreign_table)
            ->select('*')
            ->where('id', $media->foreign_id)
            ->first();
        if (!$data) {
            abort('403', trans('errors.403'));
        }
        $delete = false;

        if (auth()->user()->user_type == 'admin') {
            $delete = true;
        } else {
            if ($media->foreign_table == 'jobs' && $data->provider_id == auth()->user()->provider->id) {
                $delete = true;
            }
            if ($media->foreign_table == 'users' && $data->id == auth()->user()->id) {
                $delete = true;
            }
        }

        if (!$delete) {
            abort('403', trans('errors.403'));
        }
        return DB::transaction(function () use ($media) {
            Storage::disk('s3_public')->delete($media->media_value);
            $media->delete();
            return response(null, 204);
        });
    }

    /**
     * @param JobApplyVideoRequest $request
     * @param Media $media
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function jobApplyVideo(JobApplyVideoRequest $request, Media $media)
    {
        $file = $request->video;
        $filename = md5_file($file)
            . '.' . $file->getClientOriginalExtension();
        $path = Storage::disk('s3_public')->putFileAs(
            "temp/{$this->seekerId}/" . time(), $file, $filename
        );
        $media->media_value = $path;
        return response(['data' => $media], 201);
    }

    /**
     * @param $key
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function streamVideo($key)
    {
        //send media id and return video after validating if the user has rights
        $url = Storage::disk('s3_public')->temporaryUrl(
            $key, now()->addMinutes(10)
        );
        return response($url, 200);
    }


    /**
     * @param Request $request
     * @param $key
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function streamVideoProvider(Request $request, $key)
    {
        //        print_r($request->all());die;
        if ($request->has('application_id')) {
            $application = SeekerJob::where('id', $request->get('application_id'))->first();
            if ($application && ProviderTransaction::where('foreign_id', $application->id)
                    ->where('provider_id', auth()->user()->provider->id)
                    ->where('foreign_table', 'seeker_jobs')->first()) {
                $userdata = json_decode($application->user_data);
                $url = Storage::disk('s3_public')->temporaryUrl(
                    $userdata->video, now()->addMinutes(10)
                );
                return response($url, 200);
            }
        }
        abort(404);
//        if()
        // check provider transaction if user has paid and can access the video.

//        $url = Storage::disk('s3_public')->temporaryUrl(
//            $key, now()->addMinutes(30)
//        );
//        return response($url,200);
    }


    public function lamdaRequestVideo(Request $request)
    {
        // Instantiate the Message and Validator
        $message = Message::fromRawPostData();
        $validator = new MessageValidator();

        // Validate the message and log errors if invalid.
        try {
            $validator->validate($message);
        } catch (InvalidSnsMessageException $e) {
            // Pretend we're not here if the message is invalid.
            abort(404, 'SNS Message Validation Error: ' . $e->getMessage());
        }

        // Check the type of the message and handle the subscription.
        if ($message['Type'] === 'SubscriptionConfirmation') {
            // Confirm the subscription by sending a GET request to the SubscribeURL
            file_get_contents($message['SubscribeURL']);
        } else {
            if ($message['Type'] === 'Notification') {
                try {
                    $check_data = $message['Message'];
                    // Do whatever you want with the message body and data.
                    $file_name = time() . '.json';
                    //get the written data
                    $written_data = json_decode($check_data);
                    if ($written_data->detail->status == "COMPLETE") {
                        Storage::disk('local')->put('log/lamda/' . $file_name, $message['Message']);
                        $source = null;
                        $target = null;
                        $seeker_id = null;
                        $bucket = null;
                        //source video
                        $source = $written_data->detail->userMetadata->Key;
                        $bucket = $written_data->detail->userMetadata->Bucket;

                        if ($source != null) {
                            $arr = explode('/', $source);
                            $seeker_id = $arr[count($arr) - 2];
                        }

                        //target video
                        $video_extra_info = (is_array($written_data->detail->outputGroupDetails)) ? $written_data->detail->outputGroupDetails[0] : '';
                        foreach ($written_data->detail->outputGroupDetails as $outputGroupDetails) {
                            if (property_exists($outputGroupDetails,
                                    'playlistFilePaths') && $outputGroupDetails->type == 'HLS_GROUP') {
                                $target = $outputGroupDetails->playlistFilePaths[0];
                                break;
                            }
                        }
                        if ($source != null && $target != null && $seeker_id != null) {

                            //update the video url in database
                            $source_path = $source;
                            $target_path = explode($bucket . '/', $target)[1];
                            //find seeker job having source video path
                            $seeker_job = SeekerJob::where('seeker_id',
                                $seeker_id)->whereJsonContains('user_data->video', $source_path)->first();
                            if ($seeker_job) {
                                SeekerJob::where('seeker_id', $seeker_id)->whereJsonContains('user_data->video',
                                    $source_path)->update(['video_converted_details' => json_encode($video_extra_info)]);
                                SeekerJob::where('seeker_id', $seeker_id)->whereJsonContains('user_data->video',
                                    $source_path)->update(['user_data->video' => $target_path]);

                            }
                        }
                    } else {
                        //Storage::disk('local')->delete($file_name);
                    }
                } catch (\Exception $e) {
                }
            }
        }

        return response('ok', 200);
    }


    /**
     *
     */
    public function lamdaRequestImage(Request $request)
    {
        // Instantiate the Message and Validator
        $message = Message::fromRawPostData();
        $validator = new MessageValidator();

        // Validate the message and log errors if invalid.
        try {
            $validator->validate($message);
        } catch (InvalidSnsMessageException $e) {
            // Pretend we're not here if the message is invalid.
            abort(404, 'SNS Message Validation Error: ' . $e->getMessage());
        }

        // Check the type of the message and handle the subscription.
        if ($message['Type'] === 'SubscriptionConfirmation') {
            // Confirm the subscription by sending a GET request to the SubscribeURL
            file_get_contents($message['SubscribeURL']);
        } else {
            if ($message['Type'] === 'Notification') {
                try {
                    $written_data = $message['Message'];
                    Storage::disk('local')->put('log/lamda/image/' . time() . '.json', $message['Message']);
                    $check_data = json_decode($written_data);
                    // Do whatever you want with the message body and data.
                    $foreign_table = explode('/', $check_data->userMetadata->Key)[0];
                    $foreign_id = explode('/', $check_data->userMetadata->Key)[1];
                    $media_value = $check_data->userMetadata->Key;

                    $db_media = Media::where('foreign_id', $foreign_id)->where('foreign_table',
                        $foreign_table)->where('media_value', $media_value)->first();

                    if (property_exists($check_data, 'created') && $db_media) {
                        foreach ($check_data->created as $converted) {
                            $insert['media_value'] = $converted->Key;
                            $insert['media_type'] = 'image';
                            $insert['media_dimension'] = explode('.', explode('_', $converted->Key)[1])[0];
                            $insert['media_id'] = $db_media->id;
                            $db_media->converted()->insert($insert);
                        }
                    }
                } catch (\Exception $e) {
                    Storage::disk('local')->put('log/lamda/image/' . time() . '.json', $e);
                }
            }
        }
        return response('ok', 200);
    }
}
