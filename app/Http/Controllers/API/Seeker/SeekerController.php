<?php

namespace App\Http\Controllers\API\Seeker;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seeker\SeekerStoreRequest;
use App\Http\Requests\Seeker\SeekerUpdateRequest;
use App\Http\Resources\SeekerDetailProviderResource;
use App\Http\Resources\SeekerListAdminResource;
use App\Http\Resources\SeekerListResource;
use App\Http\Resources\SeekerResource;
use App\Models\Seeker;
use App\Models\SeekerEducation;
use App\Models\SeekerExperience;
use App\Models\SeekerMailSetting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SeekerController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @param Request $request
     * @return mixed
     */
    /**
     * @OA\Get(
     *     path="/provider/seekers",
     *     description="job seekers List provider",
     *     summary="List of Job seekers provider",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *        name="unopened",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="filter by opened/unopened",
     *        @OA\Schema(
     *            type="integer"
     *           )
     *        ),
     *     @OA\Parameter(
     *        name="employment_type",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="filter by employment_type",
     *        @OA\Schema(
     *            type="integer"
     *           )
     *        ),
     *     @OA\Parameter(
     *        name="experience_year",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="filter by experience_year",
     *        @OA\Schema(
     *            type="integer"
     *           )
     *        ),
     *     @OA\Parameter(
     *        name="japanese_language_level",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="filter by japanese_language_level",
     *        @OA\Schema(
     *            type="integer"
     *           )
     *        ),
     *     @OA\Parameter(
     *        name="english_language_level",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="filter english_language_level",
     *        @OA\Schema(
     *            type="integer"
     *           )
     *        ),
     *     @OA\Parameter(
     *        name="desired_job_category",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="filter desired_job_category",
     *        @OA\Schema(
     *            type="integer"
     *           )
     *        ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/SeekerListResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="?????",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */
    public function index(Request $request)
    {
//        $this->authorize('listOne');
        $query = Seeker::query();
        $query->join('users', 'users.id', 'seekers.user_id');
        $query->join('seeker_mail_settings', 'seeker_mail_settings.seeker_id', 'seekers.id');
        $query = $this->SearchOperation($query, $request);
        if ($this->userType == 'provider') {
            $query->where('users.verified', 1);
        }
        if ($request->filled('unopened') && $request->get('unopened') == 1) {
            $query->leftJoin('provider_transactions', 'seekers.id', 'provider_transactions.foreign_id');
            $query->where('provider_transactions.foreign_table', null);
            $query->where('provider_transactions.foreign_id', null);
        }
        if($request->filled('desired_annual_min_salary_range')){
            $query->where('seekers.desired_annual_min_salary_range', '<=',$request->input('desired_annual_min_salary_range'));
        }
        if($request->filled('experience_year')){
            $query->where('seekers.experience_year', '<=',$request->input('experience_year'));
        }
        $query->groupBy('seekers.id');
        return SeekerListResource::collection($query->paginateByUrl());
    }

    /**
     * @param Request $request
     * @return mixed
     */
    /**
     * @OA\Get(
     *     path="/provider/seekers/{id}",
     *     description="detail of job seeker",
     *     summary="Job seekers provider",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="seeker id",
     *          in="path",
     *          required=true,
     *         @OA\Schema(type="integer"),
     *          ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/SeekerDetailProviderResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="?????",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */
    public function showProvider(Seeker $seeker)
    {
        return new SeekerDetailProviderResource($seeker);
    }


    /**
     * @param Request $request
     * @return \App\Extensions\Http\Resources\Json\AnonymousResourceCollection
     */
    public function seekerListAdmin(Request $request)
    {
        $query = Seeker::query();
        $query->join('users', 'users.id', 'seekers.user_id');
        $query->join('seeker_mail_settings', 'seeker_mail_settings.seeker_id', 'seekers.id');
        $query = $this->SearchOperation($query, $request);
        $query->groupBy('seekers.id');
        return SeekerListAdminResource::collection($query->paginateByUrl());
    }

    public function store(SeekerStoreRequest $request)
    {
        $this->authorize('store');
        $data = $request->validated();
        $data['user_id'] = Auth::user()->id;
        $data['seeker_identifier_id'] = 'SEEKER';
        $seeker = Seeker::updateOrCreate(['user_id' => $data['user_id']], $data);
        return new SeekerResource($seeker);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seeker $seeker
     * @return \App\Http\Resources\SeekerResource
     *
     * @OA\Get(
     *     path="/seeker/seekers/{seeker}",
     *     description="show seeker info",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *          name="seeker",
     *          description="seeker id",
     *          in="path",
     *          required=true,
     *         @OA\Schema(type="integer"),
     *          ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     )
     * )
     */
    public function show(Seeker $seeker)
    {
        $this->authorize('show', $seeker);
        return new SeekerResource($seeker);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seeker $seeker
     * @return \App\Http\Resources\SeekerResource
     *
     * @OA\Put(
     *     path="/seeker/seekers/{id}",
     *     description="update seeker resume section",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *    @OA\Parameter(
    *        name="id",
    *        description="seeker id",
    *        in="path",
    *        required=true,
    *        @OA\Schema(type="integer")
    *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SeekerUpdateRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success update",
     *         @OA\JsonContent(ref="#/components/schemas/SeekerResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
     * )
     */
    public function update(SeekerUpdateRequest $request, Seeker $seeker)
    {
        $this->authorize('update', $seeker);
        $data = $request->validated();
        return DB::transaction(function () use ($request, $seeker, $data) {
            if (!$data['is_experienced']) {
                $data['experience'] = 0;
            };
            $seeker->fill($data)->save();
            return (new SeekerResource($seeker))->additional([
                'message' => trans('messages.updated')
            ]);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $query
     * @param $request
     * @return mixed
     */
    private function SearchOperation($query, $request)
    {
        $selectDefault = [
            'seekers.*',
            'users.dob',
            'users.katakana_full_name',
            'users.full_name',
            'users.email',
            'users.id as user_id',
            'users.gender',
            'users.last_login',
            'seeker_mail_settings.employment_type'
        ];
        $searchable = array_merge(
            (new Seeker())->getSearchable(),
            (new User())->getSearchable(),
            (new SeekerMailSetting())->getSearchable(),
            (new SeekerEducation())->getSearchable(),
            (new SeekerExperience())->getSearchable()
        );
        foreach ($searchable as $search_q) {
            if ($request->filled($search_q)) {
                $query->whereIn($search_q, str_getcsv($request->input($search_q)));
            }
        }
        if ($request->filled('graduation_status')) {
            $query->join('seeker_educations', 'seeker_educations.seeker_id', 'seekers.id');
            $selectDefault[] = 'seeker_educations.graduation_status';
        }
        if ($request->filled('annual_salary')) {
            $query->join('seeker_experiences', 'seeker_experiences.seeker_id', 'seekers.id');
            $selectDefault[] = 'seeker_experiences.annual_salary';
            $query->where('currently_working_here', 0);
        }
        if ($request->filled('skills')) {
            $query->join('seeker_skills', 'seeker_skills.seeker_id', 'seekers.id');
            $skill_search = explode('//', $request->input('skills'));
            $rawq = '( ';
            foreach ($skill_search as $key => $value) {
                if ($key > 0) {
                    $rawq = $rawq . ' or ';
                }
                $search_crit = json_decode($value);
                $rawq = $rawq . 'seeker_skills.skill_id = ' . $search_crit->skill_id;
            }
            $rawq = $rawq . ')';
            $query->whereRaw($rawq);
        }
        if ($request->filled('skillslevel')) {
            $query->join('seeker_skills', 'seeker_skills.seeker_id', 'seekers.id');
            $skill_search = explode('//', $request->input('skillslevel'));
            $rawq = '( ';
            foreach ($skill_search as $key => $value) {
                if ($key > 0) {
                    $rawq = $rawq . ' or ';
                }
                $search_crit = json_decode($value);
                $rawq = $rawq . 'seeker_skills.skill_id = ' . $search_crit->skill_id. ' and '. 'seeker_skills.level >= ' . $search_crit->level. ' and '. 'seeker_skills.deleted_at is null';
            }
            $rawq = $rawq . ')';
            $query->whereRaw($rawq);
        }
        if ($request->filled('employment_type')) {
            $empid = explode(',', $request->get('employment_type'));
            foreach ($empid as $id) {
                $query->whereRaw('FIND_IN_SET(' . $id . ',seeker_mail_settings.employment_type)');
            }
        }
        if ($request->filled('created_at')) {
            $query->whereMonth('seekers.created_at', '=', (
            new Carbon(
                $request->input('created_at')
            ))->month);
            $query->whereYear('seekers.created_at', '=', (
            new Carbon(
                $request->input('created_at')
            ))->year);
        }
        $query->select($selectDefault);
        return $query;
    }
}
