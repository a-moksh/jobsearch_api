<?php

namespace App\Http\Controllers\API\Seeker;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seeker\SeekerSkillBulkRequest;
use App\Http\Requests\Seeker\SeekerSkillStoreRequest;
use App\Http\Requests\Seeker\SeekerSkillUpdateRequest;
use App\Http\Resources\SeekerSkillResource;
use App\Models\SeekerSkill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SkillController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(['isSeeker']);
    }

    public function index(Request $request)
    {
        $query = SeekerSkill::query();

        $query->where('seeker_id', $this->seekerId);

        return SeekerSkillResource::collection($query->paginateByUrl());
    }


    public function store(SeekerSkillStoreRequest $request)
    {
        $data = $request->validated();
        $data['seeker_id'] = $this->seekerId;
        return DB::transaction(function () use ($data) {
            $data = SeekerSkill::create($data);
            return new SeekerSkillResource($data);
        });
    }


    public function show(SeekerSkill $seekerSkill)
    {
        if ($seekerSkill->seeker_id == $this->seekerId) {
            return new SeekerSkillResource($seekerSkill);
        }
    }


    public function update(SeekerSkillUpdateRequest $request, SeekerSkill $seekerSkill)
    {
        $data = $request->validated();
        if ($seekerSkill->seeker_id == $this->seekerId) {
            return DB::transaction(function () use ($seekerSkill, $data) {
                $seekerSkill->update($data);
                return new SeekerSkillResource($seekerSkill);
            });
        }
    }

    /**
     *
     * @param SeekerSkill $seekerSkill
     * @return \Illuminate\Http\Response
     *
     * @throws \Exception
     * @OA\Delete(
     *     path="/seeker/seeker-skills/{id}",
     *     description="Delete seekers skill",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="seeker skill id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No Content"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     */
    public function destroy(SeekerSkill $seekerSkill)
    {
        if ($seekerSkill->seeker_id == $this->seekerId) {
            return DB::transaction(function () use ($seekerSkill) {
                $seekerSkill->delete();
                return response(null, 204);
            });
        }
        abort(403, trans('errors.403'));
    }

    /**
     * @param  \App\Http\Requests\Seeker\SeekerSkillBulkRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Put(
     *     path="/seeker/seeker-skills-bulk",
     *     description="create seeker skills",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SeekerSkillBulkRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(ref="#/components/schemas/SeekerSkillResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function bulk(SeekerSkillBulkRequest $request, SeekerSkill $seekerSkill)
    {
        $data = $request->validated();
        return DB::transaction(function () use ($request, $data, $seekerSkill) {
            $skillArray = [];
            if ($request->has('skills')) {
                foreach ($request->skills as $skill) {
                    $skill = (object)$skill;
                    $oldSkill = $seekerSkill->where('skill_id', $skill->skill_id)
                        ->where('skill_type', $skill->skill_type)
                        ->where('level', $skill->level)
                        ->where('seeker_id', $this->seekerId)
                        ->first();
                    if ($oldSkill) {
                        array_push($skillArray, $oldSkill->id);
                    } else {
                        array_push($skillArray, SeekerSkill::create([
                            'skill_type' => $skill->skill_type,
                            'level' => $skill->level,
                            'skill_id' => $skill->skill_id,
                            'seeker_id' => $this->seekerId,
                        ])->id);
                    }
                }
            }
            $seekerSkill->whereNotIn('id', $skillArray)->where('seeker_id', $this->seekerId)->each(function (
                $item,
                $key
            ) {
                $item->delete();
            });
            return (SeekerSkillResource::collection(SeekerSkill::where('seeker_id',
                $this->seekerId)->get()))->additional([
                'message' => trans('messages.updated')
            ]);
        });
    }
}
