<?php

namespace App\Http\Controllers\API\Seeker;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seeker\SeekerRegisterRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Requests\VerifyUserEmailRequest;
use App\Http\Resources\UserResource;
use App\Models\Media;
use App\Models\Seeker;
use App\Models\SeekerMailSetting as SeekerSetting;
use App\Models\Sequence;
use App\Models\User;
use App\Models\VerifyUser;
use App\Notifications\PasswordChanged;
use App\Notifications\UserCreated;
use App\Notifications\SeekerAccountDeleted;
use App\Notifications\SeekerVerified;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SeekerUserController extends Controller
{
    use Notifiable;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SeekerRegisterRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/seeker/register",
     *     summary="Register seeker into system",
     *     description="create new seeker",
     *     tags={"Seeker APIs"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SeekerRegisterRequest")
     *     ),@OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),@OA\Property(
     *                 property="email",
     *                 description="email",
     *                 type="string",
     *             ),@OA\Property(
     *                 property="locale",
     *                 description="locale",
     *                 type="string",
     *             )
     *         )
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function store(SeekerRegisterRequest $request)
    {
        return DB::transaction(function () use ($request) {
            //prepare new user store
            $already_created = false;
            $data = $request->validated();
            $user = User::where('email', $data['email'])->where('user_type', 'seeker')->first();
            if ($user) {
                $user->update($data);
                $already_created = true;
                VerifyUser::where('user_id', $user->id)->delete();
            } else {
                $data['user_identifier_id'] = Sequence::getUserIdentifier();
                $user = User::create($data);
                //seeker table
                $seeker = new Seeker([
                    'seeker_identifier_id' => Sequence::getSeekerIdentifier()
                ]);
                $user->seeker()->save($seeker);
                //seeker setting table
                SeekerSetting::create(['seeker_id' => $user->seeker->id]);
            }

            //email verification setup
            $verifyUser = new VerifyUser(['user_id' => $user->id]);
            $user->verifyUser()->save($verifyUser);

            //fire events for register user;
            $user->notify(new UserCreated($user, app()->getLocale()));
            $response['message'] = trans('auth.register.success');
            $response['email'] = $user->email;
            $response['locale'] = app()->getLocale();
            return response($response, 201);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UserUpdateRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Put(
     *     path="/seeker/seeker-user/{user_id}",
     *     summary="update seeker user",
     *     description="update seeker user",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="user_id",
     *         description="user id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/UserUpdateRequest")
     *     ),@OA\Response(
     *         response=200,
     *         description="updated",
     *         @OA\JsonContent(ref="#/components/schemas/UserResource")
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function update(UserUpdateRequest $request, User $seeker_user)
    {

        if (auth()->user()->id !== $seeker_user->id) {
            abort(403);
        }
        $data = $request->validated();
        //check if image operation is needed or not first
        if ($request->input('newImage') == true) {
            //handle new image operation here
            if (Storage::disk('s3_private')->exists($request->input('tempImage'))) {
                $newImage = explode('/', $request->input('tempImage'));
                Storage::disk('s3_private')->move($request->input('tempImage'),
                    'users/' . auth()->user()->id . '/' . end($newImage));
                Storage::disk('s3_private')->delete($request->input('tempImage'));
                //delete all users previous image first 
                //get old medias
                $medias = Media::where('foreign_table', 'users')->where('foreign_id',
                    auth()->user()->id)->get()->toArray();
                foreach ($medias as $media) {
                    Storage::disk('s3_private')->delete($media['media_value']);
                }
                Media::where('foreign_table', 'users')->where('foreign_id', auth()->user()->id)->delete();
                Media::create([
                    'foreign_id' => auth()->user()->id,
                    'foreign_table' => 'users',
                    'media_value' => 'users/' . auth()->user()->id . '/' . end($newImage),
                    'media_type' => 'image',
                ]);
            }
        }
        if ($request->input('removeImage') == true) {
            //remove image here
            //get old medias
            $medias = Media::where('foreign_table', 'users')->where('foreign_id', auth()->user()->id)->get()->toArray();
            foreach ($medias as $media) {
                Storage::disk('s3_private')->delete($media['media_value']);
            }
            Media::where('foreign_table', 'users')->where('foreign_id', auth()->user()->id)->delete();
        }
        return DB::transaction(function () use ($request, $seeker_user, $data) {
            $token = '';
            if ($request->input('change_password') == true) {
                $user_previous = auth()->user();
                $credentials = [
                    'email' => $user_previous->email,
                    'password' => $data['password'],
                    'user_type' => $user_previous->user_type
                ];
                $data['password'] = bcrypt($data['password']);
                auth()->user()->notify(new PasswordChanged(auth()->user(), app()->getLocale()));

                //\JWTAuth::invalidate(\JWTAuth::getToken());
            }
            $seeker_user->fill($data)->save();
            if ($request->input('change_password') == true) {
                //$token = \JWTAuth::attempt($credentials);
            }

            if ($seeker_user->user_type == 'seeker') {
                $seeker = $seeker_user->seeker;
                $seeker->fill($request->validated());
                $seeker->save();
            }
            return (new UserResource($seeker_user))->additional([
                'message' => trans('messages.updated'),
                //'access_token' => $token
            ]);
        });
    }

    /**
     * Verify user Activation
     *
     * @param  \App\Http\Requests\VerifyUserEmailRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/seeker/verify/email",
     *     description="verify uer email",
     *     tags={"Seeker APIs"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/VerifyUserEmailRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful verification of email",
     *     ),@OA\Response(
     *         response=402,
     *         description="Not Authorized",
     *     ),@OA\Response(
     *         response=401,
     *         description="Token Expired",
     *     ),@OA\Response(
     *         response=400,
     *         description="already verified or error",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
     * )
     */
    public function verifyUserEmail(VerifyUserEmailRequest $request)
    {
        //only verify seeker email
        $verifyUser = VerifyUser::where('token', urldecode($request->input('token')))->first();
        if (!$verifyUser) {
            return response()->json(['message' => trans('auth.email.token_expired'), 'email' => ''], 401);
        }
        if ($verifyUser->created_at->diffInHours(Carbon::now()) > 4) {
            VerifyUser::where('token', $request->input('token'))->delete();
            return response()->json(['message' => trans('auth.email.token_expired'), 'email' => ''], 401);
        }
        $user = $verifyUser->user;
        if (!$user->verified && $user->email == $request->get('email') && $verifyUser->token == $request->get('token') && $user->user_type == 'seeker') {
            return DB::transaction(function () use ($request, $user, $verifyUser) {
                $verifyUser->user->verified = 1;
                $user->password = bcrypt($request->input('password'));
                $user->full_name = $request->input('full_name');
                $user->katakana_full_name = $request->input('katakana_full_name');
                $user->dob = $request->input('dob');
                $user->gender = $request->input('gender');
                $user->mobile = $request->input('mobile');
                $user->receive_mail_language = $request->input('receive_mail_language');
                $user->save();
                if ($user->user_type == 'seeker') {
                    $seeker = $user->seeker;
                    $seeker->fill($request->validated());
                    $seeker->save();
                }
                $user->notify(new SeekerVerified($user, $seeker, app()->getLocale()));
                $message = trans('auth.email.verified');
                return response(['message' => $message], 200);
            });
        } else {
            if ($user->user_type != 'seeker') {
                $message = trans('auth.email.notAuthorized');
                return response(['message' => $message], 402);
            } else {
                if ($user->verified && $user->email) {
                    $message = trans('auth.email.alreadyVerified');
                } else {
                    $message = trans('auth.tokenFailed');
                }
            }
            return response(['message' => $message], 400);
        }
    }

    /**
     * delete created resource from storage.
     *
     * @param  \App\Http\Requests
     * @return 
     *
     * @OA\Post(
     *     path="/seeker/delete/{user_id}",
     *     summary="delete seeker user",
     *     description="delete seeker user",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="account delete success",
     *     )
     * )
     */
    public function deleteAccount(User $user)
    {
        if(auth()->user()->id != $user->id)
            abort(404, trans('errors.404'));
        return DB::transaction(function () use ($user) {
            $user->seeker->skills()->delete();
            $user->seeker->educations()->delete();
            $user->seeker->certificates()->delete();
            $user->seeker->mailsetting()->delete();
            $user->seeker->experiences()->delete();
            $user->seeker->delete();
            $user->notify(new SeekerAccountDeleted($user, $user->receive_mail_language));
            $user->delete();
            return response(['message' => trans('messages.seeker_account_deleted')], 200);
        });
    }
}
