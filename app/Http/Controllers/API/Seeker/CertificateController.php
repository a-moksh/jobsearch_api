<?php

namespace App\Http\Controllers\API\Seeker;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seeker\SeekerCertificateBulkRequest;
use App\Http\Requests\Seeker\SeekerCertificateStoreRequest;
use App\Http\Requests\Seeker\SeekerCertificateUpdateRequest;
use App\Http\Resources\SeekerCertificateResource;
use App\Models\SeekerCertificate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CertificateController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware(['isSeeker']);
    }


    public function index(Request $request)
    {
        $query = SeekerCertificate::query();
        $query->where('seeker_id', $this->seekerId);
        return SeekerCertificateResource::collection($query->paginateByUrl());
    }


    public function store(SeekerCertificateStoreRequest $request)
    {
        $data = $request->validated();
        $data['seeker_id'] = $this->seekerId;
        return DB::transaction(function () use ($data) {
            $data = SeekerCertificate::create($data);
            return (new SeekerCertificateResource($data))->additional([
                'message' => trans('messages.created')
            ]);
        });
    }


    public function show(SeekerCertificate $seekerCertificate)
    {
        if ($seekerCertificate->seeker_id == $this->seekerId) {
            return new SeekerCertificateResource($seekerCertificate);
        }
    }


    public function update(SeekerCertificateUpdateRequest $request, SeekerCertificate $seekerCertificate)
    {
        $data = $request->validated();
        if ($seekerCertificate->seeker_id == $this->seekerId) {
            return DB::transaction(function () use ($seekerCertificate, $data) {
                $seekerCertificate->update($data);
                return new SeekerCertificateResource($seekerCertificate);
            });
        }
    }

    /**
     *
     * @param SeekerCertificate $seekerCertificate
     * @return \Illuminate\Http\Response
     *
     * @throws \Exception
     * @OA\Delete(
     *     path="/seeker/seeker-certificates/{id}",
     *     description="Delete seekers certificate",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="seeker certificate id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),@OA\Response(
     *         response=204,
     *         description="No Content"
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     */
    public function destroy(SeekerCertificate $seekerCertificate)
    {
        if ($seekerCertificate->seeker_id == $this->seekerId) {
            return DB::transaction(function () use ($seekerCertificate) {
                $seekerCertificate->delete();
                return response(null, 204);
            });
        }
        abort(403, trans('errors.403'));
    }

    /**
     *
     * @param  \App\Http\Requests\Seeker\SeekerCertificateBulkRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Put(
     *     path="/seeker/seeker-certificates-bulk",
     *     description="create seeker certificates",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SeekerCertificateBulkRequest")
     *     ),@OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(ref="#/components/schemas/SeekerCertificateResource")
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function bulk(SeekerCertificateBulkRequest $request)
    {
        $data = $request->validated();
        return DB::transaction(function () use ($request, $data) {
            if ($request->has('certificates')) {
                foreach ($request->certificates as $certificate) {
                    $certificate = (object)$certificate;
                    if (empty($certificate->id)) {
                        SeekerCertificate::create([
                            'certificate_name' => $certificate->certificate_name,
                            'year' => $certificate->year,
                            'seeker_id' => $this->seekerId,
                        ]);
                    } else {
                        $oldCertificate = SeekerCertificate::where('id', $certificate->id)->where('seeker_id',
                            $this->seekerId)->first();
                        if ($oldCertificate) {
                            $oldCertificate->fill([
                                'certificate_name' => $certificate->certificate_name,
                                'year' => $certificate->year,
                            ])->save();
                        } else {
                            abort(403, trans('errors.403'));
                        }
                    }
                }
            }
            return (SeekerCertificateResource::collection(SeekerCertificate::where('seeker_id',
                $this->seekerId)->get())->additional([
                'message' => trans('messages.updated')
            ]));
        });
    }
}
