<?php

namespace App\Http\Controllers\API\Seeker;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seeker\SeekerMailSettingUpdateRequest;
use App\Http\Resources\SeekerMailSettingResource;
use App\Models\SeekerMailSetting;
use Illuminate\Support\Facades\DB;

class MailSettingController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(['isSeeker']);
    }

    /**
     * update a seeker mail setting.
     * seeker has one setting(so update method only)
     *
     * @OA\Put(
     *     path="/seeker/seeker-mail-settings/{id}",
     *     description="Update seeker mail setting ",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="seeker id",
     *          in="path",
     *          required=true,
     *         @OA\Schema(type="integer"),
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SeekerMailSettingUpdateRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="updated",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function update(SeekerMailSettingUpdateRequest $request, SeekerMailSetting $seekerMailSetting)
    {
        $datas = $request->validated();
        foreach ($datas as $key => $data) {
            if (is_array($data)) {
                $datas[$key] = implode(',', $data);
            };
        };
        if ($seekerMailSetting->seeker_id == $this->seekerId) {
            return DB::transaction(function () use ($seekerMailSetting, $datas) {
                $seekerMailSetting->update($datas);
                return (new SeekerMailSettingResource($seekerMailSetting))->additional([
                    'message' => trans('messages.updated')
                ]);
            });
        } else {
            abort(403);
        }
    }

    public function show(SeekerMailSetting $seekerMailSetting)
    {
        if ($seekerMailSetting->seeker_id == $this->seekerId) {
            return new SeekerMailSettingResource($seekerMailSetting);
        }
        abort(404);
    }
}
