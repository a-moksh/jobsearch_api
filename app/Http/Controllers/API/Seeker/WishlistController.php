<?php

namespace App\Http\Controllers\API\Seeker;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seeker\SeekerWishlistStoreRequest;
use App\Http\Resources\SeekerWishlistResource;
use App\Models\SeekerWishlist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WishlistController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware(['isSeeker']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    /**
     * @OA\Get(
     *     path="/seeker/wishlists",
     *     description="Seeker wishlist List",
     *     summary="List of seeker wishlist",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/SeekerWishlistResource"
     *         )
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */
    public function index(Request $request)
    {
        $query = SeekerWishlist::query();
        $query->where('seeker_wishlists.seeker_id', $this->seekerId);
        $query = $query->whereHas('job', function ($job_query) use ($request) {
            $job_query->where('published_at', '>', Carbon::now()->subDays(90));
            $job_query->where('admin_status', 0);
            $job_query->where('status', 1);
        });
        return SeekerWishlistResource::collection($query->paginateByUrl())->translateLocales();
    }

    public function store(SeekerWishlistStoreRequest $request)
    {
        $data = $request->validated();
        $data['seeker_id'] = $this->seekerId;
        try {
            return DB::transaction(function () use ($data) {
                $data = SeekerWishlist::create($data);
                return new SeekerWishlistResource($data);
            });
        } catch (\Exception $e) {
            return response('errors.duplicate_entry');
        }
    }

    public function destroy(SeekerWishlist $wishlist)
    {
        if ($wishlist->seeker_id == $this->seekerId) {
            return DB::transaction(function () use ($wishlist) {
                $wishlist->delete();
                return response(null, 204);
            });
        }
    }


    /**
     * Add remove seeker wishlist
     *
     * @param SeekerWishlistStoreRequest $seekerWishlist
     * @return \Illuminate\Http\Response
     *
     * @OA\Post(
     *     path="/seeker/wishlists/job",
     *     description="seeker wishlist add remove",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SeekerWishlistStoreRequest")
     *     ),@OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(ref="#/components/schemas/SeekerWishlistResource")
     *     ),@OA\Response(
     *         response=204,
     *         description="No Content"
     *     ),@OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function wishlistJob(SeekerWishlistStoreRequest $request)
    {
        $data = $request->validated();
        $data['seeker_id'] = $this->seekerId;
        $wishlist = SeekerWishlist::where('job_id', $data['job_id'])
            ->where('seeker_id', $this->seekerId)->first();
        return DB::transaction(function () use ($wishlist, $data) {
            if (isset($wishlist) > 0) {
                $wishlist->delete();
                return response(null, 204);
            } else {
                $data = SeekerWishlist::create($data);
                return (new SeekerWishlistResource($data))->additional([
                    'message' => trans('messages.job_wishlisted')
                ]);
            }
        });
    }
}
