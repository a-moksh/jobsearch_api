<?php

namespace App\Http\Controllers\API\Seeker;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seeker\SeekerEducationBulkRequest;
use App\Http\Requests\Seeker\SeekerEducationStoreRequest;
use App\Http\Requests\Seeker\SeekerEducationUpdateRequest;
use App\Http\Resources\SeekerEducationResource;
use App\Models\SeekerEducation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EducationController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(['isSeeker']);
    }

    public function index(Request $request)
    {
        $query = SeekerEducation::query();

        $query->where('seeker_id', $this->seekerId);

        return SeekerEducationResource::collection($query->paginateByUrl());
    }


    public function store(SeekerEducationStoreRequest $request)
    {
        $data = $request->validated();
        $data['seeker_id'] = $this->seekerId;
        return DB::transaction(function () use ($data) {
            $data = SeekerEducation::create($data);
            return response(new SeekerEducationResource($data));
        });
    }


    public function show(SeekerEducation $seekerEducation)
    {
        if ($seekerEducation->seeker_id == $this->seekerId) {
            return new SeekerEducationResource($seekerEducation);
        }
    }


    public function update(SeekerEducationUpdateRequest $request, SeekerEducation $seekerEducation)
    {
        $data = $request->validated();
        if ($seekerEducation->seeker_id == $this->seekerId) {
            return DB::transaction(function () use ($seekerEducation, $data) {
                $seekerEducation->update($data);
                return new SeekerEducationResource($seekerEducation);
            });
        }
    }

    /**
     * Remove the specified seeker education from storage.
     *
     * @param SeekerEducation $seekerEducation
     * @return \Illuminate\Http\Response
     *
     * @throws \Exception
     * @OA\Delete(
     *     path="/seeker/seeker-educations/{id}",
     *     description="Delete seekers education",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="seeker education id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),@OA\Response(
     *         response=204,
     *         description="No Content"
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     */
    public function destroy(SeekerEducation $seekerEducation)
    {
        if ($seekerEducation->seeker_id == $this->seekerId) {
            return DB::transaction(function () use ($seekerEducation) {
                $seekerEducation->delete();
                return response(null, 204);
            });
        }
        abort(403, trans('errors.403'));
    }

    /**
     * Store a newly created seeker education in storage.
     *
     * @param  \App\Http\Requests\Seeker\SeekerEducationBulkRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Put(
     *     path="/seeker/seeker-educations-bulk",
     *     description="create seeker educations",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SeekerEducationBulkRequest")
     *     ),@OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(ref="#/components/schemas/SeekerEducationResource")
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function bulk(SeekerEducationBulkRequest $request)
    {
        $data = $request->validated();
        return DB::transaction(function () use ($request, $data) {
            if ($request->has('educations')) {
                foreach ($request->educations as $education) {
                    $education = (object)$education;
                    if (empty($education->id)) {
                        SeekerEducation::create([
                            'university_institute' => $education->university_institute,
                            'degree' => $education->degree,
                            'faculty' => $education->faculty,
                            'graduation_country' => $education->graduation_country,
                            'graduation_status' => $education->graduation_status,
                            'graduation_year' => $education->graduation_year,
                            'seeker_id' => $this->seekerId,
                        ]);
                    } else {
                        $oldEducaton = SeekerEducation::where('id', $education->id)->where('seeker_id',
                            $this->seekerId)->first();
                        if ($oldEducaton) {
                            $oldEducaton->fill([
                                'university_institute' => $education->university_institute,
                                'degree' => $education->degree,
                                'faculty' => $education->faculty,
                                'graduation_country' => $education->graduation_country,
                                'graduation_status' => $education->graduation_status,
                                'graduation_year' => $education->graduation_year,
                            ])->save();
                        } else {
                            abort(403, trans('errors.403'));
                        }
                    }
                }
            }
            return (SeekerEducationResource::collection(SeekerEducation::where('seeker_id',
                $this->seekerId)->get())->additional([
                'message' => trans('messages.updated')
            ]));
        });
    }
}
