<?php

namespace App\Http\Controllers\API\Seeker;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seeker\SeekerExperienceBulkRequest;
use App\Http\Requests\Seeker\SeekerExperienceStoreRequest;
use App\Http\Requests\Seeker\SeekerExperienceUpdateRequest;
use App\Http\Resources\SeekerExperienceResource;
use App\Models\SeekerExperience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExperienceController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(['isSeeker']);
    }


    public function index(Request $request)
    {
        $query = SeekerExperience::query();

        $query->where('seeker_id', $this->seekerId);

        return SeekerExperienceResource::collection($query->paginateByUrl());
    }


    public function store(SeekerExperienceStoreRequest $request)
    {
        $data = $request->validated();
        $data['seeker_id'] = $this->seekerId;
        $data = SeekerExperience::create($data);
        return new SeekerExperienceResource($data);
    }


    public function show(SeekerExperience $seekerExperience)
    {
        if ($seekerExperience->seeker_id == $this->seekerId) {
            return new SeekerExperienceResource($seekerExperience);
        }
    }


    public function update(SeekerExperienceUpdateRequest $request, SeekerExperience $seekerExperience)
    {
        $data = $request->validated();
        if ($seekerExperience->seeker_id == $this->seekerId) {
            return DB::transaction(function () use ($seekerExperience, $data) {
                $seekerExperience->update($data);
                return new SeekerExperienceResource($seekerExperience);
            });
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SeekerExperience $seekerExperience
     * @return \Illuminate\Http\Response
     *
     * @throws \Exception
     * @OA\Delete(
     *     path="/seeker/seeker-experiences/{id}",
     *     description="Delete seekers experience",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="seeker experience id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),@OA\Response(
     *         response=204,
     *         description="No Content"
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     */
    public function destroy(SeekerExperience $seekerExperience)
    {
        if ($seekerExperience->seeker_id == $this->seekerId) {
            return DB::transaction(function () use ($seekerExperience) {
                $seekerExperience->delete();
                return response(['message' => trans('messages.deleted')], 204);
            });
        }
        abort(403, trans('errors.403'));
    }

    /**
     * seeker experience bulk request
     *
     * @param  \App\Http\Requests\Seeker\SeekerExperienceBulkRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Put(
     *     path="/seeker/seeker-experiences-bulk",
     *     description="create seeker experiences",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SeekerExperienceBulkRequest")
     *     ),@OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(ref="#/components/schemas/SeekerExperienceResource")
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function bulk(SeekerExperienceBulkRequest $request)
    {
        $data = $request->validated();
        return DB::transaction(function () use ($request, $data) {
            if ($request->has('experiences')) {
                foreach ($request->experiences as $experience) {
                    $experience = (object)$experience;
                    if ($experience->currently_working_here == 1) {
                        $experience->job_to = null;
                    }
                    if (empty($experience->id)) {
                        SeekerExperience::create([
                            'company_name' => $experience->company_name,
                            'job_title' => $experience->job_title,
                            'job_from' => $experience->job_from,
                            'job_to' => $experience->job_to,
                            'annual_salary' => isset($experience->annual_salary) ? $experience->annual_salary : 0,
                            'currently_working_here' => isset($experience->currently_working_here) ? $experience->currently_working_here : 1,
                            'job_description' => $experience->job_description,
                            'seeker_id' => $this->seekerId,
                        ]);
                    } else {
                        $oldExperience = SeekerExperience::where('id', $experience->id)->where('seeker_id',
                            $this->seekerId)->first();
                        if ($oldExperience) {
                            $oldExperience->fill([
                                'company_name' => $experience->company_name,
                                'job_title' => $experience->job_title,
                                'job_from' => $experience->job_from,
                                'job_to' => $experience->job_to,
                                'currently_working_here' => isset($experience->currently_working_here) ? $experience->currently_working_here : 1,
                                'annual_salary' => isset($experience->annual_salary) ? $experience->annual_salary : '',
                                'job_description' => $experience->job_description,
                            ])->save();
                        } else {
                            abort(403, trans('errors.403'));
                        }
                    }
                }
            }
            return (SeekerExperienceResource::collection(SeekerExperience::where('seeker_id',
                $this->seekerId)->get())->additional([
                'message' => trans('messages.updated')
            ]));
        });
    }
}
