<?php

namespace App\Http\Controllers\API\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Provider\ProviderStoreRequest;
use App\Http\Requests\Provider\ProviderUpdateRequest;
use App\Http\Resources\ProviderResource;
use App\Models\Provider;
use App\Models\User;
use App\Notifications\PasswordChanged;
use App\Notifications\ProviderAccountDeleted;
use App\Notifications\ProviderAccountDeletedProvider;
use App\Notifications\ProviderProfileUpdated;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProviderController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @param Request $request
     * @return mixed
     */
    /**
     * @OA\Get(
     *     path="/providers",
     *     description="job Provider List",
     *     summary="List of Job Provider",
     *     tags={"JOB Provider API"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         required=false,
     *         allowEmptyValue=true,
     *         description="job Provider Id",
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="text",
     *         description="free text search",
     *         in="query",
     *         required=false,
     *         @OA\Schema(type="string")
     *       ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="メッセージ",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */
    public function index(Request $request)
    {
        //$request->normalize();
        //$this->authorize('list');
        $query = Provider::query();
        $query = $this->SearchOperation($query, $request);
        $query->with(['createdBy', 'updatedBy', 'user']);
        $query->groupBy('providers.id');
        return ProviderResource::collection($query->paginateByUrl())->translateLocales();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Provider\ProviderStoreRequest
     * @param Provider $provider
     * @return ProviderResource
     *
     * @OA\Post(
     *     path="/providers",
     *     description="create new job Provider",
     *     tags={"JOB Provider API"},
     *    security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/ProviderStoreRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Created",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function store(ProviderStoreRequest $request, Provider $provider)
    {
        $this->authorize('store');
        $value = $request->validated();
        $value['provider_identifier_id'] = str_random(10); //Sequence::getProviderIdentifier();

        return DB::transaction(function () use ($provider, $value) {
            $value['company_name'] = [
                'en' => $value['about_company_en'],
                'ja' => $value['about_company_ja'],
            ];
            $value['about_company'] = [
                'en' => $value['company_name_en'],
                'ja' => $value['company_name_ja'],
            ];
            $value['contact_person'] = [
                'en' => $value['contact_person_en'],
                'ja' => $value['contact_person_ja'],
            ];

            $provider = Provider::createTranslationLocales($value);

            $provider->refresh();
            return new ProviderResource($provider);
        });

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Provider $jobProvider
     * @return void
     */

    public function show(Provider $provider)
    {
//        $this->authorize('show', $provider);
        if ($provider->id !== auth()->user()->provider->id) {
            abort(403, trans('errors.403'));
        }
        return (new ProviderResource($provider))->translateLocales();
    }

    /**
     * Update the specified resource in storage.
     * @param ProviderUpdateRequest $request
     * @param Provider $provider
     *
     * /**
     * @OA\Put(
     *     path="/provider/providers/{id}",
     *     description="update specific  Job Provider",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="provider id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\requestBody(
     *         @OA\JsonContent(ref="#/components/schemas/ProviderUpdateRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\Schema(
     *             @OA\Property(
     *                 property="data",
     *                 ref="#/components/schemas/ProviderWithTimestamp"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
     * )
     * @return ProviderResource
     */
    public function update(ProviderUpdateRequest $request, Provider $provider)
    {
        if ($provider->user_id != $this->userId) {
            abort(403, trans('errors.403'));
        }
        $values = $request->validated();
        return DB::transaction(function () use ($provider, $values, $request) {
            $values['company_name'] = [
                'en' => $values['company_name_en'],
                'ja' => $values['company_name_ja'],
            ];
            $values['contact_person'] = [
                'en' => $values['contact_person_en'],
                'ja' => $values['contact_person_ja'],
            ];
            $values['ceo_name'] = [
                'en' => $values['ceo_name_ja'],
                'ja' => $values['ceo_name_en'],
            ];
            $provider->fill($values)->saveTranslationLocales();

            $user_previous = auth()->user();
            if ($request->has('password')) {
                $user_previous->password = bcrypt($request->input('password'));
                $user_previous->save();
                $user_previous->notify(new PasswordChanged($user_previous, app()->getLocale()));
            }
            // $user_previous->notify(new ProviderProfileUpdated(app()->getLocale()));
            return (new ProviderResource($provider))->translateLocales()->additional([
                'message' => trans('messages.updated')
            ]);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Provider $provider
     * @return void
     */
    /**
     * @OA\Delete(
     *     path="/provider/{id}",
     *     description="delete specific provider delete",
     *     tags={"JOB Provider API"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="provider id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *      @OA\Response(
     *         response=204,
     *         description="No Content",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         ),
     *      )
     * )
     *
     */
    public function destroy(Provider $provider)
    {
        if ($provider->user_id != $this->userId) {
            abort(403, trans('errors.403'));
        }
        return DB::transaction(function () use ($provider) {


            $applications = $provider->applications;
            foreach ($applications as $application) {
                $application->delete();
            }
            $provider->scoutmails()->delete();
            $provider->jobs()->delete();
            $provider->user->notify(new ProviderAccountDeletedProvider($provider, app()->getLocale()));
            $provider->user->delete();
            $provider->delete();
            $userAdmin = User::where('user_type', 'admin')->get();
            foreach ($userAdmin as $admin) {
                $admin->notify(new ProviderAccountDeleted($provider, app()->getLocale()));
            }
            return response(['message' => trans('message.account_deleted')], 204);
        });
    }

    private function SearchOperation($query, $request)
    {
        $searchable = array_merge(
            (new Provider())->getSearchable(),
            (new User())->getSearchable()
        );
        foreach ((new Provider())->getSearchable() as $search_q) {
            if ($request->filled($search_q)) {
                $query->whereIn($search_q, str_getcsv($request->input($search_q)));
            }
        }
        if ($request->filled('id')) {
            $query->whereIn('id', str_getcsv($request->input('id')));
        }
        if ($request->filled('company_name') || $request->filled('contact_person')) {
            $query->join('languages', 'providers.id', '=', 'languages.foreign_id');
            $query->select(
                'providers.*',
                'languages.table_name',
                'languages.column_name',
                'languages.foreign_id',
                'languages.locale_text'
            );
            $query->where('languages.table_name', 'providers');
            $c_name_q = '';
            $c_person_q = '';
            if ($request->filled('company_name')) {
                $c_name_q = '( ';
                $c_name_q = $c_name_q . "languages.column_name = 'company_name' AND ";
                $c_name_q = $c_name_q . "languages.locale_text LIKE '%" . $request->input('company_name') . "%'";
                $c_name_q = $c_name_q . ' )';
            }
            if ($request->filled('contact_person')) {
                $c_person_q = '( ';
                $c_person_q = $c_person_q . "languages.column_name = 'contact_person' AND ";
                $c_person_q = $c_person_q . "languages.locale_text LIKE '%" . $request->input('contact_person') . "%'";
                $c_person_q = $c_person_q . ' )';
            }
            $q_final = '';
            if ($c_name_q != '' && $c_person_q != '') {
                $q_final = '(' . $c_name_q . ' OR ' . $c_person_q . ')';
            } else {
                $q_final = '(' . $c_name_q . ' ' . $c_person_q . ')';
            }
            $query->whereRaw($q_final);

        }
        if ($request->filled('created_at')) {
            $query->whereMonth('providers.created_at', '=', (
            new Carbon(
                $request->input('created_at')
            ))->month);
            $query->whereYear('providers.created_at', '=', (
            new Carbon(
                $request->input('created_at')
            ))->year);
        }
        return $query;
    }
}
