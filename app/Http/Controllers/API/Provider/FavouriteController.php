<?php

namespace App\Http\Controllers\API\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Provider\FavouriteStoreRequest;
use App\Http\Resources\FavouriteListResource;
use App\Http\Resources\FavouriteResource;
use App\Models\Favourite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FavouriteController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    /**
     * @OA\Get(
     *     path="/provider/favourites",
     *     description="Provider Favourites List",
     *     summary="List of provider favourites",
     *     tags={"Provider Favourites API"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *        name="opened",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="filter by opened/unopened",
     *        @OA\Schema(
     *            type="boolean"
     *           )
     *        ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/FavouriteListResource"),
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */
    public function index(Request $request)
    {
        $query = Favourite::query();

        $query->join('seekers', 'seekers.id', 'favourites.seeker_id');
        $query->join('seeker_mail_settings', 'seeker_mail_settings.seeker_id', 'favourites.seeker_id');
        $query->join('users', 'seekers.user_id', 'users.id');
        $query->select('favourites.id as id', 'favourites.seeker_id as seeker_id', 'favourites.created_at',
            'seekers.english_language_level', 'seekers.japanese_language_level', 'seekers.prefecture_id',
            'seekers.visa_status', 'seekers.visa_expiry_date', 'seekers.seeker_identifier_id',
            'users.katakana_full_name', 'users.last_login', 'seeker_mail_settings.employment_type');

        $query->where('favourites.provider_id', $this->providerId);
        if ($request->filled('unopened') && $request->get('unopened') == 1) {
            $query->leftJoin('provider_transactions', 'seekers.id', 'provider_transactions.foreign_id');
//            $query->join('scout_mails','seekers.id','scout_mails.seeker_id');
            $query->where('provider_transactions.foreign_table', null);
            $query->where('provider_transactions.foreign_id', null);
//            $query->where('scout_mails.seeker_id',NULL);
        }

        return FavouriteListResource::collection($query->paginateByUrl());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Provider\ProviderFavouriteStoreRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/provider/favourites",
     *     description="create new provider experience",
     *     tags={"Provider Favourites API"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/ProviderFavouriteStoreRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(ref="#/components/schemas/FavouriteResource"),
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function store(FavouriteStoreRequest $request)
    {
        $data = $request->validated();
        $data['provider_id'] = $this->providerId;
        try {
            $data = Favourite::create($data);
            return new FavouriteResource($data);
        } catch (\Exception $e) {
            return response(['message' => trans('messages.duplicate_entry')], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Favourite $providerExperience
     * @return \Illuminate\Http\Response
     *
     * @throws \Exception
     * @OA\Delete(
     *     path="/provider/favourites/{id}",
     *     description="Delete providers favourite",
     *     tags={"Provider Favourites API"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="favourite id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No Content"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     */
    public function destroy(Favourite $favourite)
    {
        if ($favourite->provider_id == $this->providerId) {
            return DB::transaction(function () use ($favourite) {
                $favourite->delete();
                return response(['message' => trans('messages.deleted')], 204);
            });
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     *
     * @throws \Exception
     * @OA\Delete(
     *     path="/provider/favourites/bulk",
     *     description="Delete favourite bulk",
     *     tags={"Provider Apis"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(@OA\Property(
     *            property="id",
     *            example="[1,2]",
     *            description="array of favourite ids",
     *            type="array",
     *            @OA\Items(type="string")
     *          ))
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No Content"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     */
    public function bulkDelete(Request $request){
        if($request->has('id')){
            return DB::transaction(function () use ($request) {
                foreach($request->get('id') as $id){
                    $favorite = Favourite::findOrFail($id);
                    if($favorite->provider_id == $this->providerId){
                        $favorite->delete();
                    }
                    else{
                        return response(['message'=>trans('messages.failed')],403);
                    }
                }
            return response(['message' => trans('messages.deleted')], 204);
            });
        }
    }
}
