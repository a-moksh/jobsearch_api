<?php

namespace App\Http\Controllers\API\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Provider\ProviderRegisterRequest;
use App\Http\Requests\Provider\VerifyProviderRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\Attribute;
use App\Models\Language;
use App\Models\PaidJpLog;
use App\Models\Provider;
use App\Models\ProviderPaidjp;
use App\Models\Sequence;
use App\Models\User;
use App\Models\VerifyUser;
use App\Notifications\PaidAccountActivated;
use App\Notifications\PasswordChanged;
use App\Notifications\ProviderVerified;
use App\Notifications\SeekerVerified;
use App\Notifications\UserCreated;
use App\Notifications\UserCreatedNotifyAdmin;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class ProviderUserController extends Controller
{
    use Notifiable;

    /**
     * Register a new Provider
     *
     * @param  \App\Http\Requests\RegisterRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/provider/register",
     *     description="create new provider",
     *     tags={"Provider APIs"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/ProviderRegisterRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),@OA\Property(
     *                 property="email",
     *                 description="email",
     *                 type="string",
     *             ),@OA\Property(
     *                 property="locale",
     *                 description="locale",
     *                 type="string",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function store(ProviderRegisterRequest $request)
    {
        return DB::transaction(function () use ($request) {
            //prepare new user store
            $already_created = false;
            $data = $request->validated();
            $user = User::where('email', $data['email'])->where('user_type', 'provider')->first();
            if ($user) {
                $user->update($data);
                $already_created = true;
                VerifyUser::where('user_id', $user->id)->delete();
            } else {
                $data['user_identifier_id'] = Sequence::getUserIdentifier();
                $user = User::create($data);
                //provider table
                $provider = new Provider([
                    'company_name' => $request->input('company_name'),
                    'provider_identifier_id' => Sequence::getProviderIdentifier()
                ]);
                $user->provider()->save($provider);
            }

            //email verification setup
            $verifyUser = new VerifyUser(['user_id' => $user->id]);
            $user->verifyUser()->save($verifyUser);

            //fire events for register user;
            $user->notify(new UserCreated($user, app()->getLocale()));
            if (!$already_created) {
                if ($user->user_type != 'admin') {
                    //get all admin 
                    $userAdmin = User::where('user_type', 'admin')->where('verified', 1)->get();
                    foreach ($userAdmin as $admin) {
                        $admin->notify(new UserCreatedNotifyAdmin($user, app()->getLocale()));
                    }
                }
            }
            $response['message'] = trans('auth.register.success');
            $response['email'] = $user->email;
            return response($response, 201);
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {

        if (auth()->user()->id !== $user->id) {
            abort(403);
        }
        $data = $request->validated();
        return DB::transaction(function () use ($request, $user, $data) {
            if ($request->has('password')) {
                $data['password'] = bcrypt($data['password']);
                auth()->user()->notify(new PasswordChanged(auth()->user()));
            }
            $user->fill($data)->save();
            if ($user->user_type == 'provider') {
                $provider = $user->provider;
                $provider->fill($request->validated());
                $provider->save();
            }
            return (new UserResource($user))->additional([
                'message' => trans('messages.updated')
            ]);
        });
    }

    /**
     * Verify Provider user email
     *
     * @param  \App\Http\Requests\VerifyProviderRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/provider/verify/email",
     *     description="verify provider email",
     *     tags={"Provider APIs"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/VerifyProviderRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful verification of email",
     *     ),@OA\Response(
     *         response=402,
     *         description="Not Authorized",
     *     ),@OA\Response(
     *         response=401,
     *         description="Token Expired",
     *     ),@OA\Response(
     *         response=400,
     *         description="already verified or error",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function verifyProviderEmail(VerifyProviderRequest $request)
    {
        $verifyUser = VerifyUser::where('token', urldecode($request->input('token')))->first();

        if (!$verifyUser) {
            return response()->json(['message' => trans('auth.email.token_expired'), 'email' => ''], 401);
        }

        if ($verifyUser->created_at->diffInHours(Carbon::now()) > 4) {
            VerifyUser::where('token', $request->input('token'))->delete();
            return response()->json(['message' => trans('auth.email.token_expired'), 'email' => ''], 401);
        }
        $user = $verifyUser->user;
        if (!$user->verified && $user->email == $request->get('email') && $verifyUser->token == $request->get('token') && $user->user_type == 'provider') {
            return DB::transaction(function () use ($request, $user, $verifyUser) {
                $verifyUser->user->verified = 1;
                $user->password = bcrypt($request->input('password'));
                $user->save();
                if ($user->user_type == 'provider') {
                    $values = $request->validated();
                    $values['company_name'] = [
                        'en' => $values['company_name_en'],
                        'ja' => $values['company_name_ja'],
                    ];
                    $values['ceo_name'] = [
                        'en' => $values['ceo_name_en'],
                        'ja' => $values['ceo_name_first_ja'] . ' ' . $values['ceo_name_last_ja'],
                    ];
                    $values['contact_person'] = [
                        'en' => $values['contact_person_en'],
                        'ja' => $values['contact_person_first_ja'] . ' ' . $values['contact_person_last_ja'],
                    ];
                    $provider = $user->provider;

                    $provider->fill($values)->saveTranslationLocales();

                    $values['payment_method'] = 1;
                    $values['closing_day'] = 20;
                    $paidinfo = new ProviderPaidjp();
                    $values = json_encode($values);
                    $paidinfo->fill(['provider_id' => $provider->id, 'provider_info' => $values]);
                    $paidinfo->save();
                    $user->notify(new ProviderVerified($user, $provider, app()->getLocale()));
                }

                $message = trans('auth.email.verified');
                return response(['message' => $message], 200);
            });

        } else {
            if ($user->user_type != 'provider') {
                $message = trans('auth.email.notAuthorized');
                return response(['message' => $message], 402);
            } else {
                if ($user->verified && $user->email) {
                    $message = trans('auth.email.alreadyVerified');
                } else {
                    $message = trans('auth.tokenFailed');
                }
            }
            return response(['message' => $message], 400);
        }
    }

    /**
     * Sync paid Jp
     *
     * /**
     * @OA\Post(
     *     path="/provider/providers/syncpaidjp",
     *     description="sync paid jp",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    protected function syncPaidJp()
    {
        $user = auth()->user();
        $provider = $user->provider;

        $jpinfo = json_decode($provider->providerPaidjp->provider_info);

        if ($provider->paidId != null) {
            return response(['message' => trans('messages.already_synced')], 403);
        }

        $prefecture = Attribute::where('group_id', 2)->where('value', $provider->prefecture_id)->first();

        $company_name = Language::where('table_name', 'providers')->where('foreign_id', $provider->id)
            ->where('column_name', 'company_name')->where('locale_code', 'ja')->first();

        $data = [
            'body' => [
                'memberData' => [
                    'b2bMemberId' => $provider->provider_identifier_id,
                    'companyName' => $company_name->locale_text,
                    'companyNameKana' => $jpinfo->company_name_kana,
                    'representativeSei' => $jpinfo->ceo_name_last_ja,
                    'representativeMei' => $jpinfo->ceo_name_first_ja,
                    'representativeSeiKana' => $jpinfo->ceo_name_last_kana,
                    'representativeMeiKana' => $jpinfo->ceo_name_first_kana,
                    'zipCode' => substr_replace($provider->zip_code, '-', 3, 0),//format-
                    'Prefecture' => trans('attribute.prefecture.' . $prefecture->name),
                    'address1' => $provider->address,
                    'address2' => $provider->street_address,
                    'address3' => $jpinfo->mansion_name,
                    'clerkSei' => $jpinfo->contact_person_last_ja,
                    'clerkMei' => $jpinfo->contact_person_first_ja,
                    'clerkSeiKana' => $jpinfo->contact_person_last_kana,
                    'clerkMeiKana' => $jpinfo->contact_person_first_kana,
                    'tel' => $provider->telephone,
                    'fax' => $provider->fax,
                    'email' => $user->email,
                    'url1' => $provider->related_url,
                    'url2' => $jpinfo->url2,
                    'url3' => $jpinfo->url3,
                    'mobileTel' => $jpinfo->mobile_tel,
                    'auxName' => $jpinfo->branch_name,
                    'payment_method' => $jpinfo->payment_method,
                    'closing_day' => $jpinfo->closing_day,
                ]
            ],
            'header' => [
                'apiAuthCode' => config('app.paid_auth_code')
            ]
        ];


        $paidJp = PaidJpLog::create([
            'foreign_id' => $provider->id,
            'foreign_table' => 'providers',
            'request' => json_encode($data['body']),
        ]);

        $response = $this->checkWithPaidJp(json_encode($data), 'coop/member/register/ver1.0/p.json');

        $paidJp->update(['response' => $response]);

        if ($response) {
            $response = json_decode($response);
            if ($response->header->status == 'SUCCESS' && $response->body->result->paidId) {
                $provider->paidId = $response->body->result->paidId;
                $provider->save();
                return response(['message' => trans('messages.success_sync')], 201);
            } else {
                if ($response->header->status == 'CLIENT_ERROR' && $response->body->result->error == 'C01') {
                    return response(['message' => trans('messages.already_synced')], 400);
                }
                return response(json_encode($response), 403);
            }
        }
    }

    /**
     * check paid Jp
     *
     * /**
     * @OA\Post(
     *     path="/provider/providers/checkpaidjp",
     *     description="check paid jp",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    protected function checkPaidVerification()
    {
        $provider = auth()->user()->provider;

        if ($provider->paid_status == 2) {
            return response(['message' => trans('messages.account_already_active')], 200);
        }

        $data = [
            'body' => [
                'b2bMemberIds' => [$provider->provider_identifier_id]
            ],
            'header' => [
                'apiAuthCode' => config('app.paid_auth_code')
            ]
        ];
        $response = $this->checkWithPaidJp(json_encode($data), 'coop/member/check/ver1.0/p.json');
        if ($response) {
            $response = json_decode($response);
            if ($response->header->status == 'SUCCESS' && $response->body->results->successes[0]->memberStatusCode == 2) {
                $provider->paid_status = 2;
                $provider->save();
                auth()->user()->notify(new PaidAccountActivated(app()->getLocale()));
                return response(['message' => trans('messages.paid_active')], 200);
            } else {
                if ($response->header->status == 'SUCCESS') {
                    $provider->paid_status = $response->body->results->successes[0]->memberStatusCode;
                    $provider->save();
                    return response(['message' => 'Status:' . $provider->paid_status], 200);
                }
            }
        }
        return response(['message' => trans('messages.error')], 403);
    }

    protected function checkWithPaidJp($data, $api)
    {
        $url = config('app.paid_auth_url') . $api;

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_POST, 1);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=UTF-8']);

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }
}
