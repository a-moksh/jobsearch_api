<?php

namespace App\Http\Controllers\API\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Provider\StoreScoutMailJobRequest;
use App\Http\Requests\Provider\StoreScoutMailRequest;
use App\Http\Requests\UpdateScoutMailRequest;
use App\Http\Resources\ScoutMailListProviderResource;
use App\Http\Resources\ScoutMailListResource;
use App\Http\Resources\ScoutMailProviderResource;
use App\Http\Resources\ScoutMailResource;
use App\Http\Resources\ScoutMailSeekerResource;
use App\Models\Job;
use App\Models\JobLocation;
use App\Models\Provider;
use App\Models\ProviderTransaction;
use App\Models\ScoutDetail;
use App\Models\ScoutMail;
use App\Models\Seeker;
use App\Models\SeekerJob;
use App\Notifications\ScoutMailReceived;
use App\Notifications\ScoutMailReplied;
use App\Notifications\ScoutMailRepliedSeeker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScoutMailController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @OA\Get(
     *     path="/seeker/scout-mails",
     *     description="scout/messages",
     *     summary="list of all scout/messages",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *        name="type",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="type scout/message",
     *        @OA\Schema(
     *            type="string"
     *           )
     *        ),
     *     @OA\Parameter(
     *        name="filter",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="filter send/reception",
     *        @OA\Schema(
     *            type="string"
     *           )
     *        ),
     *      @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/ScoutMailResource"),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     *   )
     */
    public function index(Request $request)
    {
        $query = ScoutMail::query();

        $query->select('scout_mails.*', 'seekers.seeker_identifier_id', 'users.katakana_full_name');

        $query->join('seekers', 'seekers.id', 'scout_mails.seeker_id');
        $query->join('users', 'seekers.user_id', 'users.id');
        $query->where('scout_mails.seeker_id', $this->seekerId);
        if ($request->filled('type')) {
            $query->whereIn('type', str_getcsv($request->input('type')));
        }
        if ($request->filled('filter')) {
            if ($request->input('filter') == 'send') {
                $query->where('seeker_status', '!=', 0);
            } else {
                if ($request->input('filter') == 'reception') {
                    $query->whereRaw('(provider_status = 0 OR provider_status = NULL OR provider_status = 1 OR seeker_status = 0)');
                }
            }
        }
        return ScoutMailResource::collection($query->paginateByUrl())->translateLocales();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    /**
     * @OA\Get(
     *     path="/provider/scout-mails",
     *     description="Provider scoutmail  List",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/ScoutMailListResource"),
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */
    public function listMails(Request $request)
    {
        $query = ScoutMail::query();

        $query->join('seekers', 'seekers.id', 'scout_mails.seeker_id');
        $query->join('users', 'seekers.user_id', 'users.id');
        $query->join('seeker_mail_settings', 'seeker_mail_settings.seeker_id', 'scout_mails.seeker_id');

        $query->select('scout_mails.id as id', 'scout_mails.seeker_id as seeker_id', 'scout_mails.created_at',
            'seekers.english_language_level', 'seekers.japanese_language_level', 'seekers.prefecture_id',
            'seekers.seeker_identifier_id', 'seekers.visa_status', 'seekers.visa_expiry_date',
            'users.katakana_full_name', 'users.last_login', 'seeker_mail_settings.employment_type');

        $query->where('scout_mails.provider_id', $this->providerId);
        $query->where('scout_mails.type', 1);


        return ScoutMailListResource::collection($query->paginateByUrl());
    }

    /**
     * @param Request $request
     * @return mixed
     */
    /**
     * @OA\Get(
     *     path="/provider/scout-mails/list",
     *     description="Provider scoutmails  List",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/ScoutMailListProviderResource"),
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */

    public function showMails(Request $request){
        $query = ScoutMail::query();

        $query->select('scout_mails.*', 'seekers.seeker_identifier_id', 'users.katakana_full_name', 'jobs.title',
            'jobs.job_identifier_id');

        $query->join('seekers', 'seekers.id', 'scout_mails.seeker_id');
        $query->join('users', 'seekers.user_id', 'users.id');
        $query->join('jobs', 'jobs.id', 'scout_mails.job_id');
        $query->join('languages', 'jobs.id', '=', 'languages.foreign_id');
        $query->where('scout_mails.provider_id', $this->providerId);
        if ($request->filled('title')) {
            $query->where('languages.table_name', 'jobs');
            $query->where('languages.column_name', 'title');
            $query->where('languages.locale_text', 'like', '%' . $request->input('title') . '%');
        }
        if ($request->filled('message_type')) {
            if ($request->get('message_type') == 1) {
//                $query->where('provider_status',1);
                $query->whereRaw('(provider_status = 1 OR provider_read = 1)');
                $query->orderBy('seeker_replied_at','desc');
//                $query->WhereRaw('provider_read',1);
            }
            if ($request->get('message_type') == 0) {
                $query->whereIn('provider_status', [null, 0]);
                $query->orderBy('updated_at','desc');
            }
        }
        $query->groupBy('scout_mails.id');
        return ScoutMailListProviderResource::collection($query->paginateByUrl())->translateLocales();
    }

    /**
    * @OA\Get(
    *     path="/seeker/scout-mails/{scoutMail}",
    *     description="show detail of scout mail",
    *     tags={"Seeker APIs"},
    *     security={{"JWT":{""}}},
    *     @OA\Parameter(
    *        name="scoutMail",
    *        description="scout mail id",
    *        in="path",
    *        required=true,
    *        @OA\Schema(type="integer")
    *     ),@OA\Response(
    *         response=200,
    *         description="Success",
    *         @OA\JsonContent(ref="#/components/schemas/ScoutMailSeekerResource"),
    *     ),@OA\Response(
    *         response=403,
    *         description="Unauthorized",
    *         @OA\JsonContent(
    *             ref="#/components/schemas/Unauthenticated"
    *         )
    *     )
    * )
    *
    */
    public function show(ScoutMail $scoutMail){
        if($scoutMail->seeker_id != $this->seekerId){
            abort(403);
        }
        return ScoutMailSeekerResource::make($scoutMail)->translateLocales();
    }

    /**
     * @param ScoutMail $scoutMail
     * @return ScoutMailProviderResource|mixed
     */
    public function showMail(ScoutMail $scoutMail)
    {
        if ($scoutMail->provider_id != $this->providerId) {
            abort(403);
        }
        return ScoutMailProviderResource::make($scoutMail)->translateLocales();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Seeker\StoreScoutMailRequest
     * @param  \App\Http\Models\ScoutMail
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/provider/scout-mails",
     *     description="create scout mail",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/StoreScoutMailRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(ref="#/components/schemas/ScoutMailResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function store(StoreScoutMailRequest $request, ScoutMail $scoutMail)
    {
        //add validation to stop multiple
        $data = $request->validated();

        $application = SeekerJob::where('seeker_id', $data['seeker_id'])->where('job_id', $data['job_id'])->first();
        if ($application && $application->status != 0) {
            abort('403');
        }
        $job = Job::find($data['job_id']);
        if ($job->provider_id != $this->providerId) {
            abort('403');
        }
        $data['provider_id'] = $this->providerId;
        return DB::transaction(function () use ($request, $scoutMail, $data, $application, $job) {

//            $application->status = 3; //interviewwaiting
//            $application->save();

            $data['re_count'] = 1;
            $data['provider_reply_count'] = 1;
            $data['type'] = 2; //application
            $data['seeker_status'] = 0; //pending

            $scoutMail->fill($data)->save();
            if ($request->has('dateTime')) {
                foreach ($request->dateTime as $scout_detail) {
                    $scout_detail = (object)$scout_detail;
                    ScoutDetail::create([
                        'time' => $scout_detail->time,
                        'date' => $scout_detail->date,
                        'scout_mail_id' => $scoutMail->id,
                    ])->save();
                }
            }
            $company = Provider::find($data['provider_id']);
            $seeker_user = Seeker::find($data['seeker_id'])->user;
            $seeker_user->notify(new ScoutMailReplied($scoutMail, $job, $company, $seeker_user->receive_mail_language));
            return new ScoutMailResource($scoutMail);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Seeker\StoreScoutMailJobRequest
     * @param  \App\Http\Models\ScoutMail
     * @param  \App\Http\Models\JobLocation
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/provider/scout-mails/create",
     *     description="create scout mail",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/StoreScoutMailJobRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(ref="#/components/schemas/ScoutMailResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function createMail(
        StoreScoutMailJobRequest $request,
        ScoutMail $scoutMail,
        Job $job,
        JobLocation $jobLocation
    ) {

        $opened = ProviderTransaction::where('foreign_id', $request->get('seeker_id'))->where('provider_id',
            $this->providerId)->where('foreign_table', 'seekers')->first();
        if (!$opened) {
            return response(['message' => trans('messages.please_open_first')], 403);
        }

        if (ScoutMail::where('job_id', $request->get('job_id'))->where('seeker_id',
            $request->get('seeker_id'))->withTrashed()->first()) {
            return response(['message' => trans('messages.duplicate_scout_message')], 403);
        }
        return DB::transaction(function () use ($request, $scoutMail, $job, $jobLocation) {
            $data = $request->validated();
            $data['provider_id'] = $this->providerId;
            $data['type'] = 1; // scout
            $data['seeker_status'] = 0; //pending
            $data['seeker_read'] = 0;
            $data['re_count'] = 0;
            $scoutMail->fill($data)->save();
            if ($request->has('dateTime')) {
                foreach ($request->dateTime as $scout_detail) {
                    $scout_detail = (object)$scout_detail;
                    ScoutDetail::create([
                        'time' => $scout_detail->time,
                        'date' => $scout_detail->date,
                        'scout_mail_id' => $scoutMail->id,
                    ])->save();
                }
            }
            $seeker_user = Seeker::find($data['seeker_id'])->user;
            $seeker_user->notify(new ScoutMailReceived($scoutMail, $seeker_user->receive_mail_language));
            return new ScoutMailResource($scoutMail);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Job $job
     * @return \Illuminate\Http\Response
     *
     * @throws \Exception
     * @OA\Delete(
     *     path="/provider/scout-mails/bulk",
     *     description="Delete scoutmails",
     *     tags={"Provider Apis"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(@OA\Property(
     *            property="id",
     *            example="[1,2]",
     *            description="array of scoutmail ids",
     *            type="array",
     *            @OA\Items(type="string")
     *          ))
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No Content"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     */
    public function bulkDelete(Request $request)
    {
        if ($request->has('id')) {
            return DB::transaction(function () use ($request) {
                foreach ($request->get('id') as $id) {
                    $favorite = ScoutMail::findOrFail($id);
                    if ($favorite->provider_id == $this->providerId) {
                        $favorite->delete();
                    } else {
                        return response(['message' => trans('messages.failed')], 403);
                    }
                }
                return response(['message' => trans('messages.deleted')], 204);
            });
        }
    }

    /** @OA\Put(
    *     path="seeker/scout-mails/{scoutMail}",
    *     description="reply to scout mail",
    *     tags={"Seeker APIs"},
    *     security={{"JWT":{""}}},
    *     @OA\Parameter(
    *        name="scoutMail",
    *        description="scout mail id",
    *        in="path",
    *        required=true,
    *        @OA\Schema(type="integer")
    *     ),
    *     @OA\RequestBody(
    *         required=true,
    *         @OA\JsonContent(ref="#/components/schemas/UpdateScoutMailRequest")
    *     ),
    *     @OA\Response(
    *         response=201,
    *         description="Created",
    *         @OA\JsonContent(ref="#/components/schemas/ScoutMailSeekerResource")
    *     ),@OA\Response(
    *         response=403,
    *         description="Unauthorized",
    *         @OA\JsonContent(
    *             ref="#/components/schemas/Unauthenticated"
    *         )
    *     ),@OA\Response(
    *         response=422,
    *         description="Unprocessable Entity",
    *         @OA\JsonContent(
    *             @OA\Property(
    *                 property="message",
    *                 description="message",
    *                 type="string",
    *             ),
    *             @OA\Property(
    *                 property="errors",
    *                 ref="#/components/schemas/Errors",
    *             )
    *         )
    *     ),
    * )
    */
    public function update(UpdateScoutMailRequest $request, ScoutMail $scoutMail){
        if($scoutMail->seeker_id != $this->seekerId){
            abort(403);
        } else {
            $data = $request->validated();
            if ($data['seeker_status'] == 1) {
                //get time from scout details
                $interview_timing = ScoutDetail::find($data['interview_timing']);
                $data['date'] = $interview_timing->date;
                $data['time'] = $interview_timing->time;
            }
            $data['provider_status'] = 1; //unopened
            $data['provider_read'] = 0; //unread
            $data['re_count'] = $scoutMail->re_count + 1;
            $data['seeker_reply_count'] = $scoutMail->provider_reply_count + 1;
            $data['seeker_replied_at'] = now();
            $scoutMail->fill($data)->save();

            // application status changes
//                if($scoutMail->type == 2 && $data['seeker_status'] == 2){
//                    $application = SeekerJob::where('seeker_id',$scoutMail->seeker_id)->where('job_id',$scoutMail->seeker_id)->first();
//                    $application->status =5;
//                    $application->save();
//                }

            $provider = Provider::find($scoutMail->provider_id)->user;
            $seeker_user = Seeker::find($scoutMail->seeker_id)->user;
            $provider->notify(new ScoutMailRepliedSeeker($scoutMail,$seeker_user, 'ja'));

            return (ScoutMailSeekerResource::make($scoutMail)->translateLocales())->additional([
                'message' => trans('messages.scout_application_message_sent')
            ]);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Seeker\StoreScoutMailJobRequest
     * @param  \App\Http\Models\ScoutMail
     * @param  \App\Http\Models\JobLocation
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/provider/scout-mails/{id}",
     *     description="update scout mail",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="scout id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/UpdateScoutMailRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(ref="#/components/schemas/ScoutMailSeekerResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function updateProvider(UpdateScoutMailRequest $request, ScoutMail $scoutMail){

        if($scoutMail->provider_id != $this->providerId){
            abort(403);
        }else{
            $data['provider_status'] = 0; //replied
            $data['seeker_read'] = 0;
            $data['re_count'] = $scoutMail->re_count + 1;
            $data['provider_reply_count'] = $scoutMail->seeker_reply_count + 1;
            $scoutMail->fill($data)->save();
            $seeker_user = Seeker::find($scoutMail->seeker_id)->user;
            $job = Job::find($scoutMail->job_id);
            $provider = Job::find($scoutMail->provider_id);
            $seeker_user->notify(new ScoutMailReplied($scoutMail, $job, $provider, $seeker_user->receive_mail_language));
            return (ScoutMailSeekerResource::make($scoutMail)->translateLocales());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request
     * @param  \App\Http\Models\ScoutMail
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Put(
     *     path="/provider/scout-mails/read/{scoutMail}",
     *     description="update scout mail readstatus by seeker also /seeker",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="scoutMail",
     *         description="scooutmail id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="updated",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             )
     *         )
     *     )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function changeReadStatus(Request $request, ScoutMail $scoutMail)
    {
        //call to change read status for bold or not
        return DB::transaction(function () use ($request, $scoutMail) {
            if (auth()->user()->user_type == 'seeker') {
                if ($this->seekerId != $scoutMail->seeker_id) {
                    abort(403);
                }
                $data['seeker_read'] = 1;
                $scoutMail->fill($data)->save();
            }

            if (auth()->user()->user_type == 'provider') {
                if ($this->providerId != $scoutMail->provider_id) {
                    abort(403);
                }
                $data['provider_read'] = 1;
                $scoutMail->fill($data)->save();
            }
            return response(['message' => trans('messages.updated')], 200);
        });
    }
}
