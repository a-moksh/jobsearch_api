<?php

namespace App\Http\Controllers\API\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Provider\ApplicationOpenRequest;
use App\Http\Requests\Provider\PurchasePointRequest;
use App\Http\Requests\Provider\SeekerOpenRequest;
use App\Models\Charge;
use App\Models\PaidJpLog;
use App\Models\Point;
use App\Models\Provider;
use App\Models\ProviderTransaction;
use App\Models\Seeker;
use App\Models\SeekerJob;
use App\Models\Sequence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PointController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * get provider transaction list.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Get(
     *     path="/provider/points",
     *     summary="get provider point list",
     *     tags={"Provider APIs"},
     *    security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=false,
     *     ),@OA\Response(
     *         response=200,
     *         description="list"
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function index(Request $request)
    {
        $start_date = $request->get('created_at');
        $end_date = date("Y-m-t", strtotime($start_date));

        $deposit = DB::table('provider_transactions')->select('created_at', 'id', 'transaction_type', 'charge_type',
            'points', 'amount as seeker_identifier_id', 'charge_type as seeker_name')
            ->where('transaction_type', 1)//deposit
            ->where('paid_status', 1)// succesful transactiononly
            ->where('provider_id', $this->providerId);
        if ($request->has('created_at')) {
            $deposit->whereBetween('created_at', [$start_date, $end_date]);
        }

        $seekers = DB::table('provider_transactions')->select('provider_transactions.created_at',
            'provider_transactions.id', 'provider_transactions.transaction_type', 'provider_transactions.charge_type',
            'provider_transactions.points', 'seekers.seeker_identifier_id', 'users.katakana_full_name as seeker_name')
            ->join('seekers', 'seekers.id', 'provider_transactions.foreign_id')
            ->join('users', 'users.id', 'seekers.user_id')
            ->where('provider_transactions.provider_id', $this->providerId)
            ->where('charge_type', 0);
        if ($request->has('created_at')) {
            $seekers->whereBetween('provider_transactions.created_at', [$start_date, $end_date]);
        }

        $seeker_jobs = DB::table('provider_transactions')->select('provider_transactions.created_at',
            'provider_transactions.id', 'provider_transactions.transaction_type', 'provider_transactions.charge_type',
            'provider_transactions.points', 'seekers.seeker_identifier_id', 'users.katakana_full_name as seeker_name')
            ->join('seeker_jobs', 'seeker_jobs.id', 'provider_transactions.foreign_id')
            ->join('seekers', 'seekers.id', 'seeker_jobs.seeker_id')
            ->join('users', 'users.id', 'seekers.user_id')
            ->where('provider_transactions.provider_id', $this->providerId)
            ->where('charge_type', 2);
        if ($request->has('created_at')) {
            $seeker_jobs->whereBetween('provider_transactions.created_at', [$start_date, $end_date]);
        }

        $translation = DB::table('provider_transactions')->select('provider_transactions.created_at',
            'provider_transactions.id', 'provider_transactions.transaction_type', 'provider_transactions.charge_type',
            'provider_transactions.points', 'jobs.job_identifier_id as seeker_identifier_id',
            'jobs.title as seeker_name')
            ->join('jobs', 'jobs.id', 'provider_transactions.foreign_id')
            ->where('provider_transactions.provider_id', $this->providerId)
            ->where('charge_type', 1);
        if ($request->has('created_at')) {
            $translation->whereBetween('provider_transactions.created_at', [$start_date, $end_date]);
        }


//        $scout_mails = DB::table('provider_transactions')->select('provider_transactions.created_at','provider_transactions.id','provider_transactions.transaction_type','provider_transactions.charge_type','provider_transactions.points','seekers.seeker_identifier_id','users.katakana_full_name as seeker_name')
//            ->join('scout_mails','scout_mails.id','provider_transactions.foreign_id')
//            ->join('seekers','seekers.id','scout_mails.seeker_id')
//            ->join('users','users.id','seekers.user_id')
//            ->where('provider_transactions.provider_id',$this->providerId)
//            ->where('charge_type',4);

//        if($request->has('created_at')){
//            $scout_mails->whereBetween('provider_transactions.created_at',[$start_date,$end_date]);
//        }

        $order = 'desc';
        if ($request->has('direction')) {
            $order = $request->get('direction');
        }

        $translation
            ->union($deposit)
//            ->union($translation)
            ->union($seeker_jobs)
            ->union($seekers)
            ->orderBy('created_at', $order);

        $querySql = $translation->toSql();
        $data = DB::table(DB::raw("({$querySql}) as union_table"))
            ->mergeBindings($translation)
            ->paginate(10);

        $provider_transactions = ProviderTransaction::query();
        $provider_transactions->select([
            DB::Raw('SUM(CASE WHEN provider_transactions.transaction_type=1 THEN points ELSE 0 END) AS points_purchased '),
            DB::Raw('SUM(CASE WHEN provider_transactions.transaction_type=2 THEN points ELSE 0 END) AS points_spend '),
            'providers.point_balance as point_balance'
        ])
            ->join('providers', 'providers.id', 'provider_transactions.provider_id')
            ->where('provider_transactions.paid_status', 1)
            ->where('provider_id', auth()->user()->provider->id);
        if ($request->has('created_at')) {
            $provider_transactions->where('provider_transactions.created_at', '>=', $start_date);
        }


        $sum = $provider_transactions->first();
//        print_r($sum);die;
        $sum = $sum->point_balance - $sum->points_purchased + $sum->points_spend;

        return [$data, $sum];
    }

    /**
     * Purchase points from paid jp
     * @param PurchasePointRequest $request
     *
     * /**
     * @OA\Post(
     *     path="/provider/points/purchase",
     *     description="purchase points",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\requestBody(
     *         @OA\JsonContent(ref="#/components/schemas/PurchasePointRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),@OA\Property(
     *                 property="point_balance",
     *                 description="balance point",
     *                 type="string",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function purchasePoints(PurchasePointRequest $request)
    {
        $data = $request->validated();
        $point = Point::find($data['id']);
        $provider = auth()->user()->provider;
        $data = [
            'body' => [
                'credit' => [
                    'b2bMemberId' => $provider->provider_identifier_id,
                    'price' => $point->amount
                ]
            ],
            'header' => [
                'apiAuthCode' => config('app.paid_auth_code')
            ]
        ];

        $paidJp = PaidJpLog::create([
            'foreign_id' => $provider->id,
            'foreign_table' => 'providers',
            'request' => json_encode($data['body']),
        ]);

        $response = $this->checkWithPaidJp(json_encode($data), 'coop/member/creditCheck/ver1.0/p.json');

        $paidJp->update(['response' => $response]);

        if ($response) {
            $response = json_decode($response);
            if ($response->header->status == 'SUCCESS' && 2 == $response->body->result->memberStatusCode && true == $response->body->result->creditAvailable) {
                //member is eligible to buy so proceed with register order

                $transaction = ProviderTransaction::create([
                    'points' => $point->point,
                    'amount' => $point->amount,
                    'provider_id' => $provider->id,
                    'foreign_id' => $provider->id,
                    'foreign_table' => 'providers',
                    'transaction_type' => 1, //deposit
                    'charge_type' => 3, //deposit charge type
                    'paid_status' => 0, //pending
                    'trans_identifier_id' => Sequence::getTransactionIdentifier(),
                ]);

                $data = [
                    'body' => [
                        'credits' => [
                            'credit' => [
                                [
                                    'b2bMemberId' => $provider->provider_identifier_id,
                                    'price' => $point->amount,
                                    'contents' => $point->amount . ' ポイントを書いました',
//                                    'code'=>$provider->paidId,
                                    'b2bCreditId' => $transaction->trans_identifier_id,
                                ]
                            ]
                        ]
                    ],
                    'header' => [
                        'apiAuthCode' => config('app.paid_auth_code')
                    ]
                ];

                $paidJp = PaidJpLog::create([
                    'foreign_id' => $transaction->id,
                    'foreign_table' => 'provider_transactions',
                    'request' => json_encode($data['body']),
                ]);
                $response = $this->checkWithPaidJp(json_encode($data), 'coop/Register/b2bMemberId/ver1.0/p.json');

                $paidJp->update(['response' => $response]);
                if ($response) {
                    $response = json_decode($response);
                    if ($response->header->status == 'SUCCESS' && 'SUCCESS' == $response->body->detailResults->detailResult[0]->status) {
                        //order register successful
                        $data = [
                            'body' => [
                                'credits' => [
                                    'credit' => [
                                        [
                                            'b2bMemberId' => $provider->provider_identifier_id,
                                            'b2bCreditId' => $transaction->trans_identifier_id,
                                        ]
                                    ]
                                ]
                            ],
                            'header' => [
                                'apiAuthCode' => config('app.paid_auth_code')
                            ]
                        ];
                        $paidJp = PaidJpLog::create([
                            'foreign_id' => $transaction->id,
                            'foreign_table' => 'provider_transactions',
                            'request' => json_encode($data['body']),
                        ]);
                        $response = $this->checkWithPaidJp(json_encode($data), 'coop/Fix/b2bCreditId/ver1.0/p.json');
                        $paidJp->update(['response' => $response]);
                        DB::beginTransaction();
                        if ($response) {
                            $response = json_decode($response);
                            if ($response->header->status == 'SUCCESS' && 'SUCCESS' == $response->body->detailResults->detailResult[0]->status) {
                                $provider->lockForUpdate();
                                $transaction->paid_status = 1;
                                $transaction->save();
                                $provider->point_balance += $point->point;
                                $provider->purchased_on = Date('Y-m-d');
                                $provider->save();
                                DB::commit();
                                return response([
                                    'message' => trans('messages.point_purchase_success'),
                                    'point_balance' => $provider->point_balance
                                ], 200);
                            }
                            DB::rollBack();
                            return response(['message' => trans('messages.points_purchase_error')], 403);
                        }
                    }
                    return response(['message' => trans('messages.points_purchase_error')], 403);
                }

                return response(['message' => trans('messages.points_purchase_error')], 403);

            } else {
                if ($response->header->status == 'SUCCESS' && 1 == $response->body->result->memberStatusCode) {
                    return response(['message' => trans('messages.not_eligible')], 403);
                } else {
                    if (isset($response->body->result->creditAvailable) && $response->body->result->creditAvailable == false) {
                        return response(['message' => trans('messages.not_enough_balance')], 403);
                    }
                    return response(['message' => trans('messages.points_purchase_error')], 403);
                }
            }
        }
        return response(['message' => trans('messages.points_purchase_error')], 403);
    }


    function checkWithPaidJp($data, $api)
    {
        $url = config('app.paid_auth_url') . $api;

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_POST, 1);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=UTF-8']);

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    /**
     * open application
     * @param ApplicationOpenRequest $request
     *
     * /**
     * @OA\Post(
     *     path="/provider/points/application/open",
     *     description="open application",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\requestBody(
     *         @OA\JsonContent(ref="#/components/schemas/ApplicationOpenRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function openApplication(ApplicationOpenRequest $request)
    {

        $provider = auth()->user()->provider;
        $point = new Point();
        $seekerJob = new SeekerJob();
        $transaction = new ProviderTransaction();
        $charge = Charge::where('type', 'seeker_jobs')->first();

        return DB::transaction(function () use ($request, $provider, $point, $seekerJob, $transaction, $charge) {
            $provider->lockForUpdate();
            if ($provider->point_balance < count($request->get('id')) * $charge->point) {
                return response(['message' => trans('messages.points_not_enough')], 403);
            }
            foreach ($request->get('id') as $id) {
                $seekerJob = $seekerJob->find($id);
                if ($this->providerId != $seekerJob->job->provider_id) {
                    abort(403, trans('errors.403'));
                }
//                $seekerJob->status = 1; //opened
//                $seekerJob->save(); //opened
                $old = $transaction->where('foreign_id', $seekerJob->id)->where('provider_id',
                    $provider->id)->where('foreign_table', 'seeker_jobs')->first();
                if (!$old) {
                    $transaction->create([
                        'points' => $charge->point, //rate from cost table
                        'provider_id' => $provider->id,
                        'foreign_id' => $seekerJob->id,
                        'foreign_table' => 'seeker_jobs',
                        'transaction_type' => 2, //spent
                        'charge_type' => 2, //application
                    ]);
                    $provider->point_balance -= $charge->point;
                } else {
                    return response(['message' => trans('messages.application_already_opened')], 403);
                }
            }
            $provider->save();
            return response(['message' => trans('messages.application_opened')]);
        });
    }


    /**
     * open application
     * @param SeekerOpenRequest $request
     *
     * /**
     * @OA\Post(
     *     path="/provider/points/seeker/open",
     *     description="open seeker",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\requestBody(
     *         @OA\JsonContent(ref="#/components/schemas/SeekerOpenRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
     * )
     * @return \Illuminate\Http\JsonResponse
     */
    public function openSeeker(SeekerOpenRequest $request)
    {

        $provider = auth()->user()->provider;
        $point = new Point();
        $seeker = new Seeker();
        $transaction = new ProviderTransaction();
        $charge = Charge::where('type', 'seekers')->first();

        return DB::transaction(function () use ($request, $provider, $point, $seeker, $transaction, $charge) {
            $provider->lockForUpdate();
            if ($provider->point_balance < count($request->get('id')) * $charge->point) {
                return response(['message' => trans('messages.points_not_enough')], 403);
            }
            foreach ($request->get('id') as $id) {
                $seeker = $seeker->find($id);
                $old = $transaction->where('foreign_id', $seeker->id)->where('provider_id',
                    $provider->id)->where('foreign_table', 'seekers')->first();
                if (!$old) {
                    $transaction->create([
                        'points' => $charge->point, //rate from cost table
                        'provider_id' => $provider->id,
                        'foreign_id' => $seeker->id,
                        'foreign_table' => 'seekers',
                        'transaction_type' => 2, //spent
                        'charge_type' => 0, //scout
                    ]);
                    $provider->point_balance -= $charge->point;
                } else {
                    return response(['message' => trans('messages.seeker_already_opened')], 403);
                }
            }
            $provider->save();
            return response(['message' => trans('messages.seeker_opened')]);
        });
    }

    /**
     * Get SpendRates
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Get(
     *     path="/provider/points/purchase/rates",
     *     summary="get provider purchase rates",
     *     description="get provider purchase rates",
     *     security={{"JWT":{""}}},
     *     tags={"Provider APIs"},
     *     @OA\RequestBody(
     *         required=false,
     *     ),@OA\Response(
     *         response=200,
     *         description="data",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="text",
     *                 description=" description",
     *                 type="string",
     *             ),@OA\Property(
     *                 property="value",
     *                 description="id",
     *                 type="integer",
     *             )
     *         )
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function getPurchaseRates()
    {
        $query = Point::query();
        $query->select('description as text', 'id as value');
        return response($query->get()->toArray());
    }

    /**
     * Get SpendRates
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Get(
     *     path="/provider/points/spend/rates",
     *     summary="get provider spend rates",
     *     description="get provider spend rates",
     *     security={{"JWT":{""}}},
     *     tags={"Provider APIs"},
     *     @OA\RequestBody(
     *         required=false,
     *     ),@OA\Response(
     *         response=200,
     *         description="data",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="type",
     *                 description="spend type",
     *                 type="integer",
     *             ),@OA\Property(
     *                 property="point",
     *                 description="point",
     *                 type="integer",
     *             )
     *         )
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function getSpendRates()
    {
        $query = Charge::query();
        $query->select('type', 'point');
        return response($query->get()->toArray());
    }

    /**
     * Get Provider Balance
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Get(
     *     path="/provider/points/balance",
     *     summary="get provider balance,paid id",
     *     description="get provider balance,paid id",
     *     security={{"JWT":{""}}},
     *     tags={"Provider APIs"},
     *     @OA\RequestBody(
     *         required=false,
     *     ),@OA\Response(
     *         response=200,
     *         description="data",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="point_balance",
     *                 description="point_balance",
     *                 type="integer",
     *             ),@OA\Property(
     *                 property="paidid",
     *                 description="paidid",
     *                 type="integer",
     *             ),@OA\Property(
     *                 property="paid_status",
     *                 description="paid_status",
     *                 type="boolean",
     *             )
     *         )
     *     ),@OA\Response(
     *         response=403,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     ),@OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function getProviderBalance()
    {
        $query = Provider::query();
        $query->select('point_balance', 'paidid', 'paid_status');
        $query->where('id', $this->providerId);
        return response($query->get()->toArray());
    }
}
