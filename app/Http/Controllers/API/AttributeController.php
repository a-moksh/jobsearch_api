<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\AttributeResource;
use App\Models\Attribute;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Get(
     *     path="/attributes",
     *     description="attribute list",
     *     summary="list of all attribute ",
     *     tags={"Common APIs"},
     *     @OA\Parameter(
     *        name="id",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="attribute  id",
     *        @OA\Schema(
     *            type="integer",
     *            format="csv"
     *           )
     *        ),
     *     @OA\Parameter(
     *        name="group",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="attribute group name",
     *        @OA\Schema(
     *            type="string",
     *            format="csv"
     *           )
     *        ),
     *      @OA\Response(
     *         response=200,
     *         description="Success",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="メッセージ",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     *   )
     */
    public function index(Request $request)
    {
        $query = Attribute::query();
        if ($request->filled('id')) {
            $query->whereIn('id', str_getcsv($request->input('id')));
        }
        if ($request->filled('group')) {
            $query->whereHas('group', function ($query) use ($request) {
                $query->whereIn('name', str_getcsv($request->input('group')));
            });
        }
        return AttributeResource::collection(
            $query->with('group')->paginateByUrl(['per_page_max' => 1000])
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Attribute $attribute
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute $attribute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Attribute $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attribute $attribute)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Attribute $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute)
    {
        //
    }
}
