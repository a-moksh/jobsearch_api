<?php

namespace App\Http\Controllers\API\Auth;

use App\Events\LoginEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    public $successStatus = 200;
    public $maxAttempts = 5;
    public $decayMinutes = 30;
    private $user_type;

    /**
     * @param \App\Http\Requests\LoginRequest
     * @return \Illuminate\Http\Response
     *
     * @OA\Post(
     *     path="/seeker/login",
     *     tags={"Seeker APIs"},
     *     summary="Login seeker into system",
     *     description="This is public and can be called by non registered seeker also",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/LoginRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/UserResource"),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid Input",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     */
    public function login(LoginRequest $request)
    {

        $credentials = $request->only('email', 'password');
        $credentials['user_type'] = 'seeker';
        $this->user_type = 'seeker';
        return $this->loginAction($credentials, $request);
    }

    /**
     * @param \App\Http\Requests\LoginRequest
     * @return \Illuminate\Http\Response
     *
     * @OA\Post(
     *     path="/provider/login",
     *     tags={"Provider APIs"},
     *     summary="Login provider into system",
     *     description="This is public and can be called by non registered provider also",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/LoginRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/UserResource"),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid Input",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     */
    public function loginProvider(LoginRequest $request)
    {

        $credentials = $request->only('email', 'password');
        $credentials['user_type'] = 'provider';
        $this->user_type = 'provider';
        return $this->loginAction($credentials, $request);
    }

    /**
     * @param \App\Http\Requests\LoginRequest
     * @return \Illuminate\Http\Response
     *
     * @OA\Post(
     *     path="/admin/login",
     *     tags={"Admin APIs"},
     *     summary="Login admin into system",
     *     description="This is public and can be called by non registered admin also",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/LoginRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/UserResource"),
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid Input",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     */
    public function loginAdmin(LoginRequest $request){

        $credentials = $request->only('email', 'password');
        $credentials['user_type'] = 'admin';
        $this->user_type = 'admin';
        return $this->loginAction($credentials, $request);
    }

    /**
     *
     */
    private function loginAction($credentials, $request)
    {
        try {
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);
                return $this->sendLockoutResponse($request);
            }
            $credentials['verified'] = 1;
            if ($request['remember_me'] == 'true') {
                config(['jwt.ttl' => 1440 * 7]); // 1 week
            }
            if (!$token = JWTAuth::attempt($credentials)) {
                $user = User::where('email', $credentials['email'])->first();
                $this->incrementLoginAttempts($request);
                if ($user && !$user->verified) {
                    return response()->json(['message' => trans('auth.email.notVerified')], 401);
                } else {
                    return response()->json(['message' => trans('auth.failed')], 403);
                }
            }
        } catch (JWTException $e) {
            return response()->json(['message' => trans('auth.token_creation_failed')], 500);
        }
        $user = JWTAuth::User();
        event(new LoginEvent($user));
        $user->last_login = now();
        $user->save();
        $success['messages'] = trans('auth.success');
        $success['user'] = UserResource::make($user);
        $success['access_token'] = $token;
        $success['token_type'] = 'Bearer';
        $this->clearLoginAttempts($request);
        return response()->json($success);
    }

    /**
     * Get the throttle key for the given request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return string
     */
    protected function throttleKey(Request $request)
    {
        return Str::lower($request->input($this->username()) . $this->user_type) . '|' . $request->ip();
    }

    /**
     * Redirect the user after determining they are locked out.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );
        throw ValidationException::withMessages([
            $this->username() => [Lang::get('auth.throttle', ['minute' => intval($seconds / 60)])],
        ])->status(429);
    }

    /**
     *
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => trans('messages.loggedout')]);
    }
}
