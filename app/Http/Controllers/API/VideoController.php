<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    /**
     * Convert the path in the playlist
     * @param $key
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     *
     * @OA\Get(
     *     path="/videos/{key}",
     *     description="stream video",
     *     summary="get stream for the video",
     *     tags={"Common APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="key",
     *         description="key",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */
    public function index($key)
    {
        if (pathinfo($key, PATHINFO_EXTENSION) !== 'm3u8') {
            abort(404, trans('errors.404'));
        }

        $pathArray = explode('/', $key);
        if (array_first($pathArray) !== 'video') {
            $key = "video/{$key}";
        }

        $disk = Storage::disk('s3_private');

        $adapter = $disk->getAdapter();
        $client = $adapter->getClient();
        $url = $client->getObjectUrl($adapter->getBucket(), $key);
        $queryString = request()->getQueryString();
        $url = $url . ($queryString !== null ? '?' . $queryString : '');

        try {
            $content = file_get_contents($url);
        } catch (\Exception $e) {
            abort(404, trans('errors.404'));
        }

        preg_match_all('/\w+\.m3u8/', $content, $matches);

        if (!empty($matches[0])) {
            foreach ($matches[0] as $fileName) {
                $expires = now()->addMinutes(config('filesystems.pre_signed_expires', 10));
                $preSignedUrl = $disk->temporaryUrl(
                    dirname($key) . '/' . $fileName,
                    $expires
                );
                $content = preg_replace('/\w+\.m3u8/', '$0?' . parse_url($preSignedUrl, PHP_URL_QUERY), $content);
            }
        }

        preg_match_all('/\w+\.ts/', $content, $matches);

        if (!empty($matches[0])) {
            foreach ($matches[0] as $fileName) {
                $expires = now()->addMinutes(config('filesystems.pre_signed_expires', 10));
                $preSignedUrl = $disk->temporaryUrl(
                    dirname($key) . '/' . $fileName,
                    $expires
                );
                $content = str_replace($fileName, $preSignedUrl, $content);
            }
        }

        return response($content)
            ->header('Content-Type', 'application/vnd.apple.mpegurl');
    }
}
