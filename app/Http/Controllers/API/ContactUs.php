<?php

namespace App\Http\Controllers\API;

use App\Models\ContactUs as ContactModel;
use App\Models\User;
use Carbon\Carbon;
use App\Notifications\ContactEmailAdmin;
use App\Notifications\ContactEmailSentProvider;
use App\Notifications\ContactEmailSentSeeker;
use App\Notifications\ContactAdminReply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactUsRequest;
use App\Http\Requests\ContactUsUpdateRequest;
use App\Http\Resources\ContactUsResource;

class ContactUs extends Controller
{
    /**
     * contact us
     *
     * @OA\Post(
     *     path="/contact-us",
     *     description="contact us store api",
     *     tags={"Common APIs"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/ContactUsRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="message stored",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function store(ContactUsRequest $request)
    {
        return DB::transaction(function () use ($request) {
            $data = $request->validated();
            $contact = ContactModel::create($data);
            if(isset($data['type']) && $data['type'] == 2){
                $contact->notify(new ContactEmailSentProvider($contact, app()->getLocale()));
            }else{
                $contact->notify(new ContactEmailSentSeeker($contact, $data['receive_mail_language']));
            }
            $userAdmin = User::where('user_type', 'admin')->where('verified', 1)->get();
            foreach ($userAdmin as $admin) {
                $admin->notify(new ContactEmailAdmin($contact, app()->getLocale()));
            }
            $response['message'] = trans('messages.contact.success');
            return response($response, 201);
        });
    }

    public function index(Request $request)
    {
        if(auth()->user()->user_type !== 'admin')
            abort(403);
        $query = ContactModel::query();
        if ($request->filled('email')) {
            $query->whereIn('email', str_getcsv($request->input('email')));
        }
        if ($request->filled('created_at')) {
            $query->whereDay('created_at', '=', (
            new Carbon(
                $request->input('created_at')
            ))->day);
            $query->whereMonth('created_at', '=', (
            new Carbon(
                $request->input('created_at')
            ))->month);
            $query->whereYear('created_at', '=', (
            new Carbon(
                $request->input('created_at')
            ))->year);
        }
        return ContactUsResource::collection($query->paginateByUrl())->translateLocales();
    }

    /**
     * 
     */
    public function show(ContactModel $contact_us)
    {
        if(auth()->user()->user_type !== 'admin')
            abort(403);
        return new ContactUsResource($contact_us);
    }

    /**
     * 
     */
    public function update(ContactUsUpdateRequest $request, ContactModel $contact_us)
    {
        if(auth()->user()->user_type !== 'admin')
            abort(403);
        $data = $request->validated();
        return DB::transaction(function () use ($contact_us, $data) {
            $data['admin_replied'] = 1;
            $contact_us->update($data);
            $mail_data['language'] = $contact_us->receive_mail_language;
            $mail_data['admin_reply'] = $contact_us->admin_reply;
            $mail_data['full_name'] = $contact_us->full_name;
            $mail_data['company_name'] = $contact_us->company_name;
            $mail_data['type'] = $contact_us->type;
            $contact_us->notify(new ContactAdminReply($mail_data));
            return (new ContactUsResource($contact_us))->additional(['message' => trans('messages.contact.replied')]);
        });
    }
}
