<?php

namespace App\Http\Controllers\API\Job;

use App\Http\Controllers\Controller;
use App\Models\JobEmploymentType;
use Illuminate\Support\Facades\DB;

class EmploymentTypeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function destroy(JobEmploymentType $employmentType)
    {
        if ($employmentType->job->provider_id == $this->providerId) {
            return DB::transaction(function () use ($employmentType) {
                $employmentType->delete();
                return response(null, 204);
            });
        }
    }
}
