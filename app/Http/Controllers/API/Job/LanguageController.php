<?php

namespace App\Http\Controllers\API\Job;

use App\Http\Controllers\Controller;
use App\Models\JobLanguage;
use Illuminate\Support\Facades\DB;

class LanguageController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function destroy(JobLanguage $language)
    {
        if ($language->job->provider_id == $this->providerId) {
            return DB::transaction(function () use ($language) {
                $language->delete();
                return response(null, 204);
            });
        }
    }
}
