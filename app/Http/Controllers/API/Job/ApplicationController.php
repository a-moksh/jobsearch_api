<?php

namespace App\Http\Controllers\API\Job;

use App\Http\Controllers\Controller;
use App\Http\Requests\Provider\ApplicationStatusUpdateRequest;
use App\Http\Requests\Seeker\JobApplyPreCheck;
use App\Http\Requests\Seeker\SeekerJobApplyRequest;
use App\Http\Resources\ProviderSeekerJobListResource;
use App\Http\Resources\SeekerJobDetailResource;
use App\Http\Resources\SeekerJobResource;
use App\Models\Job;
use App\Models\Media;
use App\Models\ProviderTransaction;
use App\Models\Seeker;
use App\Models\SeekerCertificate;
use App\Models\SeekerEducation;
use App\Models\SeekerExperience;
use App\Models\SeekerJob;
use App\Models\SeekerSkill;
use App\Models\User;
use App\Notifications\ApplicationReceived;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApplicationController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    /**
     * @OA\Get(
     *     path="/provider/job/applications",
     *     description="provider job applications",
     *     summary="List of provider job applications",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *        name="job_id",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="filter by job",
     *        @OA\Schema(
     *            type="integer"
     *           )
     *        ),
     *     @OA\Parameter(
     *        name="opened",
     *        in="query",
     *        required=false,
     *        allowEmptyValue=true,
     *        description="filter by opened/unopened",
     *        @OA\Schema(
     *            type="boolean"
     *           )
     *        ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/ProviderSeekerJobListResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */
    public function index(Request $request)
    {
        $query = SeekerJob::query();
        $query->select('seeker_jobs.id as seeker_job_id', 'seeker_jobs.*', 'seekers.seeker_identifier_id',
            'seekers.state', 'seekers.prefecture_id', 'seekers.japanese_language_level',
            'seekers.english_language_level', 'seekers.id as seeker_id', 'seekers.visa_status',
            'seekers.visa_expiry_date', 'users.katakana_full_name', 'users.last_login');
        $query->join('jobs', 'jobs.id', 'seeker_jobs.job_id');
        $query->join('seekers', 'seeker_jobs.seeker_id', 'seekers.id');
        $query->join('users', 'users.id', 'seekers.user_id');

        if ($request->filled('job_id') && $request->input('job_id') != 'null') {
            $query->whereIn('jobs.id', str_getcsv($request->input('job_id')));
        }
        if ($request->filled('application_status') && $request->input('application_status') != 'null') {
            $query->whereIn('seeker_jobs.status', str_getcsv($request->input('application_status')));
        }
        if ($request->filled('unopened') && $request->get('unopened') == 1) {
            $transaction = ProviderTransaction::select('foreign_id')->where('provider_id', $this->providerId)
                ->where('foreign_table', 'seeker_jobs')->get();
            $query->whereNotIn('seeker_jobs.id', $transaction);
        }
        $query->where('jobs.provider_id', $this->providerId);


        return ProviderSeekerJobListResource::collection($query->paginateByUrl())->translateLocales();

    }

    /**
     *
     */
    public function seekerJobApplication(Request $request)
    {
        $query = SeekerJob::query();
        $query->where('seeker_id', $this->seekerId);
        $query->where('created_at', '>', Carbon::now()->subDays(90));
        return SeekerJobResource::collection($query->paginateByUrl())->translateLocales();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    /**
     * @OA\Get(
     *     path="/provider/job/applications/{id}",
     *     description="provider job applications detail",
     *     summary="detail of provider job applications",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="application id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/SeekerJobDetailResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */
    public function show($id, SeekerJob $seekerJob)
    {
        $seekerJob = $seekerJob->findOrFail($id);

        if ($this->providerId != $seekerJob->job->provider_id) {
            abort(403, trans('errors.403'));
        }
        return SeekerJobDetailResource::make($seekerJob)->translateLocales();
    }


    public function updateStatus($id, ApplicationStatusUpdateRequest $request)
    {
        $seekerJob = SeekerJob::findOrFail($id);
        $showinfo = ProviderTransaction::where('foreign_id', $seekerJob->id)->where('provider_id',
            auth()->user()->provider->id)->where('foreign_table', 'seeker_jobs')->first();

        if ($this->providerId != $seekerJob->job->provider_id || !$showinfo) {
            abort(403, trans('errors.403'));
        }

        $seekerJob->status = $request->get('application_status');
        $seekerJob->save();
        return SeekerJobDetailResource::make($seekerJob)->translateLocales();
    }

    /**
     * apply for a job or multiple jobs
     *
     * @param  \App\Http\Requests\Seeker\SeekerJobApplyRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/seeker/job/apply",
     *     description="apply for a job or jobs.",
     *     tags={"Seeker APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SeekerJobApplyRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Success apply",
     *         @OA\JsonContent(ref="#/components/schemas/SeekerJobResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
     * )
     */
    public function apply(SeekerJobApplyRequest $request)
    {

        //first check if profile need to be updated or not 
        if ($request->input('save') == 1) {
            //TODO::add in transaction this part
            //perform update operation
            $this->updationOperation($request);
            // perform deletion operation
            $this->deletionOperation($request->input('deleted_data'));
            $this->skillOperation($request->input('skillUpdate'));
        }

        //perform media transfer and update operation
        $image = $this->imageOperation($request);
        $video = $this->videoOperation($request);
        $jobIds = $request->input('job_id');
        $userData = $request->validated();
        $userData['email'] = auth()->user()->email;
        $userData['image'] = $image;
        $userData['video'] = $video;
        unset($userData['basicUpdate']['newImage']);
        unset($userData['basicUpdate']['tempImage']);
        unset($userData['basicUpdate']['removeImage']);
        unset($userData['save']);
        $userData = json_encode($userData);
        return DB::transaction(function () use ($jobIds, $userData) {
            if (is_array($jobIds)) {
                foreach ($jobIds as $jobId) {
                    $this->applyJob($jobId, $userData);
                }
            } else {
                $this->applyJob($jobIds, $userData);
            }

            return SeekerJobResource::collection(SeekerJob::paginateByUrl())->translateLocales()->additional([
                'message' => trans('messages.job_applied')
            ]);
            //add aditional message
        });
    }

    /**
     *
     */
    public function preCheckValidation(JobApplyPreCheck $request)
    {
        return response('', 200);
    }

    /**
     *
     */
    private function videoOperation($request)
    {
        $path = '';
        if (Storage::disk('s3_private')->exists($request->input('video_url'))) {
            $jobVideo = explode('/', $request->input('video_url'));
            $path = 'job/application/' . $this->seekerId . '/' . end($jobVideo);
            Storage::disk('s3_private')->move($request->input('video_url'), $path);
            Storage::disk('s3_private')->delete($request->input('video_url'));
        }
        return $path;
    }

    /**
     *
     */
    private function imageOperation($request)
    {
        $path = '';
        //check if image operation is needed or not first
        if ($request->input('basicUpdate.newImage') == true) {
            //handle new image operation here
            if (Storage::disk('s3_private')->exists($request->input('basicUpdate.tempImage'))) {
                $jobImage = explode('/', $request->input('basicUpdate.tempImage'));
                $path = 'users/' . auth()->user()->id . '/' . end($jobImage);
                Storage::disk('s3_private')->move($request->input('basicUpdate.tempImage'), $path);
                Storage::disk('s3_private')->delete($request->input('basicUpdate.tempImage'));
                if ($request->input('save') == 1) {
                    //delete all users previous image first 
                    $medias = Media::where('foreign_table', 'users')->where('foreign_id',
                        auth()->user()->id)->get()->toArray();
                    foreach ($medias as $media) {
                        Storage::disk('s3_private')->delete($media['media_value']);
                    }

                    Media::where('foreign_table', 'users')->where('foreign_id', auth()->user()->id)->delete();
                    Media::create([
                        'foreign_id' => auth()->user()->id,
                        'foreign_table' => 'users',
                        'media_value' => $path,
                        'media_type' => 'image',
                    ]);
                }
            }
        } else {
            $path = Media::where('foreign_table', 'users')->where('foreign_id',
                auth()->user()->id)->first()->media_value;
        }
        return $path;
    }

    /**
     *
     */
    private function skillOperation($skill_operation)
    {
        $skillArray = [];
        if (count($skill_operation['skills']) > 0) {
            foreach ($skill_operation['skills'] as $skill) {
                $skill = (object)$skill;
                $oldSkill = SeekerSkill::where('skill_id', $skill->skill_id)
                    ->where('skill_type', $skill->skill_type)
                    ->where('level', $skill->level)
                    ->where('seeker_id', $this->seekerId)
                    ->first();
                if ($oldSkill) {
                    array_push($skillArray, $oldSkill->id);
                } else {
                    array_push($skillArray, SeekerSkill::create([
                        'skill_type' => $skill->skill_type,
                        'level' => $skill->level,
                        'skill_id' => $skill->skill_id,
                        'seeker_id' => $this->seekerId,
                    ])->id);
                }
            }
        }
        SeekerSkill::whereNotIn('id', $skillArray)->each(function ($item, $key) {
            $item->delete();
        });
    }

    /**
     *
     */
    private function updationOperation($update_operation)
    {
        //call the update functions on each model
        $user = User::find($this->userId);
        $user->fill($update_operation->basicUpdate)->save();
        $seeker = Seeker::find($this->seekerId);
        $seeker->fill($update_operation->basicUpdate);
        $seeker->fill($update_operation->resumeUpdate)->save();
        //
        foreach ($update_operation->input('experienceUpdate.experiences') as $update_create_exp) {
            $update_create_exp['seeker_id'] = $this->seekerId;
            SeekerExperience::UpdateOrCreate(
                [
                    'id' => isset($update_create_exp['id']) ? $update_create_exp['id'] : null
                ],
                $update_create_exp);
        }
        foreach ($update_operation->input('certificateUpdate.certificates') as $update_create_exp) {
            $update_create_exp['seeker_id'] = $this->seekerId;
            SeekerCertificate::UpdateOrCreate(
                [
                    'id' => isset($update_create_exp['id']) ? $update_create_exp['id'] : null
                ],
                $update_create_exp);
        }
        foreach ($update_operation->input('educationUpdate.educations') as $update_create_exp) {
            $update_create_exp['seeker_id'] = $this->seekerId;
            SeekerEducation::UpdateOrCreate(
                [
                    'id' => isset($update_create_exp['id']) ? $update_create_exp['id'] : null
                ],
                $update_create_exp);
        }
    }

    /**
     *
     */
    private function deletionOperation($delete_operation)
    {
        if (count($delete_operation['experience']) > 0) {
            SeekerExperience::whereIn('id', $delete_operation['experience'])->delete();
        }
        if (count($delete_operation['education']) > 0) {
            SeekerEducation::whereIn('id', $delete_operation['education'])->delete();
        }
        if (count($delete_operation['certificate']) > 0) {
            SeekerCertificate::whereIn('id', $delete_operation['certificate'])->delete();
        }
    }

    public function check($job)
    {
        if (!isset($this->seekerId)) {
            return response(['data' => false], 200);
        }
        return response([
            'data' => !is_null(SeekerJob::where('job_id', $job)->where('seeker_id', $this->seekerId)->first())
        ], 200);
    }

    protected function applyJob($jobId, $userData)
    {
        $seeker_job = SeekerJob::updateOrCreate(
            ['seeker_id' => $this->seekerId, 'job_id' => $jobId],
            ['user_data' => $userData]
        );
        //notify to the provider
        $job_related = Job::find($jobId);
        $provider_to_notify = $job_related->provider->user;
        $provider_to_notify->notify(new ApplicationReceived($job_related, auth()->user(), $seeker_job,
            'ja'));

    }
}
