<?php

namespace App\Http\Controllers\API\Job;

use App\Http\Controllers\Controller;
use App\Http\Requests\Job\JobStoreRequest;
use App\Http\Requests\Job\JobUpdateRequest;
use App\Http\Requests\Job\ProviderJobUpdateRequest;
use App\Http\Resources\JobListResource;
use App\Http\Resources\JobResource;
use App\Http\Resources\JobSeekerResource;
use App\Http\Resources\JobTitleResource;
use App\Jobs\FindJobMatchingUser;
use App\Models\Charge;
use App\Models\Job;
use App\Models\JobLanguage;
use App\Models\JobLocation;
use App\Models\JobLogs;
use App\Models\Media;
use App\Models\ProviderTransaction;
use App\Models\Sequence;
use App\Models\User;
use App\Notifications\JobApproved;
use App\Notifications\JobCreatedNotifyAdmin;
use App\Notifications\JobDeclined;
use App\Notifications\JobDeletedNotifyAdmin;
use App\Notifications\JobUpdatedNotifyAdmin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class JobController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @param Request $request
     * @return mixed
     */
    /**
     * @OA\Get(
     *     path="/jobs",
     *     description="jobs List",
     *     summary="Job API",
     *     tags={"Job API"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */
    public function index(Request $request)
    {

        $query = Job::query();
        $query = $this->SearchOperation($query, $request);
        $query->where('jobs.status', '!=', 3);
        $query->groupBy('jobs.id');
        if ($this->userType == 'admin') {
            $query->whereRaw('((jobs.status = 0 and jobs.admin_status = 2) || jobs.status != 0)');
        }
        if ($this->userType == 'seeker' || $this->userType == 'guest') {
            $query->where('jobs.published_at', '>', Carbon::now()->subDays(90));
            return JobSeekerResource::collection($query->paginateByUrl())->translateLocales();
        } else {
            return JobListResource::collection($query->paginateByUrl())->translateLocales();
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    /**
     * @OA\Get(
     *     path="/provider/jobs",
     *     description="jobs List provider",
     *     summary="Job API",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/JobListResource"),
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     *
     */
    public function providerIndex(Request $request)
    {
        $query = Job::query();
        $query = $this->SearchOperation($query, $request);
        $query->where('jobs.status', '!=', 3);
        $query->groupBy('jobs.id');
        $query->where('jobs.provider_id', $this->providerId);
        return JobListResource::collection($query->paginateByUrl())->translateLocales();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Job\JobStoreRequest
     * @param  \App\Http\Models\Job
     * @param  \App\Http\Requests\JobLocation
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/provider/jobs",
     *     description="create new job",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/JobStoreRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(ref="#/components/schemas/JobResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function store(JobStoreRequest $request, Job $job, JobLocation $jobLocation)
    {

        DB::beginTransaction();
        $data = $request->validated();
        $data['job_identifier_id'] = Sequence::getJobIdentifier();
        $data['provider_id'] = $this->providerId;
        // @todo admin status change back to 1 status change back to 0
        $data['admin_status'] = 1; //1 for pending admin status
        $data['slug'] = $job->makeSlugFromTitle($request->get('title_en'));
        $data['description'] = [
            'en' => isset($data['description_en']) ? $data['description_en'] : '',
            'ja' => $data['description_ja'],
        ];
        $data['qualifications'] = [
            'en' => isset($data['qualifications_en']) ? $data['qualifications_en'] : '',
            'ja' => $data['qualifications_ja'],
        ];
        $data['welfare'] = [
            'en' => isset($data['welfare_en']) ? $data['welfare_en'] : '',
            'ja' => $data['welfare_ja'],
        ];
        $data['title'] = [
            'en' => isset($data['title_en']) ? $data['title_en'] : '',
            'ja' => $data['title_ja'],
        ];
        if ($request->has('holiday_days') && is_array($request->input('holiday_days'))) {
            $data['holiday_days'] = implode(",", $request->holiday_days);
        }
        $job = Job::createTranslationLocales($data)->refresh()->translateLocales();
        JobLanguage::create(['job_id' => $job->id, 'language' => 0, 'level' => $data['english_lang_level']]);
        JobLanguage::create(['job_id' => $job->id, 'language' => 1, 'level' => $data['japanese_lang_level']]);
        if ($request->has('langs')) {
            foreach ($request->langs as $language) {
                JobLanguage::create([
                    'job_id' => $job->id,
                    'language' => $language['language'],
                    'level' => $language['level']
                ]);
            }
        }
        if ($request->has('addresses')) {
            foreach ($request->addresses as $address) {
                JobLocation::create([
                    'job_id' => $job->id,
                    'prefecture_id' => $address['prefecture_id'],
                    'address' => $address['address'],
                    'street_address' => isset($address['street_address']) ? $address['street_address'] : null,
                    'zip_code' => $address['zip_code'],
                ]);
            }
        }
        /*
        if ($request->has('images')) {
            foreach ($request->images as $image) {
                if (Storage::disk('s3_private')->exists($image)) {
                    $newImage = explode('/', $image);
                    $key = 'jobs/' . $job->id . '/' . end($newImage);

                    $s3 = Storage::disk('s3_private');
                    $s3->getDriver()->getAdapter()->getClient()->copyObject([
                        'Bucket' => config('app.s3_public_bucket'),
                        'Key' => $key,
                        'CopySource' => urlencode(config('app.s3_private_bucket') . '/' . $image),
                    ]);
                    Storage::disk('s3_private')->delete($image);
                    Media::create([
                        'foreign_id' => $job->id,
                        'foreign_table' => 'jobs',
                        'media_value' => $key,
                        'media_type' => 'image',
                    ]);
                }
            }
        }
        */

        if ($job->status == 1) {
            $provider = auth()->user()->provider;
            if ($provider->paid_status == 1) {
                DB::rollBack();
                return response(['message' => trans('messages.paid_account_under_review')], 403);
            }
            JobLogs::create([
                'job_id' => $job->id,
                'job_detail' => json_encode($data),
            ]);

            if ($request->get('language_translation') == 1) {
                //notify if job status is published ie 1
                //cut point if status is published.
                //check if balance is enough
                $transaction = new ProviderTransaction();

                $charge = Charge::where('type', 'translation')->first();
                if ($provider->point_balance < $charge->point) {
                    DB::rollBack();
                    return response(['message' => trans('messages.points_not_enough')], 403);
                }
                $transaction->create([
                    'points' => $charge->point, //rate from cost table
                    'provider_id' => $this->providerId,
                    'foreign_id' => $job->id,
                    'foreign_table' => 'jobs',
                    'transaction_type' => 2, //spent
                    'charge_type' => 1, //translation
                ]);
                $provider->point_balance -= $charge->point;
                $provider->save();
            }

            //@todo notifcation, mail check, to admin for language translation
            //auth()->user()->notify( ( new JobCreated($job, app()->getLocale()) ) );
            //get all admin
            $userAdmin = User::where('user_type', 'admin')->where('verified', 1)->get();
            foreach ($userAdmin as $admin) {
                $admin->notify(new JobCreatedNotifyAdmin($job, app()->getLocale()));
            }
        }
        DB::commit();

        return (new JobResource($job));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Job $job
     * @return \App\Http\Resources\JobResource
     *
     * @OA\Get(
     *     path="/jobs/{id}",
     *     description="show seeker certificate",
     *     tags={"Job API"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="job id",
     *          in="path",
     *          required=true,
     *         @OA\Schema(type="integer"),
     *          ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(ref="#/components/schemas/JobSeekerResource"),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     )
     * )
     */
    public function show($slug)
    {
        $job = Job::where('slug', urldecode($slug))
            ->where('admin_status', 0)
            ->where('status', 1)
            ->where('published_at', '>', Carbon::now()->subDays(90))
            ->first();
        if ($this->userType == 'admin') {
            if (!$job) {
                $job = Job::find($slug);
            }
            if ($job) {
                return (new JobResource($job))->translateLocales();
            } else {
                abort(404, trans('errors.404'));
            }
        } else {
            if ($job) {
                $job->views++;
                $job->save();
                return (new JobSeekerResource($job))->translateLocales();
            } else {
                abort(404, trans('errors.404'));
            }
        }
        abort(404, trans('errors.404'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Job $job
     * @return \App\Http\Resources\JobResource
     *
     * @OA\Get(
     *     path="/provider/jobs/{id}",
     *     description="show seeker certificate",
     *     tags={"Job API"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *          name="id",
     *          description="job id",
     *          in="path",
     *          required=true,
     *         @OA\Schema(type="integer"),
     *          ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(ref="#/components/schemas/JobResource"),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized"
     *     )
     * )
     */
    public function providerShow($slug)
    {
        $job = Job::find($slug);
        if ($job) {
            if ($this->userType == 'provider' && $job->provider_id != $this->providerId) {
                abort(403, trans('errors.404'));
            }
            return (new JobResource($job))->translateLocales();
        } else {
            abort(404, trans('errors.404'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Seeker\ProviderJobUpdateRequest
     * @param  \App\Http\Models\Job
     * @param  \App\Http\Requests\JobLocation
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/provider/jobs/{id}",
     *     description="update job",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="job id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/ProviderJobUpdateRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *         @OA\JsonContent(ref="#/components/schemas/JobResource")
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function updateProvider(
        ProviderJobUpdateRequest $request,
        $job,
        JobLocation $jobLocation,
        JobLanguage $jobLanguage
    ) {
        $job = Job::findOrFail($job);
        if ($job->provider_id != $this->providerId) {
            abort(403, trans('errors.403'));
        }
        $data = $request->validated();
        DB::beginTransaction();

        if ($job->admin_status == 0) {
            //if job is already approved only change the status
            $job->fill($data)->saveTranslationLocales()->refresh();
            DB::commit();
            return (new JobResource($job))->translateLocales();
        }

        if ($data['status'] == 1 && $job->admin_status != 0) {
            $data['admin_status'] = 1;
        }

        $data['description'] = [
            'en' => isset($data['description_en']) ? $data['description_en'] : '',
            'ja' => $data['description_ja'],
        ];
        $data['qualifications'] = [
            'en' => isset($data['qualifications_en']) ? $data['qualifications_en'] : '',
            'ja' => $data['qualifications_ja'],
        ];
        $data['welfare'] = [
            'en' => isset($data['welfare_en']) ? $data['welfare_en'] : '',
            'ja' => $data['welfare_ja'],
        ];
        $data['title'] = [
            'en' => isset($data['title_en']) ? $data['title_en'] : '',
            'ja' => $data['title_ja'],
        ];
        if ($request->has('holiday_days') && is_array($request->input('holiday_days'))) {
            $data['holiday_days'] = implode(",", $request->holiday_days);
        } else {
            $data['holiday_days'] = null;
        }
        $job->fill($data)->saveTranslationLocales()->refresh();
        $lang_array = [];
        $lang = JobLanguage::where('job_id', $job->id)->where('language', 0)->first();
        $lang->level = $data['english_lang_level'];
        $lang->save();
        array_push($lang_array, $lang->id);

        $lang = JobLanguage::where('job_id', $job->id)->where('language', 1)->first();
        $lang->level = $data['japanese_lang_level'];
        $lang->save();
        array_push($lang_array, $lang->id);

        if ($request->has('holiday_days') && is_array($request->input('holiday_days'))) {
            $data['holiday_days'] = implode(",", $request->holiday_days);
        } else {
            $data['holiday_days'] = null;
        }

        if ($request->has('langs')) {
            foreach ($request->langs as $language) {
                $language = (object)$language;
                if (empty($language->id)) {
                    array_push($lang_array, JobLanguage::create([
                        'language' => $language->language,
                        'job_id' => $job->id,
                        'level' => $language->level
                    ])->id);
                } else {
                    $oldLanguage = JobLanguage::findOrFail($language->id);
                    $oldLanguage->fill(['language' => $language->language, 'level' => $language->level])->save();
                    array_push($lang_array, $oldLanguage->id);
                }
            }
            $jobLanguage->whereNotIn('id', $lang_array)->where('job_id', $job->id)->each(function ($item, $key) {
                $item->delete();
            });
        }

        if ($request->has('addresses')) {
            $addr_array = [];
            foreach ($request->addresses as $address) {
                $address = (object)$address;
                if (empty($address->id)) {
                    array_push($addr_array, JobLocation::create([
                        'job_id' => $job->id,
                        'prefecture_id' => $address->prefecture_id,
                        'address' => $address->address,
                        'street_address' => isset($address->street_address) ? $address->street_address : null,
                        'zip_code' => $address->zip_code,
                    ])->id);
                } else {
                    $oldLocation = JobLocation::findOrFail($address->id);
                    $oldLocation->fill([
                        'prefecture_id' => $address->prefecture_id,
                        'address' => $address->address,
                        'street_address' => isset($address->street_address) ? $address->street_address : null,
                        'zip_code' => $address->zip_code,
                    ])->save();
                    array_push($addr_array, $oldLocation->id);
                }
            }
            $jobLocation->whereNotIn('id', $addr_array)->where('job_id', $job->id)->each(function ($item, $key) {
                $item->delete();
            });
        }

        if ($request->has('deleted_images')) {
            foreach ($request->deleted_images as $mediaid) {
                $media = Media::findOrFail($mediaid);
                //delete file from storage after media delete
                if ($media->foreign_id == $job->id) {
                    Storage::disk('s3_public')->delete($media->media_value);
                    $media->delete();
                }
            }
        }
        $imagecount = $job->jobImages()->count();
        if ($imagecount + count($request->get('images')) > 5 || $imagecount + count($request->get('images')) < 1) {
            DB::rollBack();
            return response(['message' => trans('messages.image_limit')], 403);
        }
/*
        if ($request->has('images')) {
            foreach ($request->images as $image) {
                if (Storage::disk('s3_private')->exists($image)) {
                    $newImage = explode('/', $image);
                    $key = 'jobs/' . $job->id . '/' . end($newImage);

                    $s3 = Storage::disk('s3_private');
                    $s3->getDriver()->getAdapter()->getClient()->copyObject([
                        'Bucket' => config('app.s3_public_bucket'),
                        'Key' => $key,
                        'CopySource' => urlencode(config('app.s3_private_bucket') . '/' . $image),
                    ]);
                    Storage::disk('s3_private')->delete($image);

                    Media::create([
                        'foreign_id' => $job->id,
                        'foreign_table' => 'jobs',
                        'media_value' => $key,
                        'media_type' => 'image',
                    ]);
                }
            }
        }
*/
        if ($job->status == 1) {
            $provider = auth()->user()->provider;
            if ($provider->paid_status == 1) {
                DB::rollBack();
                return response(['message' => trans('messages.paid_account_under_review')], 403);
            }
            JobLogs::create([
                'job_id' => $job->id,
                'job_detail' => json_encode($data),
            ]);

//                check if charge is already paid
            $oldtrans = ProviderTransaction::where('foreign_id', $job->id)
                ->where('provider_id', auth()->user()->provider->id)
                ->where('foreign_table', 'jobs')->first();

            if ($request->get('language_translation') == 1 && !$oldtrans) {
                $transaction = new ProviderTransaction();
                $charge = Charge::where('type', 'translation')->first();
                if ($provider->point_balance < $charge->point) {
                    DB::rollBack();
                    return response(['message' => trans('messages.points_not_enough')], 403);
                }
                $transaction->create([
                    'points' => $charge->point, //rate from cost table
                    'provider_id' => $this->providerId,
                    'foreign_id' => $job->id,
                    'foreign_table' => 'jobs',
                    'transaction_type' => 2, //spent
                    'charge_type' => 1, //translation
                ]);
                $provider->point_balance -= $charge->point;
                $provider->save();
            }

            //only notify if published job.
            $userAdmin = User::where('user_type', 'admin')->where('verified', 1)->get();
            foreach ($userAdmin as $admin) {
                $admin->notify(new JobUpdatedNotifyAdmin($job, app()->getLocale()));
            }
        }
        DB::commit();
        return (new JobResource($job))->translateLocales();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param JobUpdateRequest $request
     * @param  \App\Models\Job $job
     * @return JobResource
     *
     * @OA\Put(
     *     path="/jobs/{id}",
     *     description="update job -",
     *     tags={"Job API"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="job id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/JobUpdateRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     * admin route
     */
    public function update(JobUpdateRequest $request, $job, JobLocation $jobLocation, JobLanguage $jobLanguage)
    {
        $job = Job::findOrFail($job);
        $data = $request->validated();
        return DB::transaction(function () use ($request, $job, $jobLocation, $data, $jobLanguage) {

            if ($data['admin_status'] == 2) {
                //job has been declined
                $data['status'] = 0; //change provider status to draft
                $job->fill($data)->saveTranslationLocales()->refresh();
                $logs = $job->jobLogs()->latest()->first();
                $logs->comment = $data['comment'];
                $logs->save();
                //notify provider job has been declined
                $job->provider->user->notify(new JobDeclined($job, app()->getLocale()));

                return (new JobResource($job))->translateLocales();
            }

            $data['description'] = [
                'en' => isset($data['description_en']) ? $data['description_en'] : '',
                'ja' => $data['description_ja'],
            ];
            $data['qualifications'] = [
                'en' => isset($data['qualifications_en']) ? $data['qualifications_en'] : '',
                'ja' => $data['qualifications_ja'],
            ];
            $data['welfare'] = [
                'en' => isset($data['welfare_en']) ? $data['welfare_en'] : '',
                'ja' => $data['welfare_ja'],
            ];
            $data['title'] = [
                'en' => isset($data['title_en']) ? $data['title_en'] : '',
                'ja' => $data['title_ja'],
            ];

            if ($data['admin_status'] == 0) {
                $data['published_at'] = date('Y-m-d H:i:s');
                $data['posting_start_date'] = date('Y-m-d H:i:s');
            }

            if ($request->has('holiday_days') && is_array($request->input('holiday_days'))) {
                $data['holiday_days'] = implode(",", $request->holiday_days);
            } else {
                $data['holiday_days'] = null;
            }
            $job->fill($data)->saveTranslationLocales()->refresh();
            $lang_array = [];
            $lang = JobLanguage::where('job_id', $job->id)->where('language', 0)->first();
            $lang->level = $data['english_lang_level'];
            $lang->save();
            array_push($lang_array, $lang->id);

            $lang = JobLanguage::where('job_id', $job->id)->where('language', 1)->first();
            $lang->level = $data['japanese_lang_level'];
            $lang->save();
            array_push($lang_array, $lang->id);
            if ($request->has('langs')) {
                foreach ($request->langs as $language) {
                    $language = (object)$language;
                    if (empty($language->id)) {
                        array_push($lang_array, JobLanguage::create([
                            'language' => $language->language,
                            'job_id' => $job->id,
                            'level' => $language->level
                        ])->id);
                    } else {
                        $oldLanguage = JobLanguage::findOrFail($language->id);
                        $oldLanguage->fill(['language' => $language->language, 'level' => $language->level])->save();
                        array_push($lang_array, $oldLanguage->id);
                    }
                }
                $jobLanguage->whereNotIn('id', $lang_array)->where('job_id', $job->id)->each(function ($item, $key) {
                    $item->delete();
                });
            }

            if ($request->has('addresses')) {
                $addr_array = [];
                foreach ($request->addresses as $address) {
                    $address = (object)$address;
                    if (empty($address->id)) {
                        array_push($addr_array, JobLocation::create([
                            'job_id' => $job->id,
                            'prefecture_id' => $address->prefecture_id,
                            'address' => $address->address,
                            'street_address' => isset($address->street_address) ? $address->street_address : null,
                            'zip_code' => $address->zip_code,
                        ])->id);
                    } else {
                        $oldLocation = JobLocation::findOrFail($address->id);
                        $oldLocation->fill([
                            'prefecture_id' => $address->prefecture_id,
                            'address' => $address->address,
                            'street_address' => isset($address->street_address) ? $address->street_address : null,
                            'zip_code' => $address->zip_code,
                        ])->save();
                        array_push($addr_array, $oldLocation->id);
                    }
                }
                $jobLocation->whereNotIn('id', $addr_array)->where('job_id', $job->id)->each(function ($item, $key) {
                    $item->delete();
                });
            }
            if ($request->has('deleted_images')) {
                foreach ($request->deleted_images as $mediaid) {
                    $media = Media::findOrFail($mediaid);
                    //delete file from storage after media delete
                    if ($media->foreign_id == $job->id) {
                        Storage::disk('s3_public')->delete($media->media_value);
                        $media->delete();
                    }
                }
            }

            $imagecount = $job->jobImages()->count();
            if ($imagecount + count($request->get('images')) > 5 || $imagecount + count($request->get('images')) < 1) {
                return response(['message' => trans('messages.image_limit')], 403);
            }
/*
            if ($request->has('images')) {
                foreach ($request->images as $image) {
                    if (Storage::disk('s3_private')->exists($image)) {
                        $newImage = explode('/', $image);
                        $key = 'jobs/' . $job->id . '/' . end($newImage);

                        $s3 = Storage::disk('s3_private');
                        $s3->getDriver()->getAdapter()->getClient()->copyObject([
                            'Bucket' => config('app.s3_public_bucket'),
                            'Key' => $key,
                            'CopySource' => urlencode(config('app.s3_private_bucket') . '/' . $image),
                        ]);
                        Storage::disk('s3_private')->delete($image);

                        Media::create([
                            'foreign_id' => $job->id,
                            'foreign_table' => 'jobs',
                            'media_value' => 'jobs/' . $job->id . '/' . end($newImage),
                            'media_type' => 'image',
                        ]);
                    }
                }
            }
*/
            //add to admin while update,approve
            //job approved notification
            if ($job->admin_status === 0) {

                $job->provider->user->notify(new JobApproved($job, app()->getLocale()));

                $is_already_processed = DB::table('job_notify_users')->where('job_id', $job->id)->first();
                if (!$is_already_processed) {
                    FindJobMatchingUser::dispatch($job);
                }
            }
            return (new JobResource($job))->translateLocales();
        });
    }

    /**
     * get user detail
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/provider/jobs/renew/{id}",
     *     description="provider job renew",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Parameter(
     *         name="id",
     *         description="job id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Job renewed",
     *     ),@OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     */
    public function renewJob($job)
    {
        $job = Job::findOrFail($job);
        if ($job->provider_id != $this->providerId && $job->admin_status != 0) {
            abort(403);
        }
        if (now() >= $job->published_at->addDays(76) && now() <= $job->published_at->addDays(90)) {
            $job->published_at = $job->published_at->addDays(90);
            $job->mail_notified_expire = 0;
            $job->save();
            return response(['message' => trans('messages.job_renewed')], 200);
        }
        abort(403);
    }


    public function destroy(Job $job)
    {
        if ($job->provider_id == $this->providerId) {
            $job->delete();
            //get all admin
            $userAdmin = User::where('user_type', 'admin')->where('verified', 1)->get();
            foreach ($userAdmin as $admin) {
                $admin->notify(new JobDeletedNotifyAdmin($job));
            }
            return response(null, 204);
        }
    }


    private function SearchOperation($query, $request)
    {
        if ($request->filled('id')) {
            $query->whereIn('id', str_getcsv($request->input('id')));
        }
        if ($request->filled('admin_status') || $this->userType == 'seeker' || $this->userType == 'guest') {
            $query->where(
                'admin_status',
                $request->filled('admin_status') ? str_getcsv($request->input('admin_status')) : 0
            );
        }
        if ($request->filled('status') || $this->userType == 'seeker' || $this->userType == 'guest') {
            $query->where(
                'status',
                $request->filled('status') ? str_getcsv($request->input('status')) : 1
            );
        }

        foreach ((new Job())->getSearchable() as $search_q) {
            if ($request->filled($search_q)) {
                $query->whereIn($search_q, str_getcsv($request->input($search_q)));
            }
        }
        if ($request->filled('salary_range')) {
            $query->where('min_salary', '>=', str_getcsv($request->input('salary_range')));
            //$query->where('max_salary','<=',str_getcsv($request->input('salary_range')));
        }
        //echo $query->toSql();die;
        if ($request->filled('prefecture_id')) {
            $query = $query->whereHas('locations', function ($location_query) use ($request) {
                $location_query->whereIn('prefecture_id', str_getcsv($request->input('prefecture_id')));
            });
        }
        if ($request->filled('japanese_language_level')) {
            $query = $query->whereHas('languages', function ($lang_query) use ($request) {
                $lang_query->where('language', 1);
                $lang_query->whereIn('level', str_getcsv($request->input('japanese_language_level')));
            });
        }

        if ($request->filled('no_japanese')) {
            $query = $query->whereHas('languages', function ($lang_query) use ($request) {
                $lang_query->where('language', 1);
                $lang_query->where('level', 100);
            });
        }
        if ($request->filled('english_language_level')) {
            $query = $query->whereHas('languages', function ($lang_query) use ($request) {
                $lang_query->where('language', 0);
                $lang_query->whereIn('level', str_getcsv($request->input('english_language_level')));
            });
        }
        //title is passed from provider or admin
        if ($request->filled('title')) {
            $query->join('languages', 'jobs.id', '=', 'languages.foreign_id');
            $query->select(
                'jobs.*',
                'languages.table_name',
                'languages.column_name',
                'languages.foreign_id',
                'languages.locale_text'
            );
            $query->where('languages.table_name', 'jobs');
            $query->where('languages.column_name', 'title');
            $query->where('languages.locale_text', 'LIKE', "%" . $request->input('title') . "%");
        }

        //full text search for seeker
        if ($request->filled('search')) {
            $query->join('languages', 'jobs.id', '=', 'languages.foreign_id');
            $query->select(
                'jobs.*',
                'languages.table_name',
                'languages.column_name',
                'languages.foreign_id',
                'languages.locale_text'
            );
            $query->where('languages.table_name', 'jobs');
            $query->whereFullText(['locale_text'], $request->input('search'));
        }

        if ($request->filled('created_at')) {
            $query->whereMonth('jobs.created_at', '=', (
            new Carbon(
                $request->input('created_at')
            ))->month);
            $query->whereYear('jobs.created_at', '=', (
            new Carbon(
                $request->input('created_at')
            ))->year);
        }
        //echo $query->toSql();die;
        return $query;
    }

    /**
     * get user detail
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Get(
     *     path="/provider/jobs/title",
     *     description="provider job titles",
     *     tags={"Provider APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\Response(
     *         response=200,
     *         description="Job titles",
     *         @OA\JsonContent(ref="#/components/schemas/JobTitleResource")
     *     ),@OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             ref="#/components/schemas/Unauthenticated"
     *         )
     *     )
     * )
     */
    public function titles()
    {
        $query = Job::query();
        $query->select('title', 'id', 'slug');
        $query->where('provider_id', $this->providerId);
        $query->where('admin_status', 0);
        $query->orderBy('published_at', 'desc');
        return JobTitleResource::collection($query->paginate(200))->translateLocales();
    }

    public function jobCreatePaidCheck()
    {
        $provider = auth()->user()->provider;
        if ($provider->paid_status == 1) {
            return 'response your account is under review';
        }
    }


    public function dispatchMailJob()
    {
        $job = Job::where('id', 1)->with(['locations', 'languages', 'provider'])->first();
        $job = Job::where('id',2)->with(['locations','languages','provider'])->first();
        /*
        $direct_match_column = [
            'visa_support',
            'social_insurance',
            'housing_allowance',
            'hairstyle_freedom',
            'clothing_freedom'
        ];
        Cache::flush();

        //print_r($job);
        $seeker_setting = SeekerMailSetting::where('receive_mail',1)->get()->toArray();
        //print_r($seeker_setting);
        $start_time = time();
        echo 'start time: '.$start_time;
        $matched_seekers = array_filter($seeker_setting, function($seeker) use ($job , $direct_match_column){
            //print_r($job);
            $match = false;
            //CONTINUE IF $match IS TRUE FROM THIS POINT FORWARD ESLE RETURN FALSE:
            //DIRECT MATCHING
            //if($match == true){
            echo 'direct//';
            foreach($direct_match_column as $key=>$val){
                //if($seeker[$val] != $job[$val] && $seeker[$val] != NULL){
                if($seeker[$val] === NULL || $seeker[$val] == 0){
                    $match = true;
                }else if($seeker[$val] == 1){
                    if($seeker[$val] != $job[$val]){
                        $match = false;
                        break;
                    }else{
                        $match = true;
                    }
                }
            };
            //}

            //SALARY MATCHING
            if(($seeker['min_salary'] !== NULL || $seeker['max_salary'] !== NULL) && $match == true){
                echo 'salary//';
                if($seeker['min_salary'] !== NULL && $job['min_salary'] >= $seeker['min_salary']) $match = true;
                if($seeker['max_salary'] !== NULL && $job['max_salary'] >= $seeker['min_salary']) $match = true;
            }

            //EMPLOYMENT TYPE MATCHING
            if($match == true && ($seeker['employment_type'] !== NULL && $seeker['employment_type'] != '') ){
                echo 'employment_type//';
                $seek_emp_type = explode(',',$seeker['employment_type']);
                $match = in_array($job['employment_type'],$seek_emp_type);
            }
            //CATEGORY MATCHING
            if($match == true && ($seeker['job_category'] !== NULL && $seeker['job_category'] != '')){
                echo 'job_category//';
                $seek_cat = explode(',',$seeker['job_category']);
                $match = in_array($job['category_id'],$seek_cat);
            }
            //JOB LOCATION
            if($match == true && ($seeker['job_location'] !== NULL && $seeker['job_location'] != '')){
                echo 'job_location//';
                $seek_job_location = explode(',',$seeker['job_location']);
                foreach($job['locations'] as $j_location){
                    if(in_array($j_location['prefecture_id'], $seek_job_location)){
                        $match = true;
                        break;
                    }else{
                        $match = false;
                        continue;
                    }
                }
            }
            //JOB LANGUAGES
            if($match == true){
                echo 'languages//';
                $japanese_match = false;
                $english_match = false;
                //english -> 0 , japanese -> 1
                foreach($job['languages'] as $j_lang){
                    //FOR JAPANESE AND ENGLISH ONLY
                    if($j_lang['language'] == 1){
                        if($seeker['japanese_level'] === NULL ){
                            $japanese_match = true;
                            continue;
                        }
                        else if($j_lang['level'] == 100){
                            $japanese_match = true;
                            continue;
                        }else if($seeker['japanese_level'] >= $j_lang['level'] )
                        {
                            $japanese_match = true;
                            continue;
                        }else{
                            $japanese_match = false;
                            continue;
                        }
                    }
                    if($j_lang['language'] == 0){
                        if($seeker['english_level'] === NULL ){
                            $english_match = true;
                            continue;
                        }else if($j_lang['level'] == 100){
                            $english_match = true;
                            continue;
                        }else if($seeker['english_level'] >= $j_lang['level'] )
                        {
                            $english_match = true;
                            continue;
                        }else{
                            $english_match = false;
                            continue;
                        }
                    }
                $match = ($english_match == true && $japanese_match == true);
                }
            }

            return $match;

        });
        $end_time = time();
        echo ' end time: '.$end_time;
        echo ' time taken: '. ($end_time - $start_time).'//';

        echo 'matched count--'.count($matched_seekers);

        //INSERT TO TABLE FOR DELIVERYING MAIL
        if(count($matched_seekers) > 0){
            $seeker_ids = [];
            foreach($matched_seekers as $match){
                $seeker_ids[] = $match['seeker_id'];
            }
            $insert_seeker_ids = implode(',',$seeker_ids);
            $insert = DB::table('job_notify_users')->insert([
                'job_id'=>$job['id'],
                'seeker_ids'=>$insert_seeker_ids
            ]);
        }*/
        FindJobMatchingUser::dispatch($job);
    }
    /**
     * public function formatDataAndSaveToFile(){
     *
     * //print 1 lakh users data here
     * //$r = [];
     * //for($i=100000;$i<=300000;$i++){
     * //  $r[] = $i;
     * //}
     * //echo implode(',',$r);die;
     * $record = DB::table('job_notify_users')->where('notified',0)->get()->toArray();
     * //echo 'Total record: '.count($record);
     *
     * $seekers_to_mail = [];
     * if(count($record) > 0){
     * foreach($record as $data){
     * $seekers = explode(',',$data->seeker_ids);
     * foreach($seekers as $seeker){
     * if(isset($seekers_to_mail[$seeker])){
     * if(!in_array($data->job_id,$seekers_to_mail[$seeker])){
     * $seekers_to_mail[$seeker][] = $data->job_id;
     * }
     * }else{
     * $seekers_to_mail[$seeker][] = $data->job_id;
     * }
     *
     * }
     * DB::table('job_notify_users')->where('id',$data->id)->update(['notified'=>1]);
     * }
     * }
     * if($seekers_to_mail){
     * $file_name = time().'.json';
     * Storage::disk('local')->put('match-seekers-mail/'.$file_name, json_encode($seekers_to_mail));
     * DB::table('file_to_queue_tracker')->insert(
     * [
     * 'file_name'=>$file_name,
     * 'processed'=>0
     * ]
     * );
     * }
     *
     * }
     *
     * public function processFileToQueue(){
     * // Read File
     * //get file in queue from table
     * $file_name = DB::table('file_to_queue_tracker')->where('processed',0)->first();
     * if($file_name){
     * $file = Storage::disk('local')->get('match-seekers-mail/'.$file_name->file_name);
     * $data = json_decode($file, true);
     * $to_db = [];
     * if(count($data) > 100){
     * $i=1;
     * foreach($data as $key=>$to_queue){
     * echo $key;
     * $i++;
     * //process to queue
     * unset($data[$key]);
     * $seeker = Seeker::find($key);
     * $mail_setting = $seeker->mailSetting()->first();
     * //$mail_setting->receive_mail_language;
     * $user_to_notify = $seeker->user;
     * $user_to_notify->notify(new SendMatchedJobToSeeker($to_queue, $mail_setting->receive_mail_language));
     * if($i == 100) break;
     * }
     * Storage::disk('local')->put('match-seekers-mail/'.$file_name->file_name, json_encode($data));
     * }else{
     * $i=1;
     * foreach($data as $key=>$to_queue){
     * $i++;
     * //process to queue
     * unset($data[$key]);
     * $seeker = Seeker::find($key);
     * $mail_setting = $seeker->mailSetting()->first();
     * //print_r($mail_setting);die;
     * $user_to_notify = $seeker->user;
     * $user_to_notify->notify(new SendMatchedJobToSeeker($to_queue, $mail_setting->receive_mail_language));
     * if($i == 100) break;
     * }
     * Storage::disk('local')->put('match-seekers-mail/'.$file_name->file_name, json_encode($data));
     * DB::table('file_to_queue_tracker')->where('file_name',$file_name->file_name)->update(['processed'=>1]);
     * }
     * }
     *
     * }
     */
}
