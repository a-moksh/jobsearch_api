<?php

namespace App\Http\Controllers\API\Job;

use App\Http\Controllers\Controller;
use App\Models\JobLocation;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function destroy(JobLocation $location)
    {
        if ($location->job->provider_id == $this->providerId) {
            return DB::transaction(function () use ($location) {
                $location->delete();
                return response(null, 204);
            });
        }
    }
}
