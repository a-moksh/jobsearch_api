<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\AttrResource;
use App\Models\Attr;
use Illuminate\Http\Request;

class AttrController extends Controller
{

    public function index(Request $request)
    {
        $request->normalize();
        $query = Attr::query();
        if ($request->filled('name')) {
            $query->whereIn('name', str_getcsv($request->input('name')));
        }
        $query->where('is_private', 0);
        $query->orderBy('display_order');
        $query->with([
            'opts' => function ($query) {
                $query->orderBy('display_order');
            },
            'opts.child' => function ($query) {
                $query->orderBy('display_order');
            },
            'opts.child.opts' => function ($query) {
                $query->orderBy('display_order');
            },
            'opts.child.opts.child' => function ($query) {
                $query->orderBy('display_order');
            }
        ]);

        return AttrResource::collection($query->paginateByUrl())->translate();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServiceAttr $serviceAttr
     * @return \App\Http\Resources\ServiceAttrResource  $serviceAttrResource
     */

    public function show(Attr $attr)
    {
        return (new AttrResource($attr))->translate();
    }
}
