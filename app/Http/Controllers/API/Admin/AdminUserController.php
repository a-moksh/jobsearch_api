<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminRegisterRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Requests\VerifyAdminEmailRequest;
use App\Http\Resources\UserResource;
use App\Models\Sequence;
use App\Models\User;
use App\Models\VerifyUser;
use App\Notifications\PasswordChanged;
use App\Notifications\UserCreated;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class AdminUserController extends Controller
{
    use Notifiable;

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\RegisterRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/admin/register",
     *     description="create new user",
     *     tags={"Admin APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/AdminRegisterRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Created",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     */
    public function store(AdminRegisterRequest $request)
    {
        return DB::transaction(function () use ($request) {
            //prepare new user store
            $already_created = false;
            $data = $request->validated();
            $data['deletable'] = ($request->has('deletable')) ? $request->input('deletable') : 1;
            $user = User::where('email', $data['email'])->where('user_type', 'admin')->first();
            if ($user) {
                $user->update($data);
                $already_created = true;
                VerifyUser::where('user_id', $user->id)->delete();
            } else {
                $data['user_identifier_id'] = Sequence::getUserIdentifier();
                $user = User::create($data);
            }
            //email verification setup
            $verifyUser = new VerifyUser(['user_id' => $user->id]);
            $user->verifyUser()->save($verifyUser);

            //fire events for register user;
            $user->notify(new UserCreated($user, app()->getLocale()));
            $response['message'] = trans('auth.register.success');
            $response['email'] = $user->email;
            return response($response, 201);
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {

        if (auth()->user()->id !== $user->id) {
            abort(403);
        }
        $data = $request->validated();
        return DB::transaction(function () use ($request, $user, $data) {
            if ($request->has('password')) {
                $data['password'] = bcrypt($data['password']);
                auth()->user()->notify(new PasswordChanged(auth()->user(), app()->getLocale()));
            }
            $user->fill($data);
            $user->save();
            return (new UserResource($user))->additional([
                'message' => trans('messages.updated')
            ]);
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        if ($user->id != auth()->user()->id && $user->deletable == 1) {
            $user->delete();
            return response(null, 204);
        } else {
            $message = trans('auth.user_not_deletable');
            return response(['message' => $message], 402);
        }

    }

    public function getUser()
    {
        return new UserResource(auth()->user());
    }

    /**
     * Verify user Activation
     *
     * @param  \App\Http\Requests\VerifyUserEmailRequest
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/admin/verify/email",
     *     description="verify uer email",
     *     tags={"Admin APIs"},
     *     security={{"JWT":{""}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/VerifyAdminEmailRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful verification of email",
     *     ),@OA\Response(
     *         response=402,
     *         description="Not Authorized",
     *     ),@OA\Response(
     *         response=401,
     *         description="Token Expired",
     *     ),@OA\Response(
     *         response=400,
     *         description="already verified or error",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="message",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     )
     * )
     */

    public function verifyAdminEmail(VerifyAdminEmailRequest $request)
    {
        $verifyUser = VerifyUser::where('token', urldecode($request->input('token')))->first();
        if ($verifyUser->created_at->diffInHours(Carbon::now()) > 4) {
            VerifyUser::where('token', $request->input('token'))->delete();
            return response()->json(['message' => trans('auth.email.token_expired'), 'email' => $user->email], 401);
        }
        $user = $verifyUser->user;
        if (!$user->verified && $user->email == $request->get('email') && $verifyUser->token == $request->get('token') && $user->user_type == 'admin') {
            return DB::transaction(function () use ($request, $user, $verifyUser) {
                $verifyUser->user->verified = 1;
                $user->password = bcrypt($request->input('password'));
                $user->full_name = $request->input('full_name');
                $user->katakana_full_name = $request->input('katakana_full_name');
                $user->save();
                $message = trans('auth.email.verified');
                return response(['message' => $message], 200);
            });
        } else {
            if ($user->user_type != 'admin') {
                $message = trans('auth.email.notAuthorized');
                return response(['message' => $message], 402);
            } else {
                if ($user->verified && $user->email) {
                    $message = trans('auth.email.alreadyVerified');
                } else {
                    $message = trans('auth.tokenFailed');
                }
            }
            return response(['message' => $message], 400);
        }
    }
}
