<?php

namespace App\Http\Controllers;

use App\Http\Resources\ZipCodeResource;
use App\Models\ZipCode;

class ZipCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     *
     * * @OA\Get(
     *     path="/zipcode/{zipcode}",
     *     description="get address from zipcode",
     *     summary="get address from zipcode",
     *     tags={"Common APIs"},
     *     @OA\Parameter(
     *         name="zipcode",
     *         description="zipcode",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/ZipCodeResource")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     )
     * )
     */
    public function __invoke($zipcode, ZipCode $zipCode)
    {
        $zipcode = $zipCode->where('zip_code', $zipcode)->first();
        if ($zipcode) {
            return new ZipCodeResource($zipcode);
        }
        abort(404, trans('errors.404'));
    }
}
