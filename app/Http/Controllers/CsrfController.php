<?php

namespace App\Http\Controllers;

class CsrfController extends Controller
{
    public function __invoke()
    {
        return response(['csrf_token' => csrf_token()]);
    }
}
