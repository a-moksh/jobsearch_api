<?php

namespace App\Http\Controllers;

use App\Http\Resources\I18nResource;
use Illuminate\Support\Facades\Cache;

class LangController extends Controller
{

    /**
     * return translate file
     * @param  string $locale
     * @param  string $key
     * @return \Illuminate\Http\Response
     *
     * @OA\Get(
     *    path="/langs/{locale}",
     *    description= "API to get translation file",
     *    summary ="translation ",
     *    tags={"Translation file"},
     *       @OA\Parameter(
     *          name="locale",
     *          description="locales name",
     *          in="path",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *              format="int64"
     *          ),
     *          ),
     *       @OA\Parameter(
     *          name="key",
     *          description="key name",
     *          in="query",
     *          required=true,
     *         @OA\Schema(type="string"),
     *          ),
     *          @OA\Response(
     *         response=422,
     *         description="Unprocessable Entity",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="message",
     *                 description="メッセージ",
     *                 type="string",
     *             ),
     *             @OA\Property(
     *                 property="errors",
     *                 ref="#/components/schemas/Errors",
     *             )
     *         )
     *     ),
     * )
     *
     */
    public function __invoke($locale = null, $key = null)
    {
        $request = request();
        if ($request->filled('key')) {
            $key = $request->input('key');
        }

        // Default locale
        $defaultLocale = config('app.fallback_locale');

        // If no locale is specified, the default locale is set
        if (empty($locale)) {
            $locale = $defaultLocale;
        }

        if (!empty($key)) {
            $key = str_replace('/', '.', $key);
        }

        // Retrieve and cache the entire list of locales
        $data = [];

        // Merge based on default locale
        $locales = [];
        $locales[] = $defaultLocale;
        if ($locale !== $defaultLocale) {
            $locales[] = $locale;
        }
        foreach ($locales as $_locale) {
            // Delete cache except production environment
            $cacheKey = "locales_{$_locale}";
            Cache::forget($cacheKey); // delete cache

            // get translated data
            $localeData = Cache::rememberForever($cacheKey, function () use ($_locale, $key) {
                $localeData = [];
                $resourceBase = resource_path("lang/{$_locale}");
                $iterator = new \RecursiveDirectoryIterator($resourceBase);
                $iterator = new \RecursiveIteratorIterator($iterator);
                foreach ($iterator as $fileinfo) {
                    if ($fileinfo->isFile()) {
                        if (preg_match('/\.php$/', $iterator->current())) {
                            $file = $iterator->getPathname();
                            $key = basename(str_replace('/', '.', substr($file, strlen($resourceBase) + 1)), '.php');
                            array_set($localeData, $key, require $file);
                        }
                    }
                }
                array_walk_recursive($localeData, function (&$val, $key) {
                    $val = preg_replace('/\:(\w+)/', '{${1}}', $val);
                });
                return $localeData;
            });

            // merge
            $data = array_replace_recursive($data, $localeData);
        }

        // Acquires and returns only the value specified by dot syntax
        $data = array_get($data, $key);
        if (empty($data)) {
            return response(null, 404);
        }
        return response(['data' => $data, 'meta' => ['locale' => $locale]]);
    }
}
