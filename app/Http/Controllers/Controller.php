<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->userType = 'guest';
            $this->seekerId = '';
            $this->providerId = '';
            $this->userId = '';
            if (auth()->user()) {
                $this->userType = auth()->user()->user_type;
                $this->userId = auth()->user()->id;
                if (auth()->user()->user_type == 'seeker') {
                    $this->seekerId = auth()->user()->seeker->id;
                } else {
                    if (auth()->user()->user_type == 'provider') {
                        $this->providerId = auth()->user()->provider->id;
                    }
                }
            } else {
                $this->userType = 'guest';
            }
            return $next($request);
        });
    }
}
