<?php
/**
 * @OA\Info(
 *     version="1.0.0",
 *     title="JOBBANK OPEN API",
 *     description="(JSON Web Token)is required. JWT: ",
 *     @OA\Contact(
 *         email="skill.rabi@gmail.co.jp"
 *     )
 * )
 */

/**
 * @OA\Server(
 *     url="/api/v1"
 * )
 */

/**
 * @OA\SecurityScheme(
 *     securityScheme="JWT",
 *     type="http",
 *     scheme="bearer",
 *     bearerFormat="JWT"
 * )
 */

/**
 * @OA\Schema(
 *     schema="Unauthenticated",
 *     type="object",
 *     @OA\Property(
 *       property="message",
 *       description="message",
 *       type="string",
 *       example="Unauthenticated."
 *     )
 * )
 */

/**
 * @OA\Schema(
 *     schema="Timestamp",
 *     type="string",
 *     format="date-time",
 *     example="2018-10-21T20:01:24+09:00"
 * )
 */

/**
 * @OA\Schema(
 *     schema="Error",
 *     type="object",
 *     @OA\Property(
 *       property="message",
 *       description="message",
 *       type="string",
 *     ),
 *     @OA\Property(
 *       property="errors",
 *       type="object",
 *       @OA\Schema(
 *           ref="#/components/schemas/Errors"
 *       )
 *     )
 * )
 */

/**
 * @OA\Schema(
 *     schema="Errors",
 *     type="object",
 *     @OA\Property(
 *       property="field",
 *       type="array",
 *       @OA\Items(
 *         description="message",
 *         type="string",
 *       )
 *     )
 * )
 */

/**
 * @OA\Schema(
 *     schema="CollectionLinks",
 *     type="object",
 *     @OA\Property(
 *       property="first",
 *       description="first page",
 *       type="string",
 *       format="uri",
 *       example="http://host/api/v1/products?page=1"
 *     ),
 *     @OA\Property(
 *       property="last",
 *       description="last page",
 *       type="string",
 *       format="uri",
 *       example="http://host/api/v1/products?page=2"
 *     ),
 *     @OA\Property(
 *       property="prev",
 *       description="previous page",
 *       type="string",
 *       format="uri",
 *       example=null
 *     ),
 *     @OA\Property(
 *       property="next",
 *       description="next page",
 *       type="string",
 *       format="uri",
 *       example="http://host/api/v1/products?page=2"
 *     )
 * )
 */

/**
 * @OA\Schema(
 *     schema="CollectionMeta",
 *     type="object",
 *     @OA\Property(
 *       property="current_page",
 *       description="current page number",
 *       type="integer",
 *       example=1
 *     ),
 *     @OA\Property(
 *       property="from",
 *       description="record start from",
 *       type="integer",
 *       example=1
 *     ),
 *     @OA\Property(
 *       property="last_page",
 *       description="last page number",
 *       type="integer",
 *       example=2
 *     ),
 *     @OA\Property(
 *       property="path",
 *       description="resource path",
 *       type="string",
 *       example="http://host/api/v1/products"
 *     ),
 *     @OA\Property(
 *       property="per_page",
 *       description="per page",
 *       type="integer",
 *       example=25
 *     ),
 *     @OA\Property(
 *       property="to",
 *       description="record up to",
 *       type="integer",
 *       example=25
 *     ),
 *     @OA\Property(
 *       property="total",
 *       description="total page",
 *       type="integer",
 *       example=33
 *     )
 * )
 */

/**
 * Class ForgotPasswordRequest
 *
 * @OA\Schema(
 *    schema="ForgotPasswordRequest",
 *      required={"email"},
 *    @OA\Property(
 *        property="email",
 *        description="user email",
 *        type="string"
 *    )
 * )
 */

/**
 * Class ResetPasswordRequest
 *
 * @OA\Schema(
 *    schema="ResetPasswordRequest",
 *      required={"email", "password", "password_confirmation", "token"},
 *    @OA\Property(
 *        property="email",
 *        description="user email",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="password",
 *        description="password",
 *        type="string",
 *    ),@OA\Property(
 *        property="token",
 *        description="password reset token",
 *        type="string",
 *    ),@OA\Property(
 *        property="password_confirmation",
 *        description="password confirmation",
 *        type="string",
 *    )
 * )
 */
