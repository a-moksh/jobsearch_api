<?php

namespace App\Http\Requests;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class LoginRequest
 * @package App\Http\Resources
 *
 * @OA\Schema(
 *    schema="LoginRequest",
 *      required={"email", "password","remember_me"},
 *    @OA\Property(
 *        property="email",
 *        description="user email address",
 *        type="string"
 *    ),@OA\Property(
 *        property="password",
 *        description="password",
 *        type="string"
 *    ),@OA\Property(
 *        property="remember_me",
 *        description="remember me of the user",
 *        type="boolean"
 *    )
 * )
 */
class LoginRequest extends FormRequest
{
    const NAME = 'auth';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|string',
            'password' => 'required|string',
            'remember_me' => 'required'
        ];
    }

}
