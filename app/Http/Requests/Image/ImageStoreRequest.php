<?php

namespace App\Http\Requests\Image;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ImageStoreRequest extends FormRequest
{
    const NAME = 'image';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => [
                'required',
                'image',
                'mimes:jpeg,jpg,png,gif',
                'max:5120',
            ],
            'job_id' => [
                Rule::exists('jobs', 'id')->where(function ($query) {
                    if (auth()->user()->user_type != 'admin') {
                        $query->where('provider_id', auth()->user()->provider->id);
                    }
                })
            ]
        ];
    }
}
