<?php

namespace App\Http\Requests\Image;

use App\Extensions\Foundation\Http\FormRequest;

class JobApplyVideoRequest extends FormRequest
{
    const NAME = 'video';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'video' => [
                'required',
//                'mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4,video/webm'
            ]
        ];
    }
}
