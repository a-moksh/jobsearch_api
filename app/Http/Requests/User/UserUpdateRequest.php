<?php

namespace App\Http\Requests\User;

use App\Extensions\Foundation\Http\FormRequest;
use App\Rules\ValidateSeekerImage;
use App\Rules\ValidZipCode;

/**
 * Class UserUpdateRequest
 *
 * @OA\Schema(
 *    schema="UserUpdateRequest",
 *    required={"email", "full_name","katakana_full_name","dob","gender","japanese_language_level","japanese_writing_level","english_language_level","country","living_country","change_password","desired_job_category","desired_annual_min_salary_range","experience_year"},
 *    @OA\Property(
 *        property="email",
 *        description="user email",
 *        type="string"
 *    ),@OA\Property(
 *        property="full_name",
 *        description="full name",
 *        type="string",
 *    ),@OA\Property(
 *        property="katakana_full_name",
 *        description="katakana full name",
 *        type="string",
 *    ),@OA\Property(
 *        property="dob",
 *        description="date of birth",
 *        type="string",
 *    ),@OA\Property(
 *        property="gender",
 *        description="gender of user",
 *        type="integer",
 *    ),@OA\Property(
 *        property="mobile",
 *        description="mobile of user",
 *        type="integer",
 *    ),@OA\Property(
 *        property="japanese_language_level",
 *        description="japanese language level of user",
 *        type="integer",
 *    ),@OA\Property(
 *        property="english_language_level",
 *        description="english language level of user",
 *        type="integer",
 *    ),@OA\Property(
 *        property="japanese_writing_level",
 *        description="japanese writing level of user",
 *        type="integer",
 *    ),@OA\Property(
 *        property="country",
 *        description="country of user",
 *        type="integer",
 *    ),@OA\Property(
 *        property="living country",
 *        description="living counry of user",
 *        type="integer",
 *    ),@OA\Property(
 *        property="visa_staus",
 *        description="visa staus of user",
 *        type="string",
 *    ),@OA\Property(
 *        property="visa_issue_date",
 *        description="visa issue date of user",
 *        type="string",
 *    ),@OA\Property(
 *        property="state",
 *        description="state of of user",
 *        type="integer",
 *    ),@OA\Property(
 *        property="visa_expiry_date",
 *        description="visa expiry date of user",
 *        type="string",
 *    ),@OA\Property(
 *        property="zip_code",
 *        description="zip code of user if living country is japan",
 *        type="integer",
 *    ),@OA\Property(
 *        property="prefecture_id",
 *        description="prefecture of user if living in japan",
 *        type="integer",
 *    ),@OA\Property(
 *        property="address",
 *        description="address of user",
 *        type="string",
 *    ),@OA\Property(
 *        property="street_address",
 *        description="street address of user",
 *        type="string",
 *    ),@OA\Property(
 *        property="change_password",
 *        description="to update password or not",
 *        type="boolean",
 *    ),@OA\Property(
 *        property="password",
 *        description="password",
 *        type="string",
 *    ),@OA\Property(
 *        property="password_confirmation",
 *        description="password confirmation",
 *        type="string",
 *    ),@OA\Property(
 *        property="willing_to_relocate",
 *        description="willing to relocate",
 *        type="boolean",
 *    ),@OA\Property(
 *        property="tempImage",
 *        description="aws image path if changed",
 *        type="string",
 *    ),@OA\Property(
 *        property="newImage",
 *        description="if image is updated or not",
 *        type="boolean",
 *    ),@OA\Property(
 *        property="removeImage",
 *        description="remove image flag",
 *        type="boolean",
 *    ),@OA\Property(
 *        property="desired_job_category",
 *        description="desired_job_category attr value",
 *        type="integer",
 *    ),@OA\Property(
 *        property="experience_year",
 *        description="experience_year attr value",
 *        type="integer",
 *    ),@OA\Property(
 *        property="desired_annual_min_salary_range",
 *        description="desired_annual_min_salary_range attr value",
 *        type="integer",
 *    ),@OA\Property(
 *        property="receive_mail_language",
 *        description="receive mail language en or ja",
 *        type="string",
 *    )
 * )
 */
class UserUpdateRequest extends FormRequest
{
    const NAME = 'user';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if (auth()->user()->user_type != 'admin') {
            $validator->sometimes('visa_issue_date', 'required', function ($input) {
                return ($input->country != 1 && $input->visa_status != 24);
            });
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (auth()->user()->user_type == 'admin') {
            $rules = [
                'full_name' => 'required|string|max:100',
                'katakana_full_name' => 'required|string|max:100|katakana',
                'password' => [
                    'confirmed',
                    'min:8',
                    'max:15',
                    'regex:/^(?=.*\d)(?=.*[a-z])[\w~@#$%^&*+=`|{}:;!.?\"()\[\]-]+$/',
                    'old_password'
                ],
            ];
        } else {
            $rules = [
                'full_name' => 'required|string|max:100',
                'katakana_full_name' => 'required|string|max:100|katakana',
                'dob' => 'required|date_format:d.m.Y|before:-18 years',
                'gender' => 'required|attr:gender',
                'telephone' => [
                    'min:99',
                    'digits_between:9,15',
                    'nullable',
                    'numeric',
                ],
                'mobile' => [
                    'min:99',
                    'digits_between:9,15',
                    'nullable',
                    'numeric',
                ],
                'password' => [
                    'confirmed',
                    'min:8',
                    'max:15',
                    'regex:/^(?=.*\d)(?=.*[a-z])[\w~@#$%^&*+=`|{}:;!.?\"()\[\]-]+$/',
                    'old_password'
                ],

                //seeker,provider table
                'japanese_language_level' => 'required|attr:lang.level',
                'english_language_level' => 'required|attr:lang.level',

                'japanese_writing_level' => 'required|attr:language_writing_skills',


                'country' => 'required|attr:country',
                'living_country' => 'required_unless:country,1|attr:country',


                'visa_status' => 'nullable|required_unless:country,1|attr:visa_status',
                'visa_issue_date' => 'nullable|date|date_format:d.m.Y|before:today',
                //sometimes required if country is not japan
                'visa_expiry_date' => 'nullable|date|date_format:d.m.Y|required_with:visa_issue_date|after:' . date('Y-m-d',
                        strtotime("+1 month")),


                'state' => 'nullable|required_unless:living_country,1|string|max:100',
                'zip_code' => [
                    'nullable',
                    'numeric',
                    'digits:7',
                    'required_if:living_country,1',
                    new ValidZipCode()
                ],
                'prefecture_id' => 'nullable|required_if:living_country,1|attr:prefecture',
                'address' => 'string|nullable|max:100',
                'street_address' => 'required|string|nullable|max:100',
                'willing_to_relocate' => 'nullable|attr:boolean',
                'is_experienced' => 'nullable|attr:boolean',
                'tempImage' => [
                    'nullable',
                    new ValidateSeekerImage()
                ],
                'newImage' => 'required|boolean',
                'removeImage' => 'required|boolean',
                'desired_job_category'=>'required|attr:job_category',
                'desired_annual_min_salary_range'=>'required|attr:annual_min_salary_range',
                'experience_year'=>'required|attr:experience_year','receive_mail_language'=>'required|in:en,ja'
            ];
        }
        return $rules;

    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.regex' => trans('messages.password_regex'),
            'living_country.required_unless' => trans('messages.required_unless_japan',
                ['attribute' => trans('resource.auth.living_country')]),
            'visa_status.required_unless' => trans('messages.required_unless_japan',
                ['attribute' => trans('resource.auth.visa_status')]),
            'visa_issue_date.required_unless' => trans('messages.required_unless_japan',
                ['attribute' => trans('resource.auth.visa_issue_date')]),
            'visa_expiry_date.after' => trans('messages.required_after_30days',
                ['attribute' => trans('resource.user.visa_expiry_date')]),
            'state.required_unless' => trans('messages.required_unless_japan',
                ['attribute' => trans('resource.auth.state')]),
            'zip_code.required_if' => trans('messages.required_if_japan',
                ['attribute' => trans('resource.auth.zip_code')]),
            'prefecture_id.required_if' => trans('messages.required_if_japan',
                ['attribute' => trans('resource.auth.prefecture_id')]),
            'telephone.min' => trans('messages.min', ['attribute' => trans('resource.seeker.basicUpdate.telephone')]),
            'mobile.min' => trans('messages.min', ['attribute' => trans('resource.seeker.basicUpdate.mobile')]),
        ];
    }
}
