<?php

namespace App\Http\Requests;

use App\Extensions\Foundation\Http\FormRequest;
/**
 * Class MediaPreSignedRequest
 *
 * @OA\Schema(
 *    schema="MediaPreSignedRequest",
 *    @OA\Property(
 *        property="type",
 *        description="image or video",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="extension",
 *        description="extension of file only - png,jpg,jpeg || mp4,mov,MOV,MP4",
 *        type="string",
 *    )
 * )
 */
class MediaPreSignedRequest extends FormRequest
{
    const NAME = 'media';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'type' => 'required|in:image,video',
        ];
        $extension = [];
        if (request('type') == 'image') {
            $extension['extension'] = 'required|in:png,jpg,jpeg';
        }
        if (request('type') == 'video') {
            $extension['extension'] = 'required|in:mp4,mov,MOV,MP4';
        }
        return array_merge($rules, $extension);
    }
}
