<?php

namespace App\Http\Requests;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;

/**
 * Class ContactUsUpdateRequest
 * @package App\Http\Resources
 *
 * @OA\Schema(
 *    schema="ContactUsUpdateRequest",
 *      required={"admin_reply"},
 *    @OA\Property(
 *        property="admin_reply",
 *        description="string admin reply",
 *        type="string"
 *    )
 * )
 */
class ContactUsUpdateRequest extends FormRequest
{
    const NAME = 'contact';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'admin_reply' => 'required|string'
        ];
    }
}
