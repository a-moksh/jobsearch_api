<?php

namespace App\Http\Requests\Provider;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class PurchasePointRequest
 *
 * @OA\Schema(
 *    schema="PurchasePointRequest",
 *    required={"id"},
 *    @OA\Property(
 *        property="id",
 *        description="points id",
 *        type="integer"
 *    ),
 * )
 */
class PurchasePointRequest extends FormRequest
{
    const NAME = 'point';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                Rule::exists('points')->where(function ($query) {
                    return $query->where('status', 1);
                })
            ]
        ];
    }
}
