<?php

namespace App\Http\Requests\Provider;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class ApplicationOpenRequest
 *
 * @OA\Schema(
 *    schema="ApplicationOpenRequest",
 *    required={"id"},
 *    @OA\Property(
 *        property="id",
 *        description="seeker job_id",
 *        type="integer"
 *    ),
 * )
 */
class ApplicationOpenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                'array',
                'exists:seeker_jobs,id'
            ]
        ];
    }
}
