<?php

namespace App\Http\Requests\Provider;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StoreScoutMailJobRequest
 *
 * @OA\Schema(
 *    schema="StoreScoutMailJobRequest",
 *     @OA\Property(
 *        property="seeker_id",
 *        description="seeker_id",
 *        type="integer"
 *    ),@OA\Property(
 *        property="interview_method",
 *        description="interview_method",
 *        type="integer"
 *    ),@OA\Property(
 *        property="provider_skype_id",
 *        description="provider_skype_id ",
 *        type="integer"
 *    ),
 *      @OA\Property(
 *        property="interview_place",
 *        description="interview_place ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="resume",
 *        description="resume",
 *        type="integer"
 *    ),@OA\Property(
 *        property="cv",
 *        description="cv",
 *        type="integer"
 *    ),@OA\Property(
 *        property="writing_util",
 *        description="writing_util",
 *        type="integer"
 *    ),@OA\Property(
 *        property="other",
 *        description="other",
 *        type="integer"
 *    ),@OA\Property(
 *        property="job_id",
 *        description="job_id",
 *        type="integer"
 *    ),@OA\Property(
 *        property="job_location_id",
 *        description="job_location_id",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *         property="dateTime",
 *         description="date time",
 *         type="array",
 *         @OA\Items(
 *           type="object",
 *           required={"overview"},
 *           @OA\Property(
 *            property="date",
 *            description="date",
 *            type="string",
 *          ),@OA\Property(
 *            property="time",
 *            description="time ",
 *            type="integer",
 *          )
 *         )
 *  )
 *  )
 * )
 */
class StoreScoutMailJobRequest extends FormRequest
{
    const NAME = 'job';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'seeker_id' => 'required|exists:seekers,id',
            'interview_method' => 'required|attr:interview_method',
            'provider_skype_id' => 'nullable|required_if:interview_method,0',
            'interview_place' => 'nullable|required_if:interview_method,1|attr:interview_location_option',
            'resume' => 'nullable|in:0,1',
            'cv' => 'nullable|in:0,1',
            'writing_util' => 'nullable|in:0,1',
            'other' => 'nullable|string|max:100',
            'dateTime' => [
                'required',
                'array',
                'min:3',
            ],
            'dateTime.*.date' => [
                'required',
                'date',
                'date_format:d.m.Y',
                'after:today',
            ],
            'dateTime.*.time' => [
                'required',
                'attr:day_hour',
            ],
            'job_id' => [
                'required',
                Rule::exists('jobs', 'id')->where(function ($query) {
                    return $query->where('admin_status', 0);
                })
            ],
            'job_location_id' => [
                'nullable',
                'required_if:interview_place,1',
                'exists:job_locations,id',
            ],
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'dateTime.*.date.after' => trans('messages.after'),
            'provider_skype_id.required_if' => trans('messages.company_skype_id_required_if'),
            'interview_place.required_if' => trans('messages.interview_place_required_if'),
            'job_location_id.required_if' => trans('messages.job_location_id_required_if'),

        ];
    }
}
