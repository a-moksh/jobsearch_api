<?php

namespace App\Http\Requests\Provider;

use App\Extensions\Foundation\Http\FormRequest;
use App\Rules\ValidZipCode;
use Illuminate\Support\Facades\Input;

/**
 * Class ProviderUpdateRequest
 *
 * @OA\Schema(
 *    schema="ProviderUpdateRequest",
 *    required={"company_name_ja","company_name_en","ceo_name_en","ceo_name_ja","contact_person_en","contact_person_ja",
 *      "established_year","telephone","capital","no_of_employee","business_content","sub_business_content","zip_code","prefecture_id","address","street_address"},
 *     @OA\Property(
 *        property="company_name_ja",
 *        description="name of the company ja",
 *        type="string"
 *    ),@OA\Property(
 *        property="company_name_en",
 *        description="name of the company en",
 *        type="string"
 *    ),@OA\Property(
 *        property="ceo_name_en",
 *        description="ceo name ja",
 *        type="string"
 *    ),@OA\Property(
 *        property="ceo_name_ja",
 *        description="ceo name ja",
 *        type="string"
 *    ),@OA\Property(
 *        property="contact_person_en",
 *        description="contact person en",
 *        type="string"
 *    ),@OA\Property(
 *        property="contact_person_ja",
 *        description="contact person ja",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="established_year",
 *        description="the year of established",
 *        type="string",
 *        format="date",
 *    ),
 *     @OA\Property(
 *        property="telephone",
 *        description="company telephone",
 *        type="string",
 *        pattern="/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/",
 *    ),
 *     @OA\Property(
 *        property="fax",
 *        description="company's fax",
 *        type="string",
 *        pattern="/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/",
 *    ),@OA\Property(
 *        property="mobile_tel",
 *        description="company's fax",
 *        type="string",
 *        pattern="/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/",
 *    ),
 *     @OA\Property(
 *        property="capital",
 *        description="capital of the company",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="no_of_employee",
 *        description="total number of employee",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="business_content",
 *        description="company bossiness content",
 *        type="integer"
 *    ),@OA\Property(
 *        property="sub_business_content",
 *        description="company sub bussiness content",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="zip_code",
 *        description="head office address",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="prefecture_id",
 *        description="prefecture id of the company",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="address",
 *        description="name of the CEO",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="street_address",
 *        description="total branch",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="related_url",
 *        description="company's website",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="mansion_name",
 *        description="mansion name",
 *        type="string"
 *    )
 * )
 */
class ProviderUpdateRequest extends FormRequest
{
    const NAME = 'provider';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->normalize();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name_ja' => 'required|string|max:50',
            'company_name_en' => 'required|string|max:50|regex:/([A-Za-z0-9 ])+/',
            'ceo_name_ja' => 'required|string|max:100',
            'ceo_name_en' => 'required|string|max:100',
            'established_year' => [
                'required',
                'before:today',
                'date_format:d.m.Y',
            ],
            'telephone' => [
                'required',
                'regex:/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/'
            ],
            'fax' => [
                'nullable',
                'regex:/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/'
            ],
            'mobile_tel' => [
                'nullable',
                'regex:/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/'
            ],
            'capital' => [
                'required',
                'numeric',
                'digits_between:3, 25',
                'min:99'
            ],
            'contact_person_ja' => 'required|string|max:100',
            'contact_person_en' => 'required|string|max:100|regex:/([A-Za-z0-9 ])+/',
            'no_of_employee' => [
                'required',
                'attr:no_of_employee'
            ],
            'business_content' => [
                'required',
                'attr:business_content'
            ],
            'sub_business_content' => [
                'required',
                'attr_sub:business_content:' . Input::get('business_content'),
            ],
            'related_url' => [
                'nullable',
                'string',
                'url'
            ],
            'zip_code' => [
                'required',
                'numeric',
                'digits:7',
                new ValidZipCode()
            ],
            'prefecture_id' => [
                'required',
                'attr:prefecture'
            ],
            'address' => [
                'required',
                'string',
                'nullable',
                'max:50',
            ],
            'mansion_name' => [
                'string',
                'nullable',
                'max:50',
            ],
            'street_address' => [
                'required',
                'string',
                'nullable',
                'max:50',
            ],
            'password' => [
                'confirmed',
                'min:8',
                'max:15',
                'regex:/^(?=.*\d)(?=.*[a-z])[\w~@#$%^&*+=`|{}:;!.?\"()\[\]-]+$/',
                'old_password'
            ],
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => trans('validation.custom.provider.custom_error.email_with_user_id_exists'),
            'established_year.before' => trans('validation.custom.provider.custom_error.established_year_before'),
            'telephone.min' => trans('messages.min', ['attribute' => trans('resource.provider.telephone')]),
            'fax.min' => trans('messages.min', ['attribute' => trans('resource.provider.fax')]),
            'capital.min' => trans('messages.min', ['attribute' => trans('resource.provider.capital')]),
            'company_name_en.regex' => trans('validation.custom.provider.custom_error.english'),
            'contact_person_en.regex' => trans('validation.custom.provider.custom_error.english'),
        ];
    }
}
