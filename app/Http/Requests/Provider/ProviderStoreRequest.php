<?php

namespace App\Http\Requests\Provider;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ProviderStoreRequest
 *
 * @OA\Schema(
 *    schema="ProviderStoreRequest",
 *    @OA\Property(
 *        property="user_id",
 *        description="user id",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="company_name_ja",
 *        description="name of the company in japanese",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="company_name_en",
 *        description="name of the company in english",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="email",
 *        description="company contact e-mail",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="ceo_name",
 *        description="name of the CEO",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="established_year",
 *        description="the year of established",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="telephone",
 *        description="company telephone",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="fax",
 *        description="company's fax",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="contact_person_ja",
 *        description="contact person of the company",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="contact_person_en",
 *        description="contact person of the company",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="capital",
 *        description="capital of the company",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="no_of_employee",
 *        description="total number of employee",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="no_of_branch",
 *        description="total number of branch",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="business_content",
 *        description="company bossiness content",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="zip_code",
 *        description="head office address",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="prefecture",
 *        description="prefecture id of the company",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="address",
 *        description="name of the CEO",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="street_address",
 *        description="total branch",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="related_url",
 *        description="company's website",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="about_company_ja",
 *        description="company short description in japanese",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="about_company_en",
 *        description="company short description in english",
 *        type="string"
 *    ),
 * )
 */
class ProviderStoreRequest extends FormRequest
{
    const NAME = 'provider';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
//        $this->normalize();

    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //return [];
        return [
            'user_id' => [
                'required',
                'integer',
                'exists:users,id',
                Rule::unique('providers', 'user_id'),
            ],
            'company_name_ja' => [
                'required',
                'string',
            ],
            'company_name_en' => [
                'required',
                'string',
            ],
            'email' => [
                'required',
                'email',
                Rule::exists('users', 'email')->where(function ($query) {
                    $query->where('id', $this->input('user_id'));
                }),
            ],
            'ceo_name' => [
                'required',
                'string'
            ],
            'established_year' => [
                'required',
                'before:' . Date('Y-m-d'),
                'date_format:Y-m-d',
            ],
            'telephone' => [
                'required',
                'numeric',
                'digits_between:7, 15'
            ],
            'fax' => [
                'nullable',
                'numeric',
                'digits_between:1,15'
            ],
            'contact_person_ja' => [
                'string',
                'required'
            ],

            'contact_person_en' => [
                'required',
                'string',
            ],

            'capital' => [
                'required',
                'numeric',
                'digits_between:6, 25'
            ],
            'no_of_employee' => [
                'required',
                'numeric',
                'digits_between:1, 10',
                'min:1',
            ],
            'no_of_branch' => [
                'required',
                'numeric',
            ],
            'business_content' => [
                'required',
                'string',
                'attr:business_content'
            ],
            'related_url' => [
                'required',
                'string',
                'url'
            ],
            'zip_code' => [
                'required',
                'numeric',
                'digits:7'
            ],
            'prefecture_id' => [
                'required',
                'attr:prefecture',
            ],
            'address' => [
                'required',
                'string',
            ],
            'street_address' => [
                'required',
                'string'
            ],
            'about_company_ja' => [
                'required',
                'string',
                'max:5000',
            ],
            'about_company_en' => [
                'required',
                'string',
                'max:5000',
            ],
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => trans('validation.custom.provider.custom_error.email_with_user_id_exists'),
            'established_year.before' => trans('validation.custom.provider.custom_error.established_year_before')
        ];
    }

}
