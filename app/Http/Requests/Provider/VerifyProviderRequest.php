<?php

namespace App\Http\Requests\Provider;

use App\Extensions\Foundation\Http\FormRequest;
use App\Rules\ValidZipCode;
use Illuminate\Support\Facades\Input;

/**
 * Class VerifyProviderRequest
 *
 * @OA\Schema(
 *    schema="VerifyProviderRequest",
 *    required={"email", "password", "password_confirmation", "token", "company_name_ja","company_name_en","company_name_kana","ceo_name_en","ceo_name_first_kana","ceo_name_last_kana","ceo_name_last_ja","ceo_name_first_ja","contact_person_en","contact_person_first_ja","contact_person_last_ja",
 * "contact_person_first_kana","contact_person_last_kana","established_year","telephone","capital","no_of_employee","business_content","sub_business_content","zip_code","prefecture_id","address","street_address"},
 *    @OA\Property(
 *        property="email",
 *        description="user email",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="password",
 *        description="password",
 *        type="string",
 *    ),@OA\Property(
 *        property="password_confirmation",
 *        description="password confirmation",
 *        type="string",
 *    ),@OA\Property(
 *        property="token",
 *        description="token",
 *        type="string",
 *    ),@OA\Property(
 *        property="company_name_ja",
 *        description="company_name_ja",
 *        type="string",
 *    ),@OA\Property(
 *        property="company_name_en",
 *        description="company_name_en",
 *        type="string",
 *    ),@OA\Property(
 *        property="company_name_kana",
 *        description="company_name_kana",
 *        type="string",
 *    ),@OA\Property(
 *        property="ceo_name_en",
 *        description="ceo_name_en",
 *        type="string",
 *    ),@OA\Property(
 *        property="ceo_name_first_kana",
 *        description="ceo_name_first_kana",
 *        type="string",
 *    ),@OA\Property(
 *        property="ceo_name_last_kana",
 *        description="ceo_name_last_kana",
 *        type="string",
 *    ),@OA\Property(
 *        property="ceo_name_last_ja",
 *        description="ceo_name_last_ja",
 *        type="string",
 *    ),@OA\Property(
 *        property="ceo_name_first_ja",
 *        description="ceo_name_first_ja",
 *        type="string",
 *    ),@OA\Property(
 *        property="contact_person_en",
 *        description="contact_person_en",
 *        type="string",
 *    ),@OA\Property(
 *        property="contact_person_first_ja",
 *        description="contact_person_first_ja",
 *        type="string",
 *    ),@OA\Property(
 *        property="contact_person_last_ja",
 *        description="contact_person_last_ja",
 *        type="string",
 *    ),@OA\Property(
 *        property="contact_person_first_kana",
 *        description="contact_person_first_kana",
 *        type="string",
 *    ),@OA\Property(
 *        property="established_year",
 *        description="established_year",
 *        type="string",
 *        format="date",
 *    ),@OA\Property(
 *        property="capital",
 *        description="capital",
 *        type="integer",
 *    ),@OA\Property(
 *        property="contact_person_last_kana",
 *        description="contact_person_last_kana",
 *        type="string",
 *    ),@OA\Property(
 *        property="telephone",
 *        description="telephone",
 *        type="string",
 *        pattern="/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/",
 *    ),@OA\Property(
 *        property="fax",
 *        description="fax",
 *        type="string",
 *        pattern="/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/",
 *    ),@OA\Property(
 *        property="mobile_tel",
 *        description="mobile_tel",
 *        type="string",
 *        pattern="/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/",
 *    ),@OA\Property(
 *        property="business_content",
 *        description="business_content",
 *        type="integer",
 *    ),@OA\Property(
 *        property="sub_business_content",
 *        description="sub_business_content",
 *        type="integer",
 *    ),@OA\Property(
 *        property="no_of_employee",
 *        description="no_of_employee",
 *        type="integer",
 *    ),@OA\Property(
 *        property="zip_code",
 *        description="zip_code",
 *        type="integer",
 *    ),@OA\Property(
 *        property="prefecture_id",
 *        description="prefecture_id",
 *        type="integer",
 *    ),@OA\Property(
 *        property="address",
 *        description="address",
 *        type="string",
 *    ),@OA\Property(
 *        property="street_address",
 *        description="street_address",
 *        type="string",
 *    ),@OA\Property(
 *        property="related_url",
 *        description="url",
 *        type="string",
 *    ),@OA\Property(
 *        property="url2",
 *        description="url",
 *        type="string",
 *    ),@OA\Property(
 *        property="url3",
 *        description="url",
 *        type="string",
 *    ),@OA\Property(
 *        property="mansion_name",
 *        description="mansion name",
 *        type="string",
 *    )
 * )
 */
class VerifyProviderRequest extends FormRequest
{
    const NAME = 'provider';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->normalize();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users',
            'token' => 'required|exists:verify_users,token',

            'company_name_ja' => 'required|string|max:50',
            'company_name_en' => 'required|string|max:50|regex:/([A-Za-z0-9 ])+/',

            'company_name_kana' => 'required|string|max:50|katakana',//changed

            'ceo_name_en' => 'required|string|max:50|regex:/([A-Za-z0-9 ])+/',

            'ceo_name_first_kana' => 'required|string|max:50|katakana',//changed
            'ceo_name_last_kana' => 'required|string|max:50|katakana',//changed
            'ceo_name_last_ja' => 'required|string|max:50',//changed
            'ceo_name_first_ja' => 'required|string|max:50',//changed

            'contact_person_en' => 'required|string|max:50|regex:/([A-Za-z0-9 ])+/',//job
            'contact_person_first_ja' => 'required|string|max:50',
            'contact_person_last_ja' => 'required|string|max:50',
            'contact_person_first_kana' => 'required|string|max:50|katakana',
            'contact_person_last_kana' => 'required|string|max:50|katakana',

            'branch_name' => 'nullable|string|max:50',//changed

            'established_year' => [
                'required',
                'before:today',
                'date_format:d.m.Y',
            ],
            'telephone' => [
                'required',
                'regex:/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/'
            ],
            'fax' => [
                'nullable',
                'regex:/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/'
            ],
            'mobile_tel' => [
                'nullable',
                'regex:/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/'
            ],

            'capital' => [
                'required',
                'numeric',
                'digits_between:3, 25',
                'min:99'
            ],
            'no_of_employee' => [
                'required',
                'attr:no_of_employee'
            ],
            'business_content' => [
                'required',
                'attr:business_content'
            ],
            'sub_business_content' => [
                'required',
                'attr_sub:business_content:' . Input::get('business_content'),
            ],
            'related_url' => [
                'nullable',
                'string',
                'url'
            ],
            'url2' => ['nullable', 'string', 'url'],
            'url3' => ['nullable', 'string', 'url'],
            'zip_code' => [
                'required',
                'numeric',
                'digits:7',
                new ValidZipCode()
            ],
            'prefecture_id' => [
                'required',
                'attr:prefecture',
            ],

            'address' => [
                'required',
                'string',
                'max:50',
            ],
            'street_address' => [
                'required',
                'string',
                'max:50',
            ],
            'mansion_name' => [
                'nullable',
                'string',
                'max:50',
            ],
            'password' => 'required|confirmed|min:8|max:15|regex:/(?=.*[0-9])(?=.*[a-z])/',

        ];
    }

    public function messages()
    {
        return [
            'email.exists' => trans('validation.custom.provider.custom_error.email_with_user_id_exists'),
            'established_year.before' => trans('validation.custom.provider.custom_error.established_year_before'),
            'telephone.regex' => trans('messages.regex', ['attribute' => trans('resource.provider.telephone')]),
            'fax.regex' => trans('messages.regex', ['attribute' => trans('resource.provider.fax')]),
            'mobile_tel.regex' => trans('messages.regex', ['attribute' => trans('resource.provider.fax')]),
            'capital.min' => trans('messages.min', ['attribute' => trans('resource.provider.capital')]),
            'company_name_en.regex' => trans('validation.custom.provider.custom_error.english'),
            'contact_person_en.regex' => trans('validation.custom.provider.custom_error.english'),
        ];
    }
}
