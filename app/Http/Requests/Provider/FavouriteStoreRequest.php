<?php

namespace App\Http\Requests\Provider;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ProviderFavouriteStoreRequest
 *
 * @OA\Schema(
 *    schema="ProviderFavouriteStoreRequest",
 *    @OA\Property(
 *        property="seeker_id",
 *        description="seeker id",
 *        type="integer"
 *    )
 * )
 */
class FavouriteStoreRequest extends FormRequest
{
    const NAME = 'favourite';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'seeker_id' => [
                'required',
                Rule::exists('seekers', 'id')->where(function ($query) {
                }),
            ]
        ];
    }
}
