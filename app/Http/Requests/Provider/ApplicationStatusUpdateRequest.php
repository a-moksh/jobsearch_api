<?php

namespace App\Http\Requests\Provider;

use App\Extensions\Foundation\Http\FormRequest;

class ApplicationStatusUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'application_status' => 'required|attr:applicant_status'
        ];
    }
}
