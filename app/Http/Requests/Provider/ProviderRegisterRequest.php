<?php

namespace App\Http\Requests\Provider;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ProviderRegisterRequest
 *
 * @OA\Schema(
 *    schema="ProviderRegisterRequest",
 *      required={"email","user_type","terms","company_name"},
 *    @OA\Property(
 *        property="email",
 *        description="user email",
 *        type="string"
 *    ),@OA\Property(
 *        property="user_type",
 *        description="user type",
 *        type="string",
 *    ),@OA\Property(
 *        property="company_name",
 *        description="co name",
 *        type="string",
 *    ),@OA\Property(
 *        property="terms",
 *        description="terms acceptance",
 *        type="boolean",
 *    )
 * )
 */
class ProviderRegisterRequest extends FormRequest
{
    const NAME = 'auth';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => [
                'required',
                'email',
                Rule::unique('users')->where(function ($query) {
                    return $query->where('deleted_at', null)->where('verified', '=', 1)->where('user_type', 'provider');
                })
            ],
            'user_type' => 'required|in:provider',
            'terms' => 'required|in:1',
            'company_name' => 'string|required|max:100',
        ];
        return $rules;
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'terms.required' => trans('messages.terms'),
            'email.unique' => trans('messages.already_exist_email'),
        ];
    }
}
