<?php

namespace App\Http\Requests;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class VerifyAdminEmailRequest
 *
 * @OA\Schema(
 *    schema="VerifyAdminEmailRequest",
 *    required={"email", "password", "password_confirmation", "token"},
 *    @OA\Property(
 *        property="email",
 *        description="user email",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="password",
 *        description="password",
 *        type="string",
 *    ),@OA\Property(
 *        property="password_confirmation",
 *        description="password confirmation",
 *        type="string",
 *    ),@OA\Property(
 *        property="token",
 *        description="token",
 *        type="string",
 *    )
 * )
 */
class VerifyAdminEmailRequest extends FormRequest
{
    const NAME = 'auth';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users',
            'full_name' => 'required|string|max:100',
            'katakana_full_name' => 'required|string|max:100|katakana',
            'token' => 'required|exists:verify_users,token',
            'password' => [
                'required',
                'confirmed',
                'min:8',
                'max:15',
                'regex:/^(?=.*\d)(?=.*[a-z])[\w~@#$%^&*+=`|{}:;!.?\"()\[\]-]+$/',
                'old_password'
            ]
        ];
    }

}
