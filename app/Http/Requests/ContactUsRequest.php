<?php

namespace App\Http\Requests;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;

/**
 * Class ContactUsRequest
 * @package App\Http\Resources
 *
 * @OA\Schema(
 *    schema="ContactUsRequest",
 *      required={"email", "full_name","message","receive_mail_language","terms"},
 *    @OA\Property(
 *        property="email",
 *        description="user email address",
 *        type="string"
 *    ),@OA\Property(
 *        property="full_name",
 *        description="full_name",
 *        type="string"
 *    ),@OA\Property(
 *        property="message",
 *        description="message",
 *        type="string"
 *    ),@OA\Property(
 *        property="receive_mail_language",
 *        description="receive_mail_language",
 *        type="string"
 *    ),@OA\Property(
 *        property="mobile",
 *        description="mobile",
 *        type="integer"
 *    ),@OA\Property(
 *        property="terms",
 *        description="terms",
 *        type="integer"
 *    )
 * )
 */
class ContactUsRequest extends FormRequest
{
    const NAME = 'contact';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(Input::get('type') == 2){
            return [
                'company_name' => 'required|string|max:100',
                'email' => 'required|email|string',
                'full_name' => 'required|string|max:100',
                'terms' => 'required|in:1',
                'mobile' => [
                    'nullable',
                    'regex:/^[0-9]{2,4}-[0-9]{2,4}-[0-9]{3,4}$/'
                ],
                'message' => 'required|max:500',
                'type'=>'in:2'
            ];
        }else{
            return [
                'email' => 'required|email|string',
                'full_name' => 'required|string',
                'terms' => 'required|in:1',
                'mobile' => 'nullable|integer',
                'receive_mail_language' => 'required|string|in:en,ja',
                'message' => 'required|max:500',
            ];
        }

    }
}
