<?php

namespace App\Http\Requests;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class UpdateScoutMailRequest
 *
 * @OA\Schema(
 *    schema="UpdateScoutMailRequest",
 *    @OA\Property(
 *        property="seeker_status",
 *        description="status of seeker",
 *        type="integer"
 *    ),
 *    @OA\Property(
 *        property="interview_timing",
 *        description="only for seeker update selected time",
 *        type="integer",
 *    ),@OA\Property(
 *        property="seeker_skype_id",
 *        description="when interview method is skype then seeker replies",
 *        type="string",
 *    ),@OA\Property(
 *        property="provider_status",
 *        description="status of provider",
 *        type="integer",
 *    ),@OA\Property(
 *        property="accept",
 *        description="provider field for acknowledgement",
 *        type="boolean",
 *    )
 * )
 */
class UpdateScoutMailRequest extends FormRequest
{
    const NAME = 'update.scout';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (auth()->user()->user_type == 'seeker') {
            $rules = [
                'seeker_status' => 'required|attr:scout_message_seeker_status',
                'interview_timing' => 'required_unless:seeker_status,2|exists:scout_details,id',
                'seeker_skype_id' => 'nullable'
            ];
        } else {
            $rules = [
                'provider_status' => 'required',
                'accept' => 'required',
            ];
        }

        return $rules;
    }

}
