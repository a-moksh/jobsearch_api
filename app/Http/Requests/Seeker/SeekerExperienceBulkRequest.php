<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class SeekerExperienceBulkRequest
 *
 * @OA\Schema(
 *    schema="SeekerExperienceBulkRequest",
 *     @OA\Property(
 *         property="experiences",
 *         description="seeker experiences",
 *         type="array",
 *         @OA\Items(
 *           type="object",
 *           required={"company_name","job_title","job_from","currently_working_here","annual_salary","job_description"},
 *           @OA\Property(
 *            property="company_name",
 *            description="company_name",
 *            type="string",
 *          ),@OA\Property(
 *            property="job_title",
 *            description="job_title ",
 *            type="string",
 *          ),@OA\Property(
 *            property="id",
 *            description="experience id for update",
 *            type="integer",
 *          ),@OA\Property(
 *            property="job_from",
 *            description="experience start date",
 *            type="string",
 *            format="date",
 *          ),@OA\Property(
 *            property="job_to",
 *            description="experience end_date",
 *            type="string",
 *            format="date",
 *          ),@OA\Property(
 *            property="currently_working_here",
 *            description="curring working?",
 *            type="boolean",
 *          ),@OA\Property(
 *            property="annual_salary",
 *            description="annual salary?",
 *            type="integer",
 *          ),@OA\Property(
 *            property="job_description",
 *            description="job description",
 *            type="string",
 *          )
 *         )
 *  )
 * )
 */
class SeekerExperienceBulkRequest extends FormRequest
{
    const NAME = 'seeker.experienceUpdate';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'experiences' => 'required',
            'experiences.*.id' => [
                'exists:seeker_experiences',
                'distinct'
            ],
            'experiences.*.company_name' => ['required', 'max:100', 'string'],
            'experiences.*.job_title' => 'required|max:100|string',
            'experiences.*.job_from' => 'required|date|date_format:d.m.Y|before:today',
            'experiences.*.job_to' => 'nullable|required_if:experiences.*.currently_working_here,0|date|date_format:d.m.Y|after:experiences.*.job_from|before:today',
            'experiences.*.currently_working_here' => 'required|in:0,1',
            'experiences.*.annual_salary' => 'required|attr:annual_salary_range',
            'experiences.*.job_description' => 'required|max:1500|string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'experiences.*.job_to.required_if' => trans('messages.required_if_not_working',
                ['attribute' => trans('resource.seeker.job_to')])
        ];
    }
}
