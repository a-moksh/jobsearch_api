<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;
use App\Rules\ValidSkillId;
use Illuminate\Support\Facades\Input;

/**
 * Class SeekerSkillBulkRequest
 *
 * @OA\Schema(
 *    schema="SeekerSkillBulkRequest",
 *     @OA\Property(
 *         property="skills",
 *         description="seeker skills",
 *         type="array",
 *         @OA\Items(
 *           type="object",
 *           required={"skill_type","skill_id","level"},
 *           @OA\Property(
 *            property="skill_type",
 *            description="skill type",
 *            type="integer",
 *          ),@OA\Property(
 *            property="skill_id",
 *            description="skill id ",
 *            type="integer",
 *          ),@OA\Property(
 *            property="id",
 *            description="skill id for update",
 *            type="integer",
 *          ),@OA\Property(
 *            property="level",
 *            description="level",
 *            type="integer",
 *          )
 *         )
 *  )
 * )
 */
class SeekerSkillBulkRequest extends FormRequest
{
    const NAME = 'seeker.skillUpdate';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
//            'skills.*.id' =>[
//                'exists:seeker_skills','distinct'
//            ],

            'skills' => 'required',
            'skills.*.skill_type' => 'required|attrgroup:skill_type',
            'skills.*.skill_id' => [
                'required',
                new ValidSkillId(Input::get('skills')),
            ],
            'skills.*.level' => 'required|attr:skill_experience'
        ];
    }
}
