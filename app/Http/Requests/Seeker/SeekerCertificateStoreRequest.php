<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class SeekerCertificateStoreRequest
 *
 * @OA\Schema(
 *    schema="SeekerCertificateStoreRequest",
 *    @OA\Property(
 *        property="certificate_name",
 *        description="Certificate name",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="year",
 *        description="year ",
 *        type="string",
 *        format="date"
 *    )
 * )
 */
class SeekerCertificateStoreRequest extends FormRequest
{
    const NAME = 'seeker_certificate';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'certificate_name' => 'required|string',
            'year' => 'required|date|date_format:d.m.Y|before:today'
        ];
    }
}
