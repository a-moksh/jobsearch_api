<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class SeekerWishlistStoreRequest
 *
 * @OA\Schema(
 *    schema="SeekerWishlistStoreRequest",
 *    @OA\Property(
 *        property="job_id",
 *        description="job id",
 *        type="integer"
 *    )
 * )
 */
class SeekerWishlistStoreRequest extends FormRequest
{
    const NAME = 'seeker_wishlist';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_id' => [
                'required',
                Rule::exists('jobs', 'id')->where(function ($query) {
                    return $query->where('status', 1)->where('admin_status', 0);
                }),
//                new ValidWishlist
            ]
        ];
    }
}
