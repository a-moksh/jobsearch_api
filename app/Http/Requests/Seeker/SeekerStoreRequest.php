<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class SeekerStoreRequest
 *
 * @OA\Schema(
 *    schema="SeekerStoreRequest",
 *    @OA\Property(
 *        property="visa_status",
 *        description="seeker visa status",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="visa_issue_date",
 *        description="visa issue date",
 *        type="string",
 *        format="date"
 *    ),
 *     @OA\Property(
 *        property="visa_expiry_date",
 *        description="visa expiry date",
 *        type="string",
 *        format="date"
 *    ),
 *     @OA\Property(
 *        property="japanese_language_level",
 *        description="japanese language level",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="english_language_level",
 *        description="english language level",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="nearest_station",
 *        description="nearest station",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="willing_to_relocate",
 *        description="willing to relocate",
 *        type="boolean"
 *    ),
 *     @OA\Property(
 *        property="desired_employment_type",
 *        description=" employment type",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="is_experienced",
 *        description="experience ?",
 *        type="boolean"
 *    ),
 *     @OA\Property(
 *        property="experience",
 *        description="experience ",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="last_annual_salary",
 *        description="last annual salary",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="last_work_region",
 *        description="last work region",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="portfolio_url",
 *        description="portfolio url",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="github_url",
 *        description="git hub url",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="self_introduction",
 *        description="self introduction",
 *        type="string"
 *    )
 * )
 */
class SeekerStoreRequest extends FormRequest
{
    const NAME = 'seeker';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function withValidator($validator)
    {

        $validator->sometimes('visa_issue_date', 'required', function ($input) {
            return Auth::user()->country != 1;
        });
        $validator->sometimes('experience', 'required', function ($input) {
            return !$input->is_experienced != 1;
        });
        $validator->sometimes('visa_status', 'required', function ($input) {
            return Auth::user()->country != 1;
        });

        return $validator;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'visa_status' => 'attr:visa_status',
            'visa_issue_date' => 'date|date_format:d.m.Y|before:today|required_unless:country,1',
            //sometimes required if country is not japan
            'visa_expiry_date' => 'date|date_format:d.m.Y|required_with:visa_issue_date|after:visa_issue_date',
            'japanese_language_level' => 'required|attr:lang.level',
            'japanese_writing_level' => 'required|attr:language_writing_skills',
            'english_language_level' => 'required|attr:lang.level',
            'desired_employment_type' => 'required|attr:employment_type',
            'nearest_station' => 'string|nullable',
            'willing_to_relocate' => 'required|in:1,0',
            'is_experienced' => 'required|in:1,0',
            'experience' => 'nullable|integer|min:1|required_if:is_experienced,1',
            'portfolio_url' => 'url',
            'qiita_id' => 'string|nullable',
            'github_url' => 'url',
            'country' => 'required|attr:country',
            'zip_code' => 'required|numeric|digits:7',
            'prefecture_id' => 'required|attr:prefecture',
            'address' => 'string|nullable',
            'street_address' => 'string|nullable',
            'self_introduction' => 'string|nullable|max:5000',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */

}
