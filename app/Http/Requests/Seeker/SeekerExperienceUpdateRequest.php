<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class SeekerExperienceUpdateRequest
 *
 * @OA\Schema(
 *    schema="SeekerExperienceUpdateRequest",
 *    @OA\Property(
 *        property="company_name",
 *        description="Company Name",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="job_title",
 *        description="Job title",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="job_from",
 *        description="date",
 *        type="string",
 *        format="date"
 *    ),
 *    @OA\Property(
 *        property="job_to",
 *        description="date",
 *         type="string",
 *        format="date"
 *    ),
 *      @OA\Property(
 *        property="currently_working_here",
 *        description="currently working here?",
 *        type="boolean"
 *    ),
 *      @OA\Property(
 *        property="annual_salary",
 *        description="annual salary",
 *        type="integer"
 *    ),
 *      @OA\Property(
 *        property="job_description",
 *        description="job description",
 *        type="string"
 *    )
 * )
 */
class SeekerExperienceUpdateRequest extends FormRequest
{
    const NAME = 'seeker_experience';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => ['required', 'max:200', 'string'],
            'job_title' => 'required|max:150',
            'job_from' => 'required|date|date_format:d.m.Y|before:job_to|before:today',
            'job_to' => 'required_if:currently_working_here,1|date|date_format:d.m.Y|before:today',
            'currently_working_here' => 'required|in:0,1',
            'annual_salary' => 'nullable|numeric|integer|min:1',
            'job_description' => 'required|max:200|string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
        ];
    }

}
