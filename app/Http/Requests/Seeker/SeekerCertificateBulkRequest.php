<?php

namespace App\Http\Requests\Seeker;

/**
 * Class SeekerCertificateBulkRequest
 *
 * @OA\Schema(
 *    schema="SeekerCertificateBulkRequest",
 *     @OA\Property(
 *         property="certificates",
 *         description="seeker certificates",
 *         type="array",
 *         @OA\Items(
 *           type="object",
 *           required={"certificate_name","year"},
 *           @OA\Property(
 *            property="certificate_name",
 *            description="certificate_name",
 *            type="string",
 *          ),@OA\Property(
 *            property="year",
 *            description="year ",
 *            type="integer",
 *          ),@OA\Property(
 *            property="id",
 *            description="certificate id for update",
 *            type="integer",
 *          )
 *         )
 *  )
 * )
 */

use App\Extensions\Foundation\Http\FormRequest;

class SeekerCertificateBulkRequest extends FormRequest
{
    const NAME = 'seeker.certificateUpdate';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'certificates' => 'required',
            'certificates.*.id' => [
                'exists:seeker_certificates',
                'distinct'
            ],
            'certificates.*.certificate_name' => 'required|string|max:100',
            'certificates.*.year' => 'required|date|date_format:d.m.Y|before:today'
        ];
    }

    public function messages()
    {
        return [
            'certificates.*.year.before' => trans('messages.certificate_year')
        ];
    }
}
