<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class SeekerUpdateRequest
 *
 * @OA\Schema(
 *    schema="SeekerUpdateRequest",
 *    @OA\Property(
 *        property="is_experienced",
 *        description="is exp or not of seeker",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="experience",
 *        description="experience value of attr for seeker",
 *        type="integer"
 *    ),@OA\Property(
 *        property="portfolio_url",
 *        description="portfolio_url f seeker",
 *        type="string"
 *    ),@OA\Property(
 *        property="qiita_id",
 *        description="quita id of seeker",
 *        type="string"
 *     ),@OA\Property(
 *        property="github_url",
 *        description="github url of seeker",
 *        type="string"
 *    )
 * )
 */
class SeekerUpdateRequest extends FormRequest
{
    const NAME = 'seeker';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_experienced' => 'nullable|in:1,0',
            'experience' => 'nullable|integer|required_if:is_experienced,1|attr:experience_year',
            'portfolio_url' => 'url|nullable',
            'qiita_id' => 'string|nullable',
            'github_url' => 'url|nullable'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'experience.required_if' => trans('messages.required_if_experienced',
                ['attribute' => trans('resource.seeker.experience')]),
        ];
    }
}
