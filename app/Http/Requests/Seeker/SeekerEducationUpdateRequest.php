<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class SeekerEducationUpdateRequest
 *
 * @OA\Schema(
 *    schema="SeekerEducationUpdateRequest",
 *    @OA\Property(
 *        property="university_institute",
 *        description="University Institute name",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="degree",
 *        description="Degree name",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="faculty",
 *        description="faculty",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="graduation_country",
 *        description="graduation country",
 *         type="string"
 *    ),
 *      @OA\Property(
 *        property="graduation_status",
 *        description="graduation status",
 *        type="string"
 *    ),
 *      @OA\Property(
 *        property="graduation_year",
 *        description="graduation year",
 *        type="string",
 *         format="date"
 *    )
 * )
 */
class SeekerEducationUpdateRequest extends FormRequest
{
    const NAME = 'seeker_education';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'university_institute' => 'required|string',
            'degree' => 'required|attr:degree',
            'faculty' => 'required|string',
            'graduation_country' => 'required|attr:country',
            'graduation_status' => 'required|attr:graduation_status',
            'graduation_year' => 'required_if:graduation_status,1|date|date_format:d.m.Y|before:today'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
        ];
    }
}
