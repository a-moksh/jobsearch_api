<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class SeekerEducationBulkRequest
 *
 * @OA\Schema(
 *    schema="SeekerEducationBulkRequest",
 *     @OA\Property(
 *         property="educations",
 *         description="seeker educations",
 *         type="array",
 *         @OA\Items(
 *           type="object",
 *           required={"university_institute","degree","faculty","graduation_country","graduation_status","graduation_year"},
 *           @OA\Property(
 *            property="university_institute",
 *            description="university_institute",
 *            type="string",
 *          ),@OA\Property(
 *            property="degree",
 *            description="degree ",
 *            type="integer",
 *          ),@OA\Property(
 *            property="id",
 *            description="education id for update",
 *            type="integer",
 *          ),@OA\Property(
 *            property="faculty",
 *            description="faculty",
 *            type="string",
 *          ),@OA\Property(
 *            property="graduation_country",
 *            description="graduation_country",
 *            type="integer",
 *          ),@OA\Property(
 *            property="graduation_status",
 *            description="graduation_status",
 *            type="integer",
 *          ),@OA\Property(
 *            property="graduation_year",
 *            description="graduation_year",
 *            type="integer",
 *          )
 *         )
 *  )
 * )
 */
class SeekerEducationBulkRequest extends FormRequest
{
    const NAME = 'seeker.educationUpdate';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'educations' => 'required',
            'educations.*.id' => [
                'exists:seeker_educations',
                'distinct'
            ],
            'educations.*.university_institute' => 'required|string|max:100',
            'educations.*.degree' => 'required|attr:degree',
            'educations.*.faculty' => 'required_if:educations.*.degree,2|required_if:educations.*.degree,3|required_if:educations.*.degree,4|string|max:100|nullable',
            'educations.*.graduation_country' => 'required|attr:country',
            'educations.*.graduation_status' => 'required|attr:graduation_status',
            'educations.*.graduation_year' => 'nullable|required_unless:educations.*.graduation_status,0|date|date_format:d.m.Y'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'educations.*.graduation_year.required_unless' => trans('messages.required_unless_graduation',
                ['attribute' => trans('resource.seeker.graduation_year')]),
            'educations.*.faculty.required_if' => trans('validation.required',
                ['attribute' => trans('resource.seeker_education.faculty')])
        ];
    }
}
