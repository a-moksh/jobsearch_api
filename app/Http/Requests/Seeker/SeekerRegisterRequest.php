<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class SeekerRegisterRequest
 *
 * @OA\Schema(
 *    schema="SeekerRegisterRequest",
 *      required={"email","user_type","terms","full_name"},
 *    @OA\Property(
 *        property="email",
 *        description="user email",
 *        type="string"
 *    ),@OA\Property(
 *        property="user_type",
 *        description="user type",
 *        type="string",
 *    ),@OA\Property(
 *        property="full_name",
 *        description="full name",
 *        type="string",
 *    ),@OA\Property(
 *        property="terms",
 *        description="terms acceptance",
 *        type="boolean",
 *    )
 * )
 */
class SeekerRegisterRequest extends FormRequest
{
    const NAME = 'auth';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => [
                'bail',
                'required',
                'email',
                Rule::unique('users')->where(function ($query) {
                    return $query->where('deleted_at', null)->where('verified', '=', 1)->where('user_type', 'seeker');
                })
            ],
            'user_type' => 'bail|required|in:seeker',
            'terms' => 'bail|required|in:1',
            'full_name' => 'bail|string|required|max:100',
        ];
        return $rules;
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'terms.required' => trans('messages.terms'),
        ];
    }
}
