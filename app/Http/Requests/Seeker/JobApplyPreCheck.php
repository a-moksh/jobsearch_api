<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;
use App\Rules\ValidateJobIdsApplication;
use App\Rules\ValidSkillId;
use App\Rules\ValidZipCode;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;

class JobApplyPreCheck extends FormRequest
{
    const NAME = 'seeker';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if (auth()->user()->user_type != 'admin') {
            $validator->sometimes('basicUpdate.visa_issue_date', 'required', function ($input) {
                return ($input->basicUpdate['country'] != 1 && $input->basicUpdate['visa_status'] != 24);
            });
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'save' => 'required',
            'job_id' => [
                Rule::exists('jobs', 'id')->where(function ($query) {
                    return $query->where('status', 1)->where('admin_status', 0);
                }),
                new ValidateJobIdsApplication()
            ],
            'job_id.*' => [
                'required',
                Rule::exists('jobs', 'id')->where(function ($query) {
                    return $query->where('status', 1)->where('admin_status', 0);
                }),
                new ValidateJobIdsApplication()
            ],

            //basic update validation
            'basicUpdate.full_name' => 'required|string',
            'basicUpdate.katakana_full_name' => 'required|string|max:100|katakana',
            'basicUpdate.dob' => 'required|date_format:d.m.Y|before:-18 years',
            'basicUpdate.gender' => 'required|attr:gender',
            'basicUpdate.telephone' => [
                'nullable',
                'numeric',
                'digits_between:9,15',
                'min:99',
            ],
            'basicUpdate.mobile' => [
                'nullable',
                'numeric',
                'digits_between:9,15',
                'min:99',
            ],
            'basicUpdate.japanese_language_level' => 'required|attr:lang.level',
            'basicUpdate.japanese_writing_level' => 'required|attr:language_writing_skills',
            'basicUpdate.english_language_level' => 'required|attr:lang.level',
            'basicUpdate.country' => 'required|attr:country',
            'basicUpdate.living_country' => 'required_unless:basicUpdate.country,1|attr:country',

            'basicUpdate.visa_status' => 'nullable|required_unless:basicUpdate.country,1|attr:visa_status',
            'basicUpdate.visa_issue_date' => 'nullable|date|date_format:d.m.Y|before:today',
            //sometimes required if country is not japan
            'basicUpdate.visa_expiry_date' => 'nullable|date|date_format:d.m.Y|required_with:basicUpdate.visa_issue_date|after:basicUpdate.visa_issue_date',

            'basicUpdate.state' => 'nullable|required_unless:basicUpdate.living_country,1|string|max:170',
            'basicUpdate.zip_code' => [
                'nullable',
                'numeric',
                'digits:7',
                'required_if:basicUpdate.living_country,1',
                new ValidZipCode()
            ],
            'basicUpdate.prefecture_id' => 'nullable|required_if:basicUpdate.living_country,1|attr:prefecture',
            'basicUpdate.address' => 'string|nullable|max:100',
            'basicUpdate.street_address' => 'required|string|nullable|max:100',
            'basicUpdate.desired_job_category'=>'required|attr:job_category',
            'basicUpdate.desired_annual_min_salary_range'=>'required|attr:annual_min_salary_range',
            'basicUpdate.experience_year'=>'required|attr:experience_year',
            'basicUpdate.receive_mail_language'=>'required|in:en,ja',

            //resumeupdate
            'resumeUpdate.nearest_station' => 'string|nullable',
            'resumeUpdate.willing_to_relocate' => 'nullable|in:1,0',
            'resumeUpdate.is_experienced' => 'nullable|in:1,0',
            'resumeUpdate.experience' => 'nullable|integer|required_if:resumeUpdate.is_experienced,1|attr:experience_year',
            'resumeUpdate.portfolio_url' => 'url|nullable',
            'resumeUpdate.qiita_id' => 'string|nullable',
            'resumeUpdate.github_url' => 'url|nullable',


            //experience validation
            'experienceUpdate.experiences.*.id' => [
                'exists:seeker_experiences',
                'distinct'
            ],
            'experienceUpdate.experiences.*.company_name' => ['required', 'max:100', 'string'],
            'experienceUpdate.experiences.*.job_title' => 'required|max:100|string',
            'experienceUpdate.experiences.*.job_from' => 'required|date|date_format:d.m.Y|before:today',
            'experienceUpdate.experiences.*.job_to' => 'nullable|required_if:experienceUpdate.experiences.*.currently_working_here,0|date|date_format:d.m.Y|after:experienceUpdate.experiences.*.job_from|before:today',
            'experienceUpdate.experiences.*.currently_working_here' => 'in:0,1',
            'experienceUpdate.experiences.*.annual_salary' => 'required|attr:annual_salary_range',
            'experienceUpdate.experiences.*.job_description' => 'required|max:1500|string',

            //education validation

            'educationUpdate.educations.*.id' => [
                'exists:seeker_educations',
                'distinct'
            ],
            'educationUpdate.educations.*.university_institute' => 'required|string',
            'educationUpdate.educations.*.degree' => 'required|attr:degree',
            'educationUpdate.educations.*.faculty' => 'required_if:educationUpdate.educations.*.degree,2|required_if:educationUpdate.educations.*.degree,3|required_if:educationUpdate.educations.*.degree,4|string|max:100|nullable',
            'educationUpdate.educations.*.graduation_country' => 'required|attr:country',
            'educationUpdate.educations.*.graduation_status' => 'required|attr:graduation_status',
            'educationUpdate.educations.*.graduation_year' => 'nullable|required_if:educationUpdate.educations.*.graduation_status,1|required_if:educationUpdate.educations.*.graduation_status,2|date|date_format:d.m.Y',

            //skills validation
            'skillUpdate.skills.*.skill_type' => 'required|attrgroup:skill_type',
            'skillUpdate.skills.*.skill_id' => [
                'required',
                new ValidSkillId(Input::get('skillUpdate.skills')),
            ],
            'skillUpdate.skills.*.level' => 'required|attr:skill_experience',

            //certificate validation
            'certificateUpdate.certificates.*.id' => [
                'exists:seeker_certificates',
                'distinct'
            ],
            'certificateUpdate.certificates.*.certificate_name' => 'required|string|max:100',
            'certificateUpdate.certificates.*.year' => 'required|date|date_format:d.m.Y|before:today'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'educationUpdate.educations.*.graduation_year.required_if' => trans('messages.required_unless_graduation',
                ['attribute' => trans('resource.seeker.graduation_year')]),
            'certificateUpdate.certificates.*.year.before' => trans('messages.certificate_year')
        ];
    }
}
