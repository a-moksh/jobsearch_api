<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class SeekerMailSettingUpdateRequest
 *
 * @OA\Schema(
 *    schema="SeekerMailSettingUpdateRequest",
 *    @OA\Property(
 *        property="employment_type",
 *        description="employment status",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="industry",
 *        description="job_title",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="japanese_level",
 *        description="japanese language level",
 *        type="string",
 *    ),@OA\Property(
 *        property="english",
 *        description="engish language level",
 *        type="string",
 *    ),@OA\Property(
 *        property="min_salary",
 *        description="min salary",
 *        type="string",
 *    ),@OA\Property(
 *        property="max_salary",
 *        description="max salary",
 *        type="string",
 *    ),@OA\Property(
 *        property="job_location",
 *        description="job location",
 *        type="string",
 *    ),@OA\Property(
 *        property="visa_support",
 *        description="visa support",
 *        type="string",
 *    ),@OA\Property(
 *        property="social_insurance",
 *        description="social insurance",
 *        type="string",
 *    ),@OA\Property(
 *        property="housing_allowance",
 *        description="housing allowance",
 *        type="string",
 *    ),@OA\Property(
 *        property="provide_uniform",
 *        description="provide uniform",
 *        type="string",
 *    ),@OA\Property(
 *        property="hair_clothing_freedom",
 *        description="hair clothing freedom ",
 *        type="string",
 *    ),
 * )
 */
class SeekerMailSettingUpdateRequest extends FormRequest
{
    const NAME = 'seeker_mail_setting';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employment_type' => 'attr:employment_type|nullable',
            'job_category' => 'attr:job_category|nullable',
            'english_level' => 'nullable|attr:lang.level',
            'japanese_level' => 'nullable|attr:lang.level',
            'min_salary' => 'nullable|integer',
            //'max_salary' => 'nullable|greater_than_field:min_salary',
            'job_location' => 'attr:prefecture|nullable',
            'visa_support' => 'nullable|in:0,1',
            'social_insurance' => 'nullable|in:0,1',
            'housing_allowance' => 'nullable|in:0,1',
            'receive_mail' => 'required|in:0,1',
            'hairstyle_freedom' => 'nullable|in:0,1',
            'clothing_freedom' => 'nullable|in:0,1',
            //'receive_mail_language' => 'nullable|required_if:receive_mail,1|in:en,ja',
            'salary_type' => 'nullable|attr:salary_type',
            'all_prefecture' => 'nullable|in:0,1',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'receive_mail_language.required_if' => trans('messages.receive_mail_language'),
            'receive_mail.required' => trans('messages.receive_mail'),
        ];
    }

}
