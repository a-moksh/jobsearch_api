<?php

namespace App\Http\Requests\Seeker;

use App\Extensions\Foundation\Http\FormRequest;

/**
 * Class SeekerSkillStoreRequest
 *
 * @OA\Schema(
 *    schema="SeekerSkillStoreRequest",
 *    @OA\Property(
 *        property="skill_id",
 *        description="Skill  id",
 *        type="string"
 *    )
 * )
 */
class SeekerSkillStoreRequest extends FormRequest
{
    const NAME = 'seeker_skill';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'skill_type' => 'required|attrgroup:skill_type',
            'skill_id' => 'required|attr:' . $this->get('skill_type'),
            'level' => 'required|attr:skill.level',
        ];
    }
}
