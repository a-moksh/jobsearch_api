<?php

namespace App\Http\Requests;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class RegisterRequest
 *
 * @OA\Schema(
 *    schema="RegisterRequest",
 *      required={"email", "password", "password_confirmation", "user_type"},
 *    @OA\Property(
 *        property="email",
 *        description="user email",
 *        type="string"
 *    ),
 *    @OA\Property(
 *        property="password",
 *        description="password",
 *        type="string",
 *    ),@OA\Property(
 *        property="user_type",
 *        description="user type",
 *        type="string",
 *    ),@OA\Property(
 *        property="company_name",
 *        description="co name",
 *        type="string",
 *    ),@OA\Property(
 *        property="password_confirmation",
 *        description="password confirmation",
 *        type="string",
 *    ),@OA\Property(
 *        property="full_name",
 *        description="full name",
 *        type="string",
 *    )
 * )
 */
class RegisterRequest extends FormRequest
{
    const NAME = 'auth';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => [
                'required',
                'email',
                Rule::unique('users')->where(function ($query) {
                    return $query->where('deleted_at', null);
                })
            ],
            'user_type' => 'required|in:seeker,provider,admin',
            'terms' => 'required|in:1',
        ];
        $extra = [];
        if (request('user_type') == 'provider') {
            $extra['company_name'] = 'string|required|max:50';
            $extra['zip_code'] = 'nullable';
        }
        if (request('user_type') == 'seeker') {
            $extra['full_name'] = 'string|required|max:100';
        }
        return array_merge($rules, $extra);
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'terms.required' => trans('messages.terms'),
        ];
    }
}
