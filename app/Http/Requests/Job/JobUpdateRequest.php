<?php

namespace App\Http\Requests\Job;

use App\Extensions\Foundation\Http\FormRequest;
use App\Models\Job;
use App\Rules\ValidJobImage;
use App\Rules\ValidZipCode;
use Illuminate\Support\Facades\Input;

/**
 * Class JobUpdateRequest
 *
 * @OA\Schema(
 *    schema="JobUpdateRequest",
 *     @OA\Property(
 *        property="title_ja",
 *        description="job title",
 *        type="string"
 *    ),
 *       @OA\Property(
 *        property="title_en",
 *        description="job title",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="employment_type",
 *        description="employment type",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="category_id",
 *        description="job category",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="min_salary",
 *        description="min salary",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="max_salary",
 *        description="max salary",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="description_ja",
 *        description="job description",
 *        type="string"
 *    ),
 *      @OA\Property(
 *        property="description_en",
 *        description="job description",
 *        type="string"
 *    ),
 *      @OA\Property(
 *        property="language_level_en",
 *        description="language level en",
 *        type="integer"
 *    ),@OA\Property(
 *        property="language_level_ja",
 *        description="language level",
 *        type="integer"
 *    ),@OA\Property(
 *        property="qualifications_en",
 *        description="qualifications en",
 *        type="integer"
 *    ),@OA\Property(
 *        property="qualifications_ja",
 *        description="qualifications japanese",
 *        type="integer"
 *    ),@OA\Property(
 *        property="welfare_ja",
 *        description="welfare japanese",
 *        type="string"
 *    ),@OA\Property(
 *        property="welfare_en",
 *        description="welfare en",
 *        type="string"
 *    ),@OA\Property(
 *        property="language_translation",
 *        description="language translation",
 *        type="integer"
 *    ),@OA\Property(
 *        property="working_hour_from",
 *        description="working hour",
 *        type="integer"
 *    ),@OA\Property(
 *        property="working_hour_to",
 *        description="working hour to",
 *        type="string"
 *    ),@OA\Property(
 *        property="night_shift",
 *        description="night shift",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="visa_support",
 *        description="visa support",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="overtime",
 *        description="overtime ",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="trial_period",
 *        description="trial period",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="social_insurance",
 *        description="social insurance",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="bonus",
 *        description="bonus",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="hairstyle_freedom",
 *        description="hairstyle freedom",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="clothing_freedom",
 *        description="clothing freedom",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="with_uniform",
 *        description="with uniform",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="housing_allowance",
 *        description="housing allowance",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="other_details",
 *        description="other details",
 *        type="string"
 *    ),@OA\Property(
 *        property="total_annual_leave",
 *        description="total annual leave",
 *        type="string"
 *    ),@OA\Property(
 *        property="salary_type",
 *        description="salary type",
 *        type="integer"
 *    ),@OA\Property(
 *        property="holiday",
 *        description="holiday ",
 *        type="integer"
 *    ),
 *      @OA\Property(
 *        property="short_term",
 *        description="short_term ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="long_term",
 *        description="long term",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="posting_start_date",
 *        description="job posting date",
 *        type="string",
 *        format="date"
 *    ),@OA\Property(
 *        property="station_distance",
 *        description="station distance walking, bus",
 *        type="boolean",
 *    ),
 *     @OA\Property(
 *        property="posting_end_date",
 *        description="job end date",
 *        type="string",
 *        format="date"
 *    ),@OA\Property(
 *        property="status",
 *        description="job status",
 *        type="integer",
 *    ),
 *     @OA\Property(
 *         property="address",
 *         description="job address",
 *         type="array",
 *         @OA\Items(
 *           type="object",
 *           required={"overview"},
 *          @OA\Property(
 *            property="id",
 *            description="job location id",
 *            type="integer",
 *          ),
 *           @OA\Property(
 *            property="prefecture_id",
 *            description="prefecture id",
 *            type="integer",
 *          ),@OA\Property(
 *            property="address",
 *            description="address ",
 *            type="string",
 *          ),@OA\Property(
 *            property="street_address",
 *            description="street address",
 *            type="string",
 *          ),@OA\Property(
 *            property="railway_name",
 *            description="railway name ",
 *            type="string",
 *          ),@OA\Property(
 *            property="station_name",
 *            description="station name",
 *            type="string",
 *          ),@OA\Property(
 *            property="zip_code",
 *            description="zip code ",
 *            type="integer",
 *          ),@OA\Property(
 *            property="station_distance",
 *            description="station distance ",
 *            type="integer",
 *          ),@OA\Property(
 *            property="walking_distance",
 *            description="walking distance ",
 *            type="integer",
 *          ),
 *         )
 *  ),@OA\Property(
 *         property="language",
 *         description="language",
 *         type="array",
 *         @OA\Items(
 *           type="object",
 *           required={"overview"},
 *         @OA\Property(
 *            property="id",
 *            description="language_id",
 *            type="integer",
 *          ),@OA\Property(
 *            property="level",
 *            description="language_id",
 *            type="integer",
 *          ),
 *          @OA\Property(
 *            property="language",
 *            description="language ",
 *            type="integer",
 *          )
 *         )
 *  ),@OA\Property(
 *         property="holiday_days",
 *         description="holiday days",
 *         type="array",
 *         @OA\Items(
 *           type="object",
 *           required={"overview"},
 *
 *         )
 *  ),
 * )
 */
class JobUpdateRequest extends FormRequest
{
    const NAME = 'job';
    private $job;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->job = Job::findOrFail($this->route('id'));
        if ($this->job->language_translation == Input::get('language_translation') && (($this->job->admin_status == 1) || ($this->job->admin_status == 3))) {
            return true;
        }
        return false;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->normalize();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (Input::get('admin_status') == 2) {
            return [
                'comment' => [
                    'required',
                    'string',
                    'max:300',
                ],
                'admin_status' => [
                    'required',
                    'attr:admin_job_status',
                ]
            ];
        } else {
            $common_rules = [
                'employment_type' => [
                    'required',
                    'distinct',
                    'attr:employment_type',
                ],
                'admin_status' => [
                    'required',
                    'attr:admin_job_status',
                ],
                'time_period' => 'nullable|required_unless:employment_type,0|attr:time_period',
                'category_id' => [
                    'required',
                    'attr:job_category'
                ],
                'sub_category_id' => [
                    'required',
                    'attr_sub:job_category:' . Input::get('category_id'),
                ],
                'title_ja' => [
                    'required',
                    'string',
                    'max:100',
                ],
                'min_salary' => [
                    'required',
                    'numeric',
                    'min:100',
                    'digits_between:1,12',
                ],
                'max_salary' => [
                    'required_with:min_salary',
                    'numeric',
                    'digits_between:1,12',
                    'greater_than_field:min_salary',
                ],
                'description_ja' => [
                    'required',
                    'string',
                    'max:5000',
                ],
                'qualifications_ja' => [
                    'required',
                    'string',
                    'max:5000',
                ],
                'welfare_ja' => [
                    'required',
                    'string',
                    'max:5000',
                ],
                'language_translation' => [
                    'required',
                    'attr:job_translation'
                ],
                'working_hour_from' => [
                    'attr:work_time',
                    'required'
                ],
                'working_hour_to' => [
                    'attr:work_time',
                    'required'
                ],
                'morning_shift' => [
                    'integer',
                    'attr:boolean',
                ],
                'night_shift' => [
                    'integer',
                    'attr:boolean'
                ],
                'visa_support' => [
                    'integer',
                    'attr:boolean'
                ],
                'social_insurance' => [
                    'integer',
                    'attr:boolean'
                ],
                'remote' => [
                    'integer',
                    'attr:boolean'
                ],
                'company_service' => [
                    'integer',
                    'attr:boolean'
                ],
                'practical_experience' => [
                    'integer',
                    'attr:boolean'
                ],
                'bonus' => [
                    'integer',
                    'attr:boolean'
                ],
                'clothing_freedom' => [
                    'integer',
                    'attr:boolean'
                ],
                'housing_allowance' => [
                    'integer',
                    'attr:boolean'
                ],
                'salary_type' => [
                    'required',
                    'integer',
                    'attr:salary_type'
                ],
                'holiday' => [
                    'required',
                    'integer',
                    'attr:holiday'
                ],
                'holiday_days.*' => [
                    'required',
                    'attr:week.days'
                ],
                'short_term' => [
                    'integer',
                    'attr:boolean'
                ],
                'long_term' => [
                    'integer',
                    'attr:boolean'
                ],
                'japanese_lang_level' => [
                    'required',
                    'attr:lang.level'
                ],
                'english_lang_level' => [
                    'required',
                    'attr:lang.level'
                ],
                'langs.*.language' => [
                    'distinct',
                    'required_with:langs.*.level',
                    'attr:lang_option_new'
                ],
                'langs.*.level' => [
                    'required_with:langs.*.language',
                    'attr:lang.level'
                ],
                'addresses.*.prefecture_id' => [
                    'required',
                    'attr:prefecture',
                ],
                'addresses.*.address' => [
                    'required',
                    'string',
                    "max:50"
                ],
                'addresses.*.street_address' => [
                    'required',
                    'string',
                    "max:100"
                ],
                'addresses.*.zip_code' => [
                    'required',
                    'numeric',
                    'digits:7',
                    new ValidZipCode()
                ],
                'addresses' => 'required',
                'status' => [
                    'required',
                    'not_in:2',
                    'attr:job_status'
                ],
                'images' => [
                    'nullable',
                    'array',
                    'distinct',
                    new ValidJobImage(),
                    'max:5',
                ],
                'deleted_images' => [
                    'nullable',
                    'array',
                    'distinct'
                ]
            ];
            $translation_rule = [];
            if (Input::get('language_translation') != 1 || Input::get('admin_status') == 0) {
                $translation_rule = [
                    'title_en' => [
                        'required',
                        'string',
                        'max:100',
                    ],
                    'description_en' => [
                        'required',
                        'string',
                        'max:5000',
                    ],
                    'qualifications_en' => [
                        'required',
                        'string',
                        'max:5000',
                    ],
                    'welfare_en' => [
                        'required',
                        'string',
                        'max:5000',
                    ],
                ];
            }
            if (Input::get('language_translation') == 1) {
                $translation_rule = [
                    'title_en' => [
                        'nullable',
                        'string',
                        'max:100',
                    ],
                    'description_en' => [
                        'nullable',
                        'string',
                        'max:5000',
                    ],
                    'qualifications_en' => [
                        'nullable',
                        'string',
                        'max:5000',
                    ],
                    'welfare_en' => [
                        'nullable',
                        'string',
                        'max:5000',
                    ],
                ];
            }
            return array_merge($common_rules, $translation_rule);
        }
    }

    public function messages()
    {
        return [
            'max_salary.greater_than_field' => trans('messages.greater_than_field')
        ];
    }
}
