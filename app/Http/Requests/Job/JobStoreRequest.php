<?php

namespace App\Http\Requests\Job;

use App\Extensions\Foundation\Http\FormRequest;
use App\Rules\ValidJobImage;
use App\Rules\ValidZipCode;
use Illuminate\Support\Facades\Input;

/**
 * Class JobStoreRequest
 *
 * @OA\Schema(
 *    schema="JobStoreRequest",
 *     @OA\Property(
 *        property="title_ja",
 *        description="job title",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="employment_type",
 *        description="employment type",
 *        type="string"
 *    ),
 *       @OA\Property(
 *        property="title_en",
 *        description="job title",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="category_id",
 *        description="job category",
 *        type="string"
 *    ),@OA\Property(
 *        property="sub_category_id",
 *        description="sub category id",
 *        type="string"
 *    ),
 *     @OA\Property(
 *        property="min_salary",
 *        description="min salary",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="max_salary",
 *        description="max salary",
 *        type="integer"
 *    ),
 *     @OA\Property(
 *        property="description_ja",
 *        description="job description",
 *        type="string"
 *    ),
 *      @OA\Property(
 *        property="description_en",
 *        description="job description",
 *        type="string"
 *    ),@OA\Property(
 *        property="qualifications_en",
 *        description="qualifications en",
 *        type="integer"
 *    ),@OA\Property(
 *        property="qualifications_ja",
 *        description="qualifications japanese",
 *        type="integer"
 *    ),@OA\Property(
 *        property="welfare_ja",
 *        description="welfare japanese",
 *        type="string"
 *    ),@OA\Property(
 *        property="welfare_en",
 *        description="welfare en",
 *        type="string"
 *    ),@OA\Property(
 *        property="language_translation",
 *        description="language translation",
 *        type="integer"
 *    ),@OA\Property(
 *        property="working_hour_from",
 *        description="working hour",
 *        type="integer"
 *    ),@OA\Property(
 *        property="working_hour_to",
 *        description="working hour to",
 *        type="string"
 *    ),@OA\Property(
 *        property="morning_shift",
 *        description="morning_shift",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="night_shift",
 *        description="night shift",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="visa_support",
 *        description="visa support",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="overtime",
 *        description="overtime ",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="japanese_lang_level",
 *        description="japanese_lang_level ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="english_lang_level",
 *        description="english_lang_level ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="trial_period",
 *        description="trial period",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="time_period",
 *        description="time_period ",
 *        type="boolean"
 *    ),@OA\Property(
 *        property="holiday",
 *        description="holiday ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="social_insurance",
 *        description="social_insurance ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="salary_type",
 *        description="salary_type ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="remote",
 *        description="remote ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="practical_experience",
 *        description="practical_experience ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="bonus",
 *        description="bonus ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="clothing_freedom",
 *        description="clothing_freedom ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="housing_allowance",
 *        description="housing_allowance ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="company_service",
 *        description="company_service ",
 *        type="integer"
 *    ),
 *      @OA\Property(
 *        property="short_term",
 *        description="short_term ",
 *        type="integer"
 *    ),@OA\Property(
 *        property="long_term",
 *        description="long term",
 *        type="integer"
 *    ),@OA\Property(
 *        property="status",
 *        description="status",
 *        type="integer"
 *    ),@OA\Property(
 *        property="images",
 *        description="images",
 *        type="string"
 *    ),
 *     @OA\Property(
 *         property="addresses",
 *         description="job address",
 *         type="array",
 *         @OA\Items(
 *           type="object",
 *           required={"overview"},
 *           @OA\Property(
 *            property="prefecture_id",
 *            description="prefecture id",
 *            type="integer",
 *          ),@OA\Property(
 *            property="address",
 *            description="address ",
 *            type="string",
 *          ),@OA\Property(
 *            property="street_address",
 *            description="street address",
 *            type="string",
 *          ),@OA\Property(
 *            property="zip_code",
 *            description="zip code ",
 *            type="integer",
 *          )
 *         )
 *  ),@OA\Property(
 *         property="langs",
 *         description="language",
 *         type="array",
 *         @OA\Items(
 *           type="object",
 *           required={"overview"},
 *          @OA\Property(
 *            property="language",
 *            description="language",
 *            type="integer",
 *          ),@OA\Property(
 *            property="level",
 *            description="level",
 *            type="integer",
 *          ),
 *         )
 *  ),@OA\Property(
 *         property="holiday_days",
 *         description="holiday days",
 *         type="array",
 *         @OA\Items(
 *           type="object",
 *           required={"overview"},
 *         )
 *  ),
 * )
 */
class JobStoreRequest extends FormRequest
{
    const NAME = 'job';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->normalize();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $common_rules = [
            'employment_type' => [
                'required',
                'distinct',
                'attr:employment_type',
            ],
            'time_period' => 'nullable|required_unless:employment_type,0|attr:time_period',
            'category_id' => [
                'required',
                'attr:job_category'
            ],
            'sub_category_id' => [
                'required',
                'attr_sub:job_category:' . Input::get('category_id'),
            ],
            'title_ja' => [
                'required',
                'string',
                'max:100',
            ],
            'min_salary' => [
                'required',
                'numeric',
                'min:100',
                'digits_between:1,12',
            ],
            'max_salary' => [
                'required_with:min_salary',
                'numeric',
                'digits_between:1,12',
                'greater_than_field:min_salary',
            ],

            'description_ja' => [
                'required',
                'string',
                'max:5000',
            ],
            'qualifications_ja' => [
                'required',
                'string',
                'max:5000',
            ],

            'welfare_ja' => [
                'required',
                'string',
                'max:5000',
            ],
            'language_translation' => [
                'required',
                'attr:job_translation'
            ],
            'working_hour_from' => [
                'attr:work_time',
                'required'
            ],
            'working_hour_to' => [
                'attr:work_time',
                'required'
            ],
            'morning_shift' => [
                'integer',
                'attr:boolean',
            ],
            'night_shift' => [
                'integer',
                'attr:boolean'
            ],
            'visa_support' => [
                'integer',
                'attr:boolean'
            ],
            'social_insurance' => [
                'integer',
                'attr:boolean'
            ],
            'remote' => [
                'integer',
                'attr:boolean'
            ],
            'company_service' => [
                'integer',
                'attr:boolean'
            ],
            'practical_experience' => [
                'integer',
                'attr:boolean'
            ],
            'bonus' => [
                'integer',
                'attr:boolean'
            ],
            'clothing_freedom' => [
                'integer',
                'attr:boolean'
            ],
            'housing_allowance' => [
                'integer',
                'attr:boolean'
            ],
            'salary_type' => [
                'required',
                'integer',
                'attr:salary_type'
            ],
            'holiday' => [
                'required',
                'integer',
                'attr:holiday'
            ],
            'holiday_days.*' => [
                'required',
                'attr:week.days'
            ],
            'short_term' => [
                'integer',
                'attr:boolean'
            ],
            'long_term' => [
                'integer',
                'attr:boolean'
            ],
            'japanese_lang_level' => [
                'required',
                'attr:lang.level'
            ],
            'english_lang_level' => [
                'required',
                'attr:lang.level'
            ],
            'langs.*.language' => [
                'distinct',
                'required_with:langs.*.level',
                'attr:lang_option_new'
            ],
            'langs.*.level' => [
                'required_with:langs.*.language',
                'attr:lang.level'
            ],
            'addresses.*.prefecture_id' => [
                'required',
                'attr:prefecture',
            ],
            'addresses.*.address' => [
                'required',
                'string',
                "max:50"
            ],
            'addresses.*.street_address' => [
                'required',
                'string',
                "max:100"
            ],
            'addresses.*.zip_code' => [
                'required',
                'numeric',
                'digits:7',
                new ValidZipCode()
            ],
            'addresses' => 'required',
            'status' => [
                'required',
                'not_in:2',
                'attr:job_status'
            ],
            'images' => [
                'required',
                'array',
                new ValidJobImage(),
                'distinct',
                'max:5',
            ]
            // default will be pending
            // add all the other options and complete.
        ];
        $translation_rule = [];
        if (Input::get('language_translation') != 1) {
            $translation_rule = [
                'title_en' => [
                    'required',
                    'string',
                    'max:100',
                ],
                'description_en' => [
                    'required',
                    'string',
                    'max:5000',
                ],
                'qualifications_en' => [
                    'required',
                    'string',
                    'max:5000',
                ],
                'welfare_en' => [
                    'required',
                    'string',
                    'max:5000',
                ],
            ];
        }
        return array_merge($common_rules, $translation_rule);
    }

    public function messages()
    {
        return [
            'max_salary.greater_than_field' => trans('messages.greater_than_field')
        ];
    }
}
