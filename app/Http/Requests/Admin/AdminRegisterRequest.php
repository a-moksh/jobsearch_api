<?php

namespace App\Http\Requests\Admin;

use App\Extensions\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
/**
 * Class AdminRegisterRequest
 *
 * @OA\Schema(
 *    schema="AdminRegisterRequest",
 *      required={"email", "user_type"},
 *    @OA\Property(
 *        property="email",
 *        description="user email",
 *        type="string"
 *    ),@OA\Property(
 *        property="user_type",
 *        description="user_type",
 *        type="string",
 *    )
 * )
 */
class AdminRegisterRequest extends FormRequest
{
    const NAME = 'auth';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => [
                'required',
                'email',
                Rule::unique('users')->where(function ($query) {
                    return $query->where('deleted_at', null)->where('verified', '=', 1)->where('user_type', 'admin');
                })
            ],
            'user_type' => 'required|in:admin',
        ];
        return $rules;
    }
}
