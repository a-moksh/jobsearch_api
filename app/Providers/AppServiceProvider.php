<?php

namespace App\Providers;

use App;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Notifications\ExceptionCaught;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Queue;
use Validator;

//use Laravel\Telescope\TelescopeServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // Serialize date for JSON
        Carbon::serializeUsing(function ($carbon) {
            return $carbon->format('Y-m-d\TH:i:sP');
        });

        view()->composer('*', function ($view) {
            view()->share('locale', App::getLocale());
        });

        Validator::extend('valid_japanese', function ($attribute, $value, $parameters, $validator) {
            return (preg_match('/[\x{4E00}-\x{9FBF}\x{3040}-\x{309F}\x{30A0}-\x{30FF}]/u', $value));
        });
        Validator::replacer('greater_than_field', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':field', $parameters[0], $message);
        });

        

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //$this->app->register(TelescopeServiceProvider::class);        
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
