<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         *
         */
        Validator::resolver(function ($translator, $data, $rules, $messages, $attributes) {
            return new \App\Extensions\Validation\Validator($translator, $data, $rules, $messages, $attributes);
        });

        /**
         * validate greater than field
         */
        Validator::extend('greater_than_field', function ($attribute, $value, $parameters, $validator) {
            $min_field = $parameters[0];
            $data = $validator->getData();
            $min_value = $data[$min_field];
            return $value >= $min_value;
        });

        /**
         * validate alpha
         */
        Validator::extend('alpha', function ($attribute, $value, $parameters, $validator) {
            return (preg_match("/^[a-z]+$/i", $value));
        });

        /**
         *
         */
        Validator::extend('attr', function ($attribute, $value, $parameters, $validator) {
            if (!empty($parameters)) {
                $query = DB::table('attributes')
                    ->join('attribute_groups', 'attributes.group_id', '=', 'attribute_groups.id')
                    ->where('attribute_groups.name', $parameters);
                if (is_array($value)) {
                    $query->whereIn('attributes.value', $value);
                    $result = $query->get();
                    return $result->count() == count($value);
                } else {
                    $query->where('attributes.value', $value);
                    $result = $query->first();
                    return !is_null($result);
                }
            }
        });

        /**
         *
         */
        Validator::extend('attr_sub', function ($attribute, $value, $parameters, $validator) {
            $result = false;
            if (!empty($parameters)) {
                $parent = explode(':', $parameters[0]);
                $result = DB::table('attributes')
                    ->join('attribute_groups', 'attributes.group_id', '=', 'attribute_groups.id')
                    ->select('attributes.name')
                    ->where('attribute_groups.name', $parent[0])
                    ->where('attributes.value', $parent[1])
                    ->first();
                if (!is_null($result)) {
                    $result = !is_null(DB::table('attributes')
                        ->join('attribute_groups', 'attributes.group_id', '=', 'attribute_groups.id')
                        ->where('attribute_groups.name', $result->name)
                        ->where('attributes.value', $value)
                        ->first());
                }
            }
            return $result;
        });


        /**
         * validating group
         */
        Validator::extend('attrgroup', function ($attribute, $value) {
            $result = DB::table('attribute_groups')
                ->where('attribute_groups.name', $value)
                ->first();
            return !is_null($result);
        });

        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            $result = false;
            if (auth()->user()) {
                if (!Hash::check($value, auth()->user()->password)) {
                    $result = true;
                }
            } else {
                if (!isset($validator->getdata()['user_type'])) {
                    abort(403);
                }
                $user = DB::table('users')->where('email', $validator->getdata()['email'])->where('user_type',
                    $validator->getdata()['user_type'])->first();
                if ($user) {
                    $result = !Hash::check($value, $user->password);
                } else {
                    $result = true;
                }
            }
            return $result;
        });

        /**
         * validating group
         */
        Validator::extend('time_after', function ($attribute, $value, $parameters, $validator) {
            if (strtotime($value) > strtotime($validator->getData()[$parameters[0]])) {
                return true;
            }
            return false;
        });

        /**
         * alpha numeric validation
         */
        Validator::extend('alpha_num', function ($attribute, $value, $parameters, $validator) {
            return (preg_match("/^[a-z0-9]+$/i", $value));
        });

        /**
         *alpha dash validation
         */
        Validator::extend('alpha_dash', function ($attribute, $value, $parameters, $validator) {
            return (preg_match("/^[a-z0-9_\-]+$/i", $value));
        });

        /**
         * validate alpha underscore
         */
        Validator::extend('alpha_under_score', function ($attribute, $value, $parameters, $validator) {
            return (preg_match("/^[a-z0-9_]+$/i", $value));
        });

        /**
         * replacer
         */
        Validator::replacer('greater_than_field', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':field', $parameters[0], $message);
        });
        Validator::extend('check_array', function ($attribute, $value, $parameters, $validator) {
            return count(array_filter($value, function ($var) use ($parameters) {
                return ($var && $var >= $parameters[0]);
            }));
        });

        /**
         *
         */
        Validator::extend('Katakana', function ($attribute, $value, $parameters, $validator) {
            if (mb_strlen($value) > 100) {
                return false;
            }

            if (preg_match('/[^ァ-ヶー_\s0-9\.]/u', $value) !== 0) {
                return false;
            }
            return true;

//            if (preg_match('/[^ァ-ヶー_\s]/u', $value) !== 0) {
//                return false;
//            }
//            return true;
        });
        /**
         *
         */
        Validator::extend('email_verified', function ($attribute, $value, $parameters, $validator) {

            if (!isset($validator->getData()['user_type'])) {
                return false;
            }
            $email = DB::table('users')->where('email', $value)->where('user_type',
                $validator->getData()['user_type'])->first();
            if ($email && $email->verified) {
                return true;
            } else {
                return false;
            }
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
