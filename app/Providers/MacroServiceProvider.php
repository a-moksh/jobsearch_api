<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Add timestamp and operator columns for Migration
         *
         * @param array $comments
         * @param integer $precision
         * @return \Illuminate\Database\Schema\Blueprint
         */
        Blueprint::macro('timestampWithOperators', function ($comments = [], $precision = 0) {
            $comments = array_merge($comments, [
                'created_at' => 'created_at',
                'created_by' => 'created_by',
                'updated_at' => 'updated_at',
                'updated_by' => 'updated_by',
                'deleted_at' => 'deleted_at',
                'deleted_by' => 'deleted_at',
            ]);
            $this->timestamp('created_at', $precision)->nullable()->comment($comments['created_at']);
            $this->unsignedInteger('created_by', $precision)->nullable()->comment($comments['created_by']);
            $this->timestamp('updated_at', $precision)->nullable()->comment($comments['updated_at']);
            $this->unsignedInteger('updated_by', $precision)->nullable()->comment($comments['updated_by']);
            $this->timestamp('deleted_at', $precision)->nullable()->comment($comments['deleted_at']);
            $this->unsignedInteger('deleted_by', $precision)->nullable()->comment($comments['deleted_by']);
        });

        /**
         * Normalize for Request
         *
         * @param string|array $keys
         * @return mixed
         */
        Request::macro('normalize', function ($keys = null) {
            $params = ($keys === null) ? request()->all() : request()->only($keys);
            $normalized = array_normalize_recursive($params);
            request()->replace($normalized);
        });

        /**
         * Except null parameters for Request
         *
         * @param string|array $keys
         * @return mixed
         */
        Request::macro('exceptNull', function ($keys = null) {
            $params = ($keys === null) ? request()->all() : request()->only($keys);
            $params = array_walk_recursive_remove($params, function ($value) {
                return $value === null;
            });
            request()->replace($params);
        });

        /**
         * PaginateByUrl for Builder
         *
         * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
         */
        Builder::macro('paginateByUrl', function () {
            $request = request();
            $pageName = 'page';
            $page = $request->query($pageName, 1);
            $fields = $request->query('fields');
            $pageLimit = $request->query('limit');
            $orderBy = $request->query('orderby');
            $direction = $request->query('direction');
            $pageLimitDefault = config('app.page_limit', 25);
            $pageLimitMax = config('app.page_limit_max', 500);

            if ($pageLimit <= 0) {
                $pageLimit = $pageLimitDefault;
            }
            if ($pageLimit > $pageLimitMax) {
                $pageLimit = $pageLimitMax;
            }
            if (empty($orderBy)) {
                $orderBy = $this->model->getKeyName();
            }
            if (!empty($orderBy)) {
                if (!empty($direction)) {
                    $direction = strtoupper($direction);
                    if (!in_array($direction, ['ASC', 'DESC'])) {
                        $direction = null;
                    }
                }
                $this->orderBy($orderBy, (!empty($direction)) ? $direction : 'DESC');
            }

            return $this->paginate($pageLimit, $fields, $pageName, $page)->appends($request->query());
        });

        /**
         * Full-text search for Builder
         *
         * @param array|string $columns
         * @param string $words
         * @return \Illuminate\Database\Eloquent\Builder
         */
        Builder::macro('whereFullText', function ($columns, $words) {
            $columns = (array)$columns;
            $columns = implode('`, `', $columns);
            $columns = "`{$columns}`";
            $words = implode(' ', array_map(function ($value) {
                return "+{$value}";
            }, explode(' ', $words)));
            return $this->whereRaw("MATCH ({$columns}) AGAINST (? IN BOOLEAN MODE)", $words);
        });

        /**
         * WhereInMultiple for Builder
         *
         * @param array|string $columns
         * @param string $keys
         * @return \Illuminate\Database\Eloquent\Builder
         */
        Builder::macro('whereInMultiple', function ($columns, $keys) {
            $columns = (array)$columns;
            $columns = implode('`, `', $columns);
            $columns = "`{$columns}`";
            $keys = implode(',', array_map(function ($values) {
                foreach ($values as $index => $value) {
                    $values[$index] = DB::connection()->getPdo()->quote($value);
                }
                return '(' . implode(',', $values) . ')';
            }, $keys));
            return $this->whereRaw('(' . $columns . ') in (' . $keys . ')');
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
