<?php

namespace App\Auth\Passwords;

use Illuminate\Auth\Passwords\PasswordResetServiceProvider;

class JBPassswordResetServiceProvider extends PasswordResetServiceProvider
{
    /**
     * Register the password broker instance.
     *
     * @return void
     */
    protected function registerPasswordBroker()
    {
        $this->app->singleton('auth.password', function ($app) {
            return new JBPasswordBrokerManager($app);
        });

        $this->app->bind('auth.password.broker', function ($app) {
            return $app->make('auth.password')->broker();
        });
    }
}
