<?php

namespace App\Policies;

use App\Models\Seeker;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SeekerPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function listOne(User $user)
    {

        return $user->user_type != 'seeker';

    }

    public function store()
    {

        return false;

    }

    public function show(User $user, Seeker $seeker)
    {

        return $user->id === $seeker->user_id;

    }

    public function update(User $user, Seeker $seeker)
    {

        return $user->id === $seeker->user_id;

    }
}
