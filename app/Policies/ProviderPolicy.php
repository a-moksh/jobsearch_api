<?php

namespace App\Policies;

use App\Models\Provider;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProviderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function list(User $user)
    {

        return $user->user_type == 'admin';

    }

    public function store()
    {

        return false;

    }

    public function show(User $user, Provider $provider)
    {

        return $user->id === $provider->user_id;

    }

    public function update(User $user, Provider $provider)
    {

        return $user->id === $provider->user_id;

    }
}
