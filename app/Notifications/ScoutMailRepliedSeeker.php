<?php

namespace App\Notifications;

use App\Models\ScoutMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ScoutMailRepliedSeeker extends Notification implements ShouldQueue
{
    use Queueable;
    private $scoutMail;
    private $language;
    private $seeker_user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ScoutMail $scoutMail, $seeker_user, $language)
    {
        $this->scoutMail = $scoutMail;
        $this->language = $language;
        $this->seeker_user = $seeker_user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->view(
            'emails.' . $this->language . '.provider.seeker_replied',
            [
                'user' => $notifiable,
                'scoutmail' => $this->scoutMail,
                'seeker_user' =>$this->seeker_user,
                'language'=>$this->language
            ]
        )->subject(trans('messages.mail.job_scout_reply_received'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message_en' => trans('notification.seeker.seeker_replied', ['job_title' => $this->scoutMail->id]),
            'message_ja' => trans('notification.seeker.seeker_replied', ['job_title' => $this->scoutMail->id]),
            'redirect_provider' => '#'
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message_en' => trans('notification.seeker.seeker_replied', ['job_title' => $this->scoutMail->id]),
            'message_ja' => trans('notification.seeker.seeker_replied', ['job_title' => $this->scoutMail->id]),
            'redirect_provider' => '#'
        ]);
    }
}
