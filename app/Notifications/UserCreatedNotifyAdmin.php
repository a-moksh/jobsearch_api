<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserCreatedNotifyAdmin extends Notification implements ShouldQueue
{
    use Queueable;

    private $user;
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $language)
    {
        //
        $this->user = $user;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->view(
            'emails.' . $this->language . '.admin.user_registered',
            [
                'user' => $this->user,
                'language'=>$this->language
            ]
        )->subject(trans('messages.mail.company_registered'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

        return [
            'message_en' => trans('notification.admin.user_created_admin_en',
                ['user_email' => $this->user->email, 'user_type' => $this->user->user_type]),
            'message_ja' => trans('notification.admin.user_created_admin_ja',
                ['user_email' => $this->user->email, 'user_type' => $this->user->user_type]),
            'user_id' => $this->user->id,
            'user_type' => $this->user->user_type,
            'redirect_admin' => ($this->user->user_type == 'seeker') ? 'users' : 'companies'
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message_en' => trans('notification.admin.user_created_admin_en',
                ['user_email' => $this->user->email, 'user_type' => $this->user->user_type]),
            'message_ja' => trans('notification.admin.user_created_admin_ja',
                ['user_email' => $this->user->email, 'user_type' => $this->user->user_type]),
            'user_id' => $this->user->id,
            'user_type' => $this->user->user_type,
            'redirect_admin' => ($this->user->user_type == 'seeker') ? 'users' : 'companies'
        ]);
    }
}
