<?php

namespace App\Notifications;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendMatchedJobToSeeker extends Notification implements ShouldQueue
{
    use Queueable;

    private $jobs = [];
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($jobs, $language)
    {
        foreach ($jobs as $job) {
            $temp = [];
            $job_db = (Job::where('id', $job)->first())->translateLocales()->toArray();
            //print_r($job_db);die;
            $temp['title'] = $job_db['title'][$language];
            $temp['slug'] = $job_db['slug'];
            $this->jobs[] = $temp;
        }
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->view(
            'emails.' . $this->language . '.seeker.matched_jobs',
            [
                'user' => $notifiable,
                'jobs' => $this->jobs
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
