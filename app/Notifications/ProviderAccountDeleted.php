<?php

namespace App\Notifications;

use App\Models\Provider;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ProviderAccountDeleted extends Notification implements ShouldQueue
{
    use Queueable;

    private $provider;
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Provider $provider, $language)
    {
        $this->provider = $provider;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->view(
            'emails.' . $this->language . '.provider.account_deleted',
            [
                'user' => $notifiable,
                'provider' => $this->provider,
            ]
        )->subject(trans('messages.mail.provider_account_deleted'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $this->provider = $this->provider->translateLocales();
        return [
            'message_en' => trans('notification.admin.provider_deleted_en',
                ['company_name' => $this->provider->company_name['en']]),
            'message_ja' => trans('notification.admin.provider_deleted_ja',
                ['company_name' => $this->provider->company_name['ja']]),
            'redirect_admin' => 'companies/'
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        $this->job = $this->provider->translateLocales();
        return new BroadcastMessage([
            'message_en' => trans('notification.admin.provider_deleted_en',
                ['company_name' => $this->provider->company_name['en']]),
            'message_ja' => trans('notification.admin.provider_deleted_ja',
                ['company_name' => $this->provider->company_name['ja']]),
            'redirect_admin' => 'companies/'
        ]);
    }
}
