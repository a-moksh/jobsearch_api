<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserCreated extends Notification implements ShouldQueue
{
    use Queueable;

    private $user;
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $language)
    {
        //
        $this->user = $user;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        \App::setLocale($this->language);
        $email_view = 'emails.' . $this->language . '.seeker.register';
        if($notifiable->user_type == 'provider'){
            $email_view = 'emails.' . $this->language . '.provider.user.register';
        }
        return (new MailMessage)->view(
            $email_view, ['user' => $this->user, 'language' => $this->language]
        )->subject(trans('messages.mail.register_subject'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

        return [
            'message_en' => trans('notification.common.user_created_en', ['user_email' => $this->user->email]),
            'message_ja' => trans('notification.common.user_created_ja', ['user_email' => $this->user->email]),
            'user_id' => $this->user->id,
            'redirect_admin' => 'profile/' . $this->user->id,
            'redirect_seeker' => 'user/profile',
            'redirect_provider' => 'setting/' . (($this->user->user_type == 'provider') ? $this->user->provider->id : ''),
        ];
    }
}
