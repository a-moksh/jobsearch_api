<?php

namespace App\Notifications;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class JobCreatedNotifyAdmin extends Notification implements ShouldQueue
{
    use Queueable;

    private $job;
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Job $job, $language)
    {
        $this->job = $job;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->view(
            'emails.' . $this->language . '.admin.job_created',
            [
                'user' => $notifiable,
                'job' => $this->job,
                'language'=> $this->language
            ]
        )->subject(trans('messages.mail.job_created'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $this->job->translateLocales();
        return [
            'message_en' => trans('notification.admin.job_created_admin_en', ['job_title' => $this->job->title['en']]),
            'message_ja' => trans('notification.admin.job_created_admin_ja', ['job_title' => $this->job->title['ja']]),
            'slug' => $this->job->slug,
            'redirect_admin' => 'jobs/edit/' . $this->job->id
        ];
    }


    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        $this->job->translateLocales();
        return [
            'message_en' => trans('notification.admin.job_created_admin_en', ['job_title' => $this->job->title['en']]),
            'message_ja' => trans('notification.admin.job_created_admin_ja', ['job_title' => $this->job->title['ja']]),
            'slug' => $this->job->slug,
            'redirect_admin' => 'jobs/edit/' . $this->job->id
        ];
    }
}
