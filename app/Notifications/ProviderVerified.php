<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProviderVerified extends Notification implements ShouldQueue
{
    use Queueable;

    private $user;
    private $provider;
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $provider, $language)
    {
        $this->user = $user;
        $this->provider = $provider;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if(\Config::get('app.landing') == true)
            $view_path = 'emails.' . $this->language . '.provider.user.email_verified_landing';
        else
            $view_path = 'emails.' . $this->language . '.provider.user.email_verified';
        return (new MailMessage)->view(
            $view_path,
            [
                'user' => $notifiable,
                'provider' => $this->provider,
                'language' => $this->language
            ]
        )->subject(trans('messages.mail.provider_email_verified'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
