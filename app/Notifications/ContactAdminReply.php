<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactAdminReply extends Notification implements ShouldQueue
{
    use Queueable;
    private $mail_data;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($mail_data)
    {
        $this->mail_data = $mail_data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
    
        if($this->mail_data['type'] == 1){
            \App::setLocale($this->mail_data['language']);
            return (new MailMessage)->view(
                'emails.' . $this->mail_data['language'] . '.seeker.contact_replied',
                [
                    'data'=>$this->mail_data
                ]
            )->subject(trans('messages.mail.contact_replied'));
        }else{
            \App::setLocale('ja');
            return (new MailMessage)->view(
                'emails.ja.provider.contact_replied',
                [
                    'data'=>$this->mail_data
                ]
            )->subject(trans('messages.mail.contact_replied'));
        }
        
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
