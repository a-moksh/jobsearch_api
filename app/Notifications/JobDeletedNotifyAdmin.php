<?php

namespace App\Notifications;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class JobDeletedNotifyAdmin extends Notification implements ShouldQueue
{
    use Queueable;

    private $job;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

        return [
            'message_en' => trans('notification.admin.job_deleted_admin_en', ['job_title' => $this->job->title]),
            'message_ja' => trans('notification.admin.job_deleted_admin_ja', ['job_title' => $this->job->title]),
            'slug' => $this->job->slug,
            'redirect_admin' => ''
        ];
    }
}
