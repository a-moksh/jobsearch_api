<?php

namespace App\Notifications;

use App\Models\ScoutMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ScoutMailReplied extends Notification implements ShouldQueue
{
    use Queueable;

    private $scoutMail;
    private $language;
    private $job;
    private $provider;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ScoutMail $scoutMail, $job, $provider, $language)
    {
        $this->scoutMail = $scoutMail;
        $this->language = $language;
        $this->job = $job;
        $this->provider = $provider; 
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        \App::setLocale($this->language);
        return (new MailMessage)->view(
            'emails.' . $this->language . '.seeker.application_replied',
            [
                'user' => $notifiable,
                'scoutmail' => $this->scoutMail,
                'job'=> $this->job,
                'provider'=>$this->provider,
                'language'=> $this->language
            ]
        )->subject(trans('messages.mail.scout_replied'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message_en' => trans('notification.seeker.provider_replied_application_en',
                ['job_title' => $this->scoutMail->id]),
            'message_ja' => trans('notification.seeker.provider_replied_application_en',
                ['job_title' => $this->scoutMail->id]),
            'redirect_seeker' => 'user/messages/' . $this->scoutMail->id
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message_en' => trans('notification.seeker.provider_replied_application_en',
                ['job_title' => $this->scoutMail->id]),
            'message_ja' => trans('notification.seeker.provider_replied_application_en',
                ['job_title' => $this->scoutMail->id]),
            'redirect_seeker' => 'user/messages/' . $this->scoutMail->id
        ]);
    }
}
