<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProviderProfileUpdated extends Notification implements ShouldQueue
{
    use Queueable;

    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($language)
    {
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->view(
            'emails.' . $this->language . '.provider.user.profile_updated', ['user' => $notifiable,'language'=>$this->language]
        )->subject(trans('messages.mail.provider_profile_updated'));
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     */
    public function toArray($notifiable)
    {
        return [
            'message_en' => trans('notification.common.profile_update_en'),
            'message_ja' => trans('notification.common.profile_update_ja'),
            'user_id' => $notifiable->id,
            'redirect_provider' => 'profile/' . $notifiable->provider->id,
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message_en' => trans('notification.common.profile_update_en'),
            'message_ja' => trans('notification.common.profile_update_ja'),
            'user_id' => $notifiable->id,
            'redirect_provider' => 'profile/' . $notifiable->provider->id,
        ]);
    }
}
