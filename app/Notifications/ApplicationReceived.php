<?php

namespace App\Notifications;

use App\Models\Job;
use App\Models\SeekerJob;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ApplicationReceived extends Notification implements ShouldQueue
{
    use Queueable;

    private $job;
    private $user;
    private $seeker_job;
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Job $job, User $user, SeekerJob $seeker_job, $language)
    {
        $this->job = $job;
        $this->user = $user;
        $this->seeker_job = $seeker_job;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->view(
            'emails.' . $this->language . '.provider.application_received',
            [
                'job' => $this->job,
                'user' => $notifiable,
                'provider' => $notifiable->provider,
                'applicant' => $this->user,
                'seeker_job' => $this->seeker_job,
                'language' => $this->language,
            ]
        )->subject(trans('messages.mail.job_application_received'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

        return [
            'message_en' => trans('notification.provider.application_received_en',
                ['job_title' => $this->job->job_title, 'user_email' => $this->user->email]),
            'message_ja' => trans('notification.provider.application_received_ja', [
                'job_title' => $this->job->job_title,
                'user_email' => $this->user->email
            ]),
            'redirect_provider' => 'applications/' . $this->seeker_job->id,
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message_en' => trans('notification.provider.application_received_en',
                ['job_title' => $this->job->job_title, 'user_email' => $this->user->email]),
            'message_ja' => trans('notification.provider.application_received_ja', [
                'job_title' => $this->job->job_title,
                'user_email' => $this->user->email
            ]),
            'redirect_provider' => 'applications/' . $this->seeker_job->id,
        ]);
    }
}
