<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SeekerVerified extends Notification implements ShouldQueue
{
    use Queueable;
    private $user;
    private $seeker;
    private $language;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $seeker, $language)
    {
        $this->user = $user;
        $this->seeker = $seeker;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        \App::setLocale($this->language);
        return (new MailMessage)->view(
            'emails.' . $this->language . '.seeker.email_verified',
            [
                'user' => $notifiable,
                'seeker' => $this->seeker,
                'language' => $this->language
            ]
        )->subject(trans('messages.mail.seeker_email_verified'));;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
