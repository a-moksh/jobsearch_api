<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordChanged extends Notification implements ShouldQueue
{
    use Queueable;

    private $user;
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $language)
    {
        $this->user = $user;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        \App::setLocale($this->language);
        $email_view = 'emails.' . $this->language . '.user.password_changed';
        if($notifiable->user_type == 'provider'){
            $email_view = 'emails.' . $this->language . '.provider.user.password_changed';
        }
        if($notifiable->user_type == 'seeker'){
            $email_view = 'emails.' . $this->language . '.seeker.password_changed';
        }
        return (new MailMessage)->view(
            $email_view, ['user' => $this->user, 'language'=>$this->language]
        )->subject(trans('messages.mail.password_changed'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

        return [
            'message_en' => trans('notification.common.password_changed_en'),
            'message_ja' => trans('notification.common.password_changed_en'),
            'user_id' => $this->user->id,
            'redirect_admin' => 'profile/' . $this->user->id,
            'redirect_seeker' => 'user/profile',
            'redirect_provider' => 'setting/' . (($this->user->user_type == 'provider') ? $this->user->provider->id : ''),
        ];
    }


    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message_en' => trans('notification.common.password_changed_en'),
            'message_ja' => trans('notification.common.password_changed_ja'),
            'user_id' => $this->user->id,
            'redirect_admin' => 'profile/' . $this->user->id,
            'redirect_seeker' => 'user/profile',
            'redirect_provider' => 'setting/' . (($this->user->user_type == 'provider') ? $this->user->provider->id : ''),
        ]);
    }
}
