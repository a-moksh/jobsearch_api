<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PaidAccountDeclined extends Notification implements ShouldQueue
{
    use Queueable;

    private $language;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($language)
    {
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->view(
            'emails.' . $this->language . '.provider.paid_declined',
            [
                'user' => $notifiable,
                'language' => $this->language,
            ]
        )->subject(trans('messages.mail.paid_verification_declined'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message_en' => trans('notification.provider.paid_account_declined_en'),
            'message_ja' => trans('notification.admin.paid_account_declined_ja'),
            'redirect_provider' => 'payment'
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'message_en' => trans('notification.provider.paid_account_declined_en'),
            'message_ja' => trans('notification.provider.paid_account_declined_ja'),
            'redirect_provider' => 'payment'
        ];
    }
}
