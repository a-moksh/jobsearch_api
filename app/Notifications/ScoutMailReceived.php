<?php

namespace App\Notifications;

use App\Models\ScoutMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ScoutMailReceived extends Notification implements ShouldQueue
{
    use Queueable;

    private $scoutMail;
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ScoutMail $scoutMail, $language)
    {
        $this->scoutMail = $scoutMail;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        \App::setLocale($this->language);
        return (new MailMessage)->view(
            'emails.' . $this->language . '.seeker.scoutmail_received',
            [
                'user' => $notifiable,
                'scoutmail' => $this->scoutMail,
                'language' => $this->language,
            ]
        )->subject(trans('messages.mail.scout_received'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message_en' => trans('notification.seeker.provider_sent_scout_en', ['job_title' => $this->scoutMail->id]),
            'message_ja' => trans('notification.seeker.provider_sent_scout_ja', ['job_title' => $this->scoutMail->id]),
            'redirect_seeker' => 'user/messages/' . $this->scoutMail->id
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message_en' => trans('notification.seeker.provider_sent_scout_en', ['job_title' => $this->scoutMail->id]),
            'message_ja' => trans('notification.seeker.provider_sent_scout_ja', ['job_title' => $this->scoutMail->id]),
            'redirect_seeker' => 'user/messages/' . $this->scoutMail->id
        ]);
    }
}
