<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class JobCreated extends Notification implements ShouldQueue
{
    use Queueable;

    private $job;
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($job, $language)
    {
        $this->job = $job;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->view(
            'emails.' . $this->language . '.job.job_created',
            [
                'job' => $this->job,
                'provider' => $notifiable
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $this->job = $this->job->translateLocales();
        return [
            'message_en' => trans('notification.provider.job_created_en', ['job_title' => $this->job->title['en']]),
            'message_ja' => trans('notification.provider.job_created_ja', ['job_title' => $this->job->title['ja']]),
            'job_id' => $this->job->id,
            'redirect_provider' => 'job/' . $this->job->id,
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message_en' => 'message en'
        ]);
    }
}
