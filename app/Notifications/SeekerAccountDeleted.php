<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SeekerAccountDeleted extends Notification implements ShouldQueue
{
    use Queueable;

    private $user;
    private $language;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $language)
    {
        $this->user = $user;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        \App::setLocale($this->language);
        return (new MailMessage)->view(
            'emails.' . $this->language . '.seeker.account_deleted',
            [
                'user' => $notifiable,
                'language'=> $this->language
            ]
        )->subject(trans('messages.mail.seeker_account_deleted'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
