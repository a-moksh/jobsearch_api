<?php

namespace App\Notifications;

use App\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class JobApproved extends Notification implements ShouldQueue
{
    use Queueable;

    private $job;
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Job $job, $language)
    {
        $this->job = $job;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->view(
            'emails.' . $this->language . '.provider.job_approved',
            [
                'user' => $notifiable,
                'job' => $this->job,
                'language' => $this->language,
            ]
        )->subject(trans('messages.mail.job_approved'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $this->job = $this->job->translateLocales();
        return [
            'message_en' => trans('notification.provider.job_approved_en', ['job_title' => $this->job->title['en']]),
            'message_ja' => trans('notification.provider.job_approved_ja', ['job_title' => $this->job->title['ja']]),
            'redirect_provider' => 'job/' . $this->job->id,
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        $this->job = $this->job->translateLocales();
        return new BroadcastMessage([
            'message_en' => trans('notification.provider.job_approved_en', ['job_title' => $this->job->title['en']]),
            'message_ja' => trans('notification.provider.job_approved_ja', ['job_title' => $this->job->title['ja']]),
            'redirect_provider' => 'job/' . $this->job->id,
        ]);
    }
}
