<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPassword extends Notification implements ShouldQueue
{
    use Queueable;

    public $token;
    public $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $language)
    {
        $this->token = $token;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        \App::setLocale($this->language);
        if ($notifiable->user_type == 'provider') {
            $verifyPath = config('app.provider_url');
        } elseif ($notifiable->user_type == 'admin') {
            $verifyPath = config('app.admin_url');
        } else {
            $verifyPath = config('app.seeker_url');
        }
        $email_view = 'emails.' . $this->language . '.user.password_reset';
        if($notifiable->user_type == 'provider'){
            $email_view = 'emails.' . $this->language . '.provider.user.password_reset';
        }
        if($notifiable->user_type == 'seeker'){
            $email_view = 'emails.' . $this->language . '.seeker.password_reset';
        }
        return (new MailMessage)->view(
            $email_view, [
                'user' => $notifiable,
                'link' => $verifyPath . '/' . $this->language . '/forgot-password/' . $this->token,
                'language' => $this->language,
            ]
        )->subject(trans('messages.mail.password_reset'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
