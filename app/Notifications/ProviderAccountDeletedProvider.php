<?php

namespace App\Notifications;

use App\Models\Provider;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProviderAccountDeletedProvider extends Notification implements ShouldQueue
{
    use Queueable;

    private $provider;
    private $language;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Provider $provider, $language)
    {
        $this->provider = $provider;
        $this->language = $language;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $email_view = 'emails.' . $this->language . '.provider.user.account_deleted';

        return (new MailMessage)->view(
            $email_view, ['user' => $notifiable, 'language'=>$this->language]
        )->subject(trans('messages.mail.provider_account_deleted'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
