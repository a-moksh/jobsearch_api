<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class ValidateJobIdsApplication implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $seeker_id = DB::table('seekers')->where('user_id', auth()->user()->id)->first()->id;
        $valid = false;
        if (is_array($value)) {
            if (empty($value)) {
                $valid = false;
            }
            foreach ($value as $job_id) {
                $count = DB::table('seeker_jobs')->where('job_id', $job_id)->where('seeker_id', $seeker_id)->count();
                if ($count > 0) {
                    $valid = false;
                    break;
                } else {
                    $valid = true;
                }
            }
        } else {
            $count = DB::table('seeker_jobs')->where('job_id', $value)->where('seeker_id', $seeker_id)->count();
            if ($count > 0) {
                $valid = false;
            } else {
                $valid = true;
            }
        }
        return $valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please apply only for those jobs which is not applied';
    }
}
