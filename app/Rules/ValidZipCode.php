<?php

namespace App\Rules;

use App\Models\ZipCode;
use Illuminate\Contracts\Validation\Rule;

class ValidZipCode implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (ZipCode::where('zip_code', $value)->first()) {
            return true;
        }
        /*
          $handle = fopen(base_path('master').'/ZIP_CODE_DATA.csv', "r");
          $valid = false;
          while ($csvLine = fgetcsv($handle, 1000, ",")) {
              if($csvLine[0] == $value)
                  $valid = true;
          }
          return $valid;
        */
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.invalid');
    }
}
