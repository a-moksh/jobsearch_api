<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class ValidJobImage implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $valid = false;

        if (is_array($value)) {
            if (empty($value)) {
                $valid = true;
            }
            foreach ($value as $image) {
                if (Storage::disk('s3_private')->exists($image)) {
                    if (Storage::disk('s3_private')->size($image) <= 5242880) {
                        $valid = true;
                    }
                }
            }
        } else {
            if (Storage::disk('s3_private')->exists($value)) {
                if (Storage::disk('s3_private')->size($value) <= 5242880) {
                    $valid = true;
                }
            }
        }
        return $valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.invalid');
    }
}
