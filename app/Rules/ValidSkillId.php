<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class ValidSkillId implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($skills)
    {
        $this->skills = $skills;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $index = explode('.', $attribute);
        if (count($index) == 3) {
            $index = $index[1];
        } else {
            $index = $index[2];
        }
        $skill_type = $this->skills[$index]['skill_type'];
        $result = DB::table('attributes')
            ->join('attribute_groups', 'attributes.group_id', '=', 'attribute_groups.id')
            ->where('attribute_groups.name', $skill_type)
            ->where('attributes.value', $value)
            ->first();
        return !is_null($result);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.invalid');
    }
}
