<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LoginEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $broadcastQueue = 'echo';

    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
//    public function broadcastOn()
//    {
//        return new PresenceChannel('login');
//    }

//    public function broadcastWith()
//    {
//        $data = array_only(UserResource::make($this->user)->toArray(request()),[
//            'id',
//            'email'
//        ]);
//        $data['ip'] = request()->ip();
//        return $data;
//    }
}
