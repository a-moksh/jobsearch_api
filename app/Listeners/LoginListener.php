<?php

namespace App\Listeners;

use App\Events\LoginEvent;

class LoginListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoginEvent $event
     * @return void
     */
    public function handle(LoginEvent $event)
    {
        // need to handle ip tracking to the system;
        //echo "login";
        //print_r($event->user);
    }
}
