<?php

namespace App\Listeners;

use App\Events\RegisterEvent;
use App\Mail\UserRegistered;
use Illuminate\Support\Facades\Mail;

class RegisterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterEvent $event
     * @return void
     */
    public function handle(RegisterEvent $event)
    {
        Mail::to($event->user->email)->send(new UserRegistered($event->user));
    }
}
