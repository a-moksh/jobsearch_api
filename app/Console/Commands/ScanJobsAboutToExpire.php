<?php

namespace App\Console\Commands;

use App\Models\Job;
use App\Models\Provider;
use App\Models\User;
use App\Notifications\JobAboutToExpire;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;

class ScanJobsAboutToExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:toExpire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'finds the jobs that are about to expire';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            //check if there are jobs that are about to expire within 2 weeks
            $expire_date = Carbon::now()->subDays(76);
            //echo $expire_date;die;
            $job_to_expire = Job::where(
                'admin_status', 0
            )->where(
                'published_at', '<', $expire_date
            )->where(
                'mail_notified_expire', 0
            )->limit(50)->get()->toArray();
            //echo count($job_to_expire);die;
            if (count($job_to_expire) > 0) {
                foreach ($job_to_expire as $jobs) {
                    //get provider email
                    $provider = Provider::find($jobs['provider_id']);
                    $user = User::find($provider->user_id);
                    if ($user) {
                        //send notification here and change jobs table status
                        $user->notify(new JobAboutToExpire($jobs, $provider));
                        Job::where('id', $jobs['id'])->update([
                            'mail_notified_expire' => 1
                        ]);
                    }
                }
            }
        } catch (Exception $e) {

        }
    }
}
