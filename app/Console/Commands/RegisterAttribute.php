<?php

namespace App\Console\Commands;

use App\Models\Attribute;
use App\Models\AttributeGroup;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RegisterAttribute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:attribute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add data to attribute table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Attribute::truncate();

        AttributeGroup::truncate();

        //
        // japan's prefecture name
        $this->prefecture();
        // country code and its calling number
        $this->countryCode();
        // others
        $this->other();
    }

    public function other()
    {
        $this->add('delete_able', 'language', [
            ['value' => '0', 'name' => 'no'],
            ['value' => '1', 'name' => 'yes'],
        ]);
        $this->add('reply_status', 'language', [
            ['value' => '0', 'name' => 'no'],
            ['value' => '1', 'name' => 'yes'],
        ]);
        $this->add('lang', 'language', [
            ['value' => 'ja', 'name' => 'ja'],
            ['value' => 'en', 'name' => 'en'],
        ]);
        $this->add('gender', 'gender', [
            ['value' => '1', 'name' => 'male'],
            ['value' => '2', 'name' => 'female'],
            ['value' => '3', 'name' => 'other'],
        ]);
        $this->add('employment_type', 'employment type', [
            ['value' => '0', 'name' => 'full_time'],
            ['value' => '1', 'name' => 'part_time'],
            ['value' => '2', 'name' => 'contract'],
            ['value' => '3', 'name' => 'temporary'],
            ['value' => '4', 'name' => 'internship'],
            ['value' => '100', 'name' => 'freelance'],

        ]);
        $this->add('visa_status', 'visa status', [
            ['value' => '24', 'name' => 'no_visa '],
            ['value' => '0', 'name' => 'designated_activities'],
            ['value' => '1', 'name' => 'spouse_or_child_of_japanese_national'],
            ['value' => '2', 'name' => 'spouse_or_child_of_permanent_resident'],
            ['value' => '3', 'name' => 'long_term_resident'],
            ['value' => '4', 'name' => 'permanent_resident'],
            ['value' => '5', 'name' => 'artist'],
            ['value' => '6', 'name' => 'entertainer'],
            ['value' => '7', 'name' => 'instructor'],
            ['value' => '8', 'name' => 'business_manager'],
            ['value' => '9', 'name' => 'journalist'],
            ['value' => '10', 'name' => 'legal_account_service'],
            ['value' => '11', 'name' => 'medical_service'],
            ['value' => '12', 'name' => 'professor '],
            ['value' => '13', 'name' => 'religious_activities'],
            ['value' => '14', 'name' => 'researcher'],
            ['value' => '15', 'name' => 'skilled_labour'],
            ['value' => '16', 'name' => 'Engineer_specialist_in_humanities_international_services'],
            ['value' => '17', 'name' => 'highly_skilled_foreign_professional'],
            ['value' => '18', 'name' => 'cultural_activist'],
            ['value' => '19', 'name' => 'student'],
            ['value' => '20', 'name' => 'training'],
            ['value' => '21', 'name' => 'general_visa_technical_intern_training'],
            ['value' => '22', 'name' => 'dependent'],
            ['value' => '23', 'name' => 'diplomat'],
            ['value' => '25', 'name' => 'religious_activities'],
            ['value' => '26', 'name' => 'official'],
            ['value' => '100', 'name' => 'other'],
        ]);

        $this->add('skill.programming', 'programming skill', [
            ['value' => '0', 'name' => 'PHP'],
            ['value' => '1', 'name' => 'javascript'],
            ['value' => '2', 'name' => 'service'],
            ['value' => '3', 'name' => 'C#'],
            ['value' => '4', 'name' => 'pythons'],
            ['value' => '5', 'name' => 'swift'],
            ['value' => '6', 'name' => 'java'],
            ['value' => '7', 'name' => 'ruby'],
            ['value' => '8', 'name' => 'c'],
            ['value' => '9', 'name' => 'c++'],
            ['value' => '10', 'name' => 'go'],
            ['value' => '11', 'name' => 'scala'],
            ['value' => '12', 'name' => 'unity'],
            ['value' => '13', 'name' => 'objective_c'],
            ['value' => '14', 'name' => 'visual_basic'],
            ['value' => '15', 'name' => 'delphi'],
            ['value' => '16', 'name' => 'cobol'],
            ['value' => '17', 'name' => 'action_script'],
            ['value' => '18', 'name' => 'ruby'],
            ['value' => '19', 'name' => 'erlang'],
            ['value' => '100', 'name' => 'perl'],
        ]);
        $this->add('skill.design', 'programming skill', [
            ['value' => '101', 'name' => 'illustrator'],
            ['value' => '102', 'name' => 'quarkxpress'],
            ['value' => '103', 'name' => 'dreamweaver'],
            ['value' => '104', 'name' => 'fireworks'],
            ['value' => '105', 'name' => 'flash'],
            ['value' => '106', 'name' => 'director'],
            ['value' => '107', 'name' => 'indesign'],
            ['value' => '108', 'name' => 'html_tag_stricking'],
            ['value' => '109', 'name' => 'CSS'],
            ['value' => '110', 'name' => 'HTML5'],
            ['value' => '111', 'name' => 'CSS3'],
            ['value' => '112', 'name' => 'CAD'],
            ['value' => '113', 'name' => '3DCG'],
            ['value' => '114', 'name' => 'adobe_xd'],
            ['value' => '115', 'name' => 'prott'],
            ['value' => '116', 'name' => 'photoshop'],
            ['value' => '200', 'name' => 'sketch'],
        ]);
        $this->add('skill.database', 'database skill', [
            ['value' => '201', 'name' => 'oracle'],
            ['value' => '202', 'name' => 'DB2'],
            ['value' => '203', 'name' => 'postgresql'],
            ['value' => '204', 'name' => 'mysql'],
        ]);
        $this->add('skill.middleware', 'middleware skill', [
            ['value' => '301', 'name' => 'apache'],
            ['value' => '302', 'name' => 'nginx'],
            ['value' => '303', 'name' => 'mamchached'],
            ['value' => '304', 'name' => 'redis'],
        ]);
        $this->add('skill.framework', 'framework skill', [
            ['value' => '401', 'name' => 'react'],
            ['value' => '402', 'name' => 'angular'],
            ['value' => '403', 'name' => 'jQuery'],
            ['value' => '404', 'name' => 'cakephp'],
            ['value' => '405', 'name' => 'laravel'],
            ['value' => '406', 'name' => 'rubyonrails'],
            ['value' => '407', 'name' => 'sintara'],
            ['value' => '408', 'name' => 'django'],
            ['value' => '409', 'name' => 'flask'],
            ['value' => '410', 'name' => 'vuejs'],
            ['value' => '411', 'name' => 'symphony'],
        ]);
        $this->add('skill.operating_system', 'operating system skill', [
            ['value' => '501', 'name' => 'mac'],
            ['value' => '502', 'name' => 'linux'],
            ['value' => '503', 'name' => 'bsd'],
            ['value' => '504', 'name' => 'solaris'],
            ['value' => '505', 'name' => 'windows'],
        ]);
        $this->add('skill.ms_office', 'microsoft office skill', [
            ['value' => '601', 'name' => 'word'],
            ['value' => '602', 'name' => 'excel'],
            ['value' => '603', 'name' => 'access'],
            ['value' => '604', 'name' => 'powerpoint'],
        ]);
        //different than skill type so starts from 0
        $this->add('skills.level', 'skill level', [
            ['value' => '0', 'name' => 'beginner'],
            ['value' => '1', 'name' => 'intermediate'],
            ['value' => '2', 'name' => 'advanced'],
            ['value' => '3', 'name' => 'expert'],
            ['value' => '4', 'name' => 'veteran'],
        ]);
        $this->add('certificates', 'different certificate', [
            ['value' => '0', 'name' => 'ccna'],
            ['value' => '1', 'name' => 'ccnp'],
            ['value' => '2', 'name' => 'mot'],
            ['value' => '3', 'name' => 'oracle'],
            ['value' => '100', 'name' => 'mos'],
        ]);
        $this->add('lang.level', 'language level', [
            ['value' => '4', 'name' => 'native'],
            ['value' => '1', 'name' => 'fluent'],
            ['value' => '2', 'name' => 'bossiness_level'],
            ['value' => '3', 'name' => 'conversation_level'],
            ['value' => '0', 'name' => 'basic'],
            ['value' => '100', 'name' => 'none'],
        ]);
        $this->add('lang.jlpt_level', 'japanese language proficiency test (JLPT) level', [
            ['value' => '0', 'name' => 'n1'],
            ['value' => '1', 'name' => 'n2'],
            ['value' => '2', 'name' => 'n3'],
            ['value' => '3', 'name' => 'n4'],
            ['value' => '100', 'name' => 'n5'],
        ]);
        $this->add('lang.toeic_score', 'language certificate level level', [
            ['value' => '0', 'name' => 'below_500'],
            ['value' => '1', 'name' => 'above_500'],
            ['value' => '2', 'name' => 'above_600'],
            ['value' => '3', 'name' => 'above_730'],
            ['value' => '100', 'name' => 'above_860'],
        ]);
        $this->add('job_translation', 'job translation', [
            ['value' => '2', 'name' => 'self_translation'],
            ['value' => '0', 'name' => 'google'],
            ['value' => '1', 'name' => 'professional'],
        ]);
        $this->add('boolean', 'boolean yes no value', [
            ['value' => '0', 'name' => 'No'],
            ['value' => '1', 'name' => 'Yes'],
        ]);
        $this->add('salary_type', 'salary type', [
            ['value' => '0', 'name' => 'hourly'],
            ['value' => '1', 'name' => 'daily'],
            ['value' => '2', 'name' => 'monthly'],
            ['value' => '3', 'name' => 'yearly'],
        ]);
        $this->add('holiday', 'holiday', [
            ['value' => '0', 'name' => 'weekly'],
            ['value' => '1', 'name' => 'shift'],
        ]);
        $this->add('lang_option', 'language options', [
            ['value' => '0', 'name' => 'english'],
            ['value' => '1', 'name' => 'japanese'],
            ['value' => '2', 'name' => 'chinese_mandarin'],
            ['value' => '3', 'name' => 'afrikaans'],
            ['value' => '4', 'name' => 'albanian'],
            ['value' => '5', 'name' => 'amharic'],
            ['value' => '6', 'name' => 'arabic_standard'],
            ['value' => '7', 'name' => 'arabic_algerian'],
            ['value' => '178', 'name' => 'arabic_tunisian'],
            ['value' => '8', 'name' => 'arabic_egyptian'],
            ['value' => '9', 'name' => 'arabic_mesopotamian'],
            ['value' => '10', 'name' => 'arabic_moroccan'],
            ['value' => '11', 'name' => 'arabic_najdi'],
            ['value' => '12', 'name' => 'arabic_north_levantine'],
            ['value' => '13', 'name' => 'arabic_saidi'],
            ['value' => '14', 'name' => 'arabic_sanaani'],
            ['value' => '14', 'name' => 'arabic_sudanese'],
            ['value' => '16', 'name' => 'aragonese'],
            ['value' => '17', 'name' => 'armenian'],
            ['value' => '18', 'name' => 'assamese'],
            ['value' => '19', 'name' => 'awadhi'],
            ['value' => '20', 'name' => 'azerbaijani_north'],
            ['value' => '21', 'name' => 'azerbaijani_south'],
            ['value' => '22', 'name' => 'bashkir'],
            ['value' => '23', 'name' => 'basque'],
            ['value' => '24', 'name' => 'belarusian'],
            ['value' => '26', 'name' => 'bengali'],
            ['value' => '27', 'name' => 'bhojpuri'],
            ['value' => '28', 'name' => 'bislama'],
            ['value' => '29', 'name' => 'bosnian'],
            ['value' => '30', 'name' => 'bulgarian'],
            ['value' => '31', 'name' => 'burmese'],
            ['value' => '32', 'name' => 'catalan'],
            ['value' => '33', 'name' => 'cebuano'],
            ['value' => '34', 'name' => 'chamorro'],
            ['value' => '35', 'name' => 'chechen'],
            ['value' => '36', 'name' => 'chewa_nyanja'],
            ['value' => '37', 'name' => 'chhattisgarhi'],
            ['value' => '38', 'name' => 'chinese_cantonese'],
            ['value' => '39', 'name' => 'chinese_gan'],
            ['value' => '40', 'name' => 'chinese_hakka'],
            ['value' => '41', 'name' => 'chinese_jinyu'],
            ['value' => '42', 'name' => 'chinese_min_bei'],
            ['value' => '43', 'name' => 'chinese_min_nan'],
            ['value' => '44', 'name' => 'chinese_wu'],
            ['value' => '45', 'name' => 'chinese_xiang'],
            ['value' => '46', 'name' => 'chittagonian'],
            ['value' => '47', 'name' => 'chuvash'],
            ['value' => '48', 'name' => 'cree'],
            ['value' => '49', 'name' => 'croatian'],
            ['value' => '50', 'name' => 'czech'],
            ['value' => '51', 'name' => 'danish'],
            ['value' => '52', 'name' => 'deccani'],
            ['value' => '53', 'name' => 'divehi'],
            ['value' => '54', 'name' => 'dutch'],
            ['value' => '55', 'name' => 'dzongkha'],
            ['value' => '56', 'name' => 'estonian'],
            ['value' => '57', 'name' => 'faroese'],
            ['value' => '58', 'name' => 'farsi_western'],
            ['value' => '59', 'name' => 'fijian'],
            ['value' => '60', 'name' => 'finnish'],
            ['value' => '61', 'name' => 'french'],
            ['value' => '62', 'name' => 'frisian'],
            ['value' => '63', 'name' => 'fulfulde_nigerian'],
            ['value' => '64', 'name' => 'gallegan'],
            ['value' => '65', 'name' => 'georgian'],
            ['value' => '66', 'name' => 'german'],
            ['value' => '67', 'name' => 'greek'],
            ['value' => '68', 'name' => 'greenlandic'],
            ['value' => '69', 'name' => 'guarani'],
            ['value' => '70', 'name' => 'gujarati'],
            ['value' => '71', 'name' => 'haitian_creole_french'],
            ['value' => '72', 'name' => 'haryanvi'],
            ['value' => '72', 'name' => 'hausa'],
            ['value' => '74', 'name' => 'hebrew'],
            ['value' => '75', 'name' => 'hindi'],
            ['value' => '76', 'name' => 'hiri_motu'],
            ['value' => '77', 'name' => 'hungarian'],
            ['value' => '78', 'name' => 'icelandic'],
            ['value' => '79', 'name' => 'igbo'],
            ['value' => '80', 'name' => 'ilocano'],
            ['value' => '81', 'name' => 'indonesian'],
            ['value' => '82', 'name' => 'inuktitut'],
            ['value' => '83', 'name' => 'irish'],
            ['value' => '84', 'name' => 'italian'],
            ['value' => '85', 'name' => 'javanese'],
            ['value' => '86', 'name' => 'kannada'],
            ['value' => '87', 'name' => 'kashmiri'],
            ['value' => '88', 'name' => 'kazakh'],
            ['value' => '89', 'name' => 'khmer'],
            ['value' => '90', 'name' => 'kinyarwanda'],
            ['value' => '91', 'name' => 'kirundi_rundi'],
            ['value' => '92', 'name' => 'komi'],
            ['value' => '93', 'name' => 'kongo'],
            ['value' => '94', 'name' => 'korean'],
            ['value' => '95', 'name' => 'kurdish'],
            ['value' => '96', 'name' => 'kyrgyz'],
            ['value' => '97', 'name' => 'lao'],
            ['value' => '98', 'name' => 'latvian'],
            ['value' => '99', 'name' => 'limburgish'],
            ['value' => '100', 'name' => 'lingala'],
            ['value' => '101', 'name' => 'lithuanian'],
            ['value' => '102', 'name' => 'lombard'],
            ['value' => '103', 'name' => 'luxembourgish'],
            ['value' => '104', 'name' => 'macedonian'],
            ['value' => '105', 'name' => 'madurese'],
            ['value' => '106', 'name' => 'magahi'],
            ['value' => '107', 'name' => 'maithili'],
            ['value' => '108', 'name' => 'malagasy'],
            ['value' => '109', 'name' => 'malay'],
            ['value' => '110', 'name' => 'malayalam'],
            ['value' => '111', 'name' => 'maltese'],
            ['value' => '112', 'name' => 'manx'],
            ['value' => '113', 'name' => 'maori'],
            ['value' => '114', 'name' => 'marathi'],
            ['value' => '115', 'name' => 'marshallese'],
            ['value' => '116', 'name' => 'marwari'],
            ['value' => '117', 'name' => 'mongolian'],
            ['value' => '118', 'name' => 'nepali'],
            ['value' => '119', 'name' => 'norwegian'],
            ['value' => '120', 'name' => 'occitan'],
            ['value' => '121', 'name' => 'oriya'],
            ['value' => '122', 'name' => 'oromo_west-central'],
            ['value' => '123', 'name' => 'ossetian'],
            ['value' => '124', 'name' => 'panjabi_eastern'],
            ['value' => '126', 'name' => 'panjabi_western'],
            ['value' => '127', 'name' => 'pashto_northern'],
            ['value' => '128', 'name' => 'pashto_southern'],
            ['value' => '129', 'name' => 'persian'],
            ['value' => '130', 'name' => 'polish'],
            ['value' => '131', 'name' => 'portuguese'],
            ['value' => '132', 'name' => 'quechua'],
            ['value' => '133', 'name' => 'romanian'],
            ['value' => '134', 'name' => 'russian'],
            ['value' => '135', 'name' => 'samoan'],
            ['value' => '136', 'name' => 'sango'],
            ['value' => '137', 'name' => 'saraiki'],
            ['value' => '138', 'name' => 'sardinian'],
            ['value' => '139', 'name' => 'scottish_gaelic'],
            ['value' => '140', 'name' => 'serbian'],
            ['value' => '141', 'name' => 'sindhi'],
            ['value' => '142', 'name' => 'sinhala'],
            ['value' => '143', 'name' => 'slovak'],
            ['value' => '144', 'name' => 'slovenian'],
            ['value' => '145', 'name' => 'somali'],
            ['value' => '146', 'name' => 'south_ndebele'],
            ['value' => '147', 'name' => 'southern_sotho'],
            ['value' => '148', 'name' => 'spanish'],
            ['value' => '149', 'name' => 'sunda'],
            ['value' => '150', 'name' => 'swahili'],
            ['value' => '151', 'name' => 'swati'],
            ['value' => '152', 'name' => 'swedish'],
            ['value' => '153', 'name' => 'tagalog'],
            ['value' => '154', 'name' => 'tajik'],
            ['value' => '155', 'name' => 'tamil'],
            ['value' => '156', 'name' => 'tatar'],
            ['value' => '157', 'name' => 'telugu'],
            ['value' => '158', 'name' => 'thai'],
            ['value' => '159', 'name' => 'thai_northeastern'],
            ['value' => '160', 'name' => 'tibetan'],
            ['value' => '161', 'name' => 'tigrinya'],
            ['value' => '162', 'name' => 'tonga'],
            ['value' => '163', 'name' => 'tsonga'],
            ['value' => '164', 'name' => 'tswana'],
            ['value' => '165', 'name' => 'turkish'],
            ['value' => '166', 'name' => 'turkmen'],
            ['value' => '167', 'name' => 'ukrainian'],
            ['value' => '168', 'name' => 'urdu'],
            ['value' => '169', 'name' => 'uyghur'],
            ['value' => '170', 'name' => 'uzbek_northern'],
            ['value' => '171', 'name' => 'venda'],
            ['value' => '172', 'name' => 'vietnamese'],
            ['value' => '173', 'name' => 'welsh'],
            ['value' => '174', 'name' => 'xhosa'],
            ['value' => '175', 'name' => 'yiddish'],
            ['value' => '176', 'name' => 'yoruba'],
            ['value' => '177', 'name' => 'zhuang'],
            ['value' => '178', 'name' => 'zulu'],

        ]);
        //language option iwthout japanese and english
        $this->add('lang_option_new', 'language options', [
            ['value' => '2', 'name' => 'chinese_mandarin'],
            ['value' => '3', 'name' => 'afrikaans'],
            ['value' => '4', 'name' => 'albanian'],
            ['value' => '5', 'name' => 'amharic'],
            ['value' => '6', 'name' => 'arabic_standard'],
            ['value' => '7', 'name' => 'arabic_algerian'],
            ['value' => '178', 'name' => 'arabic_tunisian'],
            ['value' => '8', 'name' => 'arabic_egyptian'],
            ['value' => '9', 'name' => 'arabic_mesopotamian'],
            ['value' => '10', 'name' => 'arabic_moroccan'],
            ['value' => '11', 'name' => 'arabic_najdi'],
            ['value' => '12', 'name' => 'arabic_north_levantine'],
            ['value' => '13', 'name' => 'arabic_saidi'],
            ['value' => '14', 'name' => 'arabic_sanaani'],
            ['value' => '14', 'name' => 'arabic_sudanese'],
            ['value' => '16', 'name' => 'aragonese'],
            ['value' => '17', 'name' => 'armenian'],
            ['value' => '18', 'name' => 'assamese'],
            ['value' => '19', 'name' => 'awadhi'],
            ['value' => '20', 'name' => 'azerbaijani_north'],
            ['value' => '21', 'name' => 'azerbaijani_south'],
            ['value' => '22', 'name' => 'bashkir'],
            ['value' => '23', 'name' => 'basque'],
            ['value' => '24', 'name' => 'belarusian'],
            ['value' => '26', 'name' => 'bengali'],
            ['value' => '27', 'name' => 'bhojpuri'],
            ['value' => '28', 'name' => 'bislama'],
            ['value' => '29', 'name' => 'bosnian'],
            ['value' => '30', 'name' => 'bulgarian'],
            ['value' => '31', 'name' => 'burmese'],
            ['value' => '32', 'name' => 'catalan'],
            ['value' => '33', 'name' => 'cebuano'],
            ['value' => '34', 'name' => 'chamorro'],
            ['value' => '35', 'name' => 'chechen'],
            ['value' => '36', 'name' => 'chewa_nyanja'],
            ['value' => '37', 'name' => 'chhattisgarhi'],
            ['value' => '38', 'name' => 'chinese_cantonese'],
            ['value' => '39', 'name' => 'chinese_gan'],
            ['value' => '40', 'name' => 'chinese_hakka'],
            ['value' => '41', 'name' => 'chinese_jinyu'],
            ['value' => '42', 'name' => 'chinese_min_bei'],
            ['value' => '43', 'name' => 'chinese_min_nan'],
            ['value' => '44', 'name' => 'chinese_wu'],
            ['value' => '45', 'name' => 'chinese_xiang'],
            ['value' => '46', 'name' => 'chittagonian'],
            ['value' => '47', 'name' => 'chuvash'],
            ['value' => '48', 'name' => 'cree'],
            ['value' => '49', 'name' => 'croatian'],
            ['value' => '50', 'name' => 'czech'],
            ['value' => '51', 'name' => 'danish'],
            ['value' => '52', 'name' => 'deccani'],
            ['value' => '53', 'name' => 'divehi'],
            ['value' => '54', 'name' => 'dutch'],
            ['value' => '55', 'name' => 'dzongkha'],
            ['value' => '56', 'name' => 'estonian'],
            ['value' => '57', 'name' => 'faroese'],
            ['value' => '58', 'name' => 'farsi_western'],
            ['value' => '59', 'name' => 'fijian'],
            ['value' => '60', 'name' => 'finnish'],
            ['value' => '61', 'name' => 'french'],
            ['value' => '62', 'name' => 'frisian'],
            ['value' => '63', 'name' => 'fulfulde_nigerian'],
            ['value' => '64', 'name' => 'gallegan'],
            ['value' => '65', 'name' => 'georgian'],
            ['value' => '66', 'name' => 'german'],
            ['value' => '67', 'name' => 'greek'],
            ['value' => '68', 'name' => 'greenlandic'],
            ['value' => '69', 'name' => 'guarani'],
            ['value' => '70', 'name' => 'gujarati'],
            ['value' => '71', 'name' => 'haitian_creole_french'],
            ['value' => '72', 'name' => 'haryanvi'],
            ['value' => '72', 'name' => 'hausa'],
            ['value' => '74', 'name' => 'hebrew'],
            ['value' => '75', 'name' => 'hindi'],
            ['value' => '76', 'name' => 'hiri_motu'],
            ['value' => '77', 'name' => 'hungarian'],
            ['value' => '78', 'name' => 'icelandic'],
            ['value' => '79', 'name' => 'igbo'],
            ['value' => '80', 'name' => 'ilocano'],
            ['value' => '81', 'name' => 'indonesian'],
            ['value' => '82', 'name' => 'inuktitut'],
            ['value' => '83', 'name' => 'irish'],
            ['value' => '84', 'name' => 'italian'],
            ['value' => '85', 'name' => 'javanese'],
            ['value' => '86', 'name' => 'kannada'],
            ['value' => '87', 'name' => 'kashmiri'],
            ['value' => '88', 'name' => 'kazakh'],
            ['value' => '89', 'name' => 'khmer'],
            ['value' => '90', 'name' => 'kinyarwanda'],
            ['value' => '91', 'name' => 'kirundi_rundi'],
            ['value' => '92', 'name' => 'komi'],
            ['value' => '93', 'name' => 'kongo'],
            ['value' => '94', 'name' => 'korean'],
            ['value' => '95', 'name' => 'kurdish'],
            ['value' => '96', 'name' => 'kyrgyz'],
            ['value' => '97', 'name' => 'lao'],
            ['value' => '98', 'name' => 'latvian'],
            ['value' => '99', 'name' => 'limburgish'],
            ['value' => '100', 'name' => 'lingala'],
            ['value' => '101', 'name' => 'lithuanian'],
            ['value' => '102', 'name' => 'lombard'],
            ['value' => '103', 'name' => 'luxembourgish'],
            ['value' => '104', 'name' => 'macedonian'],
            ['value' => '105', 'name' => 'madurese'],
            ['value' => '106', 'name' => 'magahi'],
            ['value' => '107', 'name' => 'maithili'],
            ['value' => '108', 'name' => 'malagasy'],
            ['value' => '109', 'name' => 'malay'],
            ['value' => '110', 'name' => 'malayalam'],
            ['value' => '111', 'name' => 'maltese'],
            ['value' => '112', 'name' => 'manx'],
            ['value' => '113', 'name' => 'maori'],
            ['value' => '114', 'name' => 'marathi'],
            ['value' => '115', 'name' => 'marshallese'],
            ['value' => '116', 'name' => 'marwari'],
            ['value' => '117', 'name' => 'mongolian'],
            ['value' => '118', 'name' => 'nepali'],
            ['value' => '119', 'name' => 'norwegian'],
            ['value' => '120', 'name' => 'occitan'],
            ['value' => '121', 'name' => 'oriya'],
            ['value' => '122', 'name' => 'oromo_west-central'],
            ['value' => '123', 'name' => 'ossetian'],
            ['value' => '124', 'name' => 'panjabi_eastern'],
            ['value' => '126', 'name' => 'panjabi_western'],
            ['value' => '127', 'name' => 'pashto_northern'],
            ['value' => '128', 'name' => 'pashto_southern'],
            ['value' => '129', 'name' => 'persian'],
            ['value' => '130', 'name' => 'polish'],
            ['value' => '131', 'name' => 'portuguese'],
            ['value' => '132', 'name' => 'quechua'],
            ['value' => '133', 'name' => 'romanian'],
            ['value' => '134', 'name' => 'russian'],
            ['value' => '135', 'name' => 'samoan'],
            ['value' => '136', 'name' => 'sango'],
            ['value' => '137', 'name' => 'saraiki'],
            ['value' => '138', 'name' => 'sardinian'],
            ['value' => '139', 'name' => 'scottish_gaelic'],
            ['value' => '140', 'name' => 'serbian'],
            ['value' => '141', 'name' => 'sindhi'],
            ['value' => '142', 'name' => 'sinhala'],
            ['value' => '143', 'name' => 'slovak'],
            ['value' => '144', 'name' => 'slovenian'],
            ['value' => '145', 'name' => 'somali'],
            ['value' => '146', 'name' => 'south_ndebele'],
            ['value' => '147', 'name' => 'southern_sotho'],
            ['value' => '148', 'name' => 'spanish'],
            ['value' => '149', 'name' => 'sunda'],
            ['value' => '150', 'name' => 'swahili'],
            ['value' => '151', 'name' => 'swati'],
            ['value' => '152', 'name' => 'swedish'],
            ['value' => '153', 'name' => 'tagalog'],
            ['value' => '154', 'name' => 'tajik'],
            ['value' => '155', 'name' => 'tamil'],
            ['value' => '156', 'name' => 'tatar'],
            ['value' => '157', 'name' => 'telugu'],
            ['value' => '158', 'name' => 'thai'],
            ['value' => '159', 'name' => 'thai_northeastern'],
            ['value' => '160', 'name' => 'tibetan'],
            ['value' => '161', 'name' => 'tigrinya'],
            ['value' => '162', 'name' => 'tonga'],
            ['value' => '163', 'name' => 'tsonga'],
            ['value' => '164', 'name' => 'tswana'],
            ['value' => '165', 'name' => 'turkish'],
            ['value' => '166', 'name' => 'turkmen'],
            ['value' => '167', 'name' => 'ukrainian'],
            ['value' => '168', 'name' => 'urdu'],
            ['value' => '169', 'name' => 'uyghur'],
            ['value' => '170', 'name' => 'uzbek_northern'],
            ['value' => '171', 'name' => 'venda'],
            ['value' => '172', 'name' => 'vietnamese'],
            ['value' => '173', 'name' => 'welsh'],
            ['value' => '174', 'name' => 'xhosa'],
            ['value' => '175', 'name' => 'yiddish'],
            ['value' => '176', 'name' => 'yoruba'],
            ['value' => '177', 'name' => 'zhuang'],
            ['value' => '178', 'name' => 'zulu'],

        ]);
        $this->add('country', 'countries', [
            ['value' => '1', 'name' => 'Japan'],
            ['value' => '2', 'name' => 'Afghanistan'],
            ['value' => '3', 'name' => 'ÅlandIslands'],
            ['value' => '4', 'name' => 'Albania'],
            ['value' => '5', 'name' => 'Algeria'],
            ['value' => '6', 'name' => 'AmericanSamoa'],
            ['value' => '7', 'name' => 'AndorrA'],
            ['value' => '8', 'name' => 'Angola'],
            ['value' => '9', 'name' => 'Anguilla'],
            ['value' => '10', 'name' => 'Antarctica'],
            ['value' => '11', 'name' => 'AntiguaAndBarbuda'],
            ['value' => '12', 'name' => 'Argentina'],
            ['value' => '13', 'name' => 'Armenia'],
            ['value' => '14', 'name' => 'Aruba'],
            ['value' => '15', 'name' => 'Australia'],
            ['value' => '16', 'name' => 'Austria'],
            ['value' => '17', 'name' => 'Azerbaijan'],
            ['value' => '18', 'name' => 'Bahamas'],
            ['value' => '19', 'name' => 'Bahrain'],
            ['value' => '20', 'name' => 'Bangladesh'],
            ['value' => '21', 'name' => 'Barbados'],
            ['value' => '22', 'name' => 'Belarus'],
            ['value' => '23', 'name' => 'Belgium'],
            ['value' => '24', 'name' => 'Belize'],
            ['value' => '25', 'name' => 'Benin'],
            ['value' => '26', 'name' => 'Bermuda'],
            ['value' => '27', 'name' => 'Bhutan'],
            ['value' => '28', 'name' => 'Bolivia'],
            ['value' => '29', 'name' => 'BosniaAndHerzegovina'],
            ['value' => '30', 'name' => 'Botswana'],
            ['value' => '31', 'name' => 'BouvetIsland'],
            ['value' => '32', 'name' => 'Brazil'],
            ['value' => '33', 'name' => 'BritishIndianOceanTerritory'],
            ['value' => '34', 'name' => 'BruneiDarussalam'],
            ['value' => '35', 'name' => 'Bulgaria'],
            ['value' => '36', 'name' => 'BurkinaFaso'],
            ['value' => '37', 'name' => 'Burundi'],
            ['value' => '38', 'name' => 'Cambodia'],
            ['value' => '39', 'name' => 'Cameroon'],
            ['value' => '40', 'name' => 'Canada'],
            ['value' => '41', 'name' => 'CapeVerde'],
            ['value' => '42', 'name' => 'CaymanIslands'],
            ['value' => '43', 'name' => 'CentralAfricanRepublic'],
            ['value' => '44', 'name' => 'Chad'],
            ['value' => '45', 'name' => 'Chile'],
            ['value' => '46', 'name' => 'China'],
            ['value' => '47', 'name' => 'ChristmasIsland'],
            ['value' => '48', 'name' => 'CocosKeelingIslands'],
            ['value' => '49', 'name' => 'Colombia'],
            ['value' => '50', 'name' => 'Comoros'],
            ['value' => '51', 'name' => 'Congo'],
            ['value' => '52', 'name' => 'CongoTheDemocraticRepublic'],
            ['value' => '53', 'name' => 'CookIslands'],
            ['value' => '54', 'name' => 'CostaRica'],
            ['value' => '55', 'name' => 'CoteDIvoire'],
            ['value' => '56', 'name' => 'Croatia'],
            ['value' => '57', 'name' => 'Cuba'],
            ['value' => '58', 'name' => 'Cyprus'],
            ['value' => '59', 'name' => 'CzechRepublic'],
            ['value' => '60', 'name' => 'Denmark'],
            ['value' => '61', 'name' => 'Djibouti'],
            ['value' => '62', 'name' => 'Dominica'],
            ['value' => '63', 'name' => 'DominicanRepublic'],
            ['value' => '64', 'name' => 'Ecuador'],
            ['value' => '65', 'name' => 'Egypt'],
            ['value' => '66', 'name' => 'ElSalvador'],
            ['value' => '67', 'name' => 'EquatorialGuinea'],
            ['value' => '68', 'name' => 'Eritrea'],
            ['value' => '69', 'name' => 'Estonia'],
            ['value' => '70', 'name' => 'Ethiopia'],
            ['value' => '71', 'name' => 'FalklandIslandsMalvinas'],
            ['value' => '72', 'name' => 'FaroeIslands'],
            ['value' => '73', 'name' => 'Fiji'],
            ['value' => '74', 'name' => 'Finland'],
            ['value' => '75', 'name' => 'France'],
            ['value' => '76', 'name' => 'FrenchGuiana'],
            ['value' => '77', 'name' => 'FrenchPolynesia'],
            ['value' => '78', 'name' => 'FrenchSouthernTerritories'],
            ['value' => '79', 'name' => 'Gabon'],
            ['value' => '80', 'name' => 'Gambia'],
            ['value' => '81', 'name' => 'Georgia'],
            ['value' => '82', 'name' => 'Germany'],
            ['value' => '83', 'name' => 'Ghana'],
            ['value' => '84', 'name' => 'Gibraltar'],
            ['value' => '85', 'name' => 'Greece'],
            ['value' => '86', 'name' => 'Greenland'],
            ['value' => '87', 'name' => 'Grenada'],
            ['value' => '88', 'name' => 'Guadeloupe'],
            ['value' => '89', 'name' => 'Guam'],
            ['value' => '90', 'name' => 'Guatemala'],
            ['value' => '91', 'name' => 'Guernsey'],
            ['value' => '92', 'name' => 'Guinea'],
            ['value' => '93', 'name' => 'GuineaBissau'],
            ['value' => '94', 'name' => 'Guyana'],
            ['value' => '95', 'name' => 'Haiti'],
            ['value' => '96', 'name' => 'HeardIslandAndMcdonaldIslands'],
            ['value' => '97', 'name' => 'HolySeeVaticanCityState'],
            ['value' => '98', 'name' => 'Honduras'],
            ['value' => '99', 'name' => 'HongKong'],
            ['value' => '100', 'name' => 'Hungary'],
            ['value' => '101', 'name' => 'Iceland'],
            ['value' => '102', 'name' => 'India'],
            ['value' => '103', 'name' => 'Indonesia'],
            ['value' => '104', 'name' => 'Iran'],
            ['value' => '105', 'name' => 'Iraq'],
            ['value' => '106', 'name' => 'Ireland'],
            ['value' => '107', 'name' => 'IsleOfMan'],
            ['value' => '108', 'name' => 'Israel'],
            ['value' => '109', 'name' => 'Italy'],
            ['value' => '110', 'name' => 'Jamaica'],
            ['value' => '112', 'name' => 'Jersey'],
            ['value' => '113', 'name' => 'Jordan'],
            ['value' => '114', 'name' => 'Kazakhstan'],
            ['value' => '115', 'name' => 'Kenya'],
            ['value' => '116', 'name' => 'Kiribati'],
            ['value' => '117', 'name' => 'NorthKorea'],
            ['value' => '118', 'name' => 'SouthKorea'],
            ['value' => '119', 'name' => 'Kuwait'],
            ['value' => '120', 'name' => 'Kyrgyzstan'],
            ['value' => '121', 'name' => 'LaoPeoplesDemocraticRepublic'],
            ['value' => '122', 'name' => 'Latvia'],
            ['value' => '123', 'name' => 'Lebanon'],
            ['value' => '124', 'name' => 'Lesotho'],
            ['value' => '125', 'name' => 'Liberia'],
            ['value' => '126', 'name' => 'LibyanArabJamahiriya'],
            ['value' => '127', 'name' => 'Liechtenstein'],
            ['value' => '128', 'name' => 'Lithuania'],
            ['value' => '129', 'name' => 'Luxembourg'],
            ['value' => '130', 'name' => 'Macao'],
            ['value' => '131', 'name' => 'Macedonia'],
            ['value' => '132', 'name' => 'Madagascar'],
            ['value' => '133', 'name' => 'Malawi'],
            ['value' => '134', 'name' => 'Malaysia'],
            ['value' => '135', 'name' => 'Maldives'],
            ['value' => '136', 'name' => 'Mali'],
            ['value' => '137', 'name' => 'Malta'],
            ['value' => '138', 'name' => 'MarshallIslands'],
            ['value' => '139', 'name' => 'Martinique'],
            ['value' => '140', 'name' => 'Mauritania'],
            ['value' => '141', 'name' => 'Mauritius'],
            ['value' => '142', 'name' => 'Mayotte'],
            ['value' => '143', 'name' => 'Mexico'],
            ['value' => '144', 'name' => 'Micronesia'],
            ['value' => '145', 'name' => 'Moldova'],
            ['value' => '146', 'name' => 'Monaco'],
            ['value' => '147', 'name' => 'Mongolia'],
            ['value' => '148', 'name' => 'Montserrat'],
            ['value' => '149', 'name' => 'Morocco'],
            ['value' => '150', 'name' => 'Mozambique'],
            ['value' => '151', 'name' => 'Myanmar'],
            ['value' => '152', 'name' => 'Namibia'],
            ['value' => '153', 'name' => 'Nauru'],
            ['value' => '154', 'name' => 'Nepal'],
            ['value' => '155', 'name' => 'Netherlands'],
            ['value' => '156', 'name' => 'NetherlandsAntilles'],
            ['value' => '157', 'name' => 'NewCaledonia'],
            ['value' => '158', 'name' => 'NewZealand'],
            ['value' => '159', 'name' => 'Nicaragua'],
            ['value' => '160', 'name' => 'Niger'],
            ['value' => '161', 'name' => 'Nigeria'],
            ['value' => '162', 'name' => 'Niue'],
            ['value' => '163', 'name' => 'NorfolkIsland'],
            ['value' => '164', 'name' => 'NorthernMarianaIslands'],
            ['value' => '165', 'name' => 'Norway'],
            ['value' => '166', 'name' => 'Oman'],
            ['value' => '167', 'name' => 'Pakistan'],
            ['value' => '168', 'name' => 'Palau'],
            ['value' => '169', 'name' => 'PalestinianTerritoryOccupied'],
            ['value' => '170', 'name' => 'Panama'],
            ['value' => '171', 'name' => 'PapuaNewGuinea'],
            ['value' => '172', 'name' => 'Paraguay'],
            ['value' => '173', 'name' => 'Peru'],
            ['value' => '174', 'name' => 'Philippines'],
            ['value' => '175', 'name' => 'Pitcairn'],
            ['value' => '176', 'name' => 'Poland'],
            ['value' => '177', 'name' => 'Portugal'],
            ['value' => '178', 'name' => 'PuertoRico'],
            ['value' => '179', 'name' => 'Qatar'],
            ['value' => '180', 'name' => 'Reunion'],
            ['value' => '181', 'name' => 'Romania'],
            ['value' => '182', 'name' => 'RussianFederation'],
            ['value' => '183', 'name' => 'Rwanda'],
            ['value' => '184', 'name' => 'SaintHelena'],
            ['value' => '185', 'name' => 'SaintKittsAndNevis'],
            ['value' => '186', 'name' => 'SaintLucia'],
            ['value' => '187', 'name' => 'SaintPierreAndMiquelon'],
            ['value' => '188', 'name' => 'SaintVincentAndTheGrenadines'],
            ['value' => '189', 'name' => 'Samoa'],
            ['value' => '190', 'name' => 'SanMarino'],
            ['value' => '191', 'name' => 'SaoTomeAndPrincipe'],
            ['value' => '192', 'name' => 'SaudiArabia'],
            ['value' => '193', 'name' => 'Senegal'],
            ['value' => '194', 'name' => 'SerbiaAndMontenegro'],
            ['value' => '195', 'name' => 'Seychelles'],
            ['value' => '196', 'name' => 'SierraLeone'],
            ['value' => '197', 'name' => 'Singapore'],
            ['value' => '198', 'name' => 'Slovakia'],
            ['value' => '199', 'name' => 'Slovenia'],
            ['value' => '200', 'name' => 'SolomonIslands'],
            ['value' => '201', 'name' => 'Somalia'],
            ['value' => '202', 'name' => 'SouthAfrica'],
            ['value' => '203', 'name' => 'SouthGeorgiaAndTheSouthSandwichIslands'],
            ['value' => '204', 'name' => 'Spain'],
            ['value' => '205', 'name' => 'SriLanka'],
            ['value' => '206', 'name' => 'Sudan'],
            ['value' => '207', 'name' => 'Suriname'],
            ['value' => '208', 'name' => 'SvalbardAndJanMayen'],
            ['value' => '209', 'name' => 'Swaziland'],
            ['value' => '210', 'name' => 'Sweden'],
            ['value' => '211', 'name' => 'Switzerland'],
            ['value' => '212', 'name' => 'SyrianArabRepublic'],
            ['value' => '213', 'name' => 'TaiwanProvinceOfChina'],
            ['value' => '214', 'name' => 'Tajikistan'],
            ['value' => '215', 'name' => 'Tanzania'],
            ['value' => '216', 'name' => 'Thailand'],
            ['value' => '217', 'name' => 'TimorLeste'],
            ['value' => '218', 'name' => 'Togo'],
            ['value' => '219', 'name' => 'Tokelau'],
            ['value' => '220', 'name' => 'Tonga'],
            ['value' => '221', 'name' => 'TrinidadAndTobago'],
            ['value' => '222', 'name' => 'Tunisia'],
            ['value' => '223', 'name' => 'Turkey'],
            ['value' => '224', 'name' => 'Turkmenistan'],
            ['value' => '225', 'name' => 'TurksAndCaicosIslands'],
            ['value' => '226', 'name' => 'Tuvalu'],
            ['value' => '227', 'name' => 'Uganda'],
            ['value' => '228', 'name' => 'Ukraine'],
            ['value' => '229', 'name' => 'UnitedArabEmirates'],
            ['value' => '230', 'name' => 'UnitedKingdom'],
            ['value' => '231', 'name' => 'UnitedStates'],
            ['value' => '232', 'name' => 'UnitedStatesMinorOutlyingIslands'],
            ['value' => '233', 'name' => 'Uruguay'],
            ['value' => '234', 'name' => 'Uzbekistan'],
            ['value' => '235', 'name' => 'Vanuatu'],
            ['value' => '236', 'name' => 'Venezuela'],
            ['value' => '237', 'name' => 'VietNam'],
            ['value' => '238', 'name' => 'VirginIslandsBritish'],
            ['value' => '239', 'name' => 'VirginIslandsUS'],
            ['value' => '240', 'name' => 'WallisAndFutuna'],
            ['value' => '241', 'name' => 'WesternSahara'],
            ['value' => '242', 'name' => 'Yemen'],
            ['value' => '243', 'name' => 'Zambia'],
            ['value' => '244', 'name' => 'Zimbabwe'],
        ]);
        $this->add('job_status', 'status', [
            ['value' => '0', 'name' => 'draft'],
            ['value' => '1', 'name' => 'published'],
            ['value' => '2', 'name' => 'unpublished'],
            //['value' => '3', 'name' => 'unlisted'],
            // datavalue will be 3 and stored in job status, but not essential on attribute
        ]);
        $this->add('job_status_front', 'status', [
            ['value' => '0', 'name' => 'draft'],
            ['value' => '1', 'name' => 'published'],
            ['value' => '2', 'name' => 'unpublished'],
            //['value' => '3', 'name' => 'unlisted'],
            // datavalue will be 3 and stored in job status, but not essential on attribute
        ]);
        //        $this->add('create_job_status','status',[
        //            ['value' => '0', 'name' => 'draft'],
        //            ['value' => '1', 'name' => 'send_verify'],
        //            //['value' => '3', 'name' => 'unlisted'],
        //            // datavalue will be 3 and stored in job status, but not essential on attribute
        //        ]);
        //        $this->add('verify_job_status','status',[
        //            ['value' => '2', 'name' => 'published'],
        //            ['value' => '3', 'name' => 'unpublished'],
        //            //['value' => '3', 'name' => 'unlisted'],
        //            // datavalue will be 3 and stored in job status, but not essential on attribute
        //        ]);
        $this->add('admin_job_status', 'status', [
            ['value' => '0', 'name' => 'approved'],
            ['value' => '1', 'name' => 'pending'],
            ['value' => '2', 'name' => 'declined'],
            ['value' => '3', 'name' => 'updating'],
        ]);
        $this->add('degree', 'seekers education degree', [
            ['value' => '0', 'name' => 'primaryschoolonly'],
            ['value' => '1', 'name' => 'highschoolnodegree'],
            ['value' => '2', 'name' => 'vocatoinalschool'],
            ['value' => '3', 'name' => 'someuniversitycourse'],
            ['value' => '4', 'name' => 'universitydegree'],
        ]);
        $this->add('graduation_status', 'graduation status', [
            ['value' => '0', 'name' => 'running'],
            ['value' => '2', 'name' => 'graduationdatefixed'],
            ['value' => '1', 'name' => 'graduated'],

        ]);
        $this->add('station.distance', 'station distance', [
            ['value' => '0', 'name' => 'bus'],
            ['value' => '1', 'name' => 'walk'],
        ]);
        $this->add('walking.distance', 'walking distance', [
            ['value' => '0', 'name' => '10min'],
            ['value' => '1', 'name' => '20min'],
            ['value' => '2', 'name' => '30min'],
            ['value' => '3', 'name' => '40min'],
            ['value' => '4', 'name' => '50min'],
            ['value' => '5', 'name' => '1hrs'],
            ['value' => '6', 'name' => '1hrs+'],
        ]);
        $this->add('week.days', 'weekdays', [
            ['value' => '0', 'name' => 'monday'],
            ['value' => '1', 'name' => 'tuesday'],
            ['value' => '2', 'name' => 'wednesday'],
            ['value' => '3', 'name' => 'thursday'],
            ['value' => '4', 'name' => 'friday'],
            ['value' => '5', 'name' => 'saturday'],
            ['value' => '6', 'name' => 'sunday'],
        ]);
        $this->add('business_content', 'companies business content', [
            ['value' => '0', 'name' => 'agriculture_forestry'],
            ['value' => '1', 'name' => 'fishery'],
            ['value' => '2', 'name' => 'mining_quarrying_gravel'],
            ['value' => '3', 'name' => 'construction_industry'],
            ['value' => '4', 'name' => 'manufacturing_industry'],
            ['value' => '5', 'name' => 'electricity_gas_heat_water_supply'],
            ['value' => '6', 'name' => 'information_communication'],
            ['value' => '7', 'name' => 'transportation_postal_service'],
            ['value' => '8', 'name' => 'wholesale_retail'],
            ['value' => '9', 'name' => 'financial_insurance_industry'],
            ['value' => '10', 'name' => 'real_estate_goods_rental_business'],
            ['value' => '11', 'name' => 'academic_research_industry'],
            ['value' => '12', 'name' => 'accommodation_food_service'],
            ['value' => '13', 'name' => 'lifestyle_related_service'],
            ['value' => '14', 'name' => 'education_learning_support'],
            ['value' => '15', 'name' => 'medical_care'],
            ['value' => '16', 'name' => 'composite_service_business'],
            ['value' => '17', 'name' => 'service_industry_not_classified'],
            ['value' => '18', 'name' => 'public_affairs_excluding'],
            ['value' => '19', 'name' => 'unclassifiable_industry'],

        ]);
        $this->add('agriculture_forestry', 'companies business content, agriculture forestry', [
            ['value' => '0', 'name' => 'agriculture', 'parent_group' => 'business_content', 'parent_value' => 0],
            ['value' => '1', 'name' => 'forestry', 'parent_group' => 'business_content', 'parent_value' => 0],
        ]);
        $this->add('fishery', 'companies business content, fishery', [
            [
                'value' => '0',
                'name' => 'fishery_excluding_aquaculture_industry',
                'parent_group' => 'business_content',
                'parent_value' => 1
            ],
            [
                'value' => '1',
                'name' => 'aqua_culture_industry',
                'parent_group' => 'business_content',
                'parent_value' => 1
            ],
        ]);
        $this->add('mining_quarrying_gravel', 'companies business content, mining_quarrying_gravel_sampling', [
            [
                'value' => '0',
                'name' => 'mining_quarrying_gravel_sampling',
                'parent_group' => 'business_content',
                'parent_value' => 2
            ],
        ]);
        $this->add('construction_industry', 'companies business content, construction_industry', [
            [
                'value' => '0',
                'name' => 'comprehensive_construction_work',
                'parent_group' => 'business_content',
                'parent_value' => 3
            ],
            [
                'value' => '1',
                'name' => 'job_specific_construction_work_excluding_facility_construction_work',
                'parent_group' => 'business_content',
                'parent_value' => 3
            ],
            [
                'value' => '2',
                'name' => 'equipment_construction_industry',
                'parent_group' => 'business_content',
                'parent_value' => 3
            ]
        ]);
        $this->add('manufacturing_industry', 'companies business content, agriculture forestry', [
            [
                'value' => '0',
                'name' => 'foodstuff_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '1',
                'name' => 'beverages_tobacco_feed_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            ['value' => '2', 'name' => 'textile_industry', 'parent_group' => 'business_content', 'parent_value' => 4],
            [
                'value' => '3',
                'name' => 'wood_and_wood_product_manufacturing_industry_excluding_furniture)',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '4',
                'name' => 'furniture_and_accessories_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '5',
                'name' => 'pulp_paper_paper_processed_goods_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '6',
                'name' => 'printing_and_related_business',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            ['value' => '7', 'name' => 'chemical_industry', 'parent_group' => 'business_content', 'parent_value' => 4],
            [
                'value' => '8',
                'name' => 'petroleum_products_coal_product_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '9',
                'name' => 'plastic_product_manufacturing_industry_excluding others',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '10',
                'name' => 'rubber_product_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '11',
                'name' => 'leather_same_product_fur_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '12',
                'name' => 'ceramic_stone_products_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '13',
                'name' => 'iron_and_steel_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '15',
                'name' => 'nonferrous_metals_manufacturing_industry)',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '16',
                'name' => 'metal_product_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '17',
                'name' => 'manufacturer_of_machinery',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '18',
                'name' => 'production_machinery_and_equipment_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '29',
                'name' => 'industrial_machinery_and_equipment',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '20',
                'name' => 'electronic_component_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '21',
                'name' => 'electrical_machinery_equipment',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '22',
                'name' => 'information_and_communication_machinery',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '23',
                'name' => 'transportation_machinery',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
            [
                'value' => '24',
                'name' => 'other_manufacturing_industry',
                'parent_group' => 'business_content',
                'parent_value' => 4
            ],
        ]);
        $this->add(
            'electricity_gas_heat_water_supply',
            'companies business content, electricity_gas_heat_water_supply',
            [
                [
                    'value' => '0',
                    'name' => 'electric_industry',
                    'parent_group' => 'business_content',
                    'parent_value' => 5
                ],
                ['value' => '1', 'name' => 'gas_industry', 'parent_group' => 'business_content', 'parent_value' => 5],
                [
                    'value' => '2',
                    'name' => 'heat_supply_industry',
                    'parent_group' => 'business_content',
                    'parent_value' => 5
                ],
                ['value' => '3', 'name' => 'water_service', 'parent_group' => 'business_content', 'parent_value' => 5]
            ]
        );
        $this->add('information_communication', 'companies business content,information_communication ', [
            [
                'value' => '0',
                'name' => 'communication_industry',
                'parent_group' => 'business_content',
                'parent_value' => 6
            ],
            [
                'value' => '1',
                'name' => 'broadcasting_industry',
                'parent_group' => 'business_content',
                'parent_value' => 6
            ],
            [
                'value' => '2',
                'name' => 'information_service_industry',
                'parent_group' => 'business_content',
                'parent_value' => 6
            ],
            [
                'value' => '3',
                'name' => 'internet_accompanying_service_industry',
                'parent_group' => 'business_content',
                'parent_value' => 6
            ],
            [
                'value' => '4',
                'name' => 'video_sound_text_information_system work',
                'parent_group' => 'business_content',
                'parent_value' => 6
            ],
        ]);
        $this->add('transportation_postal_service', 'companies business content,transportation_postal_service ', [
            ['value' => '0', 'name' => 'railway_industry', 'parent_group' => 'business_content', 'parent_value' => 8],
            [
                'value' => '1',
                'name' => 'road_passenger_transportation industry',
                'parent_group' => 'business_content',
                'parent_value' => 8
            ],
            [
                'value' => '2',
                'name' => 'road_freight_forwarding_service',
                'parent_group' => 'business_content',
                'parent_value' => 8
            ],
            [
                'value' => '3',
                'name' => 'water_transportation_industry',
                'parent_group' => 'business_content',
                'parent_value' => 8
            ],
            [
                'value' => '4',
                'name' => 'air_transportation industry',
                'parent_group' => 'business_content',
                'parent_value' => 8
            ],
            [
                'value' => '5',
                'name' => 'warehousing_business',
                'parent_group' => 'business_content',
                'parent_value' => 8
            ],
            [
                'value' => '6',
                'name' => 'service_industry_incidental',
                'parent_group' => 'business_content',
                'parent_value' => 8
            ],
            [
                'value' => '7',
                'name' => 'postal_business_including_credit',
                'parent_group' => 'business_content',
                'parent_value' => 8
            ],
        ]);
        $this->add('wholesale_retail', 'companies business content, wholesale_retail', [
            [
                'value' => '0',
                'name' => 'various_merchandise_wholesale',
                'parent_group' => 'business_content',
                'parent_value' => 9
            ],
            [
                'value' => '1',
                'name' => 'textile_clothing_etc_wholesale',
                'parent_group' => 'business_content',
                'parent_value' => 9
            ],
            [
                'value' => '2',
                'name' => 'food_and_beverage_wholesale',
                'parent_group' => 'business_content',
                'parent_value' => 9
            ],
            [
                'value' => '3',
                'name' => 'building_materials_mineral',
                'parent_group' => 'business_content',
                'parent_value' => 9
            ],
            [
                'value' => '4',
                'name' => 'machine_tool_wholesale_business',
                'parent_group' => 'business_content',
                'parent_value' => 9
            ],
            [
                'value' => '5',
                'name' => 'other_wholesale_business',
                'parent_group' => 'business_content',
                'parent_value' => 9
            ],
            [
                'value' => '6',
                'name' => 'various_product_retailers',
                'parent_group' => 'business_content',
                'parent_value' => 9
            ],
            [
                'value' => '7',
                'name' => 'textile_clothing_personal_belongings',
                'parent_group' => 'business_content',
                'parent_value' => 9
            ],
            [
                'value' => '8',
                'name' => 'food_and_beverage_retailing',
                'parent_group' => 'business_content',
                'parent_value' => 9
            ],
            [
                'value' => '9',
                'name' => 'machinery_and_equipment_retailing',
                'parent_group' => 'business_content',
                'parent_value' => 9
            ],
            ['value' => '10', 'name' => 'other_retailers', 'parent_group' => 'business_content', 'parent_value' => 9],
            [
                'value' => '11',
                'name' => 'no_store_retailing',
                'parent_group' => 'business_content',
                'parent_value' => 9
            ],
        ]);
        $this->add('financial_insurance_industry', 'companies business content, financial_insurance_industry', [
            ['value' => '0', 'name' => 'banking_industry', 'parent_group' => 'business_content', 'parent_value' => 10],
            [
                'value' => '1',
                'name' => 'cooperative_organization',
                'parent_group' => 'business_content',
                'parent_value' => 10
            ],
            [
                'value' => '2',
                'name' => 'money_lending_business_credit',
                'parent_group' => 'business_content',
                'parent_value' => 10
            ],
            [
                'value' => '3',
                'name' => 'financial_instruments_trading',
                'parent_group' => 'business_content',
                'parent_value' => 10
            ],
            [
                'value' => '4',
                'name' => 'supplementary_finance_industry',
                'parent_group' => 'business_content',
                'parent_value' => 10
            ],
            [
                'value' => '5',
                'name' => 'insurance_industry_including_agency',
                'parent_group' => 'business_content',
                'parent_value' => 10
            ],
        ]);
        $this->add(
            'real_estate_goods_rental_business',
            'companies business content, real_estate_goods_rental_business ',
            [
                [
                    'value' => '0',
                    'name' => 'real_estate_business',
                    'parent_group' => 'business_content',
                    'parent_value' => 11
                ],
                [
                    'value' => '1',
                    'name' => 'real_estate_rental_management',
                    'parent_group' => 'business_content',
                    'parent_value' => 11
                ],
                [
                    'value' => '2',
                    'name' => 'goods_rental_business',
                    'parent_group' => 'business_content',
                    'parent_value' => 11
                ],
            ]
        );
        $this->add('academic_research_industry', 'companies business content, academic_research_industry ', [
            [
                'value' => '0',
                'name' => 'academic_and_development',
                'parent_group' => 'business_content',
                'parent_value' => 12
            ],
            [
                'value' => '1',
                'name' => 'professional_service_industry',
                'parent_group' => 'business_content',
                'parent_value' => 12
            ],
            ['value' => '2', 'name' => 'advertising', 'parent_group' => 'business_content', 'parent_value' => 12],
            [
                'value' => '3',
                'name' => 'technical_service_industry',
                'parent_group' => 'business_content',
                'parent_value' => 12
            ],
        ]);
        $this->add('accommodation_food_service', 'companies business content, accommodation_food_service', [
            [
                'value' => '0',
                'name' => 'accommodation_industry',
                'parent_group' => 'business_content',
                'parent_value' => 12
            ],
            ['value' => '1', 'name' => 'restaurant', 'parent_group' => 'business_content', 'parent_value' => 12],
            [
                'value' => '2',
                'name' => 'take_away_delivery',
                'parent_group' => 'business_content',
                'parent_value' => 12
            ],
        ]);
        $this->add('lifestyle_related_service', 'companies business content, lifestyle_related_service', [
            [
                'value' => '0',
                'name' => 'laundry_barber_beauty',
                'parent_group' => 'business_content',
                'parent_value' => 13
            ],
            [
                'value' => '1',
                'name' => 'other_living_related',
                'parent_group' => 'business_content',
                'parent_value' => 13
            ],
            [
                'value' => '2',
                'name' => 'entertainment_industry',
                'parent_group' => 'business_content',
                'parent_value' => 13
            ],

        ]);
        $this->add('education_learning_support', 'companies business content, agriculture forestry', [
            ['value' => '0', 'name' => 'school_education', 'parent_group' => 'business_content', 'parent_value' => 14],
            [
                'value' => '1',
                'name' => 'other_learning_support_industry',
                'parent_group' => 'business_content',
                'parent_value' => 14
            ],
        ]);
        $this->add('medical_care', 'companies business content, agriculture forestry', [
            ['value' => '0', 'name' => 'medical_industry', 'parent_group' => 'business_content', 'parent_value' => 15],
            ['value' => '1', 'name' => 'hygiene', 'parent_group' => 'business_content', 'parent_value' => 15],
            [
                'value' => '2',
                'name' => 'social_insurance_social',
                'parent_group' => 'business_content',
                'parent_value' => 15
            ],

        ]);
        $this->add('composite_service_business', 'companies business content, agriculture forestry', [
            ['value' => '0', 'name' => 'post_office', 'parent_group' => 'business_content', 'parent_value' => 16],
            ['value' => '1', 'name' => 'cooperatives', 'parent_group' => 'business_content', 'parent_value' => 16],
        ]);
        $this->add('service_industry_not_classified', 'companies business content, agriculture forestry', [
            [
                'value' => '0',
                'name' => 'waste_disposal_industry',
                'parent_group' => 'business_content',
                'parent_value' => 17
            ],
            [
                'value' => '1',
                'name' => 'automobile_maintenance',
                'parent_group' => 'business_content',
                'parent_value' => 17
            ],
            [
                'value' => '2',
                'name' => 'machine_repair_work',
                'parent_group' => 'business_content',
                'parent_value' => 17
            ],
            [
                'value' => '3',
                'name' => 'employment_placement',
                'parent_group' => 'business_content',
                'parent_value' => 17
            ],
            [
                'value' => '4',
                'name' => 'other_business_services',
                'parent_group' => 'business_content',
                'parent_value' => 17
            ],
            [
                'value' => '5',
                'name' => 'political_economic',
                'parent_group' => 'business_content',
                'parent_value' => 17
            ],
            ['value' => '6', 'name' => 'religion', 'parent_group' => 'business_content', 'parent_value' => 17],
            [
                'value' => '7',
                'name' => 'other_service_industry',
                'parent_group' => 'business_content',
                'parent_value' => 17
            ],
            [
                'value' => '8',
                'name' => 'foreign_public_affairs',
                'parent_group' => 'business_content',
                'parent_value' => 17
            ],

        ]);
        $this->add('public_affairs_excluding', 'companies business content, agriculture forestry', [
            [
                'value' => '0',
                'name' => 'national_public_affairs',
                'parent_group' => 'business_content',
                'parent_value' => 18
            ],
            [
                'value' => '1',
                'name' => 'local_public_affairs',
                'parent_group' => 'business_content',
                'parent_value' => 18
            ],
        ]);
        $this->add('unclassifiable_industry', 'companies business content, agriculture forestry', [
            [
                'value' => '0',
                'name' => 'unclassifiable_industry',
                'parent_group' => 'business_content',
                'parent_value' => 19
            ],
        ]);
        $this->add('job_category', 'job category', [
            ['value' => '0', 'name' => 'system_engineer'],
            ['value' => '1', 'name' => 'web_design'],
            ['value' => '2', 'name' => 'web_director_marketing'],
            ['value' => '3', 'name' => 'sales_planning_management_publicity'],
            ['value' => '4', 'name' => 'others'],
        ]);
        $this->add('system_engineer', 'job category system_engineer', [
            ['value' => '0', 'name' => 'Web_engineer', 'parent_group' => 'job_category', 'parent_value' => 0],
            ['value' => '1', 'name' => 'smartphone', 'parent_group' => 'job_category', 'parent_value' => 0],
            ['value' => '2', 'name' => 'game_engineer', 'parent_group' => 'job_category', 'parent_value' => 0],
            ['value' => '3', 'name' => 'infra_engineer', 'parent_group' => 'job_category', 'parent_value' => 0],
            ['value' => '4', 'name' => 'other_engineer', 'parent_group' => 'job_category', 'parent_value' => 0],
        ]);
        $this->add('web_design', 'job category web_design', [
            ['value' => '0', 'name' => 'graphic_designer', 'parent_group' => 'job_category', 'parent_value' => 1],
            ['value' => '1', 'name' => 'web_design', 'parent_group' => 'job_category', 'parent_value' => 1],
        ]);
        $this->add('web_director_marketing', 'job category web_director_marketing', [
            ['value' => '0', 'name' => 'web_director', 'parent_group' => 'job_category', 'parent_value' => 2],
            ['value' => '1', 'name' => 'web_marketing', 'parent_group' => 'job_category', 'parent_value' => 2],
        ]);
        $this->add('sales_planning_management_publicity', 'job category sales_planning_management_publicity ', [
            ['value' => '0', 'name' => 'it_industry_sales', 'parent_group' => 'job_category', 'parent_value' => 3],
            ['value' => '1', 'name' => 'planning', 'parent_group' => 'job_category', 'parent_value' => 3],
            ['value' => '2', 'name' => 'user_support', 'parent_group' => 'job_category', 'parent_value' => 3],
            ['value' => '3', 'name' => 'management', 'parent_group' => 'job_category', 'parent_value' => 3],
        ]);
        $this->add('others', 'job category others', [
            ['value' => '0', 'name' => 'analyst', 'parent_group' => 'job_category', 'parent_value' => 4],
            ['value' => '1', 'name' => 'scientist', 'parent_group' => 'job_category', 'parent_value' => 4],
        ]);
        $this->add('annual_salary_range', 'annual_salary_range', [
            ['value' => '0', 'name' => 'less_1500000thousand'],
            ['value' => '1', 'name' => '1500000thousand'],
            ['value' => '2', 'name' => '2000000thousand'],
            ['value' => '3', 'name' => '2500000thousand'],
            ['value' => '4', 'name' => '3000000thousand'],
            ['value' => '5', 'name' => '3500000thousand'],
            ['value' => '6', 'name' => '4000000thousand'],
            ['value' => '7', 'name' => '4500000thousand'],
            ['value' => '8', 'name' => '5000000thousand'],
            ['value' => '9', 'name' => '5500000thousand'],
            ['value' => '10', 'name' => '6000000thousand'],
            ['value' => '11', 'name' => '6500000thousand'],
            ['value' => '12', 'name' => '7000000thousand'],
            ['value' => '13', 'name' => '7500000thousand'],
            ['value' => '14', 'name' => '8000000thousand'],
            ['value' => '15', 'name' => '8500000thousand'],
            ['value' => '16', 'name' => '9000000thousand'],
            ['value' => '17', 'name' => '9500000thousand'],
            ['value' => '18', 'name' => '10000000thousand'],
            ['value' => '19', 'name' => '11000000thousand'],
            ['value' => '20', 'name' => '12000000thousand'],
            ['value' => '21', 'name' => '13000000thousand'],
            ['value' => '22', 'name' => '14000000thousand'],
            ['value' => '23', 'name' => '15000000thousand'],
            ['value' => '24', 'name' => '1600000thousand'],
            ['value' => '25', 'name' => '17000000thousand'],
            ['value' => '26', 'name' => '1800000thousand'],
            ['value' => '27', 'name' => '19000000thousand'],
            ['value' => '28', 'name' => '20000000thousand'],
            ['value' => '29', 'name' => '25000000thousand'],
            ['value' => '30', 'name' => '30000000thousand'],
            ['value' => '31', 'name' => '35000000thousand'],
            ['value' => '32', 'name' => '3900000thousand'],
            ['value' => '33', 'name' => '40000000thousand_and_above'],
        ]);
        $this->add('annual_min_salary_range', 'annual_min_salary_range', [
            ['value' => '0', 'name' => 'less_than_300'],
            ['value' => '1', 'name' => '300'],
            ['value' => '2', 'name' => '350'],
            ['value' => '3', 'name' => '400'],
            ['value' => '4', 'name' => '450'],
            ['value' => '5', 'name' => '500'],
            ['value' => '6', 'name' => '550'],
            ['value' => '7', 'name' => '600'],
            ['value' => '8', 'name' => '650'],
            ['value' => '9', 'name' => '700'],
            ['value' => '10', 'name' => '750'],
            ['value' => '11', 'name' => '800'],
            ['value' => '12', 'name' => '850'],
            ['value' => '13', 'name' => '900'],
            ['value' => '14', 'name' => '950'],
            ['value' => '15', 'name' => '1000'],
            ['value' => '16', 'name' => '1050'],
            ['value' => '17', 'name' => '1100'],
            ['value' => '18', 'name' => '1200'],
            ['value' => '19', 'name' => '1300'],
            ['value' => '20', 'name' => '1400'],
            ['value' => '21', 'name' => '1500'],
            ['value' => '22', 'name' => '1600'],
            ['value' => '23', 'name' => '1700'],
            ['value' => '24', 'name' => '1800'],
            ['value' => '25', 'name' => '1900'],
            ['value' => '26', 'name' => '2000'],
            ['value' => '27', 'name' => '2500'],
            ['value' => '28', 'name' => '3000'],
            ['value' => '29', 'name' => '3500'],
            ['value' => '30', 'name' => '4000']
        ]);
        $this->add('annual_min_salary_search', 'annual_min_salary_range', [
            ['value' => '0', 'name' => 'less_than_300'],
            ['value' => '1', 'name' => '300'],
            ['value' => '2', 'name' => '350'],
            ['value' => '3', 'name' => '400'],
            ['value' => '4', 'name' => '450'],
            ['value' => '5', 'name' => '500'],
            ['value' => '6', 'name' => '550'],
            ['value' => '7', 'name' => '600'],
            ['value' => '8', 'name' => '650'],
            ['value' => '9', 'name' => '700'],
            ['value' => '10', 'name' => '750'],
            ['value' => '11', 'name' => '800'],
            ['value' => '12', 'name' => '850'],
            ['value' => '13', 'name' => '900'],
            ['value' => '14', 'name' => '950'],
            ['value' => '15', 'name' => '1000'],
            ['value' => '16', 'name' => '1050'],
            ['value' => '17', 'name' => '1100'],
            ['value' => '18', 'name' => '1200'],
            ['value' => '19', 'name' => '1300'],
            ['value' => '20', 'name' => '1400'],
            ['value' => '21', 'name' => '1500'],
            ['value' => '22', 'name' => '1600'],
            ['value' => '23', 'name' => '1700'],
            ['value' => '24', 'name' => '1800'],
            ['value' => '25', 'name' => '1900'],
            ['value' => '26', 'name' => '2000'],
            ['value' => '27', 'name' => '2500'],
            ['value' => '28', 'name' => '3000'],
            ['value' => '29', 'name' => '3500'],
            ['value' => '30', 'name' => '4000']
        ]);
        $this->add('experience_year', 'experience_year_range', [
            ['value' => '1', 'name' => 'no_exp'],
            ['value' => '2', 'name' => 'lessthan1year'],
            ['value' => '3', 'name' => '1_to_3_yrars'],
            ['value' => '4', 'name' => '4_to_6_years'],
            ['value' => '5', 'name' => '7_to_9_years'],
            ['value' => '6', 'name' => '10_and_more'],
        ]);
        $this->add('skill_experience', 'Skill_exigence_range', [
            ['value' => '0', 'name' => 'self_study'],
            ['value' => '1', 'name' => 'lessthan1year'],
            ['value' => '2', 'name' => '2years'],
            ['value' => '3', 'name' => '3years'],
            ['value' => '4', 'name' => '4years'],
            ['value' => '5', 'name' => 'more_than_5years'],

        ]);
        $this->add('language_writing_skills', 'language_writing_skills', [
            ['value' => '0', 'name' => 'none'],
            ['value' => '1', 'name' => 'hiragana'],
            ['value' => '2', 'name' => 'katakana'],
            ['value' => '3', 'name' => 'kanji'],
            ['value' => '4', 'name' => 'hiragankatakana'],
            ['value' => '5', 'name' => 'all'],
        ]);
        $this->add('work_time', 'hour and half an hour', [
            ['value' => '0', 'name' => '00_00'],
            ['value' => '1', 'name' => '01_00'],
            ['value' => '2', 'name' => '02_00'],
            ['value' => '3', 'name' => '03_00'],
            ['value' => '4', 'name' => '04_00'],
            ['value' => '5', 'name' => '05_00'],
            ['value' => '6', 'name' => '06_00'],
            ['value' => '7', 'name' => '07_00'],
            ['value' => '8', 'name' => '08_00'],
            ['value' => '9', 'name' => '09_00'],
            ['value' => '10', 'name' => '10_00'],
            ['value' => '11', 'name' => '11_00'],
            ['value' => '12', 'name' => '12_00'],
            ['value' => '13', 'name' => '13_00'],
            ['value' => '14', 'name' => '14_00'],
            ['value' => '15', 'name' => '15_00'],
            ['value' => '16', 'name' => '16_00'],
            ['value' => '17', 'name' => '17_00'],
            ['value' => '18', 'name' => '18_00'],
            ['value' => '19', 'name' => '19_00'],
            ['value' => '20', 'name' => '20_00'],
            ['value' => '21', 'name' => '21_00'],
            ['value' => '22', 'name' => '22_00'],
            ['value' => '23', 'name' => '23_00'],

        ]);
        $this->add('day_hour', 'hour and half an hour', [
            ['value' => '0', 'name' => '00_00'],
            ['value' => '1', 'name' => '00_30'],
            ['value' => '2', 'name' => '01_00'],
            ['value' => '3', 'name' => '01_30'],
            ['value' => '4', 'name' => '02_00'],
            ['value' => '5', 'name' => '02_30'],
            ['value' => '6', 'name' => '03_00'],
            ['value' => '7', 'name' => '03_30'],
            ['value' => '8', 'name' => '04_00'],
            ['value' => '9', 'name' => '04_30'],
            ['value' => '10', 'name' => '05_00'],
            ['value' => '11', 'name' => '05_30'],
            ['value' => '12', 'name' => '06_00'],
            ['value' => '14', 'name' => '06_30'],
            ['value' => '15', 'name' => '07_00'],
            ['value' => '16', 'name' => '07_30'],
            ['value' => '17', 'name' => '08_00'],
            ['value' => '18', 'name' => '08_30'],
            ['value' => '19', 'name' => '09_00'],
            ['value' => '20', 'name' => '09_30'],
            ['value' => '21', 'name' => '10_00'],
            ['value' => '22', 'name' => '10_30'],
            ['value' => '23', 'name' => '11_00'],
            ['value' => '24', 'name' => '11_30'],
            ['value' => '25', 'name' => '12_00'],
            ['value' => '26', 'name' => '12_30'],
            ['value' => '27', 'name' => '13_00'],
            ['value' => '28', 'name' => '13_30'],
            ['value' => '29', 'name' => '14_00'],
            ['value' => '30', 'name' => '14_30'],
            ['value' => '31', 'name' => '15_00'],
            ['value' => '32', 'name' => '15_30'],
            ['value' => '33', 'name' => '16_00'],
            ['value' => '34', 'name' => '16_30'],
            ['value' => '35', 'name' => '17_00'],
            ['value' => '36', 'name' => '17_30'],
            ['value' => '37', 'name' => '18_00'],
            ['value' => '38', 'name' => '18_30'],
            ['value' => '39', 'name' => '19_00'],
            ['value' => '40', 'name' => '19_30'],
            ['value' => '41', 'name' => '20_00'],
            ['value' => '42', 'name' => '20_30'],
            ['value' => '43', 'name' => '21_00'],
            ['value' => '44', 'name' => '21_30'],
            ['value' => '45', 'name' => '22_00'],
            ['value' => '46', 'name' => '22_30'],
            ['value' => '47', 'name' => '23_00'],
            ['value' => '48', 'name' => '23_30'],

        ]);
        $this->add('no_of_employee', 'numberof employee of company', [
            ['value' => '0', 'name' => 'below_10'],
            ['value' => '1', 'name' => '10_20'],
            ['value' => '2', 'name' => '20_30'],
            ['value' => '3', 'name' => '30_40'],
            ['value' => '4', 'name' => '40_50'],
            ['value' => '5', 'name' => '50_100'],
            ['value' => '6', 'name' => 'above_100'],
        ]);
        $this->add('interview_method', 'interview method attribute', [
            ['value' => '0', 'name' => 'skype'],
            ['value' => '1', 'name' => 'field'],
        ]);
        $this->add('scout_mail_status', 'scout mail status attribute', [
            ['value' => '0', 'name' => 'pending'],
            ['value' => '1', 'name' => 'refusal'],
            ['value' => '2', 'name' => 'accepted'],
        ]);
        $this->add('interview_location_option', 'scout mail status attribute', [
            ['value' => '0', 'name' => 'head_office'],
            ['value' => '1', 'name' => 'work_location'],
        ]);

        $this->add('applicant_status', 'scout message type', [
            ['value' => '0', 'name' => 'new'],
            ['value' => '1', 'name' => 'interviewed'],
            ['value' => '2', 'name' => 'rejected'],
            ['value' => '3', 'name' => 'hired'],
        ]);

        $this->add('application_status', 'scout message type', [
            ['value' => '0', 'name' => 'unopened'],
            ['value' => '1', 'name' => 'opened'],
            ['value' => '3', 'name' => 'interviewwaiting'],
            ['value' => '4', 'name' => 'rejected'],
            ['value' => '5', 'name' => 'hired'],
        ]);

        $this->add('scout_message_type', 'scout message type', [
            ['value' => '1', 'name' => 'scout'],
            ['value' => '2', 'name' => 'application'],
        ]);

        $this->add('scout_message_provider_status', 'scout message type', [
            ['value' => '0', 'name' => 'replied'],
            ['value' => '1', 'name' => 'notviewed'],
            ['value' => '2', 'name' => 'hired'],
            ['value' => '3', 'name' => 'rejected'],
        ]);
        $this->add('scout_message_seeker_status', 'scout message type seeker', [
            ['value' => '0', 'name' => 'pending'],
            ['value' => '1', 'name' => 'accepted'],
            ['value' => '2', 'name' => 'declined'],
        ]);

        $this->add('time_period', 'job time peried', [
            ['value' => '0', 'name' => 'upto_3month'],
            ['value' => '1', 'name' => 'upto_6month'],
            ['value' => '2', 'name' => 'upto_1year'],
            ['value' => '3', 'name' => 'morethan_1year'],

        ]);
        $this->add('charge_type', 'provider transaction charge type', [
            ['value' => '0', 'name' => 'scout'],
            ['value' => '1', 'name' => 'translation'],
            ['value' => '2', 'name' => 'application'],
            ['value' => '3', 'name' => 'deposit'],
            ['value' => '4', 'name' => 'scoutmessage'],
            //            ['value' => '5', 'name' => 'freezed'], this is for freezed money
        ]);
        $this->add('paid_status', 'provider paid status', [
            ['value' => '0', 'name' => 'unsynced'],
            ['value' => '1', 'name' => 'pending'],
            ['value' => '2', 'name' => 'verified'],
            ['value' => '3', 'name' => 'limit_over'],
            ['value' => '4', 'name' => 'suspended'],
        ]);
    }

    public function prefecture()
    {
        $commonData = include base_path('master/common.php');
        $regions = array_get($commonData, 'prefecture.regions');
        $regionData = [];
        $prefectureData = [];
        foreach ($regions as $name => $region) {
            $regionData[] = ['value' => $region['value'], 'name' => $name];
            foreach ($region['pref'] as $name => $pref) {
                $prefectureData[] = [
                    'value' => $pref['value'],
                    'name' => $name,
                    'parent_group' => 'region',
                    'parent_value' => $region['value']
                ];
            }
        }
        $this->add('region', 'region', $regionData);
        $this->add('prefecture', 'prefecture', $prefectureData);
    }


    /**
     * 国コード・国際電話番号データ登録
     *
     * @return void
     */
    public function countryCode()
    {
        $phoneData = include base_path('master/phone.php');
        $countries = array_get($phoneData, 'format.country_code');
        $country_code = [];
        $country_calling_code = [];
        foreach ($countries as $code => $data) {
            $name = strtolower($data['en']);
            $name = trim(preg_replace('/\(.+?\)/', '', $name));
            $name = str_replace('st.', 'saint', $name);
            $name = str_replace(',', '', $name);
            $name = str_replace('\'', '', $name);
            $name = str_replace('-', '_', $name);
            $name = str_replace('&amp;', 'and', $name);
            $name = preg_replace('/\/.+$/', '', $name);
            $name = preg_replace('/\s+/', '_', $name);
            $country_code[] = ['value' => $code, 'name' => $name];
            $country_name[] = ['value' => $code, 'name' => $name];
            $country_calling_code[] = ['value' => (int) $data['value'], 'name' => $name];
        }
        //        $this->add('country_code', '国コード', $country_code);
        $this->add('country_calling_code', '国際電話番号', $country_calling_code);
        //        $this->add('country_name', '国際電話番号', $country_name);
    }

    /**
     * @param $group
     * @param $description
     * @param $attributes
     */
    public function add($group, $description, $attributes)
    {
        DB::transaction(function () use ($group, $description, $attributes) {
            $groupRow = DB::table('attribute_groups')
                ->where('name', $group)
                ->lockForUpdate()
                ->first();

            if (empty($groupRow)) {
                $groupId = DB::table('attribute_groups')
                    ->insertGetId([
                        'name' => $group,
                        'description' => $description,
                        'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                    ]);
            } else {
                $groupId = $groupRow->id;
                DB::table('attribute_groups')
                    ->where('id', $groupId)
                    ->update([
                        'description' => $description,
                        'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
                    ]);
            }

            $attributeRow = DB::table('attributes')
                ->select(DB::raw('MAX(display_order) AS max'))
                ->where('group_id', $groupId)
                ->lockForUpdate()
                ->first();

            $displayOrder = 0;

            foreach ($attributes as $data) {
                $attributeRow = DB::table('attributes')
                    ->where('group_id', $groupId)
                    ->where('value', $data['value'])
                    ->first();

                if (!empty($data['parent_group'])) {
                    $parentGroupRow = DB::table('attribute_groups')
                        ->where('name', $data['parent_group'])
                        ->first();
                    if (empty($parentGroupRow)) {
                        continue;
                    }
                    $parentGroupId = $parentGroupRow->id;
                }

                ++$displayOrder;

                if (empty($attributeRow)) {
                    DB::table('attributes')
                        ->insert([
                            'group_id' => $groupId,
                            'value' => $data['value'],
                            'name' => $data['name'],
                            'parent_group_id' => (!empty($parentGroupId)) ? $parentGroupId : null,
                            'parent_value' => (!empty($data['parent_value'])) ? $data['parent_value'] : null,
                            'display_order' => $displayOrder,
                            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                        ]);
                } else {
                    $attributeId = $attributeRow->id;
                    DB::table('attributes')
                        ->where('id', $attributeId)
                        ->update([
                            'name' => $data['name'],
                            'parent_group_id' => (!empty($parentGroupId)) ? $parentGroupId : null,
                            'parent_value' => (!empty($data['parent_value'])) ? $data['parent_value'] : null,
                            'display_order' => $displayOrder,
                            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
                        ]);
                }
            }
        }, 3);
    }
}
