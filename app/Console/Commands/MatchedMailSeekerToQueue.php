<?php

namespace App\Console\Commands;

use App\Models\Seeker;
use App\Models\User;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Notifications\ExceptionCaught;
use App\Notifications\SendMatchedJobToSeeker;


class MatchedMailSeekerToQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'matchedseeker:queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan matched seeker files and and data to queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // Read File
            //get file in queue from table 
            $file_name = DB::table('file_to_queue_tracker')->where('processed', 0)->first();
            if ($file_name) {
                $file = Storage::disk('s3_private')->get('match-seekers-mail/' . $file_name->file_name);
                //$file = Storage::disk('local')->get('match-seekers-mail/' . $file_name->file_name);
                $data = json_decode($file, true);
                $to_db = [];
                if (count($data) > \Config::get('app.queue_limit')) {
                    $i = 1;
                    echo $i;
                    foreach ($data as $key => $to_queue) {
                        $i++;
                        //process to queue
                        unset($data[$key]);
                        $seeker = Seeker::find($key);
                        $mail_setting = $seeker->mailSetting()->first();
                        $user_to_notify = $seeker->user;
                        $user_to_notify->notify(new SendMatchedJobToSeeker($to_queue,
                            $user_to_notify->receive_mail_language));
                        if ($i == \Config::get('app.queue_limit')) {
                            break;
                        }
                    }
                    Storage::disk('s3_private')->put('match-seekers-mail/' . $file_name->file_name, json_encode($data));
                } else {
                    $i = 1;
                    foreach ($data as $key => $to_queue) {
                        $i++;
                        //process to queue
                        unset($data[$key]);
                        $seeker = Seeker::find($key);
                        $mail_setting = $seeker->mailSetting()->first();
                        $user_to_notify = $seeker->user;
                        $user_to_notify->notify(new SendMatchedJobToSeeker($to_queue,
                            $user_to_notify->receive_mail_language));
                        if ($i == \Config::get('app.queue_limit')) {
                            break;
                        }
                    }
                    Storage::disk('s3_private')->put('match-seekers-mail/' . $file_name->file_name, json_encode($data));
                    DB::table('file_to_queue_tracker')->where('file_name',
                        $file_name->file_name)->update(['processed' => 1]);
                }
            }
        } catch (Exception $e) {
            $err_message = $e->getMessage();
            $admins = User::whereIn('email',\Config::get('app.admin_exception_mail_list'))->get();
            \Notification::send($admins, new ExceptionCaught($err_message, '/commands/matchedmailseekertoqueue'));
            report($e);
            return false;
        }
    }
}
