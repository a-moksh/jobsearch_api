<?php

namespace App\Console\Commands;

use App\Http\Requests\AttrStoreRequest;
use App\Models\Attr;
use App\Models\AttrOpt;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AttributeRegisterCommand extends Command
{
    protected $names = [];

    protected $opts = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:attributes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attribute registration command';

    /**
     * Create a new co mmand instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $items = $this->getAttributes();
        $bar = $this->output->createProgressBar(count($items));
        $this->info('Start');

        foreach ($items as $index => $item) {
            $name = $this->getKeyName($item['table_name'], $item['column_name']);
            $this->names[] = $name;
            $this->opts[$name] = [];
            if (!empty($item['opts'])) {
                $this->opts[$name] = $item['opts'];
            }
        }

        $counts = array_count_values($this->names);
        foreach ($counts as $key => $count) {
            if ($count > 1) {
                $this->error('Duplicate key ' . $key);
                return false;
            }
        }

        foreach ($items as $index => $item) {
            if (!empty($item['reference'])) {
                $name = $this->getKeyName($item['reference']['table_name'], $item['reference']['column_name']);
                if (isset($this->opts[$name])) {
                    $items[$index]['opts'] = $this->opts[$name];
                }
            }
        }

        foreach ($items as $index => $item) {
            $item = $this->validate($item);
            if (!$item) {
                break;
            }
            $this->add($item);
            $bar->advance();
        }

        $bar->finish();
        $this->line('');
    }

    public function add($item)
    {
        DB::transaction(function () use ($item) {
            $item['name'] = $this->getKeyName($item['table_name'], $item['column_name']);
            $query = Attr::query();
            $query->where('name', $item['name']);
            $attr = $query->lockForUpdate()->first();

            unset($item['table_name']);
            unset($item['column_name']);
            unset($item['reference']);

            if (!$attr) {
                $item['display_order'] = Attr::generateDisplayOrder();
                $attr = Attr::createTranslationLocales($item);
                $this->addOpts($item, $attr);
            } else {
                $attr->fill($item);
                $attr->saveTranslationLocales();
                $this->addOpts($item, $attr);
            }
        });
    }

    public function addOpts($item, $attr)
    {
        if (!empty($item['opts'])) {
            foreach ($item['opts'] as $index => $opt) {
                $opt['attr_id'] = $attr->id;
                $opt['data_type'] = $attr->data_type;
                $attrOpt = $attr->opts->where('value', $opt['value'])->first();

                if (!empty($opt['child'])) {
                    if ($opt['child']['table_name'] !== '' && $opt['child']['column_name'] !== '') {
                        $opt['child']['name'] = $this->getKeyName($opt['child']['table_name'],
                            $opt['child']['column_name']);
                        if ($row = Attr::where('name', $opt['child']['name'])->first()) {
                            $opt['child_id'] = $row->id;
                        } else {
                            $this->error(sprintf('child not found [%s]',
                                "{$opt['child']['table_name']}.{$opt['child']['column_name']}"));
                            return false;
                        }
                    }
                }
                unset($opt['child']);


                if ($attrOpt) {
                    $attrOpt->fill($opt);
                    $attrOpt->saveTranslationLocales();
                    unset($item['opts'][$index]);
                    continue;
                }

                $item['opts'][$index] = $opt;
            }
            if (!empty($item['opts'])) {
                $lastNumber = AttrOpt::generateDisplayOrder('attr_id', $attr->id, count($item['opts']));
                $reverseNumber = 0;
                foreach (array_reverse($item['opts'], true) as $index => $opt) {
                    $opt['display_order'] = $lastNumber - $reverseNumber++;
                    $item['opts'][$index] = $opt;

                }
                foreach ($item['opts'] as $opt) {
                    AttrOpt::createTranslationLocales($opt);
                }
            }
        }
    }

    public function getKeyName($tableName, $columnName)
    {
        return preg_replace('/^\*\./', '', implode('.', [$tableName, $columnName]));
    }

    public function getColumnName($columnName)
    {
        return array_last(explode('.', $columnName));
    }

    public function validate($item)
    {
        $defaults = [
            'table_name' => '',
            'column_name' => '',
            'label' => '',
            'data_type' => '',
            'default_value' => '',
            'description' => '',
            'reference' => '',
            'is_private' => 0,
            'column_check' => true,
            'opts' => []
        ];
        $item = array_merge($defaults, $item);
        $item = array_only($item, array_keys($defaults));

        array_walk_recursive($item, function (&$value, $key) {
            if ($value !== null) {
                $value = $key === 'opts' ? (array)$value : $value;
                if (is_string($value)) {
                    $value = trim($value);
                }
                if (is_array($value) && empty($value)) {
                    $value = null;
                } else {
                    if ($value === '') {
                        $value = null;
                    }
                }
            }
        });

        $required = [
            'table_name',
            'column_name',
            'label',
            'data_type',
            'opts',
        ];

        foreach ($required as $key) {
            if ($item[$key] === null) {
                $this->error($key . ' is required');
                return false;
            }
        }

        $optValues = array_column($item['opts'], 'value');

        if (!$this->validateDataType($item['data_type'])) {
            $this->error('The specified data type is invalid');
            return false;
        }
        if ((string)$item['default_value'] !== '') {
            if (!in_array($item['default_value'], $optValues)) {
                $this->error('The value specified as the initial value can not be found');
            }
        }
        if ($item['table_name'] !== '*') {
            if (!$this->tableExists($item['table_name'])) {
                $this->error(sprintf('table not found [%s]', $item['table_name']));
                return false;
            }
        }
        if ($item['column_check']) {
            if (!$this->columnExists($item['table_name'], $item['column_name'])) {
                $this->error(sprintf('column not found [%s]', "{$item['table_name']}.{$item['column_name']}"));
                return false;
            }
        }

        if (!empty($item['reference'])) {
            if ((string)$item['reference']['table_name'] === '') {
                $this->error('reference.table_name is required');
                return false;
            }
            if ((string)$item['reference']['column_name'] === '') {
                $this->error('reference.column_name is required');
                return false;
            }
            if (!in_array($this->getKeyName($item['reference']['table_name'], $item['reference']['column_name']),
                $this->names)) {
                $this->error(sprintf('reference not found [%s]',
                    "{$item['reference']['table_name']}.{$item['reference']['column_name']}"));
                return false;
            }
        }

        foreach ($item['opts'] as $opt) {
            $defaults = [
                'label' => '',
                'value' => '',
                'child' => '',
            ];
            $opt = array_merge($defaults, $opt);
            $opt = array_only($opt, array_keys($defaults));

            if (empty($opt['label'])) {
                $this->error('label is required');
                return false;
            }
            if ((string)$opt['value'] === '') {
                $this->error('value is required');
                return false;
            }
            $counts = array_count_values($optValues);
            foreach ($counts as $key => $count) {
                if ($count > 1) {
                    $this->error('Duplicate option value ' . $key);
                    return false;
                }
            }

            if (!$this->validateDataTypeValue($item['data_type'], $opt['value'])) {
                $this->error(sprintf('The specified data type and the given value do not match [%s]',
                    implode('-', [$item['data_type'], $opt['value']])));
                return false;
            }

            if (!empty($opt['child'])) {
                if ($opt['child']['table_name'] === '') {
                    $this->error('child.table_name is required');
                    return false;
                }
                if ($opt['child']['column_name'] === '') {
                    $this->error('child.column_name is required');
                    return false;
                }
                if (!in_array($this->getKeyName($opt['child']['table_name'], $opt['child']['column_name']),
                    $this->names)) {
                    if ($opt['child']['table_name'] !== '*') {
                        if (!$this->tableExists($opt['child']['table_name'])) {
                            $this->error(sprintf('table not found [%s]', $opt['child']['table_name']));
                            return false;
                        }
                    }
                    if ($item['column_check']) {
                        if (!$this->columnExists($opt['child']['table_name'], $opt['child']['column_name'])) {
                            $this->error(sprintf('column not found [%s]',
                                "{$opt['child']['table_name']}.{$opt['child']['column_name']}"));
                            return false;
                        }
                    }
                }
            }
        }

        return $item;
    }

    public function tableExists($table)
    {
        static $tables;

        if (!isset($tables[$table])) {
            $rows = DB::select("SHOW TABLES LIKE '{$table}'");
            $tables[$table] = count($rows) > 0;
        }
        return $tables[$table];
    }

    public function columnExists($table, $column)
    {
        static $columns;

        $name = "{$table}.{$column}";
        if (!isset($columns[$name])) {
            if ($table !== '*') {
                $rows = DB::select("SHOW COLUMNS FROM {$table}");
                $fields = array_column((array)$rows, 'Field');
                $columns[$name] = in_array($this->getColumnName($column), $fields);
            } else {
                $tableSchema = env('DB_DATABASE');
                $row = DB::table('information_schema.COLUMNS')->where([
                    'TABLE_SCHEMA' => $tableSchema,
                    'COLUMN_NAME' => $this->getColumnName($column)
                ])->first();
                $columns[$name] = (!empty($row));
            }
        }
        return $columns[$name];
    }

    public function validateDataType($dataType)
    {
        return in_array($dataType, ['integer', 'boolean', 'string']);
    }

    public function validateDataTypeValue($dataType, $value)
    {
        return gettype($value) === $dataType;
    }

    public function getAttributes()
    {
        return include __DIR__ . '/datas/attributes.php';
    }
}
