<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class OpenapiOutput extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'openapi:output';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'vendor/bin/openapi app -o swagger';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $baseDir = getcwd();
        $outputDir = base_path('/public/swagger/swagger.json');
        $command = "{$baseDir}/vendor/bin/openapi app -o {$outputDir}";
        $this->info(shell_exec($command));
    }
}
