<?php

namespace App\Console\Commands;

use App\Models\Provider;
use App\Models\User;
use App\Notifications\ExceptionCaught;
use App\Notifications\PaidAccountActivated;
use App\Notifications\PaidAccountDeclined;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckPaidStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:paidstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check providers paid status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            DB::table('providers')->where('paid_status',1)->chunkById(100, function ($providers) {
                $ids = $providers->pluck('provider_identifier_id');
                $data = [
                    'body' => [
                        'b2bMemberIds' => $ids->all()
                    ],
                    'header' => [
                        'apiAuthCode' => config('app.paid_auth_code')
                    ]
                ];

                $response = $this->checkWithPaidJp(json_encode($data), 'coop/member/check/ver1.0/p.json');

                if ($response) {
                    $response = json_decode($response);

                    if ($response->header->status == 'SUCCESS' && $response->body->results->successes) {
                        foreach ($response->body->results->successes as $provider){
                            if($provider->memberStatusCode == 2){
                                $providerid = Provider::where('provider_identifier_id',$provider->b2bMemberId)->first();
                                $providerid->paid_status = $provider->memberStatusCode;
                                $providerid->save();
                                $providerid->user->notify(new PaidAccountActivated('ja'));
                            }
                            else if($provider->memberStatusCode == 4){
                                $providerid = Provider::where('provider_identifier_id',$provider->b2bMemberId)->first();
                                $providerid->paid_status = $provider->memberStatusCode;
                                $providerid->save();
                                $providerid->user->notify(new PaidAccountDeclined('ja'));
                            }
                            // other case for 3???
                        }
                    }
                }
            });
        }catch (Exception $e){
            $err_message = $e->getMessage();
            $admins = User::whereIn('email',\Config::get('app.admin_exception_mail_list'))->get();
            \Notification::send($admins, new ExceptionCaught($err_message, '/commands/checkpaidstatus'));
            report($e);
            return false;
        }

    }

    protected function checkWithPaidJp($data, $api)
    {
        $url = config('app.paid_auth_url') . $api;

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_POST, 1);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=UTF-8']);

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }
}
