<?php

namespace App\Console\Commands;

class ResourceMakeCommand extends \Illuminate\Foundation\Console\ResourceMakeCommand
{
    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/stubs/resource.stub';
    }
}
