<?php

namespace App\Console\Commands;

use App\Models\ZipCode;
use Illuminate\Console\Command;

class ImportZipCodeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:zip-code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import Zip code to the zip code table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ZipCode::truncate();
        $csv_path = base_path('master') . '/ADDRESS_ALL.csv';
        $converted_csv_path = base_path('master') . '/postal_code_utf8.csv';

        file_put_contents(
            $converted_csv_path,
            mb_convert_encoding(
                file_get_contents($csv_path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES),
                'UTF-8',
                'SJIS-win'
            )
        );
        $file = new \SplFileObject($converted_csv_path);
        $file->setFlags(\SplFileObject::READ_CSV);

        if (!empty($file)) {
            foreach ($file as $row) {
                if (!empty($row) && count($row) > 6) {
                    print_r($row);
                    ZipCode::create([
                        'zip_code' => $row[0],
                        'prefecture_ja' => $row[1],
                        'address_ja' => $row[2],
                        'street_address_ja' => $row[3],
                        'prefecture_en' => ucfirst($row[4]),
                        'address_en' => ucfirst($row[5]),
                        'street_address_en' => ucfirst($row[6]),
                    ]);
                } else {
                    $this->info(sprintf('Upload Success'));
                    return false;
                }

            }
        } else {
            $this->error(sprintf('There is some Error please Check your file'));
            return false;
        }

        /*
 //        $csv_path = storage_path('app/csv/KEN_ALL_pld.CSV');
         $csv_path = base_path('master').'/KEN_ALL_ROME.csv';
 //        $converted_csv_path = storage_path('app/csv/postal_code_utf8.csv');
         $converted_csv_path = base_path('master').'/ZIP_CODE_DATA_UTF8.csv';
 //        file_put_contents(
 //            $converted_csv_path,
 //            mb_convert_encoding(
 //                file_get_contents($csv_path),
 //                'UTF-8',
 //                "SJIS-win"
 //            )
 //        );

         $file = new \SplFileObject($csv_path);
         $file->setFlags(\SplFileObject::READ_CSV);
         $encoding = "sjis-win";

         foreach ($file as $row) {
             $row[2] = preg_replace("@[ ?]@u", '',urldecode(mb_convert_encoding($row[2], 'UTF-8', $encoding)));
             $row[3] = preg_replace("@[ ?]@u", '',urldecode(mb_convert_encoding($row[3], 'UTF-8', $encoding)));
             $row[2] = str_replace('@','',$row[2]);
             ZipCode::create([
                 'zip_code' => intval($row[0]),
                 'prefecture_ja' => preg_replace("@[ ?]@u", ' ',mb_convert_encoding($row[1], 'UTF-8', $encoding)),
                 'address_ja' => $row[2],
                 'street_address_ja' => (str_contains($row[3], '(')) ? current(explode('(', $row[3])) : $row[3],
                 'prefecture_en' => $row[4],
                 'address_en' => $row[5],
                 'street_address_en' => (str_contains($row[6], '(')) ? current(explode('(', $row[6])) : $row[6]
             ]);
 //            die;
 //            ZipCode::create([
 //                'zip_code' => intval($row[0]),
 //                'prefecture_ja' => $row[1],
 //                'address_ja' => $row[2],
 //                'street_address_ja' => (str_contains($row[3], '?')) ? current(explode('?', $row[3])) : $row[3],
 //                'prefecture_en' => $row[4],
 //                'address_en' => $row[5],
 //                'street_address_en' => (str_contains($row[6], '?')) ? current(explode('?', $row[6])) : $row[6]
 //            ]);

         }
        */
    }
}
