<?php

namespace App\Console\Commands;

use App\Models\Provider;
use App\Models\ProviderTransaction;
use App\Models\Sequence;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CheckProviderPointStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:providerpointstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check provider last point purchase date and make remaining 0 if before 1 year.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $expiry_days = config('app.point_expiry_days');

        $last_date = Carbon::today()->subDays($expiry_days-1);

        Provider::where('point_balance','>',0)
            ->where('purchased_on','<',$last_date)
            ->orderBy('id')->chunk(100, function ($providers) {
            foreach ($providers as $provider) {
                $provider->lockForUpdate();
                ProviderTransaction::create([
                    'points' => $provider->point_balance,
                    'amount' => $provider->point_balance,
                    'provider_id' => $provider->id,
                    'foreign_id' => $provider->id,
                    'foreign_table' => 'providers',
                    'transaction_type' => 3, //freezed or subtracted
                    'charge_type' => 5, //freezed charge type
                    'paid_status' => 0, //pending
                ]);
                $provider->point_balance = 0;
                $provider->save();
            }
        });
    }
}
