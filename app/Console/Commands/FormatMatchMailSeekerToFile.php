<?php

namespace App\Console\Commands;

use Exception;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Notifications\ExceptionCaught;
use Illuminate\Support\Facades\Storage;

class FormatMatchMailSeekerToFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'matchedseeker:file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan database and format users data to json file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $record = DB::table('job_notify_users')->where('notified', 0)->get()->toArray();
            $seekers_to_mail = [];
            if (count($record) > 0) {
                foreach ($record as $data) {
                    $seekers = explode(',', $data->seeker_ids);
                    foreach ($seekers as $seeker) {
                        if (isset($seekers_to_mail[$seeker])) {
                            if (!in_array($data->job_id, $seekers_to_mail[$seeker])) {
                                $seekers_to_mail[$seeker][] = $data->job_id;
                            }
                        } else {
                            $seekers_to_mail[$seeker][] = $data->job_id;
                        }

                    }
                    DB::table('job_notify_users')->where('id', $data->id)->update(['notified' => 1]);
                }
            }
            if ($seekers_to_mail) {
                $file_name = time() . '.json';
                Storage::disk('s3_private')->put('match-seekers-mail/' . $file_name, json_encode($seekers_to_mail));
                //Storage::disk('local')->put('match-seekers-mail/' . $file_name, json_encode($seekers_to_mail));
                DB::table('file_to_queue_tracker')->insert(
                    [
                        'file_name' => $file_name,
                        'processed' => 0
                    ]
                );
            }
        } catch (Exception $e) {
            $err_message = $e->getMessage();
            $admins = User::whereIn('email',\Config::get('app.admin_exception_mail_list'))->get();
            \Notification::send($admins, new ExceptionCaught($err_message, '/commands/formatmatchmailseekertofile'));
            report($e);
            return false;
        }
    }
}
