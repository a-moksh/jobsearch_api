<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\ModelMakeCommand',
        'App\Console\Commands\RequestMakeCommand',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('matchedseeker:file')->timezone('Asia/Tokyo')->dailyAt('20:00');
        //$schedule->command('matchedseeker:file')->everyMinute();
        $schedule->command('matchedseeker:queue')->timezone('Asia/Tokyo')->everyFiveMinutes()->between('21:00', '24:00');
        //$schedule->command('matchedseeker:queue')->everyFiveMinutes();

        //jobs expire check and queue
        $schedule->command('jobs:toExpire')->twiceDaily(1, 13);
        $schedule->command('check:paidstatus')->twiceDaily(1, 13);
        $schedule->command('check:providerpointstatus')->twiceDaily(1, 13);

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
