<?php

namespace App\Services;

use App\Models\Attribute;

class AttributeService
{
    protected $attributes = [];

    public function get($group)
    {
        if (!empty($group)) {
            return array_column($this->getRaw($group)->toArray(), 'name', 'value');
        }
    }

    public function getValues($group)
    {
        return array_keys($this->get($group));
    }

    public function getRaw($group)
    {
        if (!is_string($group)) {
            throw new \Exception('Please specify a character string');
        }
        if (empty($group)) {
            throw new \Exception('Empty string is not allowed');
        }
        if (!isset($this->attributes[$group])) {
            $this->attributes[$group] = [];
            if (!empty($group)) {
                $query = Attribute::query();
                $query->whereHas('group', function ($query) use ($group) {
                    $query->where('name', $group);
                });
                $this->attributes[$group] = $query->get();
            }
        }
        return $this->attributes[$group];
    }
}
