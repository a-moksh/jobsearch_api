<?php

namespace App\Services;

use App\Models\Language;

class TranslationService
{
    protected static $retrieved = [];

    protected static $completed = [];

    protected static $languages;

    /**
     * Translate the language data associated with the model.
     *
     * @param \Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection $model
     * @param string|array $locales
     * @return mixed
     */
    public static function translate($models, $locales = null)
    {
        $locales = $locales ? (array)$locales : (array)app()->getLocale();
        $invalid = array_diff($locales, config('app.defined_locales'));
        if (!empty($invalid)) {
            abort(500, sprintf('Locale is not allowed [%s]', implode(',', $invalid)));
        }
        if (!empty($locales)) {
            static::acceptLanguage($models, $locales);
        }

        return $models;
    }

    /**
     * Create a model and register a translation.
     *
     * @param array $values
     * @return \Illuminate\Database\Eloquent\Model
     */
    public static function create($className, $values, $locales = null)
    {
        if ($className !== Language::class) {
            $model = $className::make();
            $fillableTranslation = $model->getFillableTranslation();
            $fallbackLocale = config('app.fallback_locale');
            $original = $values;

            foreach ($fillableTranslation as $name) {
                if (isset($values[$name]) && is_array($values[$name])) {
                    if (isset($values[$name][$fallbackLocale])) {
                        $values[$name] = $values[$name][$fallbackLocale];
                    }
                }
            }

            $model = $className::create($values);
            $model->fill($original);
            return static::save($model, $locales);
        }
    }

    /**
     * Save model and languages
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $locale
     */
    public static function save($model, $locales = null)
    {
        $fallbackLocale = config('app.fallback_locale');
        $locales = $locales ? (array)$locales : (array)$fallbackLocale;
        $invalid = array_diff($locales, config('app.defined_locales'));
        $table = $model->getTable();
        $key = $model->getKey();
        $attributes = $model->getAttributes();
        $fillableTranslation = $model->getFillableTranslation();

        if (!empty($invalid)) {
            abort(500, sprintf('Locale is not allowed [%s]', implode(',', $invalid)));
        }
        if (empty($fillableTranslation)) {
            abort(500, sprintf('Add keys to fillableTranslation property to allow mass assignment on [%s].',
                get_class($model)));
        }

        foreach ($fillableTranslation as $name) {
            if (count($locales) > 0) {
                if (is_array($model->{$name})) {
                    foreach ($locales as $locale) {
                        if (isset($model->{$name}[$locale])) {
                            static::saveFieldValue($table, $key, $name, $model->{$name}[$locale], $locale);
                        }
                    }
                } else {
                    static::saveFieldValue($table, $key, $name, $model->{$name}, $fallbackLocale);
                }
            } else {
                static::saveFieldValue($table, $key, $name, $model->{$name}, array_first($locales));
            }
        }

        if (count($locales) > 0) {
            if (in_array($fallbackLocale, $locales)) {
                foreach ($fillableTranslation as $name) {
                    if (isset($model->{$name}[$fallbackLocale])) {
                        $model->{$name} = $model->{$name}[$fallbackLocale];
                    }
                }
            } else {
                foreach ($fillableTranslation as $name) {
                    $model->{$name} = $model->getOriginal();
                }
            }
        }

        if (!$model->wasRecentlyCreated) {
            $model->save();
        }

        static::addRetrieved($model);

        return $model->fill($attributes);
    }

    /**
     * Save translation field value
     *
     * @param $table
     * @param $key
     * @param $name
     * @param $value
     * @param $locale
     * @return mixed
     */
    protected static function saveFieldValue($table, $key, $name, $value, $locale)
    {
        return Language::updateOrCreate([
            'table_name' => $table,
            'foreign_id' => $key,
            'column_name' => $name,
            'locale_code' => $locale,
        ], [
            'locale_text' => $value
        ]);
    }

    protected static function isAllowedModel($model)
    {
        if ($model !== null) {
            return get_class($model) !== Language::class;
        }
        return false;
    }

    /**
     * Delete language data associated with the model.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public static function delete($model)
    {
        if (static::isAllowedModel($model)) {
            $table = $model->getTable();
            $key = $model->getKey();
            $languages = Language::where('table_name', $table)->where('foreign_id', $key)->get();
            $ids = $languages->pluck('id');
            if (!empty($ids)) {
                Language::destroy($ids);
            }
        }

        return $model;
    }

    /**
     * Add the retrieved
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public static function addRetrieved($model)
    {
        if (static::isAllowedModel($model)) {
            $key = [
                $model->getTable(),
                $model->getKey(),
            ];
            if (!in_array($key, static::$retrieved)) {
                static::$retrieved[] = $key;
            }
        }
    }

    /**
     * Get the retrieved
     *
     * @return array
     */
    protected static function getRetrieved()
    {
        return static::$retrieved;
    }

    /**
     *
     * Accept language.
     *
     * @param \Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection $model
     * @param string|array $locales
     * @return mixed
     */
    protected static function acceptLanguage($models, $locales)
    {
        if ($models instanceof \Illuminate\Support\Collection) {
            foreach ($models as $model) {
                static::acceptLanguageModel($model, $locales);
            }
        } else {
            static::acceptLanguageModel($models, $locales);
        }

        return $models;
    }

    /**
     * Accept language in model.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string|array $locales
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected static function acceptLanguageModel($model, $locales)
    {
        if (static::isAllowedModel($model)) {
            $fillableTranslation = $model->getFillableTranslation();
            if (!empty($fillableTranslation)) {
                $languages = static::getLanguages()
                    ->where('table_name', $model->getTable())
                    ->where('foreign_id', $model->getKey());

                foreach ($fillableTranslation as $name) {
                    if (count($locales) > 1) {
                        $localeValues = [];
                        foreach ($locales as $locale) {
                            $language = $languages
                                ->where('column_name', $name)
                                ->where('locale_code', $locale)->first();
                            if ($language) {
                                $localeValues[$locale] = $language->locale_text;
                            } else {
                                if ($locale === config('app.fallback_locale')) {
                                    $localeValues[$locale] = $model->getOriginal($name);
                                } else {
                                    $localeValues[$locale] = null;
                                }
                            }
                        }
                        $model->{$name} = $localeValues;
                    } else {
                        $locale = array_first($locales);
                        $language = $languages
                            ->where('column_name', $name)
                            ->where('locale_code', $locale)->first();
                        if ($language) {
                            $model->{$name} = $language->locale_text;
                        } else {
                            if ($locale === config('app.fallback_locale')) {
                                $model->{$name} = $model->getOriginal($name);
                            } else {
                                $model->{$name} = null;
                            }
                        }
                    }
                }
            }

            $relations = $model->getRelations();
            if ($relations) {
                foreach ($relations as $relation) {
                    static::acceptLanguage($relation, $locales);
                }
            }
        }

        return $model;
    }

    /**
     * Get language data.
     * The result of this function is cached in memory.
     *
     * @param null $locale
     * @return \Illuminate\Support\Collection
     */
    protected static function getLanguages()
    {
        if (!isset(static::$languages)) {
            static::$languages = collect([]);
        }

        $retrieved = static::getRetrieved();
        if (!empty($retrieved)) {
            $languages = Language::whereInMultiple(['table_name', 'foreign_id'], $retrieved)->get();
            static::$languages = static::$languages->merge($languages)->keyBy('id');
            static::$retrieved = [];
        }

        return static::$languages;
    }
}
