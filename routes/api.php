<?php

//use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*Route::group(['middleware'=>'localization','prefix'=>'v1'],function() {
    Route::group(['prefix'=>'user'],function(){
        Route::post('login/admin', 'API\Auth\LoginController@loginAdmin');  
    });
});

Route::group(['middleware'=>['localization','throttle:60,1'],'prefix'=>'v1'],function() {
    Route::group(['prefix'=>'user'],function(){
        Route::post('register/admin', 'API\Admin\AdminUserController@store');
        Route::post('verify/email/provider','API\Provider\ProviderUserController@verifyProviderEmail');
        Route::post('verify/email/admin','API\Admin\AdminUserController@verifyAdminEmail');
    });

    Route::group(['middleware'=>['tokenLogin']],function(){



        Route::apiResources([
            'jobs'=>'API\Job\JobController',
        ]);
        //Route::get('job/apply/check/{job}', 'API\Job\ApplicationController@check');
        Route::get('video/{key}', 'API\ImageController@streamVideo')->where('key', '.*');

       // Route::get('images/{key}', 'API\ImageController@index')->where('key', '.*');
    });

    Route::group(['middleware'=>['auth:api']],function() {
        //Route::post('logout', 'API\Auth\LoginController@logout');
        //Route::get('user', 'API\UserController@getUser');



        Route::apiResources([
            'providers' => 'API\Provider\ProviderController',
            'users/providers' => 'API\Provider\ProviderUserController',
            'users/admins' => 'API\Admin\AdminUserController',
            //'seeker-mail-settings' => 'API\Seeker\MailSettingController',
            //'wishlists' => 'API\Seeker\WishlistController',
//            'scout-mails' => 'API\Provider\ScoutMailController',
        ]);



        Route::delete('job/languages/{language}', 'API\Job\LanguageController@destroy');
        Route::delete('job/employment-types/{employmentType}', 'API\Job\EmploymentTypeController@destroy');
        Route::delete('job/locations/{location}', 'API\Job\LocationController@destroy');

        //job application routes for seeker
        //Route::post('job/apply', 'API\Job\ApplicationController@apply');


        //Route::post('wishlists/job', 'API\Seeker\WishlistController@wishlistJob');



        //Route::post('images/user', 'API\ImageController@image');
        //Route::post('job/apply/video', 'API\ImageController@jobApplyVideo');
    });
});*/

//Route::get('v1/langs/{locale}/{key?}', 'LangController')->where('key', '.*');
