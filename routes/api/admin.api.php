<?php
Route::name('admin.')->group(function () {

    Route::group(['middleware'=>'localization','prefix'=>'v1/admin'],function() {
        Route::post('login', 'API\Auth\LoginController@loginAdmin')->name('login'); 
        Route::post('register', 'API\Admin\AdminUserController@store')->name('register');
        Route::post('verify/email','API\Admin\AdminUserController@verifyAdminEmail')->name('verifyEmail');

        Route::group(['middleware'=>['auth:api','isAdmin']],function(){
            Route::get('contact', 'API\ContactUs@index')->name('get contact');
            Route::get('contact/{contact_us}', 'API\ContactUs@show')->name('get contact detail');
            Route::put('contact/{contact_us}', 'API\ContactUs@update')->name('update contact detail');
            Route::get('user', 'API\UserController@getUser')->name('getseekerUser');
            Route::get('seekers', 'API\Seeker\SeekerController@seekerListAdmin')->name('getseekerUser');
            Route::get('providers', 'API\Provider\ProviderController@index')->name('getProviderUser');
            Route::get('users', 'API\UserController@index')->name('getUser');
            //Route::get('images/{key}', 'API\ImageController@index')->where('key', '.*')->name('fetchImage');
            Route::delete('users/{user}','API\Admin\AdminUserController@destroy')->name('deleteAdmin');
            Route::put('users/{user}','API\Admin\AdminUserController@update')->name('updateAdmin');
            //image
            //Route::get('images/{key}', 'API\ImageController@indexAdmin')->where('key', '.*')->name('fetchImage');

            //Route::delete('images/{media}', 'API\ImageController@destroy');
            Route::post('jobs/{id}', 'API\Job\JobController@update');
        });
    });
    
});