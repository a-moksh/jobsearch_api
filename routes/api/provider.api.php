<?php
Route::name('provider.')->group(function () {
    Route::group(['middleware'=>'localization','prefix'=>'v1/provider'],function() {

        ///provider unauthenticated routes
        Route::group(['middleware'=>['isLanding']],function(){
            Route::post('login', 'API\Auth\LoginController@loginProvider')->name('loginProvider');  
        });
        
        Route::post('register', 'API\Provider\ProviderUserController@store')->name('registerProvider');
        Route::post('verify/email','API\Provider\ProviderUserController@verifyProviderEmail')->name('verifyProviderEmail');

        //seeker auth and itself seeker check
        Route::group(['middleware'=>['isLanding','auth:api','isProvider']],function(){
            Route::get('user', 'API\UserController@getProviderUser')->name('getproviderUser');

            //application routes
            Route::get('job/applications', 'API\Job\ApplicationController@index');
            Route::post('job/applications/status/{id}', 'API\Job\ApplicationController@updateStatus');
            Route::get('job/applications/{id}', 'API\Job\ApplicationController@show');

            //job routes
            Route::get('jobs/title', 'API\Job\JobController@titles');
            Route::get('jobs', 'API\Job\JobController@providerIndex');
            Route::get('jobs/{id}', 'API\Job\JobController@providerShow');
            Route::post('jobs/{id}', 'API\Job\JobController@updateProvider');
            Route::post('jobs/renew/{id}', 'API\Job\JobController@renewJob');
            Route::post('jobs', 'API\Job\JobController@store');

            //points routes
            Route::get('points', 'API\Provider\PointController@index');
            Route::get('points/balance', 'API\Provider\PointController@getProviderBalance');
            Route::get('points/purchase/rates', 'API\Provider\PointController@getPurchaseRates');
            Route::get('points/spend/rates', 'API\Provider\PointController@getSpendRates');
            Route::post('points/purchase', 'API\Provider\PointController@purchasePoints');
            Route::post('points/seeker/open', 'API\Provider\PointController@openSeeker');
            Route::post('points/application/open', 'API\Provider\PointController@openApplication');

            // favourite resource
            Route::delete('favourites/bulk', 'API\Provider\FavouriteController@bulkDelete');//
            Route::apiResources([
                'favourites' => 'API\Provider\FavouriteController',
            ]);


            //seekers
            Route::get('seekers', 'API\Seeker\SeekerController@index');
            Route::get('seekers/{seeker}', 'API\Seeker\SeekerController@showProvider');

            //scoutmails
            Route::post('scout-mails/create', 'API\Provider\ScoutMailController@createMail');
            Route::get('scout-mails', 'API\Provider\ScoutMailController@listMails');
            Route::post('scout-mails', 'API\Provider\ScoutMailController@store');
            Route::get('scout-mails/list', 'API\Provider\ScoutMailController@showMails');
            Route::get('scout-mails/{scoutMail}', 'API\Provider\ScoutMailController@showMail');
            Route::post('scout-mails/{scoutMail}', 'API\Provider\ScoutMailController@updateProvider');
            Route::delete('scout-mails/bulk', 'API\Provider\ScoutMailController@bulkDelete');

            //read status 
            Route::put('scout-mails/read/{scoutMail}','API\Provider\ScoutMailController@changeReadStatus')->name('read message status');

            //provider
            Route::post('providers/syncpaidjp', 'API\Provider\ProviderUserController@syncPaidJp');
            Route::post('providers/checkpaidjp', 'API\Provider\ProviderUserController@checkPaidVerification');
            Route::get('providers/{provider}', 'API\Provider\ProviderController@show');//
            Route::put('providers/{provider}', 'API\Provider\ProviderController@update');
            Route::delete('providers/{provider}', 'API\Provider\ProviderController@destroy');//
            //google translation controller
            Route::post('job/translations','API\GoogleTranslateController@getTranslation');
        });
    });
});
