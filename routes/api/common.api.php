<?php 
Route::name('common.')->group(function () {
    Route::group(['middleware'=>['localization','throttle:60,1'],'prefix'=>'v1'],function() {

        Route::group(['middleware'=>['isLanding']],function(){
            Route::get('videos/{key}', 'API\VideoController@index')->where('key', '.*');
        });

        Route::post('password/forgot','API\Auth\ForgotPasswordController@sendResetLinkEmail')->name('requestForgotPassword');
        Route::post('password/reset','API\Auth\ResetPasswordController@reset')->name('passwordReset');
        Route::post('logout', 'API\Auth\LoginController@logout')->name('logout');
        // @todo Get HLS manifest file testing
        Route::get('attributes','API\AttributeController@index');
        Route::get('attribute-groups','API\AttributeGroupController@index');
        Route::post('contact-us','API\ContactUs@store')->name('store contact us detail');

        Route::get('zipcode/{zipcode}', 'ZipCodeController')->name('getZipcode');
        Route::post('verify/email/pre','API\UserController@getUserFromEmailToken')->name('fetchUserEmailFromToken');
        

        //notification routes
        Route::group(['middleware'=>['auth:api']],function(){
            Route::get('user/notifications', 'API\NotificationController@index')->name('getUserNotifications');
            Route::post('user/notifications/{notification}', 'API\NotificationController@markRead')->name('MarkUserNotifications');
            Route::get('media/pre-signed-url','API\ImageController@getPreSignedUrlPublic')->name('PresignedUrlMediaUpload');
        });

        Route::get('langs/{locale}/{key?}', 'LangController')->where('key', '.*')->name('language');

        Route::group(['middleware'=>['tokenLogin']],function(){
            Route::get('jobs','API\Job\JobController@index')->name('getJobList');
            Route::get('jobs/{slug}','API\Job\JobController@show')->name('getJobDetail');
        });


        //lamda request image and video endpoints
        Route::post('request-from-lamba','API\ImageController@lamdaRequestVideo')->name('request from lamda video');
        Route::post('request-from-lamba-image','API\ImageController@lamdaRequestImage')->name('request from lamda image');



        //TODO::remove this after testing below this
        /**
        Route::get('/dispatch-mail-setting-job','API\Job\JobController@dispatchMailJob')->name('dispatch mail setting job');
        Route::get('/format-to-file','API\Job\JobController@formatDataAndSaveToFile')->name('dispatch mail setting job');
        Route::get('/file-to-queue','API\Job\JobController@processFileToQueue')->name('dispatch mail setting job');
        Route::get('/test-real-time/{user}',function(\App\Models\User $user){
            $user->notify(new \App\Notifications\TestNotification());
        });
        Route::get('/check-image-exists','API\ImageController@testExists');
         */
        //TODO::remove this after testing above this

    });
});
