<?php
Route::name('seeker.')->group(function () {

    Route::group(['middleware'=>'localization','prefix'=>'v1/seeker'],function() {
        //seeker unauthenticated routes 
        Route::post('register', 'API\Seeker\SeekerUserController@store')->name('register');
        Route::post('verify/email','API\Seeker\SeekerUserController@verifyUserEmail')->name('verifyEmail');
        
        Route::group(['middleware'=>['isLanding']],function(){
            Route::post('login', 'API\Auth\LoginController@login')->name('login'); 
        });
        //seeker auth and itself seeker check
        Route::group(['middleware'=>['isLanding','auth:api','isSeeker']],function(){
            Route::get('user', 'API\UserController@getUser')->name('getseekerUser');
            Route::put('seeker-user/{seeker_user}','API\Seeker\SeekerUserController@update')->name('updateSeekerUser');
            
            //seeker-skills actions update and delete only
            Route::put('seeker-skills-bulk', 'API\Seeker\SkillController@bulk')->name('updateSeekerSkills');
            Route::delete('seeker-skills/{seekerSkill}', 'API\Seeker\SkillController@destroy')->name('deleteSeekerSkills');

            //seeker-certificate actions update and delete only
            Route::put('seeker-certificates-bulk', 'API\Seeker\CertificateController@bulk')->name('updateSeekerCertificates');;
            Route::delete('seeker-certificates/{seekerCertificate}', 'API\Seeker\CertificateController@destroy')->name('deleteSeekerCertificates');

            //seeker-certificate actions update and delete only
            Route::put('seeker-experiences-bulk', 'API\Seeker\ExperienceController@bulk')->name('updateSeekerExperiences');
            Route::delete('seeker-experiences/{seekerExperience}', 'API\Seeker\ExperienceController@destroy')->name('deleteSeekerExperience');
            
            //seeker-educations actions update and delete only
            Route::put('seeker-educations-bulk', 'API\Seeker\EducationController@bulk')->name('updateSeekerEducations');
            Route::delete('seeker-educations/{seekerEducation}', 'API\Seeker\EducationController@destroy')->name('deleteSeekerEducation');
            
            //seeker specific seeker table apis
            Route::put('seekers/{seeker}','API\Seeker\SeekerController@update')->name('seekerUpdate');
            Route::get('seekers/{seeker}','API\Seeker\SeekerController@show')->name('seekerShow');
            

            //jobs related apis for seeker
            Route::post('wishlists/job', 'API\Seeker\WishlistController@wishlistJob')->name('addRemoveWishlist');
            Route::get('wishlists','API\Seeker\WishlistController@index')->name('getWishlistedJob');

            Route::post('job/apply', 'API\Job\ApplicationController@apply')->name('applyJob');
            Route::post('job/apply/pre-check', 'API\Job\ApplicationController@preCheckValidation')->name('precheckValidation');
            Route::get('job/applications', 'API\Job\ApplicationController@seekerJobApplication')->name('getApplications');


            //scout and messages apis
            Route::get('scout-mails','API\Provider\ScoutMailController@index')->name('getScoutMessages');

            Route::put('scout-mails/{scoutMail}','API\Provider\ScoutMailController@update')->name('replyScoutMessages');

            Route::put('scout-mails/read/{scoutMail}','API\Provider\ScoutMailController@changeReadStatus')->name('read message status');
            Route::get('scout-mails/{scoutMail}','API\Provider\ScoutMailController@show')->name('scoutMessageDetail');

            Route::put('seeker-mail-settings/{seekerMailSetting}','API\Seeker\MailSettingController@update')->name('updateSettings');
            
            Route::get('media/pre-signed-url','API\ImageController@getPreSignedUrlPrivate')->name('PresignedUrlMediaUpload');

            //delete account
            Route::post('delete/{user}', 'API\Seeker\SeekerUserController@deleteAccount')->name('delete account seeker');

        });
        Route::group(['middleware'=>['isLanding','tokenLogin']],function(){
            Route::get('job/apply/check/{job}', 'API\Job\ApplicationController@check')->name('checkIfJobApplied');
            Route::get('images/{key}', 'API\ImageController@index')->middleware('seekerProfileImageAuthorize')->where('key', '.*')->name('fetchImage');
        });
    });
});