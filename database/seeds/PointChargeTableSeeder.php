<?php

use Illuminate\Database\Seeder;

class PointChargeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Point::updateOrCreate(
            ['point' => 10000],
            [
                'amount'=>10000,
                'description'=>'¥ 10,000'
            ]);
        \App\Models\Point::updateOrCreate(
            ['point' => 30000],
            [
                'amount'=>30000,
                'description'=>'¥ 30,000',
            ]
        );
        \App\Models\Point::updateOrCreate(
            ['point' => 50000],
            [
                'amount'=>50000,
                'description'=>'¥ 50,000',
            ]
        );
        \App\Models\Point::updateOrCreate(
            ['point' => 100000],
            [
                'amount'=>100000,
                'description'=>'¥ 100,000',
            ]
        );
        \App\Models\Point::updateOrCreate(
            ['point' => 200000],
            [
                'amount'=>200000,
                'description'=>'¥ 200,000',
            ]
        );

        //charge begins
        \App\Models\Charge::updateOrCreate(
            ['type' => 'seeker_jobs'],
            [
                'point'=>5000,
            ]
        );
        \App\Models\Charge::updateOrCreate(
            ['type' => 'seekers'],
            [
                'point'=>5000,
            ]
        );
        \App\Models\Charge::updateOrCreate(
            ['type' => 'scout_mails'],
            [
                'point'=>5000,
            ]
        );
        \App\Models\Charge::updateOrCreate(
            ['type' => 'translation'],
            [
                'point'=>5000,
            ]
        );
    }
}
