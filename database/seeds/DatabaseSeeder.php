<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(JobBankDatabaseSeeder::class);
        $this->call(PointChargeTableSeeder::class);

//        $this->seedProviderTransactions();
    }


    public function seedProviderTransactions(){
        $charge_type = [0,1,2,3,4];
        $points = [5000,1000,2000,3000,4000];
        $transaction_type = [1,2];
        $provider_id = 1;
        $i = 0;
        while($i<30000){
            //purchase
            \App\Models\ProviderTransaction::create(
                [
                    'provider_id'=>$provider_id,
                    'amount'=>5000,
                    'transaction_type'=>1,
                    'points'=>5000,
                    'foreign_table'=>'providers',
                    'foreign_id'=>$provider_id,
                    'charge_type'=>3,
                    'paid_status'=>1,
                ]);
            $i++;
        }

        while($i<40000){

            \App\Models\ProviderTransaction::create(
                [
                    'provider_id'=>$provider_id,
                    'amount'=>5000,
                    'transaction_type'=>2,
                    'points'=>5000,
                    'foreign_table'=>'seeker_jobs',
                    'foreign_id'=>4,
                    'charge_type'=>2,
                    'paid_status'=>1,
                ]);
            $i++;
        }

        while($i<25000){

            \App\Models\ProviderTransaction::create(
                [
                    'provider_id'=>$provider_id,
                    'amount'=>5000,
                    'transaction_type'=>2,
                    'points'=>5000,
                    'foreign_table'=>'seekers',
                    'foreign_id'=>44,
                    'charge_type'=>0,
                    'paid_status'=>1,
                ]);
            $i++;
        }

        while($i<20000){

            \App\Models\ProviderTransaction::create(
                [
                    'provider_id'=>$provider_id,
                    'amount'=>5000,
                    'transaction_type'=>2,
                    'points'=>5000,
                    'foreign_table'=>'jobs',
                    'foreign_id'=>162,
                    'charge_type'=>1,
                    'paid_status'=>1,
                ]);
            $i++;
        }

    }
}
