<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;
class JobBankDatabaseSeeder extends Seeder
{

    //TODO::DISPATCH NOTIFICATION ON RELATED ACTIONS
    //job application
    private function _construct(){
        $this->seekers = [];
        $this->providers = [];
        $this->jobs = [];
        parent::construct();
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $start_time = time();
//        DB::beginTransaction();
        $this->users();
        //print_r($this->providers);
        //print_r($this->seekers);
        //print_r($this->jobs);

        $this->seekerWishlist($this->seekers, $this->jobs, $this->providers);
        $this->providerWishlist($this->providers , $this->seekers);
        $this->seekerApplication($this->seekers, $this->jobs);

        $end_time = time();

        echo 'Start Time = '.$start_time.' // ';
        echo 'End Time = '.$end_time.' // ';
        echo 'Duration = '. ($end_time - $start_time);
    }

    /**
     * 
     */
    public function users(){
        //User table seeder
        $count = DB::table('users')->count() + 1;
        for( $i=$count; $i<=$count + Config::get('app.seed_volume'); $i++ ){
            $user = factory(App\Models\User::class)->create([
                'email'=>'user'.$i.'@yopmail.com',
                'user_type'=> ( $i % 2 == 0 ) ? 'provider' : 'seeker'
            ]);
            if($user->user_type == 'seeker'){
                $this->seeker($user);
            }

            if($user->user_type == 'provider'){
                $this->provider($user);
            }


        }

        if($count < 1){
            User::updateOrCreate(
                ['email' => 'jobbankadmin@yopmail.com'],
                [
                    'full_name'=>'Job Bank Admin',
                    'last_name'=>'',
                    'user_identifier_id'=>str_random(5),
                    'email' => 'jobbankadmin'.'@yopmail.com',
                    'password' => bcrypt('password123!@#'),
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> date('Y-m-d H:i:s'),
                    'user_type'=> 'admin',
                    'verified'=>1,
                    'deletable'=>0
                ]
            );
        }
       
    }

    /** 
     * 
     */
    public function seeker($user){
        $seeker = factory(App\Models\Seeker::class)->create([
            'user_id'=> $user->id
        ]);
        $this->seekers[] = $seeker->id;
        //SETTINGS
        $seeker_mail_setting = factory(App\Models\SeekerMailSetting::class)->create(['seeker_id'=>$seeker->id]);

        //EDUCATION
        $seeker_education = factory(App\Models\SeekerEducation::class,4)->create(['seeker_id'=>$seeker->id]);

        //EXPERIENCE
        $seeker_experience = factory(App\Models\SeekerExperience::class,3)->create(['seeker_id'=>$seeker->id]);

        //SKILLS
        $seeker_skill = factory(App\Models\SeekerSkill::class)->create(['seeker_id'=>$seeker->id]);

        //CERITFICATE
        $seeker_certificate = factory(App\Models\SeekerCertificate::class,3)->create(['seeker_id'=>$seeker->id]);

        //MEDIA
        $path = 'user_seed/';
        \App\Models\Media::create(
            [
                'foreign_table' => 'users',
                'foreign_id'=> $user->id,
                'media_type'=> 'image',
                'media_value'=>$path.rand(1,25).'.png'
            ]
        );
    }

    /**
     * 
     */
    public function provider($user){
        //make provider relation tables
        $provider = factory(App\Models\Provider::class)->create([
            'user_id'=> $user->id,
            'email'=>$user->email
        ]);
        $this->providers[] = $provider->id;
        $language_column_name = [
            'company_name',
            'ceo_name',
            'contact_person'
        ];
        $languages = Config::get('app.defined_locales');

        //PROVIDER LANGUAGE TABLE
        foreach($language_column_name as $lcn){
            foreach($languages as $lang){
                $provider_lang = factory(App\Models\Language::class)->create([
                    'table_name'=>'providers',
                    'foreign_id'=>$provider->id,
                    'column_name'=>$lcn,
                    'locale_code'=>$lang,
                    'locale_text'=> ($lang == 'en') ? \Faker\Factory::create('ja_US')->name : \Faker\Factory::create('ja_JP')->name
                ]);
            }
        }

        \App\Models\ProviderPaidjp::create([
            'provider_id'=>$provider->id,
            'provider_info'=>json_encode([
                'email'=>$provider->user->email,
                'company_name_kana'=>'????',
                'ceo_name_last_ja'=>substr(\Faker\Factory::create('ja_JP')->lastName,0,49),
                'ceo_name_first_ja'=>substr(\Faker\Factory::create('ja_JP')->firstName,0,49),
                'ceo_name_last_kana'=>'??????',
                'ceo_name_first_kana'=>'??????',
                'mansion_name'=>$provider->mansion_name,
                'contact_person_last_ja'=>substr(\Faker\Factory::create('ja_JP')->lastName,0,49),
                'contact_person_first_ja'=>substr(\Faker\Factory::create('ja_JP')->firstName,0,49),
                'contact_person_last_kana'=>'??????',
                'contact_person_first_kana'=>'??????',
                'url2'=>$provider->related_url,
                'url3'=>$provider->related_url,
                'mobile_tel'=>$provider->telephone,
                'branch_name'=>substr($provider->address,0,49),
                'payment_method'=>1,
                'closing_day'=>20,
            ])
        ]);

        $this->jobs($provider);
    }

    /** 
     * 
     */
    public function jobs($provider){
        $jobs = factory(App\Models\Job::class, 2)->create([
            'provider_id'=> $provider->id
        ]);
        
        //Now job related table maintaining
        $language_column_name = [
            'description',
            'qualifications',
            'welfare',
            'title'
        ];
        $languages = Config::get('app.defined_locales');

        
        foreach($jobs as $job){
            $this->jobs[] = $job->id;
            //NORMAL LANGUAGES
            foreach($language_column_name as $lcn){
                foreach($languages as $lang){
                    
                    if($lang == 'ja'){
                        $locale_text = ($lcn == 'title') ? \Faker\Factory::create('ja_JP')->name : \Faker\Factory::create('ja_JP')->text;
                    }else{
                        $locale_text = ($lcn == 'title') ? \Faker\Factory::create('en_US')->name : \Faker\Factory::create('en_US')->text;
                    }
                    $provider_lang = factory(App\Models\Language::class)->create([
                        'table_name'=>'jobs',
                        'foreign_id'=>$job->id,
                        'column_name'=>$lcn,
                        'locale_code'=>$lang,
                        'locale_text'=> $locale_text

                    ]);
                }
            }

            //JOB LANGUAGES - 3 languages for each job english, japanese and random another
            for($lang_count = 0;$lang_count<=2;$lang_count++){
                \App\Models\JobLanguage::create(
                    [
                        'job_id' => $job->id,
                        'language'=>($lang_count == 2) ? rand(3,100) : $lang_count,
                        'level'=> [0,1,2,3,4,100][array_rand([0,1,2,3,4,100])]
                    ]
                );
            }
            
            //LOCATIONS
            for($location_count = 0;$location_count<=1;$location_count++){
                \App\Models\JobLocation::create(
                    [
                        'job_id' => $job->id,
                        'address'=> \Faker\Factory::create('en_US')->address,
                        'street_address'=> \Faker\Factory::create('en_US')->streetAddress,
                        'zip_code'=>'2520318',
                        'prefecture_id'=>rand(0,11)
                    ]
                );
            }

            $path = 'job_seed/';
            //MEDIA
            \App\Models\Media::create(
                [
                    'foreign_table' => 'jobs',
                    'foreign_id'=> $job->id,
                    'media_type'=> 'image',
                    'media_value'=>$path.rand(1,21).'.jpg'
                ]
            );
        }
    }


    /**
     * 
     */
    public function seekerWishlist( $seekers, $jobs, $providers ){
        if(count($providers) > 0 && count($seekers) > 0){
            $job_count = count($jobs);
            foreach($seekers as $seeker){
                //insert 2 wishlist for each new created seeker
                \App\Models\SeekerWishlist::create(
                    [
                        'job_id' => $jobs[0],
                        'seeker_id'=>$seeker,
                    ]
                );
                \App\Models\SeekerWishlist::create(
                    [
                        'job_id' => ($job_count == 2) ? $jobs[1] : $jobs[rand(2,$job_count - 1)],
                        'seeker_id'=>$seeker,
                    ]
                );
                
            }
        }else{
            //this case might not happen in huge data insert 
            //can occur if there are limited number of data seeded and no provider users are created at that time
        }
        
    }

    /**
     * 
     */
    public function seekerApplication( $seekers, $jobs ){
        if(count($seekers) > 0 && count($jobs)>0){
            $seeker_index = count($seekers) - 1;
            $index_track = 0;
            foreach($jobs as $job){

                $user_data = $this->generateUserDataApplicationSeeker($seekers[$index_track]);

                \App\Models\SeekerJob::create(
                    [
                        'job_id' => $job,
                        'seeker_id'=>$seekers[$index_track],
                        'user_data'=> $user_data,
                        'status'=>0
                    ]
                );

                $index_track++;
                $index_track = ($index_track > $seeker_index) ? 0 : $index_track;
            }
        }
    }

    /**
     * 
     */
    public function providerWishlist( $providers, $seekers ){
        if(count($providers) > 0 && count($seekers) > 0){
            $seeker_count = count($seekers);
            foreach($providers as $provider){
                //insert 2 wishlist for each new created seeker
                try{
                    \App\Models\Favourite::create(
                        [
                            'seeker_id' => $seekers[0],
                            'provider_id'=>$provider,
                        ]
                    );
                    \App\Models\Favourite::create(
                        [
                            'seeker_id' => ($seeker_count == 2) ? $seekers[1] : $seekers[rand(2,$seeker_count - 1)],
                            'provider_id'=>$provider,
                        ]
                    );
                }
                catch(\Exception $e){
                    echo $e->getMessage();
                }

                
            }
        }else{
            //noting to do here
        }
    }

    /**
     * 
     */
    public function generateUserDataApplicationSeeker ( $seeker_id ){
        $video_path = 'video/job/application/';
        $video_key_arry = [
            '1/1552021084eDbdq','1/1552021499kI5zB','1/1552022247S7P0Q',
            '1/1552025181bcIfR','1/1552027543ah8x7','1/chec1k','1/check','7/552023694TXlyY'
            ,'12/1552351680ZSw2a'
        ];

        $video_key = $video_key_arry[array_rand($video_key_arry)];
        $video_url = $video_path.$video_key.'.m3u8';
        $seeker_data = \App\Models\Seeker::where('id',$seeker_id)->with(
            'user',
            'experiences',
            'educations',
            'skills',
            'certificates'
        )->first()->toArray();

        $media = \App\Models\Media::where('foreign_table','users')->where('foreign_id', $seeker_data['user']['id'])->first();
        //print_r($seeker_data);die;
        $user_data = array(
            'basicUpdate'=>array(
                'full_name'=>$seeker_data['user']['full_name'],
                'katakana_full_name'=>$seeker_data['user']['katakana_full_name'],
                'dob'=> date('d.m.Y',strtotime($seeker_data['user']['dob'])),
                'gender'=>$seeker_data['user']['gender'],
                'telephone'=>$seeker_data['user']['telephone'],
                'japanese_language_level'=>$seeker_data['japanese_language_level'],
                'japanese_writing_level'=>$seeker_data['japanese_writing_level'],
                'english_language_level'=>$seeker_data['english_language_level'],
                'country'=>$seeker_data['country'],
                'living_country'=>$seeker_data['living_country'],
                'visa_status'=>$seeker_data['visa_status'],
                'visa_issue_date'=>date("d.m.Y",strtotime($seeker_data['visa_issue_date'])),
                'visa_expiry_date'=>date("d.m.Y",strtotime($seeker_data['visa_issue_date'])),
                'state'=>$seeker_data['state'],
                'zip_code'=>$seeker_data['zip_code'],
                'prefecture_id'=>$seeker_data['prefecture_id'],
                'address'=>$seeker_data['address'],
                'street_address'=>$seeker_data['street_address'],
            ),
            'resumeUpdate'=>array(
                'willing_to_relocate'=>$seeker_data['willing_to_relocate'],
                'is_experienced'=>$seeker_data['is_experienced'],
                'experience'=>$seeker_data['experience'],
                'portfolio_url'=>$seeker_data['portfolio_url'],
                'qiita_id'=>$seeker_data['qiita_id'],
                'github_url'=>$seeker_data['github_url'],
            ),
            'experienceUpdate'=>array(
                'experiences'=>$seeker_data['experiences']
            ),
            'educationUpdate'=>array(
                'educations'=>$seeker_data['educations']
            ),
            'skillUpdate'=>array(
                'skills'=>$seeker_data['skills']
            ),
            'certificateUpdate'=>array(
                'certificates'=>$seeker_data['certificates']
            ),
            'email' => $seeker_data['user']['email'],
            'image' => $media->media_value,
            'video'=> $video_url
        );
        return json_encode($user_data);
    }
}
