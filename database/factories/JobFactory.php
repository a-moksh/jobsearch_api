<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Job::class, function (Faker $faker) {
    $category = [0,1,2,3,4];
    $time_period = [0,1,2];
    $employment_type = array_rand([0,1,2,3]);
    $bool = [0,1];
    $bool_rand = rand(0,1);
    $salary = $faker->numberBetween(1000,10000);
    $work_time = array_rand([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]);
    return [
        'category_id' => array_rand($category),
        'sub_category_id' => array_rand($bool),
        'title' => $faker->jobTitle,
        'min_salary' => $salary,
        'max_salary' => $faker->numberBetween($salary,$salary + 50000),
        'description' => $faker->paragraph(2),
        'qualifications' => $faker->paragraph(2),
        'welfare' => $faker->paragraph(2),
        'language_translation' => array_rand($bool),
        'working_hour_from' => $work_time,
        'working_hour_to' => $work_time + 3,
        'night_shift'=>array_rand($bool),
        'morning_shift'=>array_rand($bool),
        'visa_support'=>array_rand($bool),
        'overtime'=>array_rand($bool),
        'social_insurance'=>array_rand($bool),
        'bonus'=>array_rand($bool),
        'clothing_freedom'=>array_rand($bool),
        'housing_allowance'=>array_rand($bool),
        'with_uniform'=>array_rand($bool),
        'other_details'=>$faker->text,
        'slug' => $faker->unique()->name,
        'salary_type'=>array_rand([0,1,2,3]),
        'time_period'=>($employment_type != 0) ? array_rand($time_period) : NULL,
        'holiday'=>$bool_rand,
        'holiday_days'=>array_rand([0,1,2,3,4,5,6]),
        'short_term'=>array_rand($bool),
        'long_term'=>array_rand($bool),
        'admin_status' => 0, //1 for pending admin status
        'status' => 1,
        'views'=>rand(200,1000),
        'employment_type' => $employment_type,
        'remote'=>array_rand($bool),
        'company_service'=>array_rand($bool),
        'practical_experience'=>array_rand($bool),
        'job_identifier_id'=>'J-'.str_random(10),
        'published_at'=> Carbon::now()
    ];
});

$factory->define(App\Models\JobLanguage::class, function (Faker $faker) {
    $five = [0,1,2,3];
    return [
        'language' => array_rand($five),
        'level' => array_rand($five),
    ];
});

$factory->define(App\Models\JobLocation::class, function (Faker $faker) {
    $five = [0,1,2,3];
    $pref = [0,1,2,3,4];
    return [
        'prefecture_id' => array_rand($pref),
        'address' => $faker->address,
        'street_address' =>$faker->streetAddress,
        'zip_code' => 2520318,
    ];
});