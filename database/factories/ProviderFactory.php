<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Provider::class, function (Faker $faker) {
    $array = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19];
    $five = [0,1,2,3,4,5];
    $bool = [0,1];
    return [
        'related_url' => 'http://'.$faker->domainName,
        'provider_identifier_id'=>'P-'.str_random(10),
        'ceo_name'=>substr($faker->name,0,49),
        'email'=>$faker->companyEmail,
        'business_content'=>array_rand($array),
        'sub_business_content'=>array_rand($bool),
        'no_of_employee'=>array_rand($five),
        'no_of_branch'=>array_rand($five),
        'capital'=>rand(1000000,10000000),
        'telephone'=>'044-982-0686',
        'fax'=>'044-982-0687',
        'established_year'=>date('Y-m-d'),
        'contact_person' =>substr($faker->name,0,49),
        'company_name' =>substr($faker->company,0,49),
        'address'=>substr($faker->address,0,49),
        'about_company'=>$faker->text,
        'prefecture_id'=>array_rand($array),
        'street_address'=>substr($faker->streetAddress,0,49),
        'zip_code'=>2520318,
        'point_balance'=>0.00,
        'mansion_name'=>substr($faker->company,0,49)
    ];
});