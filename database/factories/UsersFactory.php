<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(App\Models\User::class, function (Faker $faker) {

    $user_type_rand = ['seeker','provider'][array_rand(['seeker','provider'])];
    $gender_rand = [1,2,3][array_rand([1,2,3])];
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('password123'),
        'remember_token' => str_random(10),
        'full_name'=> ($user_type_rand == 'seeker') ? $faker->firstName.' '.$faker->lastName : NULL,
        'katakana_full_name'=>($user_type_rand == 'seeker') ? 'カタカナナマ' : NULL,
        'user_identifier_id'=>'JBU-'.str_random(10),
        'dob'=>date('Y-m-d'),
        'gender'=> ($user_type_rand == 'seeker') ? $gender_rand : NULL,
        'telephone'=>1111111111,
        'mobile'=>1111111111,
        'verified'=>1,
        'created_at'=> date('Y-m-d H:i:s'),
        'last_login'=> date('Y-m-d H:i:s'),
        'updated_at'=> date('Y-m-d H:i:s'),
        'user_type'=> $user_type_rand,
        'receive_mail_language'=>['en','ja'][array_rand(['en','ja'])]
    ];
});