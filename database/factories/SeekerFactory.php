<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Seeker::class, function (Faker $faker) {
    $user_type = [0,1,2,3,4];
    $country = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    $country_rand = array_rand($country);
    $rand_index = array_rand($user_type);

    $boolean = rand(0,1);
    return [
        'seeker_identifier_id'=>'S-'.str_random(10),
        'portfolio_url' => 'http://'.$faker->domainName,
        'github_url' => 'http://github.com/'.str_random(5),
        'qiita_id'=>$faker->name,
        'japanese_language_level'=>array_rand($user_type),
        'english_language_level'=>array_rand($user_type),
        'japanese_writing_level'=>array_rand($user_type),
        'country'=> $country_rand,
        'living_country'=>$country_rand,
        'visa_status'=> ($country_rand != 1) ? array_rand($user_type) : NULL,
        'visa_issue_date'=> ($country_rand != 1) ? date('Y-m-d') : NULL,
        'visa_expiry_date'=> ($country_rand != 1) ? date('Y-m-d',strtotime("+10 month")) : NULL,
        'state'=>$faker->address,
        'prefecture_id'=> ($country_rand == 1) ? array_rand($country) : NULL,
        'street_address'=>$faker->streetAddress,
        'zip_code'=>($country_rand == 1) ? 2520318 : NULL,
        'is_experienced' => $boolean,
        'experience' => ($boolean == 1) ? rand(1,6) : 0,
        'willing_to_relocate'=>($country_rand == 1) ? rand(0,1) : NULL,
        'desired_job_category'=>1,
        'desired_annual_min_salary_range'=>2,
        'experience_year'=>3
    ];
});

$factory->define(App\Models\SeekerEducation::class, function (Faker $faker) {
    $dateint= rand(696016801,1519620789);
    $dates = date('D.M.Y',$dateint );
    $graduation_status= rand(0,2);
    return [
        'university_institute'=>$faker->company,
        'degree'=>rand(0,4),
        'faculty'=>$faker->name,
        'graduation_country'=>rand(1,244),
        'graduation_status'=>1,
        'graduation_year'=>($graduation_status != 0) ? $dates : NULL
    ];
});


$factory->define(App\Models\SeekerExperience::class, function (Faker $faker) {

    $dateint= rand(696016801,1519620789);
    $annual_salary = [1,2,3,4,5,6,7,8];
    $dates = date('D.M.Y',$dateint );
    $currently_working_here = rand(0,1);
    return [
        'company_name'=>$faker->company,
        'job_title'=>$faker->jobTitle,
        'job_from'=>$dates,
        'job_to'=>($currently_working_here == 0) ? Carbon::now()->addMonths(2) : NULL,
        'currently_working_here'=>$currently_working_here,
        'job_description'=>$faker->paragraph(1),
        'annual_salary'=>[1,2,3,4,5,6,7,8][array_rand($annual_salary)],
    ];
});


$factory->define(App\Models\SeekerSkill::class, function (Faker $faker) {
    $skills = [0,1,2,3,4,5,6,7,8,9];
    $level = [0,1,2];
    $skill_type = ['skill.database', 'skill.framework', 'skill.middleware', 'skill.design', 'skill.programming','operating_system','ms_office'];
   
    return [
        'skill_type'=>'skill.programming',
        'skill_id' => rand(0,1),
        'level' => array_rand($level),
    ];
});


$factory->define(App\Models\SeekerCertificate::class, function (Faker $faker) {
    $certificate = ['database', 'framework', 'middleware', 'design', 'programming'][array_rand(['database', 'framework', 'middleware', 'design', 'programming'])];
    $dateint= rand(696016801,1519620789);
    $dates = date('D.M.Y',$dateint );
    return [
        'certificate_name'=>$certificate,
        'year' => $dates,
    ];
});

$factory->define(App\Models\SeekerMailSetting::class, function (Faker $faker) {
    return [
        'receive_mail'=>rand(0,1)
    ];
});