<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Language::class, function (Faker $faker) {
    return [
        'table_name'=>'fakeTable',
        'foreign_id'=>rand(0,1),
        'column_name'=>'fakeColumn',
        'locale_code'=>Config::get('app.defined_locales')[array_rand(Config::get('app.defined_locales'))],
        'locale_text'=>'this is fake locale text'
    ];
});