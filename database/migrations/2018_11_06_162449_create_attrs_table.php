<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttrsTable extends Migration
{
// ALTER TABLE `service_attrs` ADD FULLTEXT( `label`) WITH PARSER NGRAM
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attrs', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('属性ID');
            $table->string('name', 128)->comment('属性名');
            $table->string('label', 128)->comment('属性ラベル');
            $table->string('data_type', 16)->comment('データタイプ');
            $table->string('default_value', 64)->nullable()->comment('初期値');
            $table->text('description')->nullable()->comment('説明');
            $table->unsignedInteger('display_order')->comment('表示順');
            $table->boolean('is_private')->default(0)->comment('非表示');
            $table->boolean('unique_in')->nullable()->default(1)->comment('ユニーク');
            $table->timestampWithOperators();
            $table->unique(['name', 'unique_in']);
        });

        DB::statement("ALTER TABLE attrs COMMENT '属性'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attrs', function (Blueprint $table) {
            //
        });
    }
}
