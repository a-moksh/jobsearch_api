<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileNameTracker201902281845 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_to_queue_tracker', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('primary id');
            $table->string('file_name')->comment('file_name');
            $table->boolean('processed')->default(0)->comment('track to processed or not');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_to_queue_tracker');
    }
}
