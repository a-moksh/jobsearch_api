<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Removecolumsprovider201904161318 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->dropColumn('hairstyle_freedom');
            $table->dropColumn('with_uniform');
            $table->dropColumn('posting_end_date');
        });

        Schema::table('job_locations', function (Blueprint $table) {
            $table->dropColumn('railway_name');
            $table->dropColumn('station_name');
            $table->dropColumn('station_distance');
            $table->dropColumn('walking_distance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->integer('hairstyle_freedom')->nullable()->comment('hairstyle');
            $table->integer('with_uniform')->nullable()->comment('with_uniform');
            $table->date('posting_end_date')->nullable()->comment('posting end date');
        });

        Schema::table('jobs_locations', function (Blueprint $table) {
            $table->string('railway_name')->nullable()->comment('rail way name');
            $table->string('station_name')->nullable()->comment('station name');
            $table->enum('station_distance',[0,1])->nullable()->comment('0 walking distance, 1 bus distance');
            $table->integer('walking_distance')->nullable()->comment('walking distance');
        });
    }
}
