<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactUsTable201904111235 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_us', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name',255)->comment('contact us full name');;
            $table->string('email',255)->comment('contact us email');;
            $table->string('mobile')->nullable()->comment('contact us mobile number');
            $table->string('receive_mail_language')->nullable()->comment('reply in mail language');
            $table->text('message')->comment('contact us text');
            $table->boolean('terms')->comment('terms and condition accept');
            $table->text('admin_reply')->comment('reply from admin');
            $table->boolean('admin_replied')->default(0)->comment('replied or not');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_us');
    }
}
