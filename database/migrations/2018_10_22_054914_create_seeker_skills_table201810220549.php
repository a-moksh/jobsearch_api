<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeekerSkillsTable201810220549 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seeker_skills', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('skill id');
            $table->unsignedInteger('skill_id')->comment('skill attribute table Id');
            $table->integer('level')->comment('skill table Id');
            $table->string('skill_type')->comment('skill type');
            $table->unsignedInteger('seeker_id')->comment('seekers table Id');
            $table->foreign('seeker_id')->references('id')->on('seekers')->comment('seekers table id as a foreign');
            $table->timestamps();
            $table->softDeletes();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seeker_skills');
    }
}
