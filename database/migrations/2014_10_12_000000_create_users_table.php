<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(/**
         * @param Blueprint $table
         */
            'users', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('user ID for database');

            $table->string('user_identifier_id')->comment('users unique formatted identity for external purpose');
            $table->string('first_name')->nullable()->comment('user first name');
            $table->string('last_name')->nullable()->comment('users last name');
            $table->string('full_name')->nullable()->comment('users full name');
            $table->string('katakana_first_name')->nullable()->comment('users katakana first name');
            $table->string('katakana_last_name')->nullable()->comment('users katakana last name');
            $table->string('katakana_full_name')->nullable()->comment('users katakana full name');
            $table->date('dob')->nullable()->comment('users date of birth');
            $table->integer('gender')->nullable()->comment('Users gender');

            $table->string('email')->comment('users email address');
            $table->string('password')->nullable()->comment('users password');
            $table->string('telephone', 20)->nullable()->comment('users telephone number');
            $table->string('mobile', 20)->nullable()->comment('users mobile number');
            $table->string('confirmation_token')->nullable()->comment('conformation token send by system');
            $table->string('user_type')->default('seeker')->comment('user type ');
            $table->timestamp('confirmed_at')->nullable()->comment('time stamp confirmation date ');
            $table->timestamp('confirmation_sent_at')->nullable()->comment('confirmation sent date');
            $table->boolean('verified')->default(false)->comment('varified status');
            $table->rememberToken()->comment('remember token remember or not');
            $table->timestamps();
            $table->softDeletes();
//            $this->addCommonColumns($table);
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
            $table->boolean('unique_in')->nullable()->default(1)->comment('????');
            $table->unique(['email','unique_in'],'users_unique');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
