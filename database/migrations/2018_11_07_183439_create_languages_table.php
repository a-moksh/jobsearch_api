<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('言語ID');
            $table->string('table_name', 64)->comment('テーブル名');
            $table->unsignedInteger('foreign_id')->comment('外部キーID');
            $table->string('column_name', 64)->comment('カラム名');
            $table->string('locale_code', 2)->comment('ロケール');
            $table->text('locale_text')->comment('ロケールテキスト');
            $table->boolean('unique_in')->nullable()->default(1)->comment('ユニーク');
            $table->timestampWithOperators();
            $table->unique(['table_name', 'foreign_id', 'column_name', 'locale_code', 'unique_in'], 'languages_unique_01');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
