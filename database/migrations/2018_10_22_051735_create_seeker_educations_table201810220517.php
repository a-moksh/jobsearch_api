<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeekerEducationsTable201810220517 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seeker_educations', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('experience id for database');

            $table->unsignedInteger('seeker_id')->comment('seeker Id');
            $table->foreign('seeker_id')->references('id')->on('seekers')->comment('seekers table id as a foreign ');

            $table->string('university_institute')->nullable()->comment('seekers university or other institute name');
            $table->string('degree')->nullable()->comment('seekers degree in respect to uni');

            $table->string('faculty')->nullable()->comment('seekers faculty in respect to uni');
            $table->string('graduation_country')->nullable()->comment('seekers graduation country');
            $table->string('graduation_status')->nullable()->comment('seekers graduation status');
            $table->date('graduation_year')->nullable()->comment('seekers graduation year');

            $table->timestamps();
            $table->softDeletes();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seeker_educations');
    }
}
