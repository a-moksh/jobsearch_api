<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterJobsTable201812131257 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->boolean('remote')->nullable()->comment('remote service available');
            $table->boolean('company_service')->nullable()->comment('company services');
            $table->boolean('practical_experience')->nullable()->comment('practical experience required');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->dropColumn('remote');
            $table->dropColumn('company_service');
            $table->dropColumn('practical_experience');
        });
    }
}
