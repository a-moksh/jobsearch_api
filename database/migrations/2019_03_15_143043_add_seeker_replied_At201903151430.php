<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeekerRepliedAt201903151430 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            $table->timestamp('seeker_replied_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            $table->dropColumn('seeker_replied_at');
        });
    }
}
