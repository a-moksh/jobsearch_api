<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeekerCertificatesTable201810220557 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seeker_certificates', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('certificate id');
            $table->unsignedInteger('seeker_id')->comment('seekers table Id');
            $table->foreign('seeker_id')->references('id')->on('seekers')->comment('seekers table id as a foreign');
            $table->date('year')->nullable()->comment('year the certificate is received');
            $table->string('certificate_name')->nullable()->comment('certificate name');
            $table->timestamps();
            $table->softDeletes();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seeker_certificates');
    }
}
