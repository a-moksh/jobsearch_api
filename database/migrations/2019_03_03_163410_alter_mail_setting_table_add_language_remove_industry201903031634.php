<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMailSettingTableAddLanguageRemoveIndustry201903031634 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seeker_mail_settings', function (Blueprint $table) {
            $table->dropColumn('industry');
            $table->string('receive_mail_language')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seeker_mail_settings', function (Blueprint $table) {
            $table->dropColumn('receive_mail_language');
            $table->string('industry');
        });
    }
}
