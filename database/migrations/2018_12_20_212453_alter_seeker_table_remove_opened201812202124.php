<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSeekerTableRemoveOpened201812202124 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seeker_jobs', function (Blueprint $table) {
            $table->dropColumn('opened');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seeker_jobs', function (Blueprint $table) {
            $table->boolean('opened')->comment('is opened application');
        });
    }
}
