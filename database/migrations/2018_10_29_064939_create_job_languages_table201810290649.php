<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobLanguagesTable201810290649 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('job_id')->comment('job Id');
            $table->foreign('job_id')->references('id')->on('jobs')->comment('foreign job id');
            $table->integer('language')->comment('job language');
            $table->integer('level')->nullable()->comment('job language level');
            $table->timestamps();
            $table->softDeletes();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');

            $table->boolean('unique_in')->nullable()->default(1)->comment('????');
            $table->unique(['job_id', 'language','unique_in'],'job_language_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_languages');
    }
}
