<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobLogsTable201903031921 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('job_id')->comment('job id');
            $table->foreign('job_id')->references('id')->on('jobs')->comment('create foreign ID');
            $table->longText('job_detail');
            $table->text('comment')->nullable();

            $table->softDeletes();
            $table->timestamps();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_logs');
    }
}
