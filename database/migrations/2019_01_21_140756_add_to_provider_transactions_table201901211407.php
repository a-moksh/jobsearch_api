<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToProviderTransactionsTable201901211407 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('provider_transactions', function (Blueprint $table) {
            $table->string('foreign_table', 64)->comment('foreign table name');
            $table->unsignedInteger('foreign_id')->comment('foreign table id');
            $table->decimal('points')->change();
            $table->integer('transaction_type')->change();
            $table->integer('paid_status')->default(1); //0 pending // 1 completed
            $table->string('trans_identifier_id')->comment('transactions unique formatted identity for external purpose');
            $table->integer('charge_type')->comment('charge type of transaction');
            $table->integer('amount')->nullable()->comment('amount sent to paid jp');
//            $table->boolean('unique_in')->nullable()->default(1)->comment('unique in');
//            $table->unique(['foreign_table','foreign_id','unique_in'],'provider_transaction_unique_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('provider_transactions', function (Blueprint $table) {
            //
        });
    }
}
