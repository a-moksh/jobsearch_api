<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSeekerTableAddWritingSkills201812131400 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seekers', function (Blueprint $table) {
            $table->integer('japanese_writing_level')->nullable()->comment('seekers japanese writing level');
            $table->integer('english_writing_level')->nullable()->comment('seekers english writing level');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seekers', function (Blueprint $table) {
            $table->dropColumn('japanese_writing_level');
            $table->dropColumn('english_writing_level');
        });
    }
}
