<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable201810160527 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('attribute id');
            $table->unsignedInteger('group_id')->comment('attribute group Id');
            $table->string('value', 50)->comment('attribute value');
            $table->string('name')->comment('Attribute name (translation key)');
            $table->unsignedInteger('parent_group_id')->nullable()->comment('parent attribute group Id');
            $table->string('parent_value', 50)->nullable()->comment('parent value');
            $table->unsignedInteger('display_order')->comment('display order');
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['group_id', 'value']);
            $table->index(['parent_group_id', 'parent_value']);
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
        });

        DB::statement("ALTER TABLE attributes COMMENT 'General purpose attribute'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
