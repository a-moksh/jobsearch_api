<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaidJpTable201901211414 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paid_jp_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('foreign_table', 64)->comment('foreign table name');
            $table->unsignedInteger('foreign_id')->comment('foreign table id');
            $table->text('request')->nullable()->comment('foreign table id');
            $table->text('response')->nullable()->comment('foreign table id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paid_jp_log');
    }
}
