<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSequencesTable201810311554 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sequences', function (Blueprint $table) {
            $table->string('table_name')->comment('table name ');
            $table->string('column_name')->comment('name of the column');
            $table->string('group_column_name')->comment('name of the group column');
            $table->string('group_value')->comment('value of group');
            $table->unsignedInteger('last_number')->comment('last number');
            $table->primary(['table_name', 'column_name', 'group_column_name','group_value'], 'sequence_unique_001');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sequences');
    }
}
