<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalaryTypeUserSetting201903051924 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seeker_mail_settings', function (Blueprint $table) {
            $table->integer('salary_type')->nullable()->comment('desired salary type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seeker_mail_settings', function (Blueprint $table) {
            $table->dropColumn('salary_type');
        });
    }
}
