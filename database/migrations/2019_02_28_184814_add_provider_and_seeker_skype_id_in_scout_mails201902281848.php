<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProviderAndSeekerSkypeIdInScoutMails201902281848 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            //
            $table->string('provider_skype_id',200)->nullable()->comment('provider skype id');;
            $table->string('seeker_skype_id', 200)->nullable()->comment('seeker skype id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            //
        });
    }
}
