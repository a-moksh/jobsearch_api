<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReadUnreadToScoutMailsTable201902221537 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            $table->boolean('provider_read')->default(0)->comment('provider read status');;
            $table->boolean('seeker_read')->default(0)->comment('seeker read status');
            $table->integer('re_count')->default(0)->comment('re count for reply count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            $table->dropColumn('provider_read');
            $table->dropColumn('seeker_read');
            $table->dropColumn('re_count');
        });
    }
}
