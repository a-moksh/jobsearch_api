<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoutDetailsTable201812181621 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scout_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('scout_mail_id');
            $table->foreign('scout_mail_id')->references('id')->on('scout_mails')->comment('scout mails foreign key');
            $table->date('date')->nullable()->comment('date');
            $table->time('time')->nullable()->comment('time');
            $table->timestamps();
            $table->softDeletes();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scout_details');
    }
}
