<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeekerExperiencesTable201810220517 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seeker_experiences', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('experience id for database');

            $table->unsignedInteger('seeker_id')->comment('seeker Id');
            $table->foreign('seeker_id')->references('id')->on('seekers')->comment('seekers table id as a foreign ');


            $table->string('company_name')->comment('seekers company name that they worked on');

            $table->string('job_title')->comment('seekers job title they worked');

            $table->date('job_from')->comment('seekers job from date they worked');
            $table->date('job_to')->nullable()->comment('seekers job to date they worked');
            $table->boolean('currently_working_here')->default(0)->comment('seekers if they are currently working here');

            $table->string('annual_salary')->comment('seekers annual salary for the particular company');
            $table->text('job_description')->comment('seekers work description/ job description');

            $table->timestamps();
            $table->softDeletes();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seeker_experiences');
    }
}
