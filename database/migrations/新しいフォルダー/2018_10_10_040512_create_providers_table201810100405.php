<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable201810100405 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('provider Id');
            $table->unsignedInteger('user_id')->unique()->comment('user Id');
            $table->foreign('user_id')->references('id')->on('users')->comment('create foreign ID');
            $table->string('provider_identifier_id')->comment('providers unique formatted identity for external purpose');

            $table->string('email')->nullable()->comment('email');
            $table->string('ceo_name')->nullable()->comment('name of the ceo');
            $table->date('established_year')->nullable()->comment(' date of company start');
            $table->string('telephone')->nullable()->comment('company telephone number');
            $table->string('fax')->nullable()->comment('fax number');
            $table->string('capital')->nullable()->comment('capital of the company');
            $table->integer('no_of_employee')->nullable()->comment('total number of employee');
            $table->integer('no_of_branch')->nullable()->comment('total branch of the company');
            $table->string('business_content')->nullable()->comment('prefecture name');
            $table->integer('zip_code')->length(7)->nullable()->comment('postal number ');
            $table->integer('prefecture_id')->nullable()->comment('prefecture name');
            $table->string('address')->nullable()->comment('address');
            $table->string('street_address')->nullable()->comment('detail adddress info');
            $table->string('related_url')->nullable()->comment('companys releted url');
            $table->string('about_company')->nullable()->comment('company description japanese');
            $table->string('company_name')->nullable()->comment('name of the company english');
            $table->string('contact_person')->nullable()->comment('contact person name ja');

            $table->softDeletes();
            $table->timestamps();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');

            $table->boolean('unique_in')->nullable()->default(1)->comment('????');
            $table->unique(['email','unique_in'],'providers_unique');
        });

        DB::statement('ALTER TABLE providers ADD FULLTEXT fulltext_index (company_name, about_company)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
