<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeekerWishlistsTable201811131550 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seeker_wishlists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('seeker_id')->comment('seekers Id');
            $table->foreign('seeker_id')->references('id')->on('seekers')->comment('foreign key seekers table');
            $table->unsignedInteger('job_id')->comment('job Id');
            $table->foreign('job_id')->references('id')->on('jobs')->comment('foreign key on jobs table');
            $table->timestamps();
            $table->softDeletes();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
            // unique keys

            $table->boolean('unique_in')->nullable()->default(1)->comment('????');
            $table->unique(['job_id', 'seeker_id','unique_in'],'seeker_wishlists_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seeker_wishlists');
    }
}
