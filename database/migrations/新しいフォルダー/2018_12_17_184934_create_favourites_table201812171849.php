<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavouritesTable201812171849 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favourites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('seeker_id')->comment('seekers Id');
            $table->foreign('seeker_id')->references('id')->on('seekers')->comment('foreign key seekers table');
            $table->unsignedInteger('provider_id')->comment('providers Id');
            $table->foreign('provider_id')->references('id')->on('providers')->comment('foreign key providers table');
            $table->timestamps();
            $table->softDeletes();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
            // unique keys

            $table->boolean('unique_in')->nullable()->default(1)->comment('????');
            $table->unique(['provider_id', 'seeker_id','unique_in'],'provider_favourite_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favourites');
    }
}
