<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeekersTable201810220507 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seekers', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('seekers id for database');
            $table->unsignedInteger('user_id')->unique()->comment('user Id');
            $table->foreign('user_id')->references('id')->on('users')->comment('users table id as a foreign ');
            $table->string('seeker_identifier_id')->unique()->comment('seekers unique formatted identity for external purpose');

            $table->integer('visa_status')->nullable()->comment('seekers current visa status');
            $table->date('visa_issue_date')->nullable()->comment('seekers visa issue date');
            $table->date('visa_expiry_date')->nullable()->comment('seekers visa expiry date');

            $table->integer('japanese_language_level')->nullable()->comment('seekers japanese language level');
            $table->integer('english_language_level')->nullable()->comment('seekers english language level');
            $table->integer('desired_employment_type')->nullable()->comment('seekers desired employment type');

            $table->string('nearest_station')->nullable()->comment('seekers nearest station');
            $table->integer('willing_to_relocate')->nullable()->comment('is seeker willing to relocate');

            $table->integer('is_experienced')->default(0)->comment('is seeker experienced');
            $table->tinyInteger('experience')->unsigned()->default(0)->comment('seekers total experience years in number');

            $table->string('country')->nullable()->comment('users nationality');
            $table->integer('zip_code')->length(7)->nullable()->comment('postal number ');
            $table->integer('prefecture_id')->nullable()->comment('prefecture name');
            $table->string('address')->nullable()->comment('address');
            $table->string('street_address')->nullable()->comment('detail adddress info');

            $table->string('portfolio_url')->nullable()->comment('seekers portfolio url');
            $table->string('github_url')->nullable()->comment('seekers github url');
            $table->string('qiita_id')->nullable()->comment('qiita id');
            $table->text('self_introduction')->nullable()->comment('seekers cover letter to introduce themselves');
            $table->timestamps();
            $table->softDeletes();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seekers');
    }
}
