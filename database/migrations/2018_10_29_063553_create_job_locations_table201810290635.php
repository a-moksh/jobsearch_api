<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobLocationsTable201810290635 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('job_id')->comment('job Id');
            $table->foreign('job_id')->references('id')->on('jobs')->comment('foreign job id');

            $table->string('railway_name')->nullable()->comment('rail way name');
            $table->string('station_name')->nullable()->comment('station name');
            $table->enum('station_distance',[0,1])->nullable()->comment('0 walking distance, 1 bus distance');
            $table->integer('walking_distance')->nullable()->comment('walking distance');

            $table->integer('zip_code')->length(7)->nullable()->comment('postal number ');
            $table->integer('prefecture_id')->nullable()->comment('prefecture name');
            $table->string('address')->nullable()->comment('address');
            $table->string('street_address')->nullable()->comment('detail address info');

            $table->timestamps();
            $table->softDeletes();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_locations');
    }
}
