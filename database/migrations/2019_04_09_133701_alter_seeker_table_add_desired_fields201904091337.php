<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSeekerTableAddDesiredFields201904091337 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seekers', function (Blueprint $table) {
            $table->integer('desired_job_category')->nullable();
            $table->integer('desired_annual_min_salary_range')->nullable();
            $table->integer('experience_year')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seekers', function (Blueprint $table) {
            $table->dropColumn('experience_year');
            $table->dropColumn('desired_job_category');
            $table->dropColumn('desired_annual_min_salary_range');
        });
    }
}
