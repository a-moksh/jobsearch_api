<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeekerMailSettingsTable201810220601 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seeker_mail_settings', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('certificate id');
            $table->unsignedInteger('seeker_id')->unique()->comment('seekers table Id');
            $table->foreign('seeker_id')->references('id')->on('seekers')->comment('seekers table id as a foreign');

            $table->integer('employment_type')->nullable()->comment('employment status to receive mail of');
            $table->integer('industry')->nullable()->comment('industry to receive mail of');
            $table->integer('japanese_level')->nullable()->comment('japanese level to receive mail of');
            $table->integer('english_level')->nullable()->comment('english level to receive mail of');

            $table->string('min_salary')->nullable()->comment('min salary to receive mail of');
            $table->string('max_salary')->nullable()->comment('max salary to receive mail of');
            $table->string('job_location')->nullable()->comment('location to receive mail of');

            $table->boolean('visa_support')->nullable()->comment('visa support to receive mail');
            $table->boolean('social_insurance')->nullable()->comment('social insurance to receive mail');
            $table->boolean('housing_allowance')->nullable()->comment('housing allowance to receive mail');
            $table->boolean('receive_mail')->nullable()->comment('provide uniform to receive mail');

            $table->boolean('hairstyle_freedom')->nullable()->comment('hair clothing freedom to receive mail');
            $table->boolean('clothing_freedom')->nullable()->comment('hair clothing freedom to receive mail');

            $table->timestamps();
            $table->softDeletes();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seeker_mail_settings');
    }
}
