<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterScoutMailsTable201812181620 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            $table->softDeletes();
            $table->integer('job_id')->comment('job_id');
            $table->integer('interview_method')->nullable()->comment('interview method');
            $table->integer('interview_place')->nullable()->comment('interview place');
            $table->integer('job_location_id')->nullable()->comment('interview place job location id');
            $table->boolean('resume')->nullable()->comment('seekers english writing level');
            $table->boolean('cv')->nullable()->comment('cv');
            $table->boolean('writing_util')->nullable()->comment('writing utensils');
            $table->string('other')->nullable()->comment('other');
            $table->integer('status')->nullable()->comment('status');
            $table->date('date')->nullable()->comment('date');
            $table->time('time')->nullable()->comment('time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            $table->dropColumn('job_id');
            $table->dropColumn('interview_method');
            $table->dropColumn('interview_place');
            $table->dropColumn('job_location_id');
            $table->dropColumn('resume');
            $table->dropColumn('cv');
            $table->dropColumn('writing_util');
            $table->dropColumn('other');
            $table->dropColumn('status');
            $table->dropColumn('date');
            $table->dropColumn('time');
        });
    }
}
