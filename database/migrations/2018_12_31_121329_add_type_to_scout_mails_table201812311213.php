<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToScoutMailsTable201812311213 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            $table->integer('type')->nullable()->comment('scout_message type');
            $table->integer('seeker_status')->nullable()->comment('scout_message status');
            $table->integer('provider_status')->nullable()->comment('scout_message status provider');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('seeker_status');
            $table->dropColumn('provider_status');
        });
    }
}
