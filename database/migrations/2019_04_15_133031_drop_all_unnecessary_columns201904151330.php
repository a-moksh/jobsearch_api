<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAllUnnecessaryColumns201904151330 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('katakana_first_name');
            $table->dropColumn('katakana_last_name');
        });
        Schema::table('seekers', function (Blueprint $table) {
            $table->dropColumn('self_introduction');
        });
        Schema::table('seeker_mail_settings', function (Blueprint $table) {
            $table->dropColumn('receive_mail_language');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name')->nullable()->comment('user first name');
            $table->string('last_name')->nullable()->comment('users last name');
            $table->string('katakana_first_name')->nullable()->comment('users katakana first name');
            $table->string('katakana_last_name')->nullable()->comment('users katakana last name');
        });
        Schema::table('seekers', function (Blueprint $table) {
            $table->text('self_introduction')->nullable()->comment('seekers cover letter to introduce themselves');
        });
        Schema::table('seeker_mail_settings', function (Blueprint $table) {
            $table->string('receive_mail_language')->default('ja')->comment('receive mail for users');
        });
    }
}
