<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterScoutMailAddReplyCountIndividual201903131123 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            $table->integer('seeker_reply_count')->default(0)->nullable();
            $table->integer('provider_reply_count')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scout_mails', function (Blueprint $table) {
            $table->dropColumn('seeker_reply_count');
            $table->dropColumn('provider_reply_count');
        });
    }
}
