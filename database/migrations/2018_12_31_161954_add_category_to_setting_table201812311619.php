<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryToSettingTable201812311619 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seeker_mail_settings', function (Blueprint $table) {
            $table->string('job_category')->comment('job category');
            $table->string('industry')->change();
            $table->string('job_location')->change();
            $table->string('employment_type')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seeker_mail_settings', function (Blueprint $table) {
            //
        });
    }
}
