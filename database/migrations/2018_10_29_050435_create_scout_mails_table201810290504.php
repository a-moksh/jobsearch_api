<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoutMailsTable201810290504 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scout_mails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('provider_id')->comment('provider Id');
            $table->foreign('provider_id')->references('id')->on('providers')->comment('foreign key providers table');
            $table->unsignedInteger('seeker_id')->comment('Seeker Id');
            $table->foreign('seeker_id')->references('id')->on('seekers')->comment('foreign key seekers table');
            $table->longText('message')->comment('message from provider');
            $table->timestamps();
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scout_mails');
    }
}
