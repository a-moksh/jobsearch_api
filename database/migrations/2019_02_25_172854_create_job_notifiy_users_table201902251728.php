<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobNotifiyUsersTable201902251728 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_notify_users', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('primary id');
            $table->integer('job_id')->comment('job table id');
            $table->string('seeker_ids')->comment('seeker table id');
            $table->boolean('notified')->default(0)->comment('is notified for user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_notify_users');
    }
}
