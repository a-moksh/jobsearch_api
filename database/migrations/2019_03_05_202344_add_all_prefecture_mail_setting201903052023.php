<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllPrefectureMailSetting201903052023 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seeker_mail_settings', function (Blueprint $table) {
            $table->boolean('all_prefecture')->nullable()->default(0)->comment('all prefecture');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seeker_mail_settings', function (Blueprint $table) {
            $table->dropColumn('all_prefecture');
        });
    }
}
