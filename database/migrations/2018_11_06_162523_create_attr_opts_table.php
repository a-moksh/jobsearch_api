<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttrOptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attr_opts', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('属性選択肢ID');
            $table->unsignedInteger('attr_id')->comment('属性ID');
            $table->string('data_type', 16)->comment('データタイプ');
            $table->string('label', 64)->comment('属性選択肢ラベル');
            $table->string('value', 64)->comment('属性選択肢値');
            $table->unsignedInteger('child_id')->nullable()->comment('子属性ID');
            $table->text('description')->nullable()->comment('説明');
            $table->unsignedInteger('display_order')->comment('表示順');
            $table->boolean('unique_in')->nullable()->default(1)->comment('ユニークフラグ');
            $table->timestampWithOperators();
            $table->unique(['attr_id', 'value', 'unique_in']);
        });

        DB::statement("ALTER TABLE attr_opts COMMENT '属性選択肢'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attr_opts', function (Blueprint $table) {
            //
        });
    }
}
