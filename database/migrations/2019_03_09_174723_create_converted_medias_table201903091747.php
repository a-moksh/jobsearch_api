<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvertedMediasTable201903091747 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('converted_medias', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('primary id for database');
            $table->unsignedInteger('media_id')->comment('media id for table refrence');
            $table->foreign('media_id')->references('id')->on('medias')->comment('medias table id as a foreign ');
            $table->string('media_type')->nullable()->comment('type of media');
            $table->string('media_value')->nullable()->comment('value of the media');
            $table->string('media_dimension')->nullable()->comment('dimension of the media');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('converted_medias');
    }
}
