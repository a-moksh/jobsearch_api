<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusSeekerJobsTable201901031538 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seeker_jobs', function (Blueprint $table) {
            $table->integer('status')->nullable()->default(0)->comment('applicant status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seeker_jobs', function (Blueprint $table) {
            //
        });
    }
}
