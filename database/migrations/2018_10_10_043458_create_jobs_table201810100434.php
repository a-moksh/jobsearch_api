<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable201810100434 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('job id primary key');
            $table->unsignedInteger('provider_id')->comment('provider Id');
            $table->unsignedInteger('category_id')->comment('category Id');
            $table->foreign('provider_id')->references('id')->on('providers')->comment('sending foreign key');
            $table->string('title')->comment('job title ');
            $table->integer('max_salary')->nullable()->comment('maximum salary ');
            $table->integer('min_salary')->nullable()->comment('minimum salary ');
            $table->longText('description')->nullable()->comment('job description english');
            $table->longText('qualifications')->nullable()->comment('job qualifications');
            $table->longText('welfare')->nullable()->comment('welfare ');
            $table->integer('language_translation')->nullable()->comment('translation google or professional');
            $table->time('working_hour_from')->nullable()->comment('working hour from');
            $table->time('working_hour_to')->nullable()->comment('working hour to');
            $table->integer('morning_shift')->nullable()->comment('morning shift');
            $table->integer('night_shift')->nullable()->comment('night shift');
            $table->integer('visa_support')->nullable()->comment('visa support');
            $table->integer('overtime')->nullable()->comment('overtime');
            $table->integer('trial_period')->nullable()->comment('job trial period');
            $table->integer('social_insurance')->nullable()->comment('social insurance available');
            $table->integer('bonus')->nullable()->comment('bonus availability');
            $table->integer('hairstyle_freedom')->nullable()->comment('hairstyle freedom');
            $table->integer('clothing_freedom')->nullable()->comment('clothing freedom');
            $table->integer('with_uniform')->nullable()->comment('uniform availability');
            $table->integer('housing_allowance')->nullable()->comment('housing allowance');
            $table->longText('other_details')->nullable()->comment('other charectoristics');
            $table->integer('total_annual_leave')->nullable()->comment('total annual leave');
            $table->date('posting_start_date')->nullable()->comment('posting ate');
            $table->date('posting_end_date')->nullable()->comment('posting end date');
            $table->string('slug')->comment('job slug');
            $table->integer('salary_type')->nullable()->comment('salary type');
            $table->integer('holiday')->nullable()->comment('holiday type');
            $table->string('holiday_days')->nullable()->comment('holiday days');
            $table->string('long_term')->nullable()->comment('long term?');
            $table->string('short_term')->nullable()->comment('short term?');
            $table->integer('status')->comment('job publish status');
            $table->integer('admin_status')->default(1)->comment('admin approval,denial status');
            $table->integer('views')->default(0)->comment('job view count');

            $table->timestamps();
            $table->softDeletes();
            //common table column
            //common table column
            $table->integer('created_by')->nullable()->comment('created_by');
            $table->integer('updated_by')->nullable()->comment('updated_by');
            $table->integer('deleted_by')->nullable()->comment('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
