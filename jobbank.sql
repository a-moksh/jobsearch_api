-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: jobbank
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES binary */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attr_opts`
--

DROP TABLE IF EXISTS `attr_opts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attr_opts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '属性選択肢ID',
  `attr_id` int(10) unsigned NOT NULL COMMENT '属性ID',
  `data_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL COMMENT 'データタイプ',
  `label` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '属性選択肢ラベル',
  `value` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '属性選択肢値',
  `child_id` int(10) unsigned DEFAULT NULL COMMENT '子属性ID',
  `description` text COLLATE utf8_unicode_ci COMMENT '説明',
  `display_order` int(10) unsigned NOT NULL COMMENT '表示順',
  `unique_in` tinyint(1) DEFAULT '1' COMMENT 'ユニークフラグ',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'created_at',
  `created_by` int(10) unsigned DEFAULT NULL COMMENT 'created_by',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'updated_at',
  `updated_by` int(10) unsigned DEFAULT NULL COMMENT 'updated_by',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted_at',
  `deleted_by` int(10) unsigned DEFAULT NULL COMMENT 'deleted_at',
  PRIMARY KEY (`id`),
  UNIQUE KEY `attr_opts_attr_id_value_unique_in_unique` (`attr_id`,`value`,`unique_in`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='属性選択肢';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attr_opts`
--

LOCK TABLES `attr_opts` WRITE;
/*!40000 ALTER TABLE `attr_opts` DISABLE KEYS */;
/*!40000 ALTER TABLE `attr_opts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_groups`
--

DROP TABLE IF EXISTS `attribute_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_groups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'group name',
  `description` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'description',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_groups`
--

LOCK TABLES `attribute_groups` WRITE;
/*!40000 ALTER TABLE `attribute_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'attribute id',
  `group_id` int(10) unsigned NOT NULL COMMENT 'attribute group Id',
  `value` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'attribute value',
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Attribute name (translation key)',
  `parent_group_id` int(10) unsigned DEFAULT NULL COMMENT 'parent attribute group Id',
  `parent_value` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'parent value',
  `display_order` int(10) unsigned NOT NULL COMMENT 'display order',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`),
  UNIQUE KEY `attributes_group_id_value_unique` (`group_id`,`value`),
  KEY `attributes_parent_group_id_parent_value_index` (`parent_group_id`,`parent_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='General purpose attribute';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributes`
--

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attrs`
--

DROP TABLE IF EXISTS `attrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attrs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '属性ID',
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT '属性名',
  `label` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT '属性ラベル',
  `data_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL COMMENT 'データタイプ',
  `default_value` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '初期値',
  `description` text COLLATE utf8_unicode_ci COMMENT '説明',
  `display_order` int(10) unsigned NOT NULL COMMENT '表示順',
  `is_private` tinyint(1) NOT NULL DEFAULT '0' COMMENT '非表示',
  `unique_in` tinyint(1) DEFAULT '1' COMMENT 'ユニーク',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'created_at',
  `created_by` int(10) unsigned DEFAULT NULL COMMENT 'created_by',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'updated_at',
  `updated_by` int(10) unsigned DEFAULT NULL COMMENT 'updated_by',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted_at',
  `deleted_by` int(10) unsigned DEFAULT NULL COMMENT 'deleted_at',
  PRIMARY KEY (`id`),
  UNIQUE KEY `attrs_name_unique_in_unique` (`name`,`unique_in`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='属性';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attrs`
--

LOCK TABLES `attrs` WRITE;
/*!40000 ALTER TABLE `attrs` DISABLE KEYS */;
/*!40000 ALTER TABLE `attrs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `charges`
--

DROP TABLE IF EXISTS `charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `point` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `charges`
--

LOCK TABLES `charges` WRITE;
/*!40000 ALTER TABLE `charges` DISABLE KEYS */;
/*!40000 ALTER TABLE `charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_us` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'contact us full name',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'contact us email',
  `mobile` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'contact us mobile number',
  `receive_mail_language` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'reply in mail language',
  `message` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'contact us text',
  `terms` tinyint(1) NOT NULL COMMENT 'terms and condition accept',
  `admin_reply` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'reply from admin',
  `admin_replied` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'replied or not',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1 seeker, 2 provider',
  `company_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'company name',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_us`
--

LOCK TABLES `contact_us` WRITE;
/*!40000 ALTER TABLE `contact_us` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `converted_medias`
--

DROP TABLE IF EXISTS `converted_medias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `converted_medias` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary id for database',
  `media_id` int(10) unsigned NOT NULL COMMENT 'media id for table refrence',
  `media_type` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'type of media',
  `media_value` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'value of the media',
  `media_dimension` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'dimension of the media',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `converted_medias`
--

LOCK TABLES `converted_medias` WRITE;
/*!40000 ALTER TABLE `converted_medias` DISABLE KEYS */;
/*!40000 ALTER TABLE `converted_medias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favourites`
--

DROP TABLE IF EXISTS `favourites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favourites` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `seeker_id` int(10) unsigned NOT NULL COMMENT 'seekers Id',
  `provider_id` int(10) unsigned NOT NULL COMMENT 'providers Id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `unique_in` tinyint(1) DEFAULT '1' COMMENT '????',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favourites`
--

LOCK TABLES `favourites` WRITE;
/*!40000 ALTER TABLE `favourites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favourites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_to_queue_tracker`
--

DROP TABLE IF EXISTS `file_to_queue_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_to_queue_tracker` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary id',
  `file_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'file_name',
  `processed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'track to processed or not',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_to_queue_tracker`
--

LOCK TABLES `file_to_queue_tracker` WRITE;
/*!40000 ALTER TABLE `file_to_queue_tracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_to_queue_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_languages`
--

DROP TABLE IF EXISTS `job_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_languages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(10) unsigned NOT NULL COMMENT 'job Id',
  `language` int(11) NOT NULL COMMENT 'job language',
  `level` int(11) DEFAULT NULL COMMENT 'job language level',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `unique_in` tinyint(1) DEFAULT '1' COMMENT '????',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_languages`
--

LOCK TABLES `job_languages` WRITE;
/*!40000 ALTER TABLE `job_languages` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_locations`
--

DROP TABLE IF EXISTS `job_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_locations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(10) unsigned NOT NULL COMMENT 'job Id',
  `prefecture_id` int(11) DEFAULT NULL COMMENT 'prefecture name',
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'address',
  `street_address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'detail address info',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `zip_code` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'zip code of seeker address',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_locations`
--

LOCK TABLES `job_locations` WRITE;
/*!40000 ALTER TABLE `job_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_logs`
--

DROP TABLE IF EXISTS `job_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(10) unsigned NOT NULL COMMENT 'job id',
  `job_detail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_logs`
--

LOCK TABLES `job_logs` WRITE;
/*!40000 ALTER TABLE `job_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_notify_users`
--

DROP TABLE IF EXISTS `job_notify_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_notify_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary id',
  `job_id` int(11) NOT NULL COMMENT 'job table id',
  `seeker_ids` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'seeker table id',
  `notified` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'is notified for user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_notify_users`
--

LOCK TABLES `job_notify_users` WRITE;
/*!40000 ALTER TABLE `job_notify_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_notify_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_queues`
--

DROP TABLE IF EXISTS `job_queues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_queues` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_queues_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_queues`
--

LOCK TABLES `job_queues` WRITE;
/*!40000 ALTER TABLE `job_queues` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_queues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'job id primary key',
  `provider_id` int(10) unsigned NOT NULL COMMENT 'provider Id',
  `category_id` int(10) unsigned NOT NULL COMMENT 'category Id',
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'job title ',
  `max_salary` int(11) DEFAULT NULL COMMENT 'maximum salary ',
  `min_salary` int(11) DEFAULT NULL COMMENT 'minimum salary ',
  `description` longtext COLLATE utf8_unicode_ci COMMENT 'job description english',
  `qualifications` longtext COLLATE utf8_unicode_ci COMMENT 'job qualifications',
  `welfare` longtext COLLATE utf8_unicode_ci COMMENT 'welfare ',
  `language_translation` int(11) DEFAULT NULL COMMENT 'translation google or professional',
  `working_hour_from` int(11) DEFAULT NULL COMMENT 'working hour from',
  `working_hour_to` int(11) DEFAULT NULL COMMENT 'working hour to',
  `morning_shift` int(11) DEFAULT NULL COMMENT 'morning shift',
  `night_shift` int(11) DEFAULT NULL COMMENT 'night shift',
  `visa_support` int(11) DEFAULT NULL COMMENT 'visa support',
  `overtime` int(11) DEFAULT NULL COMMENT 'overtime',
  `trial_period` int(11) DEFAULT NULL COMMENT 'job trial period',
  `social_insurance` int(11) DEFAULT NULL COMMENT 'social insurance available',
  `bonus` int(11) DEFAULT NULL COMMENT 'bonus availability',
  `clothing_freedom` int(11) DEFAULT NULL COMMENT 'clothing freedom',
  `housing_allowance` int(11) DEFAULT NULL COMMENT 'housing allowance',
  `other_details` longtext COLLATE utf8_unicode_ci COMMENT 'other charectoristics',
  `total_annual_leave` int(11) DEFAULT NULL COMMENT 'total annual leave',
  `posting_start_date` date DEFAULT NULL COMMENT 'posting ate',
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'job slug',
  `salary_type` int(11) DEFAULT NULL COMMENT 'salary type',
  `holiday` int(11) DEFAULT NULL COMMENT 'holiday type',
  `holiday_days` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'holiday days',
  `long_term` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'long term?',
  `short_term` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'short term?',
  `status` int(11) NOT NULL COMMENT 'job publish status',
  `admin_status` int(11) NOT NULL DEFAULT '1' COMMENT 'admin approval,denial status',
  `views` int(11) NOT NULL DEFAULT '0' COMMENT 'job view count',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `employment_type` int(11) NOT NULL COMMENT 'employment type of job',
  `sub_category_id` int(11) NOT NULL COMMENT 'sub category Id',
  `remote` tinyint(1) DEFAULT NULL COMMENT 'remote service available',
  `company_service` tinyint(1) DEFAULT NULL COMMENT 'company services',
  `practical_experience` tinyint(1) DEFAULT NULL COMMENT 'practical experience required',
  `job_identifier_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'users unique formatted identity for external purpose',
  `time_period` int(11) DEFAULT NULL COMMENT 'time period of job',
  `published_at` timestamp NULL DEFAULT NULL,
  `mail_notified_expire` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '言語ID',
  `table_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'テーブル名',
  `foreign_id` int(10) unsigned NOT NULL COMMENT '外部キーID',
  `column_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'カラム名',
  `locale_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ロケール',
  `locale_text` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'ロケールテキスト',
  `unique_in` tinyint(1) DEFAULT '1' COMMENT 'ユニーク',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'created_at',
  `created_by` int(10) unsigned DEFAULT NULL COMMENT 'created_by',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'updated_at',
  `updated_by` int(10) unsigned DEFAULT NULL COMMENT 'updated_by',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT 'deleted_at',
  `deleted_by` int(10) unsigned DEFAULT NULL COMMENT 'deleted_at',
  PRIMARY KEY (`id`),
  UNIQUE KEY `languages_unique_01` (`table_name`,`foreign_id`,`column_name`,`locale_code`,`unique_in`),
  FULLTEXT KEY `fulltext_index_language` (`locale_text`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medias`
--

DROP TABLE IF EXISTS `medias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medias` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'media Id',
  `foreign_table` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'foreign table name',
  `foreign_id` int(11) NOT NULL COMMENT 'foreign id ',
  `media_type` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'type of media',
  `media_value` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'value of the media',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medias`
--

LOCK TABLES `medias` WRITE;
/*!40000 ALTER TABLE `medias` DISABLE KEYS */;
/*!40000 ALTER TABLE `medias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_10_10_040512_create_providers_table201810100405',1),(4,'2018_10_10_042500_create_medias_table201810100425',2),(5,'2018_10_10_043458_create_jobs_table201810100434',1),(6,'2018_10_12_023842_create_verify_users_table201810120238',1),(7,'2018_10_15_062427_create_attribute_group_table201810150624',3),(8,'2018_10_16_052721_create_attributes_table201810160527',3),(9,'2018_10_22_051721_create_seeker_experiences_table201810220517',1),(10,'2018_10_16_052721_create_attributes_table201810160527',3),(11,'2018_10_22_050751_create_seekers_table201810220507',1),(12,'2018_10_22_051735_create_seeker_educations_table201810220517',1),(13,'2018_10_22_054914_create_seeker_skills_table201810220549',1),(14,'2018_10_22_055703_create_seeker_certificates_table201810220557',1),(15,'2018_10_22_060117_create_seeker_mail_settings_table201810220601',1),(16,'2018_10_29_050435_create_scout_mails_table201810290504',1),(17,'2018_10_29_051141_create_provider_transactions_table201810290511',1),(18,'2018_10_29_063553_create_job_locations_table201810290635',1),(19,'2018_10_29_064939_create_job_languages_table201810290649',1),(20,'2018_10_31_155451_create_sequences_table201810311554',4),(21,'2018_11_06_162449_create_attrs_table',4),(22,'2018_11_06_162523_create_attr_opts_table',4),(23,'2018_11_07_183439_create_languages_table',4),(24,'2018_11_13_155003_create_seeker_wishlists_table201811131550',1),(26,'2018_11_21_115102_add_employment_type_jobs_table201811211151',5),(27,'2018_11_30_221622_create_notifications_table',5),(28,'2018_11_30_224613_create_job_queues_table',5),(29,'2018_12_06_102517_add_seekers_table201812061025',5),(30,'2018_12_06_110933_alter_jobs_table201812061109',5),(31,'2018_12_11_121002_alter_seeer_job_table_add_opened201812111210',5),(32,'2018_12_13_125728_alter_jobs_table201812131257',5),(33,'2018_12_13_140059_alter_seeker_table_add_writing_skills201812131400',5),(34,'2018_12_17_184934_create_favourites_table201812171849',5),(35,'2018_12_18_162051_alter_scout_mails_table201812181620',6),(36,'2018_12_18_162140_create_scout_details_table201812181621',5),(37,'2018_12_19_122042_create_zip_codes_table201812191220',7),(38,'2018_12_20_123025_alter_providers_table201812201230',7),(39,'2018_12_20_212453_alter_seeker_table_remove_opened201812202124',7),(40,'2018_12_25_161013_alter_remove_zip_code_from_seeker_table201812251610',7),(41,'2018_12_25_161105_alter_remove_zip_code_from_seeker_table201812251611',7),(42,'2018_12_25_161235_alter_remove_zip_code_from_job_location_table201812251612',7),(43,'2018_12_25_161304_alter_add_zip_code_in_job_location_table201812251613',7),(44,'2018_12_25_161616_alter_remove_zip_code_from_provider_table201812251616',7),(45,'2018_12_25_161644_alter_add_zip_code_in_provider_table201812251616',7),(46,'2018_12_26_171522_alter_job_identifier_in_job_table201812261715',7),(47,'2018_12_31_121329_add_type_to_scout_mails_table201812311213',7),(48,'2018_12_31_161954_add_category_to_setting_table201812311619',7),(49,'2019_01_03_153803_add_status_seeker_jobs_table201901031538',7),(50,'2019_01_03_155403_add_last_login_users_table201901031554',7),(51,'2019_01_08_140302_add_time_period_jobs_table201901081403',7),(52,'2019_01_09_202847_alter_users_table_add_deletable201901092028',7),(53,'2019_01_11_120024_alter_table_scout_details_change_time_int201901111200',7),(54,'2019_01_11_120058_alter_table_scout_mails_change_time_int201901111200',7),(55,'2019_01_21_140756_add_to_provider_transactions_table201901211407',7),(56,'2019_01_21_141443_create_paid_jp_table201901211414',7),(57,'2019_01_23_121035_add_points_to_providers_table201901231210',7),(58,'2019_01_23_190548_create_points_table201901231905',7),(59,'2019_01_24_194346_create_charges_table201901241943',7),(60,'2019_01_25_143903_alter_password_reset_table_add_user_type201901251439',7),(61,'2019_01_25_153719_alter_user_table_add_unique_user_type201901251537',7),(62,'2019_01_31_133352_alter_language_table_add_full_text201901311333',8),(63,'2019_02_04_175832_create_provider_paidjp_table201902041758',8),(64,'2019_02_08_161315_change_time_jobs_table201902081613',9),(65,'2019_02_13_135144_add_mansion_column_in_providers_table201902131351',9),(66,'2019_02_13_231233_add_to_provider_paidjp_table201902132312',9),(67,'2019_02_22_134208_create_failed_jobs_table',9),(68,'2019_02_22_153726_add_read_unread_to_scout_mails_table201902221537',9),(69,'2019_02_25_172854_create_job_notifiy_users_table201902251728',9),(70,'2019_02_28_184501_create_file_name_tracker201902281845',9),(71,'2019_02_28_184814_add_provider_and_seeker_skype_id_in_scout_mails201902281848',9),(72,'2019_03_03_163410_alter_mail_setting_table_add_language_remove_industry201903031634',9),(73,'2019_03_03_192143_create_job_logs_table201903031921',8),(74,'2019_03_05_192450_add_salary_type_user_setting201903051924',10),(75,'2019_03_05_202344_add_all_prefecture_mail_setting201903052023',10),(76,'2019_03_05_222607_add_published_to_jobs_table201903052226',10),(77,'2019_03_08_123847_alter_seeker_job_table_user_data_to_json201903081238',10),(78,'2019_03_09_174723_create_converted_medias_table201903091747',10),(79,'2019_03_13_112356_alter_scout_mail_add_reply_count_individual201903131123',11),(80,'2019_03_15_143043_add_seeker_replied_At201903151430',11),(81,'2019_03_18_151119_alter_seeker_job_mail_tracker_table_add_longtext201903181511',11),(82,'2019_03_19_232838_alter_jobs_table_add_mail_notified_expire201903192328',11),(83,'2019_03_20_211900_add_paid_status_providers_table201903202119',11),(84,'2018_11_15_173310_create_seeker_jobs_table201811151733',11),(85,'2019_04_09_133701_alter_seeker_table_add_desired_fields201904091337',12),(86,'2019_04_11_123552_create_contact_us_table201904111235',12),(87,'2019_04_12_114411_add_provider_column_table201904121144',12),(88,'2019_04_15_130003_add_receive_mail_languages_tousers201904151300',12),(89,'2019_04_15_133031_drop_all_unnecessary_columns201904151330',12),(90,'2019_04_15_151722_alter_contact_us_table_add_softdelete201904151517',12),(91,'2019_04_16_131828_removecolumsprovider201904161318',12),(92,'2019_04_25_145626_add_purchased_on_providers_table201904251456',12);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paid_jp_logs`
--

DROP TABLE IF EXISTS `paid_jp_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paid_jp_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `foreign_table` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'foreign table name',
  `foreign_id` int(10) unsigned NOT NULL COMMENT 'foreign table id',
  `request` text COLLATE utf8_unicode_ci COMMENT 'foreign table id',
  `response` text COLLATE utf8_unicode_ci COMMENT 'foreign table id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paid_jp_logs`
--

LOCK TABLES `paid_jp_logs` WRITE;
/*!40000 ALTER TABLE `paid_jp_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `paid_jp_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email address',
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'token',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'time stamp created at deleted at',
  `user_type` enum('seeker','provider','admin') COLLATE utf8_unicode_ci NOT NULL COMMENT 'user type for the password reset',
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `points`
--

DROP TABLE IF EXISTS `points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `points` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `point` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT 'point amount',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT 'amount equivalent',
  `description` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT 'description',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'point status',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `points`
--

LOCK TABLES `points` WRITE;
/*!40000 ALTER TABLE `points` DISABLE KEYS */;
/*!40000 ALTER TABLE `points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_paidjp`
--

DROP TABLE IF EXISTS `provider_paidjp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_paidjp` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `provider_id` int(10) unsigned NOT NULL COMMENT 'provider Id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `provider_info` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'json provider_info',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_paidjp`
--

LOCK TABLES `provider_paidjp` WRITE;
/*!40000 ALTER TABLE `provider_paidjp` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_paidjp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_transactions`
--

DROP TABLE IF EXISTS `provider_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `provider_id` int(10) unsigned NOT NULL COMMENT 'provider Id',
  `points` decimal(8,2) NOT NULL COMMENT 'points spent',
  `transaction_type` int(11) NOT NULL COMMENT 'transaction type',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `foreign_table` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'foreign table name',
  `foreign_id` int(10) unsigned NOT NULL COMMENT 'foreign table id',
  `paid_status` int(11) NOT NULL DEFAULT '1',
  `trans_identifier_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'transactions unique formatted identity for external purpose',
  `charge_type` int(11) NOT NULL COMMENT 'charge type of transaction',
  `amount` int(11) DEFAULT NULL COMMENT 'amount sent to paid jp',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_transactions`
--

LOCK TABLES `provider_transactions` WRITE;
/*!40000 ALTER TABLE `provider_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `providers`
--

DROP TABLE IF EXISTS `providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'provider Id',
  `user_id` int(10) unsigned NOT NULL COMMENT 'user Id',
  `provider_identifier_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'providers unique formatted identity for external purpose',
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'email',
  `ceo_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'name of the ceo',
  `established_year` date DEFAULT NULL COMMENT ' date of company start',
  `telephone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'company telephone number',
  `fax` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'fax number',
  `capital` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'capital of the company',
  `no_of_employee` int(11) DEFAULT NULL COMMENT 'total number of employee',
  `no_of_branch` int(11) DEFAULT NULL COMMENT 'total branch of the company',
  `business_content` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'prefecture name',
  `prefecture_id` int(11) DEFAULT NULL COMMENT 'prefecture name',
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'address',
  `street_address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'detail adddress info',
  `related_url` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'companys releted url',
  `about_company` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'company description japanese',
  `company_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'name of the company english',
  `contact_person` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'contact person name ja',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `unique_in` tinyint(1) DEFAULT '1' COMMENT '????',
  `sub_business_content` int(11) DEFAULT NULL COMMENT 'sub business content',
  `zip_code` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'zip code of seeker address',
  `point_balance` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT 'point balance of a provider',
  `paidId` int(11) DEFAULT NULL COMMENT 'paid id for paidjp',
  `mansion_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'mansion name',
  `paid_status` int(11) NOT NULL DEFAULT '0',
  `purchased_on` date DEFAULT NULL COMMENT 'point last purchased date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `providers`
--

LOCK TABLES `providers` WRITE;
/*!40000 ALTER TABLE `providers` DISABLE KEYS */;
/*!40000 ALTER TABLE `providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scout_details`
--

DROP TABLE IF EXISTS `scout_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scout_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `scout_mail_id` int(10) unsigned NOT NULL,
  `date` date DEFAULT NULL COMMENT 'date',
  `time` int(11) DEFAULT NULL COMMENT 'time',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scout_details`
--

LOCK TABLES `scout_details` WRITE;
/*!40000 ALTER TABLE `scout_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `scout_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scout_mails`
--

DROP TABLE IF EXISTS `scout_mails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scout_mails` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `provider_id` int(10) unsigned NOT NULL COMMENT 'provider Id',
  `seeker_id` int(10) unsigned NOT NULL COMMENT 'Seeker Id',
  `message` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'message from provider',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `job_id` int(11) NOT NULL COMMENT 'job_id',
  `interview_method` int(11) DEFAULT NULL COMMENT 'interview method',
  `interview_place` int(11) DEFAULT NULL COMMENT 'interview place',
  `job_location_id` int(11) DEFAULT NULL COMMENT 'interview place job location id',
  `resume` tinyint(1) DEFAULT NULL COMMENT 'seekers english writing level',
  `cv` tinyint(1) DEFAULT NULL COMMENT 'cv',
  `writing_util` tinyint(1) DEFAULT NULL COMMENT 'writing utensils',
  `other` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'other',
  `status` int(11) DEFAULT NULL COMMENT 'status',
  `date` date DEFAULT NULL COMMENT 'date',
  `time` int(11) DEFAULT NULL COMMENT 'time',
  `type` int(11) DEFAULT NULL COMMENT 'scout_message type',
  `seeker_status` int(11) DEFAULT NULL COMMENT 'scout_message status',
  `provider_status` int(11) DEFAULT NULL COMMENT 'scout_message status provider',
  `provider_read` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'provider read status',
  `seeker_read` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'seeker read status',
  `re_count` int(11) NOT NULL DEFAULT '0' COMMENT 're count for reply count',
  `provider_skype_id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'provider skype id',
  `seeker_skype_id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seeker skype id',
  `seeker_reply_count` int(11) DEFAULT '0',
  `provider_reply_count` int(11) DEFAULT '0',
  `seeker_replied_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scout_mails`
--

LOCK TABLES `scout_mails` WRITE;
/*!40000 ALTER TABLE `scout_mails` DISABLE KEYS */;
/*!40000 ALTER TABLE `scout_mails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seeker_certificates`
--

DROP TABLE IF EXISTS `seeker_certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seeker_certificates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'certificate id',
  `seeker_id` int(10) unsigned NOT NULL COMMENT 'seekers table Id',
  `year` date DEFAULT NULL COMMENT 'year the certificate is received',
  `certificate_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'certificate name',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seeker_certificates`
--

LOCK TABLES `seeker_certificates` WRITE;
/*!40000 ALTER TABLE `seeker_certificates` DISABLE KEYS */;
/*!40000 ALTER TABLE `seeker_certificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seeker_educations`
--

DROP TABLE IF EXISTS `seeker_educations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seeker_educations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'experience id for database',
  `seeker_id` int(10) unsigned NOT NULL COMMENT 'seeker Id',
  `university_institute` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seekers university or other institute name',
  `degree` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seekers degree in respect to uni',
  `faculty` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seekers faculty in respect to uni',
  `graduation_country` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seekers graduation country',
  `graduation_status` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seekers graduation status',
  `graduation_year` date DEFAULT NULL COMMENT 'seekers graduation year',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seeker_educations`
--

LOCK TABLES `seeker_educations` WRITE;
/*!40000 ALTER TABLE `seeker_educations` DISABLE KEYS */;
/*!40000 ALTER TABLE `seeker_educations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seeker_experiences`
--

DROP TABLE IF EXISTS `seeker_experiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seeker_experiences` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'experience id for database',
  `seeker_id` int(10) unsigned NOT NULL COMMENT 'seeker Id',
  `company_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'seekers company name that they worked on',
  `job_title` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'seekers job title they worked',
  `job_from` date NOT NULL COMMENT 'seekers job from date they worked',
  `job_to` date DEFAULT NULL COMMENT 'seekers job to date they worked',
  `currently_working_here` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'seekers if they are currently working here',
  `annual_salary` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'seekers annual salary for the particular company',
  `job_description` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'seekers work description/ job description',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seeker_experiences`
--

LOCK TABLES `seeker_experiences` WRITE;
/*!40000 ALTER TABLE `seeker_experiences` DISABLE KEYS */;
/*!40000 ALTER TABLE `seeker_experiences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seeker_jobs`
--

DROP TABLE IF EXISTS `seeker_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seeker_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `seeker_id` int(10) unsigned NOT NULL COMMENT 'seekers Id',
  `job_id` int(10) unsigned NOT NULL COMMENT 'job Id',
  `user_data` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'user information for job application',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `unique_in` tinyint(1) DEFAULT '1' COMMENT '????',
  `status` int(11) DEFAULT '0' COMMENT 'applicant status',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seeker_jobs`
--

LOCK TABLES `seeker_jobs` WRITE;
/*!40000 ALTER TABLE `seeker_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `seeker_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seeker_mail_settings`
--

DROP TABLE IF EXISTS `seeker_mail_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seeker_mail_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'certificate id',
  `seeker_id` int(10) unsigned NOT NULL COMMENT 'seekers table Id',
  `employment_type` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'employment status to receive mail of',
  `japanese_level` int(11) DEFAULT NULL COMMENT 'japanese level to receive mail of',
  `english_level` int(11) DEFAULT NULL COMMENT 'english level to receive mail of',
  `min_salary` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'min salary to receive mail of',
  `max_salary` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'max salary to receive mail of',
  `job_location` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'location to receive mail of',
  `visa_support` tinyint(1) DEFAULT NULL COMMENT 'visa support to receive mail',
  `social_insurance` tinyint(1) DEFAULT NULL COMMENT 'social insurance to receive mail',
  `housing_allowance` tinyint(1) DEFAULT NULL COMMENT 'housing allowance to receive mail',
  `receive_mail` tinyint(1) DEFAULT NULL COMMENT 'provide uniform to receive mail',
  `hairstyle_freedom` tinyint(1) DEFAULT NULL COMMENT 'hair clothing freedom to receive mail',
  `clothing_freedom` tinyint(1) DEFAULT NULL COMMENT 'hair clothing freedom to receive mail',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `job_category` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'job category',
  `salary_type` int(11) DEFAULT NULL COMMENT 'desired salary type',
  `all_prefecture` tinyint(1) DEFAULT '0' COMMENT 'all prefecture',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seeker_mail_settings`
--

LOCK TABLES `seeker_mail_settings` WRITE;
/*!40000 ALTER TABLE `seeker_mail_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `seeker_mail_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seeker_skills`
--

DROP TABLE IF EXISTS `seeker_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seeker_skills` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'skill id',
  `skill_id` int(10) unsigned NOT NULL COMMENT 'skill attribute table Id',
  `level` int(11) NOT NULL COMMENT 'skill table Id',
  `skill_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'skill type',
  `seeker_id` int(10) unsigned NOT NULL COMMENT 'seekers table Id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seeker_skills`
--

LOCK TABLES `seeker_skills` WRITE;
/*!40000 ALTER TABLE `seeker_skills` DISABLE KEYS */;
/*!40000 ALTER TABLE `seeker_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seeker_wishlists`
--

DROP TABLE IF EXISTS `seeker_wishlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seeker_wishlists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `seeker_id` int(10) unsigned NOT NULL COMMENT 'seekers Id',
  `job_id` int(10) unsigned NOT NULL COMMENT 'job Id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `unique_in` tinyint(1) DEFAULT '1' COMMENT '????',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seeker_wishlists`
--

LOCK TABLES `seeker_wishlists` WRITE;
/*!40000 ALTER TABLE `seeker_wishlists` DISABLE KEYS */;
/*!40000 ALTER TABLE `seeker_wishlists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seekers`
--

DROP TABLE IF EXISTS `seekers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seekers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'seekers id for database',
  `user_id` int(10) unsigned NOT NULL COMMENT 'user Id',
  `seeker_identifier_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'seekers unique formatted identity for external purpose',
  `visa_status` int(11) DEFAULT NULL COMMENT 'seekers current visa status',
  `visa_issue_date` date DEFAULT NULL COMMENT 'seekers visa issue date',
  `visa_expiry_date` date DEFAULT NULL COMMENT 'seekers visa expiry date',
  `japanese_language_level` int(11) DEFAULT NULL COMMENT 'seekers japanese language level',
  `english_language_level` int(11) DEFAULT NULL COMMENT 'seekers english language level',
  `desired_employment_type` int(11) DEFAULT NULL COMMENT 'seekers desired employment type',
  `nearest_station` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seekers nearest station',
  `willing_to_relocate` int(11) DEFAULT NULL COMMENT 'is seeker willing to relocate',
  `is_experienced` int(11) NOT NULL DEFAULT '0' COMMENT 'is seeker experienced',
  `experience` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'seekers total experience years in number',
  `country` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'users nationality',
  `prefecture_id` int(11) DEFAULT NULL COMMENT 'prefecture name',
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'address',
  `street_address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'detail adddress info',
  `portfolio_url` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seekers portfolio url',
  `github_url` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'seekers github url',
  `qiita_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'qiita id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `living_country` int(11) DEFAULT NULL COMMENT 'seeker living country',
  `state` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'state',
  `japanese_writing_level` int(11) DEFAULT NULL COMMENT 'seekers japanese writing level',
  `english_writing_level` int(11) DEFAULT NULL COMMENT 'seekers english writing level',
  `zip_code` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'zip code of seeker address',
  `desired_job_category` int(11) DEFAULT NULL,
  `desired_annual_min_salary_range` int(11) DEFAULT NULL,
  `experience_year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seekers`
--

LOCK TABLES `seekers` WRITE;
/*!40000 ALTER TABLE `seekers` DISABLE KEYS */;
INSERT INTO `seekers` VALUES (1,1,'0001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `seekers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequences`
--

DROP TABLE IF EXISTS `sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequences` (
  `table_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'table name ',
  `column_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'name of the column',
  `group_column_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'name of the group column',
  `group_value` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'value of group',
  `last_number` int(10) unsigned NOT NULL COMMENT 'last number',
  PRIMARY KEY (`table_name`,`column_name`,`group_column_name`,`group_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequences`
--

LOCK TABLES `sequences` WRITE;
/*!40000 ALTER TABLE `sequences` DISABLE KEYS */;
/*!40000 ALTER TABLE `sequences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'user ID for database',
  `user_identifier_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'users unique formatted identity for external purpose',
  `full_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'users full name',
  `katakana_full_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'users katakana full name',
  `dob` date DEFAULT NULL COMMENT 'users date of birth',
  `gender` int(11) DEFAULT NULL COMMENT 'Users gender',
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'users email address',
  `password` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'users password',
  `telephone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'users telephone number',
  `mobile` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'users mobile number',
  `confirmation_token` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'conformation token send by system',
  `user_type` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'seeker' COMMENT 'user type ',
  `confirmed_at` timestamp NULL DEFAULT NULL COMMENT 'time stamp confirmation date ',
  `confirmation_sent_at` timestamp NULL DEFAULT NULL COMMENT 'confirmation sent date',
  `verified` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'varified status',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'remember token remember or not',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  `unique_in` tinyint(1) DEFAULT '1' COMMENT '????',
  `last_login` timestamp NULL DEFAULT NULL COMMENT 'users last login time',
  `deletable` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'is user deletable',
  `receive_mail_language` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ja' COMMENT 'receive mail for users',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_unique_type` (`email`,`unique_in`,`user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'0001','test first name','テスト','1990-11-21',1,'testemail@gmail.com','$2y$10$iGQVWLNhkYpdzGWpVkm2Ke2a0.K.744SokhUNxvE7PbvCtn0w3p0a','0123456789','01213456789',NULL,'seeker',NULL,NULL,1,NULL,NULL,'2020-07-07 16:02:12',NULL,NULL,1,NULL,1,'2020-07-07 16:02:12',1,'ja'),(2,'00012','test first name','テスト','1990-11-21',1,'testemail@gmail.com','$2y$10$iGQVWLNhkYpdzGWpVkm2Ke2a0.K.744SokhUNxvE7PbvCtn0w3p0a','0123456789','01213456789',NULL,'admin',NULL,NULL,1,NULL,NULL,'2020-07-07 16:03:56',NULL,NULL,2,NULL,1,'2020-07-07 16:03:56',1,'ja');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verify_users`
--

DROP TABLE IF EXISTS `verify_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verify_users` (
  `user_id` int(10) unsigned NOT NULL,
  `token` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verify_users`
--

LOCK TABLES `verify_users` WRITE;
/*!40000 ALTER TABLE `verify_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `verify_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zip_codes`
--

DROP TABLE IF EXISTS `zip_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zip_codes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `prefecture_ja` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `address_ja` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `street_address_ja` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `prefecture_en` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `address_en` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `street_address_en` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL COMMENT 'created_by',
  `updated_by` int(11) DEFAULT NULL COMMENT 'updated_by',
  `deleted_by` int(11) DEFAULT NULL COMMENT 'deleted_by',
  PRIMARY KEY (`id`),
  KEY `zip_codes_zip_code_index` (`zip_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zip_codes`
--

LOCK TABLES `zip_codes` WRITE;
/*!40000 ALTER TABLE `zip_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `zip_codes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-07  6:57:26
